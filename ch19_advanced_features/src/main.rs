// Also see the Cargo.toml file in this project for hints on how to optimize for
// smaller compiled program size.
// https://github.com/johnthagen/min-sized-rust
//
// Minimal program default build:   1666K
// Build in release mode:           1644K
// strip target/release/$progname:   191K
// opt-level = 'z'                   191K
// lto = true                        179K
// codegen-units = 1                 179K
// panic = 'abort'                   163K
use ch19_advanced_features::myvec;
use num_cpus;
use std::fmt;
use std::ops::{Add, Deref};
use std::slice;
use std::thread;
use std::time;
use unicode_segmentation::UnicodeSegmentation;

fn main() {
    println!("Ch19: Advanced Features");
    ch19_1_unsafe();
    ch19_2_advanced_traits();
    ch19_3_advanced_types();
    ch19_4_advanced_functions_closures();
    ch19_5_macros();
}

fn ch19_1_unsafe() {
    println!("\nCh19.1: Unsafe Rust");
    ch19_1_unsafe_mem();
    ch19_1_unsafe_extern();
    ch19_1_unsafe_globals();
}

fn ch19_1_unsafe_mem() {
    println!("\nCh19.1: Unsafe Rust: slices and memory handling");

    let _nullptr = 0 as *const u32;
    // Uncommenting this code would result in this crash:
    // Segmentation fault (core dumped)
    /*
    unsafe {
        let _unknown = *_nullptr;
    }
    */

    let mut num = 5;
    let r1 = &num as *const i32;
    let r2 = &mut num as *mut i32;
    // This code is unsafe but should be sound - no one is trying to modify the
    // contents of num; read access via multiple pointers should be Ok.
    unsafe {
        println!("*r1 == {}", *r1);
        println!("*r2 == {}", *r2);
    }

    let mut a = [1, 2, 3, 4, 5, 6];
    let (s1, s2) = split_at_mut(&mut a, 3);
    println!("s1 = {:?}", s1);
    println!("s2 = {:?}", s2);
}

// Slices already have a split_at_mut() method
// https://doc.rust-lang.org/std/primitive.slice.html#method.split_at_mut
// This is just to illustrate a justfied use of the "unsafe" keyword.
fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = slice.len();
    let ptr = slice.as_mut_ptr();

    assert!(mid <= len);

    unsafe {
        (
            slice::from_raw_parts_mut(ptr, mid),
            slice::from_raw_parts_mut(ptr.offset(mid as isize), len - mid),
        )
    }
}

extern "C" {
    // This assumes sizeof(int) == 4, which is the case for the
    // Linux i686 (32-bit) and x86_64 (64-bit) systems that I tested.
    fn abs(input: i32) -> i32;
}

fn ch19_1_unsafe_extern() {
    println!("\nCh19.1 Unsafe Rust: calling to and from external code");
    // Also see the code in lib.rs
    let n: i32 = -3;
    let a = unsafe { abs(n) };
    println!("abs({}) -> {}", n, a);
}

static mut COUNTER: u32 = 0;

fn spawn_incrementer(count: u32) -> thread::JoinHandle<u32> {
    thread::spawn(move || {
        for _ in 0..count {
            unsafe {
                COUNTER += 1;
            }
        }
        count
    })
}

// A demonstration of why it is unsafe to modify global variables.
fn ch19_1_unsafe_globals() {
    println!("\nCh19.1 Unsafe Rust: global ('static') variables");
    let num_counters = num_cpus::get();
    let num_loops_per_thread = 1000;
    let mut thread_handles =
        Vec::<thread::JoinHandle<u32>>::with_capacity(num_counters);
    println!("Spawning {} independent counter threads!", num_counters);
    for _ in 0..num_counters {
        thread_handles.push(spawn_incrementer(num_loops_per_thread));
    }
    thread::sleep(time::Duration::from_millis(100));
    // Collect the values returned from the threads
    let mut returned_count = 0;
    for thread_handle in thread_handles {
        returned_count += thread_handle.join().unwrap();
    }
    let actual_count = unsafe { COUNTER };
    println!("Expected counter value: {}", returned_count);
    println!("Actual counter value: {}", actual_count);
    println!(
        "Errors due to unsynchronized changes: {}",
        returned_count - actual_count
    );
}

// This silly example is meant to illustrate why it is generally a bad idea to
// implement Add on different datatypes - you can't add apples and oranges!

#[derive(Debug, Copy, Clone)]
struct Apple {
    pub crunchiness: f64,
}

#[derive(Debug)]
struct Orange {
    pub squishiness: f64,
}

#[derive(Debug)]
struct Pear {
    pub pearness: f64,
}

impl Add<&Orange> for Apple {
    type Output = Pear;

    fn add(self, other: &Orange) -> Pear {
        Pear {
            pearness: -self.crunchiness + (4.0 * other.squishiness),
        }
    }
}

// This silly example is straight from "The Book" to illustrate disambiguating
// method calls when a struct implements multiple traits and the traits and the
// struct all implement the same method name.

trait Pilot {
    fn fly(&self);
    fn job_description() -> String {
        "I fly airplanes!".to_string()
    }
}

trait Wizard {
    fn fly(&self);
    fn job_description() -> String {
        "I do magic".to_string()
    }
}

trait WingedInsect {
    fn fly(&self);
    fn job_description() -> String {
        "I irritate humans".to_string()
    }
}

struct Human;

impl Pilot for Human {
    fn fly(&self) {
        println!("This is your captain speaking.");
    }
}

impl Wizard for Human {
    fn fly(&self) {
        println!("Up!");
    }
}

impl Human {
    fn fly(&self) {
        println!(
            "**waving arms furiously** I can't fly, {}",
            Self::job_description()
        );
    }
    fn job_description() -> String {
        "I'm a software developer".to_string()
    }
}

fn ch19_2_advanced_traits() {
    println!("\nCh19.2 Advanced Traits");
    let apple = Apple { crunchiness: 32.5 };
    let orange = Orange { squishiness: 7.2 };
    // Apple on the left, Orange on the right or it won't compile.
    let pear = apple + &orange;
    println!("{:?} + {:?} == {:?}", apple, orange, pear);

    let person = Human;
    person.fly();
    Pilot::fly(&person);
    Wizard::fly(&person);

    println!("{}", Human::job_description());
    println!("{}", <Human as Pilot>::job_description());
    println!("{}", <Human as Wizard>::job_description());
    // This is not allowed: Pilot::job_description()
    // error[E0283]: type annotations required: cannot resolve `_: Pilot`
}

trait OutlinePrint: fmt::Display {
    fn outline_print(&self) {
        let output = self.to_string();
        // Calculate length assuming all text is ASCII bytes (horrible)
        // let len = output.len();

        // Calculate length by Unicode Scalar Values (better than ASCII bytes)
        // let len = output.chars().count();

        // Calculate length by Grapheme Clusters (better than Unicode Values)
        let len = UnicodeSegmentation::graphemes(output.as_str(), true).count();

        println!("{}", "*".repeat(len + 4));
        println!("*{}*", " ".repeat(len + 2));
        println!("* {} *", output);
        println!("*{}*", " ".repeat(len + 2));
        println!("{}", "*".repeat(len + 4));
    }
}

struct Point {
    x: i32,
    y: i32,
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl OutlinePrint for Point {}

struct Wrapper(Vec<String>);

impl fmt::Display for Wrapper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}]", self.0.join(", "))
    }
}

impl Deref for Wrapper {
    type Target = Vec<String>;

    fn deref(&self) -> &Vec<String> {
        &self.0
    }
}

fn func_that_takes_unsized_parameter<T: ?Sized>(x: &T) {
    println!("Inside func_that_takes_unsized_parameter: address of x: {:p}", x);
}

fn ch19_3_advanced_types() {
    println!("\nCh19.3 Advanced Types");

    let p1 = Point { x: 3, y: 5 };
    println!("p1 = {}", p1);
    p1.outline_print();

    let v = vec!["hello".to_string(), "world".to_string()];
    // Can't do this: error[E0277]: `std::vec::Vec<std::string::String>`
    //                              doesn't implement `std::fmt::Display`
    // println!("v = {}", v);

    let w = Wrapper(v);
    // This makes use of the fmt::Display implementation.
    println!("w = {}", w);
    // This makes use of the Deref trait implementation.
    println!("w.len() = {}", w.len());

    // Side quest - returning a value from loop via `break`
    let a = loop {
        break 32
    };
    println!("a = {}", a);

    func_that_takes_unsized_parameter(&a);
}

fn add_one(x: i32) -> i32 {
    x + 1
}

fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
    f(arg) + f(arg)
}

// I think it is usually better style to define callback types:
type Callback = fn(i32) -> i32;

fn do_twice2(f: Callback, arg: i32) -> i32 {
    f(arg) + f(arg)
}

#[derive(Debug)]
enum Status {
    Value(u32),
    Stop,
}

enum Op {
    Add,
    Mul,
}

fn select_operation(op: Op, arg: i32) -> Box<Fn(i32) -> i32> {
    match op {
        Op::Add => Box::new(move |x| { arg + x }),
        Op::Mul => Box::new(move |x| { arg * x }),
    }
}

fn ch19_4_advanced_functions_closures() {
    println!("\nCh19.4 Advanced Functions and Closures");

    println!("do_twice(add_one({})) -> {}", 2, do_twice(add_one, 2));
    println!("do_twice2(add_one({})) -> {}", 3, do_twice2(add_one, 3));

    let list_of_numbers = vec![1, 2, 3];
    let list_of_strings: Vec<String> = list_of_numbers
        .iter()
        .map(ToString::to_string) // passing function instead of closure
        .collect();
    println!("list_of_strings = {:?}", list_of_strings);

    let mut list_of_statuses: Vec<Status> =
        (0u32..10)
        .map(Status::Value)
        .collect();
    list_of_statuses.push(Status::Stop);
    println!("list_of_statuses = {:?}", list_of_statuses);

    let f1 = select_operation(Op::Add, 2);
    let f2 = select_operation(Op::Mul, 2);
    println!("f1({}) -> {}", 3, f1(3));
    println!("f2({}) -> {}", 3, f2(3));
}

fn ch19_5_macros() {
    println!("\nCh19.5 Macros");

    let a = myvec![1, 2, 3];
    println!("a = {:?}", a);
}
