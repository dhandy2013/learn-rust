use ch17_objects::AveragedCollection;
use ch17_objects::gui;
use ch17_objects::blog;

fn main() {
    println!("Ch17: Object Oriented Programming");
    ch_17_1_encapsulation();
    ch_17_2_trait_objects();
    ch17_3_state_pattern_with_objects();
    ch17_3_state_pattern_with_types();
}

fn ch_17_1_encapsulation() {
    println!("\nCh17.1 Encapsulation");
    let ac1 = AveragedCollection::from_slice(&[]);
    let ac2 = AveragedCollection::from_slice(&[25, 75]);
    println!("Average of empty collection: {}", ac1.average());
    println!("Average of collection with two items: {}", ac2.average());
}

fn ch_17_2_trait_objects() {
    println!("\nCh17.2 Trait Objects");
    let screen = gui::Screen {
        components: vec![
            Box::new(gui::Button { }),
            Box::new(gui::SelectBox { }),
        ],
    };
    screen.run();
}

fn ch17_3_state_pattern_with_objects() {
    println!("\nCh17.3 Implementing the State Pattern with Objects");

    let mut post = blog::Post::new();

    post.add_text("This is my blog post for today.");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.approve();
    // Finally the blog post content becomes available
    assert_eq!("This is my blog post for today.", post.content());
}

pub struct Post {
    content: String,
}

impl Post {
    pub fn new() -> DraftPost {
        DraftPost {
            content: String::new(),
        }
    }

    pub fn content(&self) -> &str {
        &self.content
    }
}

pub struct DraftPost {
    content: String,
}

impl DraftPost {
    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn request_review(self) -> PendingReviewPost {
        PendingReviewPost {
            content: self.content,
        }
    }
}

pub struct PendingReviewPost {
    content: String,
}

impl PendingReviewPost {
    pub fn approve(self) -> Post {
        Post {
            content: self.content,
        }
    }
}

fn ch17_3_state_pattern_with_types() {
    println!("\nCh17.3 Implementing the State Pattern with types");
    let mut post = Post::new();
    post.add_text("Here's my daily post!");
    let post = post.request_review();
    let post = post.approve();
    assert_eq!("Here's my daily post!", post.content());
}
