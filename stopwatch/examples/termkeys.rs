//! Read input in raw mode and dump input bytes in hex mode with timing
use nix::poll::{self, PollFd, PollFlags};
use std::error::Error;
use std::time::Instant;
use stopwatch::{
    format_elapsed_ms_bytes, format_hex_ascii_line, term::RawTerminal,
};

fn main() -> Result<(), Box<dyn Error>> {
    println!("Press keys on the keyboard to see their hex and ASCII bytes.");
    println!("Press Ctrl-C or Ctrl-D to exit.");

    let term = RawTerminal::new()?;

    let stdin_fd = term.stdin_fd();
    let stdout_fd = term.stdout_fd();
    let mut poll_fds = [
        PollFd::new(&stdin_fd, PollFlags::POLLIN),
        PollFd::new(&stdout_fd, PollFlags::empty()),
    ];

    let mut inbuf: [u8; 16] = [0; 16];
    let mut outbuf: Vec<u8> = Vec::with_capacity(82);
    let mut stop_flag = false;
    let start_time = Instant::now();
    while !(stop_flag && outbuf.is_empty()) {
        poll_fds[1] = if !outbuf.is_empty() {
            PollFd::new(&stdout_fd, PollFlags::POLLOUT)
        } else {
            PollFd::new(&stdout_fd, PollFlags::empty())
        };
        let result = poll::poll(&mut poll_fds, 10)?;
        if result == 0 {
            continue;
        }
        let t = start_time.elapsed();
        for poll_fd in poll_fds.iter() {
            let poll_flags = match poll_fd.revents() {
                Some(pf) => pf,
                None => continue,
            };
            if poll_flags.contains(PollFlags::POLLIN) {
                let n = term.read_bytes(&mut inbuf)?;
                if n == 0 {
                    // Somehow stdin became closed.
                    stop_flag = true;
                }
                if !stop_flag {
                    // Trace the elapsed time since the input loop began
                    let elapsed_ms = t.as_millis() as u32;
                    format_elapsed_ms_bytes(elapsed_ms, &mut outbuf);
                    outbuf.extend(b" ");
                    // Hexdump up to 16 input bytes
                    format_hex_ascii_line(&inbuf[..n], &mut outbuf)?;
                    outbuf.extend(b"\r\n");
                    // Check for Ctrl-C (code 3) or Ctrl-D (code 4)
                    if inbuf[..n].iter().any(|&byte| byte == 3 || byte == 4) {
                        stop_flag = true;
                    }
                }
            } else if poll_flags.contains(PollFlags::POLLOUT) {
                // Process output
                let n = term.write_bytes(&outbuf)?;
                // Remove from output buffer bytes successfully written
                outbuf.copy_within(n.., 0);
                outbuf.truncate(outbuf.len() - n);
            }
        }
    }
    drop(term);

    Ok(())
}
