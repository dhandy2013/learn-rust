//! Measurement of speed writing to the terminal
//!
//! This program blasts bytes to the terminal as fast as it can. On my desktop
//! computer running Fedora Linux the terminal write speed is between 10 and 15
//! MB/sec. This program uses 2% CPU and the terminal process uses about 80%.
//!
//! By default the `write()` function call never does partial writes. It always
//! blocks until it has written every byte, even when trying to write 95M bytes
//! at a time, *unless* you use `fcntl()` and set O_NONBLOCK on the file
//! descriptor before writing to it.
use nix::poll::{self, PollFd, PollFlags};
use std::error::Error;
use std::time::Instant;
use stopwatch::term::RawTerminal;

/// Number of bytes to attempt to write in each call to `write`.
/// At 3840 I never get partial writes. At 3841 I get partial writes.
const BLOCK_SIZE: usize = 3840;

/// Number of milliseconds to wait for input or output in each call to `poll`.
const POLL_TIMEOUT_MS: i32 = 10;

/// An infinite iterator that yields ASCII bytes for write speed testing
struct InfiniteByteIter {
    cur_byte: u8,
}

impl InfiniteByteIter {
    const START_BYTE: u8 = 32;
    const END_BYTE: u8 = 126;
    fn new() -> Self {
        Self {
            cur_byte: Self::START_BYTE,
        }
    }
}

impl Iterator for InfiniteByteIter {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        let result = self.cur_byte;
        if self.cur_byte == Self::END_BYTE {
            self.cur_byte = Self::START_BYTE;
        } else {
            self.cur_byte += 1;
        }
        Some(result)
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    println!("Terminal write speed measurement");
    println!("Press any keep to stop.");

    let mut byte_iter = InfiniteByteIter::new();
    let mut buf = Vec::<u8>::with_capacity(BLOCK_SIZE);
    for _ in 0..BLOCK_SIZE {
        buf.push(byte_iter.next().unwrap());
    }

    let term = RawTerminal::new()?;
    let stdin_fd = term.stdin_fd();
    let stdout_fd = term.stdout_fd();
    let mut poll_fds = [
        PollFd::new(&stdin_fd, PollFlags::POLLIN),
        PollFd::new(&stdout_fd, PollFlags::POLLOUT),
    ];

    let mut inbuf: [u8; 16] = [0; 16];
    let mut total_bytes_written: usize = 0;
    let mut num_blocks_written: usize = 0;
    let mut num_partial_writes: usize = 0;
    let mut num_poll_timeouts: usize = 0;
    let t0 = Instant::now();
    'writeloop: loop {
        let result = poll::poll(&mut poll_fds, POLL_TIMEOUT_MS)?;
        if result > 0 {
            for poll_fd in poll_fds.iter() {
                if let Some(poll_flags) = poll_fd.revents() {
                    if poll_flags.contains(PollFlags::POLLIN) {
                        // A key was pressed - end the program
                        let _ = term.read_bytes(&mut inbuf);
                        break 'writeloop;
                    } else if poll_flags.contains(PollFlags::POLLOUT) {
                        // Ready for writing - write another block
                        let n = term.write_bytes(&buf[..])?;
                        total_bytes_written += n;
                        num_blocks_written += 1;
                        if n < buf.len() {
                            num_partial_writes += 1;
                        }
                    }
                }
            }
        } else {
            num_poll_timeouts += 1;
        }
    }
    let total_time = t0.elapsed();
    drop(term);

    println!();
    println!("Total bytes written      : {total_bytes_written}");
    println!("Number of blocks written : {num_blocks_written}");
    println!("Number of partial writes : {num_partial_writes}");
    println!("Number of poll timeouts  : {num_poll_timeouts}");
    println!("Poll timeout milliseconds: {POLL_TIMEOUT_MS}");
    println!("Block size in bytes      : {BLOCK_SIZE}");
    println!("Total time in seconds    : {}", total_time.as_secs_f64());
    // bytes per second
    let write_speed_bps: f64 =
        total_bytes_written as f64 / total_time.as_secs_f64();
    println!(
        "Megabytes per second     : {}",
        write_speed_bps / (1024.0 * 1024.0)
    );
    println!("Done.");
    Ok(())
}
