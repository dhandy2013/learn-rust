//! Experiment with setting and clearing poll flags
use nix::{
    poll::{self, PollFd, PollFlags},
    unistd::{read, write},
};
use std::{
    error::Error,
    io::{self, Write},
    os::fd::{AsFd, AsRawFd},
};

fn main() -> Result<(), Box<dyn Error>> {
    println!("Setting and clearing poll flags");
    println!("Type something and press Enter. Press Ctrl-D to quit.");
    let stdin = io::stdin();
    let stdout = io::stdout();
    let stdin_fd = stdin.as_fd();
    let stdout_fd = stdout.as_fd();
    let mut poll_fds = [
        PollFd::new(&stdin_fd, PollFlags::POLLIN),
        PollFd::new(&stdout_fd, PollFlags::empty()),
    ];
    let mut inbuf: [u8; 16] = [0; 16];
    let mut outbuf: Vec<u8> = Vec::with_capacity(80);
    let mut num_loops: usize = 0;
    let mut num_timeouts: usize = 0;
    let mut num_input_ready: usize = 0;
    let mut num_input_bytes: usize = 0;
    let mut num_output_ready: usize = 0;
    let mut num_output_bytes: usize = 0;
    let mut stop_flag = false;
    'mainloop: while !(stop_flag && outbuf.is_empty()) {
        num_loops += 1;
        poll_fds[1] = if outbuf.is_empty() {
            PollFd::new(&stdout_fd, PollFlags::empty())
        } else {
            PollFd::new(&stdout_fd, PollFlags::POLLOUT)
        };
        let result = poll::poll(&mut poll_fds, 10)?;
        if result == 0 {
            num_timeouts += 1;
            continue 'mainloop;
        }
        for poll_fd in poll_fds.iter() {
            if let Some(poll_flags) = poll_fd.revents() {
                if poll_flags.contains(PollFlags::POLLIN) {
                    // Process input
                    num_input_ready += 1;
                    let n = read(stdin_fd.as_raw_fd(), &mut inbuf)?;
                    num_input_bytes += n;
                    // For "cooked" input, if the number of bytes read from
                    // stdin is zero that means the user pressed Ctrl-D.
                    if n == 0 {
                        stop_flag = true;
                    }
                    if !stop_flag {
                        for &b in inbuf[0..n].iter() {
                            // Echo each input byte in hex, decimal, and ASCII
                            let c: char =
                                if b.is_ascii() { char::from(b) } else { '?' };
                            write!(&mut outbuf, "{b:02X} {b:3} {c:?}\r\n")?;
                        }
                    }
                } else if poll_flags.contains(PollFlags::POLLOUT) {
                    // Process output
                    num_output_ready += 1;
                    if outbuf.is_empty() {
                        continue;
                    }
                    // Write bytes out one at a time to guarantee no blocking
                    let n = write(stdout_fd.as_raw_fd(), &outbuf[..1])?;
                    num_output_bytes += n;
                    // Remove from output buffer bytes successfully written
                    outbuf.copy_within(n.., 0);
                    outbuf.truncate(outbuf.len() - n);
                }
            }
        }
    }
    println!();
    println!("num_loops: {num_loops}");
    println!("num_input_ready: {num_input_ready}");
    println!("num_input_bytes: {num_input_bytes}");
    println!("num_output_ready: {num_output_ready}");
    println!("num_output_bytes: {num_output_bytes}");
    println!("num_timeouts: {num_timeouts}");
    Ok(())
}
