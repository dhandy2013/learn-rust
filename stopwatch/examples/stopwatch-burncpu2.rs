//! Command-Line Stopwatch
//! Uses a LOT of CPU in spin loops!
//! Mainly for demo and experimentation purposes.
use std::io;
use std::io::{stdout, Write};
use stopwatch::task::StopWatchTask;
use termion::async_stdin;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

fn main() -> io::Result<()> {
    let stdout = stdout();
    let mut stdout = stdout.lock().into_raw_mode().unwrap();
    let mut stdin = async_stdin().keys();
    write!(stdout, "\r\nStopwatch - CPU burning version\r\n")?;
    write!(
        stdout,
        "Enter: start/stop, Backspace: mark/clear, q: quit\r\n"
    )?;
    let mut task = StopWatchTask::new();

    while !task.is_finished() {
        let out = task.pending_output();
        let there_was_output = if out.len() > 0 {
            stdout.write_all(out)?;
            stdout.flush()?;
            task.handle_output(out.len());
            true
        } else {
            false
        };

        let there_was_input = if let Some(key) = stdin.next() {
            match key? {
                Key::Char('\n') => task.handle_input(b"\r"),
                Key::Backspace => task.handle_input(b"\x7f"),
                Key::Char('q') => task.handle_input(b"q"),
                _ => {}
            }
            true
        } else {
            false
        };

        if !(there_was_input || there_was_output) {
            task.update();
        }
    }

    write!(stdout, "\nStop\r\n")?;
    stdout.flush()?;
    Ok(())
}
