//! Command-Line Stopwatch
//! Uses a LOT of CPU in spin loops!
//! Mainly for demo and experimentation purposes.
use std::io;
use std::io::{stdout, Write};
use std::time::{Duration, Instant};
use stopwatch::format_elapsed_ms_string;
use termion::async_stdin;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

fn main() -> io::Result<()> {
    let stdout = stdout();
    let mut stdout = stdout.lock().into_raw_mode().unwrap();
    let mut stdin = async_stdin().keys();
    write!(stdout, "\r\nStopwatch - CPU burning version\r\n")?;
    write!(
        stdout,
        "Enter: start/stop, Backspace: mark/clear, q: quit\r\n"
    )?;
    let mut buf = String::with_capacity(80);

    'mainloop: loop {
        write!(stdout, "\r\n")?;
        stdout.flush()?;
        let mut total_duration = Duration::new(0, 0);
        buf.clear();
        format_elapsed_ms_string(total_duration.as_millis() as u32, &mut buf);
        write!(stdout, "{}\r", &buf)?;
        stdout.flush()?;

        // Wait for Enter or q
        loop {
            if let Some(key) = stdin.next() {
                match key? {
                    Key::Char('\r') | Key::Char('\n') => {
                        // start
                        break;
                    }
                    Key::Char('q') => {
                        // quit
                        break 'mainloop;
                    }
                    _ => {} // keep waiting
                }
            }
        }

        'runtimer: loop {
            let t0 = Instant::now();
            let mut prev_duration = total_duration + t0.elapsed();
            let mut quit_flag = false;
            loop {
                // Handle key input if any
                if let Some(key) = stdin.next() {
                    match key? {
                        Key::Char('\r') | Key::Char('\n') => {
                            // stop
                            total_duration += t0.elapsed();
                            write!(stdout, "\n")?;
                            stdout.flush()?;
                            break;
                        }
                        Key::Backspace => {
                            // mark
                            write!(stdout, "\n")?;
                            stdout.flush()?;
                        }
                        Key::Char('q') => {
                            quit_flag = true;
                        }
                        _ => {} // ignore all other keys
                    }
                }

                // Show the current elapsed time if at least 10ms have passed
                // or the quit key was pressed.
                let cur_duration = total_duration + t0.elapsed();
                if !quit_flag && (cur_duration - prev_duration).as_millis() < 10
                {
                    continue;
                }
                prev_duration = cur_duration;
                buf.clear();
                format_elapsed_ms_string(
                    cur_duration.as_millis() as u32,
                    &mut buf,
                );
                write!(stdout, "{}\r", &buf)?;
                stdout.flush()?;

                if quit_flag {
                    write!(stdout, "\n")?;
                    stdout.flush()?;
                    break 'mainloop;
                }
            }

            // Wait for Enter (to resume), Backspace (to clear) or q (to quit)
            loop {
                if let Some(key) = stdin.next() {
                    match key? {
                        Key::Char('\r') | Key::Char('\n') => {
                            // resume
                            continue 'runtimer;
                        }
                        Key::Backspace => {
                            // clear
                            continue 'mainloop;
                        }
                        Key::Char('q') => {
                            // quit
                            break 'mainloop;
                        }
                        _ => {} // ignore all other keys
                    }
                }
            }
        }
    }

    write!(stdout, "\nStop\r\n")?;
    stdout.flush()?;
    Ok(())
}
