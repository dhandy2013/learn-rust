# Command-Line Stopwatch Program and example programs

This is a stopwatch program that runs in a Linux terminal.

Also included are example programs including alternative implementations of the
stopwatch program and demonstrations of how to get immediate keyboard input from
a Linux terminal.

## stopwatch

This version of the stopwatch program uses only the
[nix](https://docs.rs/nix/latest/nix/) crate plus `std`.  It uses the
[poll](https://docs.rs/nix/latest/nix/poll/fn.poll.html) function to wait for
keyboard input without blocking indefinitely and without using a separate
thread. The `poll` function times out after 10 milliseconds, at which point I
read and display the current elapsed time. Since I set the terminal stdin and
stdout files to non-blocking mode, writing never blocks. This ensures minimum
latency detecting start/stop/clear commands.

To run in debug mode: `cargo run -q`

The `nix` crate has most of its functionality gated behind features. I used these features:

* `fs`: `isatty`
* `poll`: non-blocking input
* `term`: `tcgetattr`, `tcsetattr`, `cfmakeraw`

## Example programs

See the source files under `examples/`.

### stopwatch-burncpu

This version of the stopwatch program uses only the
[termion](https://gitlab.redox-os.org/redox-os/termion) library plus the
standard library.

To run in debug mode: `cargo run -q --example stopwatch-burncpu`

Warning, this burns a LOT of CPU! It runs in a tight loop polling for keyboard
input and checking the time.  The `termion::async_stdin` function creates a
background thread that does blocking I/O. The `AsyncReader` object reads from a
queue connected to the background thread.

It uses the `into_raw_mode()` method on `stdout()` to disable echoing typed
characters.  This is helpful but probably we don't need to pull in the entire
`termion` library for this purpose.

Debug binary size: 6.3M

Release binary size: 4.6M

### stopwatch-burncpu2

This version of the stopwatch program is similar to `stopwatch-burncpu` in that
it uses the `termion` crate and burns lots of CPU. But the stopwatch logic is
implemented by the `stopwatch::task` module, just like the in the main stopwatch
program.

### termkeys

Press keys on the keyboard. Each time input is read from the keyboard it prints
the elapsed time since the start of the program with millisecond precision,
followed by the raw input bytes in hex and ASCII formats. In a Linux terminal
pressing one key such as an arrow key can result in multiple bytes being input;
this program will tell you exactly what those bytes are.

Press Ctrl-C or Ctrl-D to exit the program.

### termspeed

This program blasts bytes to the terminal as fast as possible and measures the
throughput. Press any key to stop the program and get a statistics report.

To run in debug mode: `cargo run -q --example termspeed`

### try-pollflags

Unlike most of the other programs, this one does not put the terminal in raw
mode and does not put standard input and output in non-blocking mode.  But it
does use `poll()` to detect when input is available and demonstrates running
code in a loop while waiting for input.

Enter lines of text and press Enter. To quit, press Ctrl-D on a line by itself.

## References

[Text-mode (terminal) application with asynchronous input/output](https://users.rust-lang.org/t/text-mode-terminal-application-with-asynchronous-input-output/74760)
