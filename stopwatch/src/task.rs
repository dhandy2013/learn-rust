//! Stopwatch task object - "sans-io" algorithm for running the stopwatch.
use crate::format_elapsed_ms_bytes;
use std::time::{Duration, Instant};

pub struct StopWatchTask {
    /// Output buffer
    buf: Vec<u8>,

    /// The time the stopwatch was most recently started
    start_time: Instant,

    /// Total duration of time the stopwatch has run so far.
    total_duration: Duration,

    /// Stopwatch duration as of the last stop, clear, or update
    prev_duration: Duration,

    /// True iff the stopwatch is currently running.
    is_running: bool,

    /// True iff the 'q' command to quit the stopwatch task has been received.
    quit_flag: bool,
}

impl StopWatchTask {
    pub fn new() -> Self {
        let mut buf = Vec::with_capacity(80);
        buf.extend(b"\r\n");
        format_elapsed_ms_bytes(0, &mut buf);
        buf.push(b'\r');
        Self {
            buf,
            start_time: Instant::now(),
            total_duration: Duration::new(0, 0),
            prev_duration: Duration::new(0, 0),
            is_running: false,
            quit_flag: false,
        }
    }

    /// Return true if the 'q' key has been pressed and there is no pending
    /// output. Otherwise return false, indicating that the task has not
    /// finished yet.
    pub fn is_finished(&self) -> bool {
        self.quit_flag && self.buf.is_empty()
    }

    /// Return true if there are pending output bytes in the buffer.
    pub fn has_pending_output(&self) -> bool {
        !self.buf.is_empty()
    }

    /// Return a slice containing the current pending output bytes.
    /// If no output is pending, return an empty slice.
    pub fn pending_output(&self) -> &[u8] {
        &self.buf[..]
    }

    /// Update the state of the stopwatch task based on input bytes from the
    /// terminal:
    /// - The Enter key ('\r' byte) starts or stops the stopwatch.
    /// - The Backspace key ('\x7f' byte) marks the current elapsed time if the
    ///   stopwatch is running, otherwise it clears the elapsed time.
    /// - The q key stops the stopwatch and shuts down the task.
    ///
    /// Note: This method expects to receive "raw" bytes as detailed above.
    /// The bytes read from stdin when it is in "cooked" mode are different:
    /// The Enter key translates to '\n' and the backspace key is '\x08'.
    pub fn handle_input(&mut self, inbuf: &[u8]) {
        for &c in inbuf.iter() {
            match c {
                b'\r' => {
                    if self.is_running {
                        self.stop();
                    } else {
                        self.start();
                    }
                }
                b'\x7f' => {
                    if self.is_running {
                        // mark current elapsed time by inserting newline
                        self.buf.push(b'\n');
                        return;
                    } else {
                        self.clear();
                    }
                }
                b'q' | b'\x03' => {
                    self.quit_flag = true;
                    if self.is_running {
                        self.stop()
                    }
                }
                _ => {}
            }
        }
    }

    fn start(&mut self) {
        self.is_running = true;
        self.start_time = Instant::now();
    }

    fn stop(&mut self) {
        self.is_running = false;
        self.total_duration += self.start_time.elapsed();
        self.prev_duration = self.total_duration;
        self.output_duration(self.total_duration);
        self.buf.push(b'\n');
    }

    fn clear(&mut self) {
        if self.total_duration.is_zero() {
            // Total stopwatch time has already been cleared.
            return;
        }

        self.total_duration = Duration::new(0, 0);
        self.prev_duration = self.total_duration;
        self.buf.push(b'\n');
        self.output_duration(self.total_duration);
    }

    /// This method is called when `n` bytes from the output buffer have been
    /// successfully written to the terminal.
    /// Remove `n` bytes from the start of the output buffer.
    pub fn handle_output(&mut self, n: usize) {
        if n >= self.buf.len() {
            self.buf.clear();
            return;
        }
        self.buf.copy_within(n.., 0);
        self.buf.truncate(self.buf.len() - n);
    }

    /// This method is called periodically while waiting for input or output.
    ///
    /// If the stopwatch has been running for 10ms or more since the last call
    /// to this method, output the current elapsed time.
    pub fn update(&mut self) {
        if !self.is_running {
            // Don't update displayed elapsed time if stopwatch is not running.
            return;
        }
        if self.buf.len() > 0 {
            // Don't update displayed elapsed time if we are still outputting
            // the previous elapsed time.
            return;
        }
        let cur_duration = self.total_duration + self.start_time.elapsed();
        if (cur_duration - self.prev_duration).as_millis() >= 10 {
            self.output_duration(cur_duration);
            self.prev_duration = cur_duration;
        }
    }

    fn output_duration(&mut self, duration: Duration) {
        format_elapsed_ms_bytes(duration.as_millis() as u32, &mut self.buf);
        self.buf.push(b'\r');
    }
}
