//! Command-Line stopwatch program for Linux that does efficient polling.
use nix::poll::{self, PollFd, PollFlags};
use std::error::Error;
use stopwatch::{task::StopWatchTask, term::RawTerminal};

fn main() -> Result<(), Box<dyn Error>> {
    println!("Stopwatch");
    println!("Enter: start/stop, Backspace: mark/clear, q: quit");

    let term = RawTerminal::new()?;
    let mut task = StopWatchTask::new();

    let stdin_fd = term.stdin_fd();
    let stdout_fd = term.stdout_fd();
    let mut poll_fds = [
        PollFd::new(&stdin_fd, PollFlags::POLLIN),
        PollFd::new(&stdout_fd, PollFlags::empty()),
    ];

    let mut inbuf: [u8; 16] = [0; 16];
    while !task.is_finished() {
        poll_fds[1] = if task.has_pending_output() {
            PollFd::new(&stdout_fd, PollFlags::POLLOUT)
        } else {
            PollFd::new(&stdout_fd, PollFlags::empty())
        };
        let result = poll::poll(&mut poll_fds, 10)?;
        if result > 0 {
            for poll_fd in poll_fds.iter() {
                if let Some(poll_flags) = poll_fd.revents() {
                    if poll_flags.contains(PollFlags::POLLIN) {
                        // Process input
                        let n = term.read_bytes(&mut inbuf)?;
                        task.handle_input(&inbuf[..n]);
                    } else if poll_flags.contains(PollFlags::POLLOUT) {
                        // Process output
                        let out = task.pending_output();
                        let n = term.write_bytes(out)?;
                        task.handle_output(n);
                    }
                }
            }
        } else {
            task.update();
        }
    }
    drop(term);

    println!("\nStop");
    Ok(())
}
