//! Terminal control
use nix::{
    fcntl::{self, FcntlArg, OFlag},
    sys::termios::{cfmakeraw, tcgetattr, tcsetattr, SetArg, Termios},
    unistd::{isatty, read, write},
};
use std::{
    io::{self, Stdin, Stdout},
    os::fd::{AsFd, AsRawFd, BorrowedFd},
};

pub struct RawTerminal {
    stdin: Stdin,
    stdout: Stdout,
    /// Original terminal settings
    saved_termios: Termios,
    /// Original "OFlag" (file open options) settings
    saved_oflags: i32,
}

impl RawTerminal {
    /// Put the terminal in "raw" mode with non-blocking I/O.
    /// This object then can be used for controlling the terminal.
    pub fn new() -> io::Result<Self> {
        check_tty()?;
        let stdin = io::stdin();
        let stdout = io::stdout();

        // Get and save current terminal settings
        let saved_termios = tcgetattr(stdout.as_fd())?;

        // Get and save the file descriptor settings
        // `fcntl(STDOUT_FILENO, F_GETFL)`
        let saved_oflags = fcntl::fcntl(stdout.as_raw_fd(), FcntlArg::F_GETFL)?;

        // Put the terminal into "raw" mode.
        // The terminal will be restored automatically when this object
        // is dropped.
        let mut termios = saved_termios.clone();
        cfmakeraw(&mut termios);
        tcsetattr(stdout.as_fd(), SetArg::TCSANOW, &termios)?;

        // Make all I/O operations on the terminal non-blocking.
        if let Err(err) = fcntl::fcntl(
            stdout.as_raw_fd(),
            FcntlArg::F_SETFL(
                OFlag::from_bits_retain(saved_oflags) | OFlag::O_NONBLOCK,
            ),
        ) {
            // Somehow we couldn't set stdout to non-blocking mode.
            // Restore the original termios settings before returning an error.
            let _ = tcsetattr(stdout.as_fd(), SetArg::TCSANOW, &saved_termios);
            return Err(io::Error::new(io::ErrorKind::Other, err));
        }

        Ok(Self {
            stdin,
            stdout,
            saved_termios,
            saved_oflags,
        })
    }

    pub fn stdin_fd(&self) -> BorrowedFd {
        self.stdin.as_fd()
    }

    pub fn stdout_fd(&self) -> BorrowedFd {
        self.stdout.as_fd()
    }

    /// Write the bytes in `buf` to stdout.
    /// Return the number of bytes written, or an io::Error.
    pub fn write_bytes(&self, buf: &[u8]) -> io::Result<usize> {
        write(self.stdout_fd().as_raw_fd(), buf)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))
    }

    /// Read bytes from stdin into `buf`.
    /// Return the number of bytes read, or an io::Error.
    pub fn read_bytes(&self, buf: &mut [u8]) -> io::Result<usize> {
        read(self.stdin_fd().as_raw_fd(), buf)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))
    }
}

impl Drop for RawTerminal {
    fn drop(&mut self) {
        // Restore the original file flags
        if let Err(err) = fcntl::fcntl(
            self.stdout.as_raw_fd(),
            FcntlArg::F_SETFL(OFlag::from_bits_retain(self.saved_oflags)),
        ) {
            eprintln!("Error while restoring terminal file flags: {err}");
        }
        // Restore the original terminal settings
        if let Err(err) =
            tcsetattr(self.stdout.as_fd(), SetArg::TCSANOW, &self.saved_termios)
        {
            eprintln!("Error while restoring terminal settings: {err}");
        }
    }
}

/// Ensure that both standard input and standard output are connected to an
/// interactive tty.
pub fn check_tty() -> io::Result<()> {
    let stdin_fd = io::stdin().as_raw_fd();
    let stdout_fd = io::stdout().as_raw_fd();
    let is_tty = isatty(stdin_fd)? && isatty(stdout_fd)?;
    if !is_tty {
        let msg =
            "Both stdin and stdout must be tty (no redirecting to/from files)";
        return Err(io::Error::new(io::ErrorKind::Other, msg));
    }
    Ok(())
}
