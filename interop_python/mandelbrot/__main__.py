# Mandelbrot Set calculation and display - in Python and Rust
# David H <cpif@handysoftware.com>  Oct 2019
"""
Mandelbrot Set Explorer

Left mouse button: zoom in
Ctrl-left mouse button: zoom out
Right mouse button: pan
F5: Force recalc (to update timing numbers)
F10: toggle fullscreen
Esc: quit
"""
import argparse
import ast
import importlib
import sys
import time

import numpy as np
import pygame

DEFAULT_WIDTH, DEFAULT_HEIGHT = 640, 480
DEFAULT_CENTER_X, DEFAULT_CENTER_Y = 0.0, 0.0
DEFAULT_SIZE = 2.0
DEFAULT_MAX_COUNT = 100
DEFAULT_SET_COLOR = (16, 48, 32)
DEFAULT_LANGUAGE = "python"

LANGUAGE_MODULE_MAP = {
    "python": ".mandelbrot",
    "cython": ".cy_mandelbrot",
    "rust": ".rs_mandelbrot",
}


def get_module(args):
    module_name = LANGUAGE_MODULE_MAP.get(args.language)
    if not module_name:
        raise ValueError(
            f"Invalid language '{args.language}', "
            f"valid languages are: {' '.join(list(LANGUAGE_MODULE_MAP))}"
        )
    return importlib.import_module(module_name, package="mandelbrot")


class MandelbrotDrawer:
    def __init__(
        self, home_center_x, home_center_y, home_size, max_count, set_color, module,
    ):
        self.home_center_x = home_center_x
        self.home_center_y = home_center_y
        self.home_size = home_size
        self.max_count = max_count
        self.set_color = set_color
        self.module = module
        self.reset()

    def reset(self):
        self.center_x = self.home_center_x
        self.center_y = self.home_center_y
        self.size = self.home_size

    @classmethod
    def from_args(cls, args):
        module = get_module(args)
        return cls(
            home_center_x=args.center_x,
            home_center_y=args.center_y,
            home_size=args.size,
            max_count=args.max_count,
            set_color=_convert_color(args.set_color),
            module=module,
        )

    def draw(self, pixels):
        options = {
            "center_x": self.center_x,
            "center_y": self.center_y,
            "size": self.size,
            "max_count": self.max_count,
            "set_color": self.set_color,
        }
        self.module.draw(pixels, options)

    def pan_and_zoom(self, vx, vy, scalefactor):
        # print(f"MandelbrotDrawer.pan_and_zoom({vx}, {vy}, {scalefactor})")
        self.center_x += vx * self.size
        self.center_y += vy * self.size
        self.size *= scalefactor


def _convert_color(color_str):
    if isinstance(color_str, str):
        try:
            color = ast.literal_eval(color_str)
        except ValueError:
            raise TypeError(f"Not a valid (R,G,B) color: {color_str!r}")
    else:
        color = color_str
    if not isinstance(color, tuple):
        raise TypeError(f"Not a valid (R,G,B) color tuple: {color_str!r}")
    if len(color) != 3:
        raise TypeError(f"Not a valid (R,G,B) color 3-tuple: {color_str!r}")
    if not all(isinstance(i, int) for i in color):
        raise TypeError(f"(R,G,B) values not all ints: {color_str!r}")
    if not all(0 <= i < 256 for i in color):
        raise TypeError(f"(R,G,B) values not all in range(256): {color_str!r}")
    return color


class TestPatternDrawer:
    def __init__(self, module):
        self.module = module

    @classmethod
    def from_args(cls, args):
        module = get_module(args)
        return cls(module)

    def reset(self):
        pass

    def draw(self, pixels):
        self.module.draw_test_pattern(pixels)

    def pan_and_zoom(self, vx, vy, scalefactor):
        # print(f"TestPatternDrawer.pan_and_zoom({vx}, {vy}, {scalefactor})")
        pass


class MouseStateStart:
    def __init__(self, window):
        self.window = window

    def __call__(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                # Left mouse button pressed
                mods = pygame.key.get_mods()
                if mods & pygame.KMOD_CTRL:
                    return MouseStateZoomOut(self.window, event.pos)
                return MouseStateZoomIn(self.window, event.pos)
            elif event.button == 3:
                # Right mouse button pressed
                return MouseStatePan(self.window, event.pos)
        return self

    def draw(self):
        pass


class MouseStateZoomBase:

    color = (32, 128, 32)

    def __init__(self, window, start_pos):
        self.window = window
        self.start_pos = start_pos
        self.rect = self._calc_rect(start_pos)
        self.window.need_refresh = True

    def __call__(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            self._pan_and_zoom(event.pos)
            self.window.need_refresh = True
            return MouseStateStart(self.window)
        elif event.type == pygame.MOUSEMOTION:
            self.rect = self._calc_rect(event.pos)
            self.window.need_refresh = True
        return self

    def draw(self):
        pygame.draw.rect(self.window.screen, self.color, self.rect, 2)

    def _calc_rect(self, pos):
        x1, y1 = self.start_pos
        x2, y2 = pos
        w = x2 - x1
        h = y2 - y1
        return (self.start_pos, (w, h))

    def _pan_and_zoom(self, pos):
        vx, vy, rect_length, win_length = _calc_pan_zoom_stuff(
            self.start_pos, pos, self.window.size
        )
        if rect_length < 2:
            # Too short of a distance for reasonable pan/zoom
            return
        # Calculate zoom scale factor
        scalefactor = self._calc_scale_factor(rect_length, win_length)
        # Pan and zoom
        self.window.drawobj.pan_and_zoom(vx, vy, scalefactor)
        self.window.need_draw = True

    def _calc_scale_factor(self, rect_length, win_length):
        raise NotImplementedError()


def _calc_pan_zoom_stuff(start_pos, end_pos, window_size):
    """
    Return vx, vy, rect_length, win_length
    Where (vx, vy) is the pan vector
    And (rect_length, win_length) are useful for calculating zoom factor
    """
    x1, y1 = start_pos
    x2, y2 = end_pos
    rect_w = abs(x2 - x1)
    rect_h = abs(y2 - y1)
    width, height = window_size
    if height < width:
        win_length = height
        rect_length = rect_h
    else:
        win_length = width
        rect_length = rect_w
    if win_length < 1:
        win_length = 1
    # Current window center
    cx1 = width // 2
    cy1 = height // 2
    # Center of drawn rectangle
    cx2 = (x1 + x2) // 2
    cy2 = (y1 + y2) // 2
    # Calculate pan vector
    vx = (cx2 - cx1) / win_length
    vy = (cy1 - cy2) / win_length  # Y axis has opposite sign in math
    return (vx, vy, rect_length, win_length)


class MouseStateZoomIn(MouseStateZoomBase):

    color = (128, 32, 32)

    def __init__(self, window, start_pos):
        MouseStateZoomBase.__init__(self, window, start_pos)

    def _calc_scale_factor(self, rect_length, win_length):
        return rect_length / win_length


class MouseStateZoomOut(MouseStateZoomBase):

    color = (32, 32, 128)

    def __init__(self, window, start_pos):
        MouseStateZoomBase.__init__(self, window, start_pos)

    def _calc_scale_factor(self, rect_length, win_length):
        return win_length / rect_length


class MouseStatePan:

    color = (32, 128, 32)

    def __init__(self, window, start_pos):
        self.window = window
        self.start_pos = start_pos
        self.end_pos = start_pos
        self.window.need_refresh = True

    def __call__(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            if event.pos != self.start_pos:
                self._pan(event.pos)
                self.window.need_refresh = True
            return MouseStateStart(self.window)
        elif event.type == pygame.MOUSEMOTION:
            self.end_pos = event.pos
            self.window.need_refresh = True
        return self

    def draw(self):
        pygame.draw.line(
            self.window.screen, self.color, self.start_pos, self.end_pos, 2
        )

    def _pan(self, pos):
        x1, y1 = self.start_pos
        x2, y2 = pos
        width, height = self.window.size
        if height < width:
            win_length = height
        else:
            win_length = width
        if win_length < 1:
            win_length = 1
        vx = (x1 - x2) / win_length
        vy = (y2 - y1) / win_length
        self.window.drawobj.pan_and_zoom(vx, vy, 1.0)
        self.window.need_draw = True


class WindowRunner:
    def __init__(self, size, drawobj):
        """
        size: (width, height)
        drawobj: Something with a draw(pixels) callable attribute
        """
        self.size = size
        if size == (0, 0):
            self.prev_size = (800, 600)
        else:
            self.prev_size = size
        self.drawobj = drawobj
        self.screen = None
        self.pixels = None
        self.need_draw = False
        self.need_refresh = False
        self.mouse_state = MouseStateStart(self)
        self.running = False

    def run(self):
        pygame.init()
        pygame.display.set_caption("Mandelbrot Set")
        self._init_screen()
        self._event_loop()
        pygame.quit()

    def _init_screen(self, size=None):
        if size is None:
            size = self.size
        if size == (0, 0):
            size = pygame.display.list_modes()[0]
            print(f"set_mode({size}, FULLSCREEN)")
            screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
        else:
            print(f"set_mode({size}, RESIZABLE)")
            screen = pygame.display.set_mode(size, pygame.RESIZABLE)
        if self.size != (0, 0):
            self.prev_size = self.size
        self.size = size
        self.screen = screen
        self.pixels = None  # invalidated drawing buffer
        self.need_draw = True
        self.need_refresh = True

    def _draw(self):
        pixels = self.pixels
        if pixels is None or self.size != pixels.shape[:2]:
            width, height = self.size
            pixels = np.zeros((width, height, 3), dtype=np.uint8)
            self.pixels = pixels
        t0 = time.perf_counter()
        self.drawobj.draw(pixels)
        calc_time = time.perf_counter() - t0
        caption = f"Mandlebrot Set - calc time: {calc_time:.6f}"
        pygame.display.set_caption(caption)
        # Also print calculation time, otherwise it is not visible if window in
        # fullscreen mode.
        print(caption)
        self.need_refresh = True

    def _event_loop(self):
        clock = pygame.time.Clock()
        self.running = True
        while self.running:
            for event in pygame.event.get():
                self._handle_event(event)
            if not self.running:
                break
            if self.screen is None or self.screen.get_size() != self.size:
                self._init_screen()
            if self.need_draw:
                self._draw()
                self.need_draw = False
            if self.need_refresh:
                if self.pixels is None:
                    self.screen.fill((0, 0, 0))
                else:
                    pygame.surfarray.blit_array(self.screen, self.pixels)
                self.mouse_state.draw()
                pygame.display.flip()
                self.need_refresh = False
            clock.tick(60)

    def _handle_event(self, event):
        if event.type == pygame.QUIT:
            self.running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                self.running = False
            elif event.key == pygame.K_HOME:
                self.drawobj.reset()
                self.need_draw = True
            elif event.key == pygame.K_F5:
                # Force recalc
                self.need_draw = True
            elif event.key == pygame.K_F10:
                # Toggle fullscreen
                if self.screen.get_flags() & pygame.FULLSCREEN:
                    self._init_screen(self.prev_size)
                else:
                    self._init_screen((0, 0))
        elif event.type in (
            pygame.MOUSEBUTTONUP,
            pygame.MOUSEBUTTONDOWN,
            pygame.MOUSEMOTION,
        ):
            self.mouse_state = self.mouse_state(event)
        elif event.type == pygame.VIDEORESIZE:
            print(f"VIDEORESIZE: {event.size}")
            self.size = event.size


def main():
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "--language",
        default=DEFAULT_LANGUAGE,
        help=f"Programming langage of mandelbrot calculator,"
        f" valid choices: {' '.join(LANGUAGE_MODULE_MAP)},"
        f" default: {DEFAULT_LANGUAGE}",
    )
    parser.add_argument(
        "--width",
        type=int,
        default=DEFAULT_WIDTH,
        help=f"Window width in pixels  [{DEFAULT_WIDTH}]",
    )
    parser.add_argument(
        "--height",
        type=int,
        default=DEFAULT_HEIGHT,
        help=f"Window height in pixels  [{DEFAULT_HEIGHT}]",
    )
    parser.add_argument(
        "--center-x",
        type=float,
        default=DEFAULT_CENTER_X,
        help=f"Starting X center of image in math coordinate space"
        f" [{DEFAULT_CENTER_X}]",
    )
    parser.add_argument(
        "--center-y",
        type=float,
        default=DEFAULT_CENTER_Y,
        help=f"Starting Y center of image in math coordinate space"
        f"  [{DEFAULT_CENTER_Y}]",
    )
    parser.add_argument(
        "--size",
        type=float,
        default=DEFAULT_SIZE,
        help=f"Starting size of image in math coordinate units" f"  [{DEFAULT_SIZE}]",
    )
    parser.add_argument(
        "--max_count",
        type=int,
        default=DEFAULT_MAX_COUNT,
        help=f"Max iteration count per pixel [{DEFAULT_MAX_COUNT}]",
    )
    parser.add_argument(
        "--set-color",
        default=DEFAULT_SET_COLOR,
        help=f"Color of pixels inside Mandelbrot set  [{DEFAULT_SET_COLOR}]",
    )
    parser.add_argument(
        "--test-pattern",
        action="store_true",
        help="Show a test pattern instead of Mandelbrot set",
    )
    args = parser.parse_args()
    if args.test_pattern:
        drawobj = TestPatternDrawer.from_args(args)
    else:
        drawobj = MandelbrotDrawer.from_args(args)
    window_runner = WindowRunner((args.width, args.height), drawobj)
    window_runner.run()
    return 0


if __name__ == "__main__":
    sys.exit(main())
