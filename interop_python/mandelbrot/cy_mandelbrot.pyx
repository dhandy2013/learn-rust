"""
Calculate the Mandelbrot Set
"""
import math

cimport cython
#import numpy as np


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def draw(unsigned char[:,:,:] pixels not None, options):
    """
    Draw the mandelbrot set into an off-screen pixel buffer
    pixels: numpy array with shape=(width, height, 3), dtype=uint8
    options: dict containing these key/values:
        center_x, center_y: Center image, in mathematical space [(0.0, 0.0)]
        size: Size in mathematical units of smallest window dimension [2.0]
        max_count: Max iterations to run algorithm per point  [100]
        set_color: (R, G, B) color of points in Mandelbrot set [(0, 0, 0)]
    """
    cdef double cx, cy, size, sizex, sizey, dx, dy, x, y
    cdef Py_ssize_t width, height, px, py
    cdef unsigned int max_count, count
    cdef unsigned char set_r, set_g, set_b, color
    cdef unsigned char[:,:] pixel_col
    cdef unsigned char[:] pixel
    if pixels.ndim != 3:
        raise TypeError("pixels must be a 3D array")
    if pixels.shape[2] != 3:
        raise TypeError("pixels 3rd dimension must be (R, G, B) colors")
    #if pixels.dtype != np.uint8:
    #    raise TypeError("pixels (R, G, B) color values must be type uint8")
    width, height = pixels.shape[:2]
    if width < 1 or height < 1:
        raise TypeError(f"Invalid pixels width x height: {width} x {height}")
    cx = options["center_x"]
    cy = options["center_y"]
    size = options["size"]
    max_count = options["max_count"]
    if max_count < 1:
        raise ValueError("max_count must be an integer >= 1")
    set_color = options["set_color"]
    set_r, set_g, set_b = set_color
    if height < width:
        sizey = size
        sizex = sizey * (float(width) / height)
    else:
        sizex = size
        sizey = sizex * (float(height) / width)
    dx = sizex / width  # units per pixel
    dy = sizey / height  # units per pixel

    x = cx - (sizex / 2.0)
    for px in range(width):
        pixel_col = pixels[px]
        y = cy + (sizey / 2.0)
        for py in range(height):
            pixel = pixel_col[py]
            count = _calc_mandelbrot_count(x, y, max_count)
            if count == <unsigned int>0xffffffff:
                pixel[0] = set_r
                pixel[1] = set_g
                pixel[2] = set_b
            else:
                color = <unsigned char>((float(count) / max_count) * 255.0)
                pixel[0] = color
                pixel[1] = color
                pixel[2] = color
            y -= dy
        x += dx


cdef unsigned int _calc_mandelbrot_count(double x, double y,
                                         unsigned int max_count):
    cdef complex c
    cdef complex z
    cdef unsigned int count
    c = x + y * 1j
    z = c
    for count in range(max_count + 1):
        if abs(z) >= 2.0:
            return count
        z = (z * z) + c
    else:
        return 0xffffffff


@cython.boundscheck(False)
@cython.wraparound(False)
def draw_test_pattern(unsigned char[:,:,:] pixels not None):
    cdef Py_ssize_t width, height, x, y
    cdef unsigned char r, g, b
    cdef unsigned char[:] pixel

    width, height = pixels.shape[:2]
    for y in range(width):
        for x in range(height):
            r = x & 0xff
            g = y & 0xff
            b = int(math.hypot(x, y)) & 0xff
            pixel = pixels[y][x]
            pixel[0] = r
            pixel[1] = g
            pixel[2] = b
