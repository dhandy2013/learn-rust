// This is the old version of this library based on the rust-cpython crate.
// It is no longer being compiled, it is just here for reference.
#[macro_use]
extern crate cpython;
use cpython::buffer::PyBuffer;
use cpython::{exc, PyDict, PyErr, PyObject, PyResult, PyTuple, Python};
use std::slice;

const BYTES_PER_PIXEL: usize = 3;

// The Python-importable name of this module is rs_mandelbrot
py_module_initializer!(
    rs_mandelbrot,
    initrs_mandelbrot,
    PyInit_rs_mandelbrot,
    |py, m| {
        m.add(
            py,
            "__doc__",
            "Mandelbrot set calculator written in Rust, importable in Python",
        )?;
        m.add(
            py,
            "draw",
            py_fn!(py, py_draw_mandelbrot(pixels: &PyObject, options: &PyDict)),
        )?;
        m.add(
            py,
            "draw_test_pattern",
            py_fn!(py, py_draw_test_pattern(pixels: &PyObject)),
        )?;
        Ok(())
    }
);

fn py_draw_mandelbrot(
    py: Python,
    pixels: &PyObject,
    options: &PyDict,
) -> PyResult<PyObject> {
    let (width, height, mut pixel_bytes) = _get_pixel_bytes(py, &pixels)?;

    // Get the other Mandelbrot set parameters
    let cx = _get_item_f64(py, &options, "center_x")?;
    let cy = _get_item_f64(py, &options, "center_y")?;
    let size = _get_item_f64(py, &options, "size")?;
    let max_count = _get_item_u32(py, &options, "max_count")?;
    if max_count < 1 {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            "max_count must be an integer >= 1",
        ));
    }
    let set_color = _get_item_color_tuple(py, &options, "set_color")?;

    draw_mandelbrot(
        &mut pixel_bytes,
        width,
        height,
        cx,
        cy,
        size,
        max_count,
        set_color,
    );

    Ok(py.None())
}

/// Return (width, height, pixel_bytes)
fn _get_pixel_bytes<'a>(
    py: Python,
    pixels: &'a PyObject,
) -> PyResult<(u32, u32, &'a mut [u8])> {
    // Get the pixel buffer
    let pixels = PyBuffer::get(py, pixels)?;
    check_buf(py, &pixels, "B", 1)?;
    if pixels.dimensions() != 3 {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            "pixels must be a 3D array",
        ));
    }
    let shape = pixels.shape();
    if shape[2] != 3 {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            "pixels 3rd dimension must be (R, G, B) colors",
        ));
    }
    if !pixels.is_c_contiguous() {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            "Expected pixels buffer to be in C order and contiguous",
        ));
    }
    let (width, height) = (shape[0] as u32, shape[1] as u32);
    if width < 1 || height < 1 {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Invalid pixels width x height: {width} x {height}",
                width = width,
                height = height
            ),
        ));
    }
    // We have just verified that the buffer is this size, but check it anyway
    let len_bytes = (width as usize) * (height as usize) * BYTES_PER_PIXEL;
    assert_eq!(len_bytes, pixels.len_bytes());
    let pixel_bytes = unsafe {
        slice::from_raw_parts_mut(pixels.buf_ptr() as *mut u8, len_bytes)
    };
    Ok((width, height, pixel_bytes))
}

/* This fails to compile due to lifetime issues.
fn _get_item<'a, T>(py: Python<'a>, d: &PyDict, key: &str)
                -> PyResult<T>
                where T: cpython::FromPyObject<'a> + Copy {
    if let Some(pyobj) = d.get_item(py, key) {
        pyobj.extract::<T>(py)
    } else {
        Err(PyErr::new::<exc::KeyError, _>(py, format!("{}", key)))
    }
}
*/

fn _get_item_f64(py: Python, d: &PyDict, key: &str) -> PyResult<f64> {
    if let Some(pyobj) = d.get_item(py, key) {
        pyobj.extract::<f64>(py)
    } else {
        Err(PyErr::new::<exc::KeyError, _>(py, format!("{}", key)))
    }
}

fn _get_item_u32(py: Python, d: &PyDict, key: &str) -> PyResult<u32> {
    if let Some(pyobj) = d.get_item(py, key) {
        pyobj.extract::<u32>(py)
    } else {
        Err(PyErr::new::<exc::KeyError, _>(py, format!("{}", key)))
    }
}

#[derive(Clone, Copy, Debug)]
struct ColorTuple(u8, u8, u8);

fn _get_item_color_tuple(
    py: Python,
    d: &PyDict,
    key: &str,
) -> PyResult<ColorTuple> {
    if let Some(pyobj) = d.get_item(py, key) {
        let py_color_tuple = pyobj.extract::<PyTuple>(py)?;
        if py_color_tuple.len(py) != 3 {
            return Err(PyErr::new::<exc::TypeError, _>(
                py,
                "color tuple must have length 3",
            ));
        }
        let mut result = ColorTuple(0, 0, 0);
        result.0 = py_color_tuple.get_item(py, 0).extract::<u8>(py)?;
        result.1 = py_color_tuple.get_item(py, 1).extract::<u8>(py)?;
        result.2 = py_color_tuple.get_item(py, 2).extract::<u8>(py)?;
        Ok(result)
    } else {
        Err(PyErr::new::<exc::KeyError, _>(py, format!("{}", key)))
    }
}

/// Verify that the PyBuffer contains items of the expected format and size.
fn check_buf(
    py: Python,
    a: &PyBuffer,
    expected_format: &str,
    expected_item_size: usize,
) -> PyResult<()> {
    let format: String = a.format().to_string_lossy().to_string();
    if format != expected_format {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Need buffer of format '{}' not '{}'",
                expected_format, format
            ),
        ));
    }
    if a.item_size() != expected_item_size {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Need buffer of item size {} not {}",
                expected_item_size,
                a.item_size()
            ),
        ));
    }
    if !(a.is_c_contiguous() || a.is_fortran_contiguous()) {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            "Need contiguous buffer",
        ));
    }
    if a.shape().len() != a.strides().len() {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Length of shape {:?} doesn't match length of strides {:?}",
                a.shape(),
                a.strides()
            ),
        ));
    }
    Ok(())
}

/// Draw the Mandelbrot set into the given pixel buffer
fn draw_mandelbrot(
    pixel_bytes: &mut [u8],
    width: u32,
    height: u32,
    center_x: f64,
    center_y: f64,
    size: f64,
    max_count: u32,
    set_color: ColorTuple,
) {
    let (sizex, sizey) = if height < width {
        (size * (width as f64 / height as f64), size)
    } else {
        (size, size * (height as f64 / width as f64))
    };
    let dx = sizex / width as f64;
    let dy = sizey / height as f64;

    let mut x = center_x - (sizex / 2.0);
    for pixel_col in pixel_bytes.chunks_exact_mut((height as usize) * 3) {
        let mut y = center_y + (sizey / 2.0);
        for pixel in pixel_col.chunks_exact_mut(3) {
            let count = _calc_mandelbrot_count(x, y, max_count);
            if count == u32::max_value() {
                pixel[0] = set_color.0;
                pixel[1] = set_color.1;
                pixel[2] = set_color.2;
            } else {
                let color = ((count as f64 / max_count as f64) * 255.0) as u8;
                pixel[0] = color;
                pixel[1] = color;
                pixel[2] = color;
            }
            y -= dy;
        }
        x += dx;
    }
}

fn _calc_mandelbrot_count(x: f64, y: f64, max_count: u32) -> u32 {
    let (c_r, c_i) = (x, y);
    let (mut z_r, mut z_i) = (c_r, c_i);
    for count in 0..(max_count + 1) {
        if ((z_r * z_r) + (z_i * z_i)).sqrt() >= 2.0 {
            return count;
        }
        let z_r_next = (z_r * z_r) - (z_i * z_i) + c_r;
        z_i = (2.0 * z_r * z_i) + c_i;
        z_r = z_r_next;
    }
    u32::max_value()
}

fn py_draw_test_pattern(py: Python, pixels: &PyObject) -> PyResult<PyObject> {
    let (width, height, pixel_bytes) = _get_pixel_bytes(py, &pixels)?;
    for x in 0..width {
        for y in 0..height {
            let r = (x & 0xff) as u8;
            let g = (y & 0xff) as u8;
            let b = (((x as f64).hypot(y as f64) as u32) & 0xff) as u8;
            let i =
                ((x as usize * height as usize) + y as usize) * BYTES_PER_PIXEL;
            pixel_bytes[i] = r;
            pixel_bytes[i + 1] = g;
            pixel_bytes[i + 2] = b;
        }
    }

    Ok(py.None())
}
