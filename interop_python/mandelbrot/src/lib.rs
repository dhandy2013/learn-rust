use pyo3::buffer::PyBuffer;
use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;
use pyo3::types::{PyDict, PyTuple};
use std::slice;

const BYTES_PER_PIXEL: usize = 3;

/// Draw the mandelbrot set into a pygame-compatible pixel buffer.
/// pixels: A 3-dimensional byte buffer [col][row][r, g, b]
/// options: Python dict with string keys and float or integer values:
///     center_x (float): center X of mandelbrot rendering, math units
///     center_y (float): center Y of mandelbrot rendering, math units
///     size (float): size of smallest dimension of rendering, math units
///     max_count (int): max iterations to run mandelbrot algorithm per pixel
///     set_color (r, g, b): tuple of Red, Green, Blue byte values
#[pyfunction(name = "draw")]
fn py_draw_mandelbrot(
    py: Python<'_>,
    pixels: PyBuffer<u8>,
    options: &PyDict,
) -> PyResult<()> {
    let (width, height, mut pixel_bytes) = _get_pixel_bytes(py, pixels)?;

    // Get the other Mandelbrot set parameters
    let cx = _get_item_f64(&options, "center_x")?;
    let cy = _get_item_f64(&options, "center_y")?;
    let size = _get_item_f64(&options, "size")?;
    let max_count = _get_item_u32(&options, "max_count")?;
    if max_count < 1 {
        return Err(PyValueError::new_err(
            "max_count option must be an integer >= 1",
        ));
    }
    let set_color = _get_item_color_tuple(&options, "set_color")?;

    draw_mandelbrot(
        &mut pixel_bytes,
        width,
        height,
        cx,
        cy,
        size,
        max_count,
        set_color,
    );

    Ok(())
}

/// Draw a test pattern into a pygame-compatible pixel buffer.
#[pyfunction(name = "draw_test_pattern")]
fn py_draw_test_pattern(py: Python<'_>, pixels: PyBuffer<u8>) -> PyResult<()> {
    let (width, height, pixel_bytes) = _get_pixel_bytes(py, pixels)?;
    for x in 0..width {
        for y in 0..height {
            let r = (x & 0xff) as u8;
            let g = (y & 0xff) as u8;
            let b = (((x as f64).hypot(y as f64) as u32) & 0xff) as u8;
            let i =
                ((x as usize * height as usize) + y as usize) * BYTES_PER_PIXEL;
            pixel_bytes[i] = r;
            pixel_bytes[i + 1] = g;
            pixel_bytes[i + 2] = b;
        }
    }

    Ok(())
}

/// Return (width, height, pixel_bytes).
/// The _py parameter is used just to determine the lifetime of pixel_bytes,
/// to indicate that it lasts as long as we hold the GIL.
fn _get_pixel_bytes<'a>(
    _py: Python<'a>,
    pixels: PyBuffer<u8>,
) -> PyResult<(u32, u32, &'a mut [u8])> {
    if pixels.dimensions() != 3 {
        return Err(PyValueError::new_err("pixels must be a 3D array"));
    }
    let shape = pixels.shape();
    if shape[2] != 3 {
        return Err(PyValueError::new_err(
            "pixels 3rd dimension must be (R, G, B) colors",
        ));
    }
    if !pixels.is_c_contiguous() {
        return Err(PyValueError::new_err(
            "Expected pixels buffer to be in C order and contiguous",
        ));
    }
    let (width, height) = (shape[0] as u32, shape[1] as u32);
    if width < 1 || height < 1 {
        return Err(PyValueError::new_err(format!(
            "Invalid pixels width x height: {width} x {height}",
            width = width,
            height = height
        )));
    }
    // We have just verified that the buffer is this size, but check it anyway
    let len_bytes = (width as usize) * (height as usize) * BYTES_PER_PIXEL;
    assert_eq!(len_bytes, pixels.len_bytes());
    let pixel_bytes = unsafe {
        slice::from_raw_parts_mut(pixels.buf_ptr() as *mut u8, len_bytes)
    };
    Ok((width, height, pixel_bytes))
}

/// Get an item from a Python dictionary, assuming a string key and f64 value
fn _get_item_f64(d: &PyDict, key: &str) -> PyResult<f64> {
    let pyobj = PyAny::get_item(d, key)?;
    let result: f64 = pyobj.extract()?;
    Ok(result)
}

/// Get an item from a Python dictionary, assuming a string key and u32 value
fn _get_item_u32(d: &PyDict, key: &str) -> PyResult<u32> {
    let pyobj = PyAny::get_item(d, key)?;
    let result: u32 = pyobj.extract()?;
    Ok(result)
}

#[derive(Clone, Copy, Debug)]
struct ColorTuple(u8, u8, u8);

/// Get an item from a Python dictionary, assuming string key and (r, g, b) byte
/// tuple value.
fn _get_item_color_tuple(d: &PyDict, key: &str) -> PyResult<ColorTuple> {
    let pyobj = PyAny::get_item(d, key)?;
    let py_color_tuple: &PyTuple = pyobj.downcast()?;
    if py_color_tuple.len() != 3 {
        return Err(PyValueError::new_err("color tuple must have length 3"));
    }
    let mut result = ColorTuple(0, 0, 0);
    result.0 = py_color_tuple.get_item(0)?.extract()?;
    result.1 = py_color_tuple.get_item(1)?.extract()?;
    result.2 = py_color_tuple.get_item(2)?.extract()?;
    Ok(result)
}

/// Draw the Mandelbrot set into the given pixel buffer
fn draw_mandelbrot(
    pixel_bytes: &mut [u8],
    width: u32,
    height: u32,
    center_x: f64,
    center_y: f64,
    size: f64,
    max_count: u32,
    set_color: ColorTuple,
) {
    let (sizex, sizey) = if height < width {
        (size * (width as f64 / height as f64), size)
    } else {
        (size, size * (height as f64 / width as f64))
    };
    let dx = sizex / width as f64;
    let dy = sizey / height as f64;

    let mut x = center_x - (sizex / 2.0);
    for pixel_col in pixel_bytes.chunks_exact_mut((height as usize) * 3) {
        let mut y = center_y + (sizey / 2.0);
        for pixel in pixel_col.chunks_exact_mut(3) {
            let count = calc_mandelbrot_count(x, y, max_count);
            if count == u32::max_value() {
                pixel[0] = set_color.0;
                pixel[1] = set_color.1;
                pixel[2] = set_color.2;
            } else {
                let color = ((count as f64 / max_count as f64) * 255.0) as u8;
                pixel[0] = color;
                pixel[1] = color;
                pixel[2] = color;
            }
            y -= dy;
        }
        x += dx;
    }
}

/// Run the mandelbrot algorithm for complex number (x, y * j).
/// Return the number of iterations before it blows up, or u32::max_value()
/// (0xffffffff) if it stays stable for max_count iterations.
fn calc_mandelbrot_count(x: f64, y: f64, max_count: u32) -> u32 {
    let (c_r, c_i) = (x, y);
    let (mut z_r, mut z_i) = (c_r, c_i);
    for count in 0..(max_count + 1) {
        if ((z_r * z_r) + (z_i * z_i)).sqrt() >= 2.0 {
            return count;
        }
        let z_r_next = (z_r * z_r) - (z_i * z_i) + c_r;
        z_i = (2.0 * z_r * z_i) + c_i;
        z_r = z_r_next;
    }
    u32::max_value()
}

/// Mandelbrot set calculator written in Rust, importable in Python
#[pymodule]
fn rs_mandelbrot(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(py_draw_mandelbrot, m)?)
        .unwrap();
    m.add_function(wrap_pyfunction!(py_draw_test_pattern, m)?)
        .unwrap();
    Ok(())
}
