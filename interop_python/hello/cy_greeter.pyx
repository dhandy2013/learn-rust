# To compile: cythonize -3 -i -a hello/*.pyx

def is_missing_value(v):
    """Return True if the given value is a {"?": missing} value."""
    return type(v) is dict and len(v) == 1 and "?" in v
