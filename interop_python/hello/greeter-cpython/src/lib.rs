use cpython::{
    py_fn, py_module_initializer, FromPyObject as _, PyDict, PyObject,
    PyResult, Python,
};

/// Return True iff the given value is a {"?": missing} value marker.
fn is_missing_value(py: Python, obj: &PyObject) -> PyResult<bool> {
    let maybe_dict: PyResult<PyDict> = PyDict::extract(py, &obj);
    if let Ok(dict) = maybe_dict {
        if dict.len(py) != 1 {
            return Ok(false);
        }
        if !dict.contains(py, "?")? {
            return Ok(false);
        }
        return Ok(true);
    }
    Ok(false)
}

// Our module is named 'rs_greeter_cpython', and can be imported using `import
// rs_greeter_cpython`.  This requires that the output binary file is named
// `rs_greeter_cpython.so` (or Windows: `rs_greeter_cpython.pyd`), but the built
// compiled output file is actually `target/release/librs_greeter_cpython.so`.
// As the output name cannot be configured in cargo
// (https://github.com/rust-lang/cargo/issues/1970), you'll have to rename the
// output file in a separate build step.
py_module_initializer!(
    rs_greeter_cpython,
    initrs_greeter_cpython,
    PyInit_rs_greeter_cpython,
    |py, m| {
        m.add(
            py,
            "__doc__",
            "Basic example of Python functions implemented in Rust",
        )?;
        m.add(
            py,
            "is_missing_value",
            py_fn!(py, is_missing_value(obj: &PyObject)),
        )?;
        Ok(())
    }
);
