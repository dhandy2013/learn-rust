use pyo3::prelude::*;
use pyo3::types::PyDict;

/// Return True iff the given value is a {"?": missing} value marker.
#[pyfunction]
fn is_missing_value(obj: &PyAny) -> PyResult<bool> {
    if let Ok(dict) = obj.downcast::<PyDict>() {
        if dict.len() != 1 {
            return Ok(false);
        }
        if !dict.contains("?")? {
            return Ok(false);
        }
        return Ok(true);
    }
    Ok(false)
}

// Our module is named 'rs_greeter_pyo3', and can be imported using `import
// rs_greeter_pyo3`.  This requires that the output binary file is named
// `rs_greeter_pyo3.so` (or Windows: `rs_greeter_pyo3.pyd`), but the built
// compiled output file is actually `target/release/librs_greeter_pyo3.so`.
// As the output name cannot be configured in cargo
// (https://github.com/rust-lang/cargo/issues/1970), you'll have to rename the
// output file in a separate build step.

/// Basic example of Python functions implemented in Rust
#[pymodule]
fn rs_greeter_pyo3(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(is_missing_value, m)?)
        .unwrap();
    Ok(())
}
