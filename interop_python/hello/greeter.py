def is_missing_value(v):
    """Return True if the given value is a {"?": missing} value."""
    return type(v) is dict and len(v) == 1 and "?" in v


def func(a, b):
    return f"func({a}, {b})"


def run(*args, **kwargs):
    print("Python says: Hello world!")
    for arg in args:
        print("Python got", arg)
    for key, val in kwargs.items():
        print(f"{key} = {val}")
