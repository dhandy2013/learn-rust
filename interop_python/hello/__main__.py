import sys
import timeit

from hello.greeter import is_missing_value as is_missing_value_python
from hello.cy_greeter import is_missing_value as is_missing_value_cython
from hello.rs_greeter_cpython import is_missing_value as is_missing_value_rust_cpython
from hello.rs_greeter_pyo3 import is_missing_value as is_missing_value_rust_pyo3


def main():
    test_vector = [1, "A", {}, {"A": 1}, {"?": -1, "!": 0}, {"?": -1}]
    print("Param               Python  Cython  Rust    Rust")
    print("                                    cpython pyo3")
    print("-----------------   ------  -----   ------- -------")
    for a in test_vector:
        output = [a]
        for f in (
            is_missing_value_python,
            is_missing_value_cython,
            is_missing_value_rust_cpython,
            is_missing_value_rust_pyo3,
        ):
            output.append(f(a))
        print("{!r:17}   {!r:7} {!r:7} {!r:7} {!r:7}".format(*output))
    print()

    test_values = [{"?": -1}] * 1000000

    def _time_python_func():
        for v in test_values:
            if is_missing_value_python(v):
                pass

    def _time_python_inline():
        for v in test_values:
            if type(v) is dict and len(v) == 1 and "?" in v:
                pass

    def _time_rust_pyo3_func():
        for v in test_values:
            if is_missing_value_rust_pyo3(v):
                pass

    def _time_rust_cpython_func():
        for v in test_values:
            if is_missing_value_rust_cpython(v):
                pass

    def _time_cython_func():
        for v in test_values:
            if is_missing_value_cython(v):
                pass

    def _time_loop_overhead():
        for v in test_values:
            pass

    best_python_func = min(timeit.repeat(_time_python_func, repeat=3, number=1))
    best_python_inline = min(timeit.repeat(_time_python_inline, repeat=3, number=1))
    best_rust_pyo3_func = min(timeit.repeat(_time_rust_pyo3_func, repeat=3, number=1))
    best_rust_cpython_func = min(
        timeit.repeat(_time_rust_cpython_func, repeat=3, number=1)
    )
    best_cython_func = min(timeit.repeat(_time_cython_func, repeat=3, number=1))
    best_loop_overhead = min(timeit.repeat(_time_loop_overhead, repeat=3, number=1))
    print(f"Python function        : {best_python_func:.6f}")
    print(f"Python expression      : {best_python_inline:.6f}")
    print(f"Rust-pyo3 function     : {best_rust_pyo3_func:.6f}")
    print(f"Rust-cpython function  : {best_rust_cpython_func:.6f}")
    print(f"Cython function        : {best_cython_func:.6f}")
    print(f"Loop overhead          : {best_loop_overhead:.6f}")


if __name__ == "__main__":
    sys.exit(main())
