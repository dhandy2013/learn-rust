Python - Rust Interop
=====================

Examples of writing Python extensions modules in Rust
-----------------------------------------------------

These are the example directories under this directory:

- ``hello``: Basic extension module example
- ``bounce``: Simple particle model, shows implementing Python class in Rust
- ``fwnumpy``: Fun With numpy - passing numpy buffers to Rust
- ``mandelbrot``: Compute a Mandlebrot set and display it using pygame

Run ``build.py <example-dir>`` to compile Rust extension modules for the given example.
For instance, to build the ``mandelbrot`` example::

    ./build.py mandelbrot

Any Cython code (.pyx files) in the directory will be compiled also.

Run ``python3 -m <example-dir>`` to run the associated demo. Example::

    python3 -m mandelbrot

Creating a new Python extension module in Rust
----------------------------------------------

Run ``cargo new --lib <example-dir>``

Edit ``<example-dir>/Cargo.toml`` and ``<example-dir>/src/lib.rs``

Look at the other example modules for hints.

Naming Convention
-----------------

In the case where I implement a module with the same functionality in multiple
languages, if ``<module>.py`` is the name of the file implementing the
pure-Python version, then ``rs_<module>`` is the Rust version and
``cy_<module>`` is the Cython version.

For some examples I implement the extension module in Rust two different ways,
using the ``rust-cpython`` crate and using ``pyo3``. In that case, the module names
will be ``rs_<module>_cpython`` and ``rs_<module>_pyo3``, respectively.

Cython info
-----------

Main website: https://pypi.org/project/Cython/

Cython lets you develop compiled Python extension modules in a Python-like language.
Some of the examples here show implementing a Python module with the same interface
in Python, Cython, and Rust for comparison.

To manually recompile Cython modules: ``cythonize -3 -i -a <example-dir>/*.pyx``

Setup Fedora Linux to compile pygame 2.x for development
--------------------------------------------------------

http://www.pygame.org/wiki/CompileUbuntu#pygame%20with%20sdl2%20(alpha)

::

    sudo yum install python3-devel SDL2-devel SDL2_image-devel \
        SDL2_mixer-devel SDL_ttf-devel freetype-devel portmidi-devel \
        libjpeg-turbo-devel

