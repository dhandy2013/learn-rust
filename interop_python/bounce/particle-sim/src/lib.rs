use std::fmt;

pub struct Particle {
    r: f64,
    x: f64,
    y: f64,
    vx: f64,
    vy: f64,
}

impl Particle {
    pub fn new(r: f64, x: f64, y: f64, vx: f64, vy: f64) -> Particle {
        Particle { r, x, y, vx, vy }
    }

    pub fn iter_fields(&self) -> ParticleFieldIter {
        ParticleFieldIter {
            data: &self,
            index: 0,
        }
    }

    pub fn update(&mut self) {
        self.x += self.vx;
        self.y += self.vy;
    }
}

impl fmt::Display for Particle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Particle(r={}, x={}, y={}, vx={}, vy={})",
            self.r, self.x, self.y, self.vx, self.vy,
        )
    }
}

// Iterate over all the fields in a Particle and yield (name, value) pairs.
// Meant to facilitate converting a simple Rust struct to a Python dict without
// making the fields public or bringing in some large serde crate.

pub struct ParticleFieldIter<'a> {
    data: &'a Particle,
    index: i32,
}

impl<'a> Iterator for ParticleFieldIter<'a> {
    type Item = (&'static str, f64);

    fn next(&mut self) -> Option<Self::Item> {
        self.index += 1;
        match self.index {
            1 => Some(("r", self.data.r)),
            2 => Some(("x", self.data.x)),
            3 => Some(("y", self.data.y)),
            4 => Some(("vx", self.data.vx)),
            5 => Some(("vy", self.data.vy)),
            _ => {
                self.index = -1;
                None
            }
        }
    }
}
