class Particle:
    def __init__(self, r, x, y, vx, vy):
        self.r = r
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy

    def update(self):
        """Update the state of the particle"""
        self.x += self.vx
        self.y += self.vy

    def as_dict(self):
        """Return the particle fields as a dict"""
        return {
            "r": self.r,
            "x": self.x,
            "y": self.y,
            "vx": self.vx,
            "vy": self.vy,
        }

    def __repr__(self):
        return (
            f"Particle("
            f"r={self.r}, "
            f"x={self.x}, "
            f"y={self.y}, "
            f"vx={self.vx}, "
            f"vy={self.vy})"
        )
