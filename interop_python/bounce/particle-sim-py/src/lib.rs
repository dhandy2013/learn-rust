use particle_sim::Particle;
use pyo3::prelude::*;
use pyo3::types::PyDict;

#[pyclass(name = "Particle")]
struct PyParticle {
    inner: Particle,
}

#[pymethods]
impl PyParticle {
    #[new]
    fn new(r: f64, x: f64, y: f64, vx: f64, vy: f64) -> Self {
        Self {
            inner: Particle::new(r, x, y, vx, vy),
        }
    }

    fn __repr__(&self) -> PyResult<String> {
        Ok(format!("{}", self.inner))
    }

    /// Update the state of the particle
    fn update(&mut self) {
        self.inner.update()
    }

    /// Return the particle fields as a dict
    fn as_dict<'a>(&self, py: Python<'a>) -> PyResult<&'a PyDict> {
        let result = PyDict::new(py);
        for (name, value) in self.inner.iter_fields() {
            result.set_item(name, value)?;
        }
        Ok(result)
    }
}

/// Particle simulator implemented in Rust
#[pymodule]
fn rs_particle_sim(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyParticle>()?;
    Ok(())
}
