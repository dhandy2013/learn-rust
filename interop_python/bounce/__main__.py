"""Particle Simulator Rust/Python demo"""
import argparse
import importlib
import sys

DEFAULT_LANGUAGE = "python"

LANGUAGE_MODULE_MAP = {
    "python": ".particle_sim",
    "rust": ".rs_particle_sim",
}


def main():
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "--language",
        default=DEFAULT_LANGUAGE,
        help=f"Programming langage of particle simulator."
        f" Valid choices: {' '.join(LANGUAGE_MODULE_MAP)},"
        f" default: {DEFAULT_LANGUAGE}",
    )
    args = parser.parse_args()
    module_name = LANGUAGE_MODULE_MAP.get(args.language)
    if not module_name:
        raise ValueError(
            f"Invalid language '{args.language}', "
            f"valid languages are: {' '.join(list(LANGUAGE_MODULE_MAP))}"
        )
    module = importlib.import_module(module_name, package="bounce")
    p1 = module.Particle(1.0, 4.0, 3.0, 1.5, -0.5)
    print(f"p1 == {p1}")
    print(p1.as_dict())
    p1.update()
    print(p1.as_dict())


if __name__ == "__main__":
    sys.exit(main())
