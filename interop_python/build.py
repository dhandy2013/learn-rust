#!/usr/bin/env python3
"""Build script for Rust/Python and Cython integration"""
import argparse
import glob
import os
import re
import shutil
import subprocess
import sys
import sysconfig

try:
    from Cython.Build.Cythonize import main as cythonize
except ModuleNotFoundError:
    cythonize = None


def _check_module_name(module_name):
    if not module_name:
        raise ValueError("Missing module_name")
    if not module_name == module_name.lower():
        raise ValueError("module_name must be all lowercase characters")
    if not re.match(r"[a-z_]\w+$", module_name):
        raise ValueError("module_name must be a valid identifier longer than 1 char")


def run_cargo_build(args):
    subdir = args.subdir_name
    cmd = ["cargo", "build", "--release"]
    cargo_path = os.path.join(subdir, "Cargo.toml")
    if not os.path.isfile(cargo_path):
        raise ValueError(f"{subdir} is not a rust crate, missing file {cargo_path}")
    env = os.environ.copy()
    # Set PYO3_PYTHON to tell pyo3 which Python version to use.
    # https://pyo3.rs/v0.17.1/building_and_distribution.html
    env["PYO3_PYTHON"] = sys.executable
    print("pushd", subdir)
    print(f"PYO3_PYTHON={env['PYO3_PYTHON']} {subprocess.list2cmdline(cmd)}")
    try:
        subprocess.run(cmd, cwd=subdir, check=True, env=env)
    except subprocess.CalledProcessError as err:
        print(err, file=sys.stderr)
        return err.returncode
    print("popd")


def copy_linux_artifacts(args):
    subdir = args.subdir_name
    target_files = glob.glob(os.path.join(subdir, "target", "release", "lib*.so"))
    for target_file in target_files:
        target_name = os.path.basename(target_file)
        assert target_name.startswith("lib")
        assert target_name.endswith(".so")
        # Strip off leading 'lib' and trailing '.so'
        module_name = (target_name[3:])[:-3]
        # Add appropriate extension filename suffix based on Python version
        # e.g. .cpython-38-x86_64-linux-gnu.so
        importable_so_name = module_name + sysconfig.get_config_var("EXT_SUFFIX")
        importable_so_path = os.path.join(subdir, importable_so_name)
        print("cp", target_file, importable_so_path)
        shutil.copy(target_file, importable_so_path)


def copy_windows_artifacts(args):
    subdir = args.subdir_name
    target_files = glob.glob(os.path.join(subdir, "target", "release", "*.dll"))
    for target_file in target_files:
        target_name = os.path.basename(target_file)
        assert target_name.endswith(".dll")
        importable_dll = os.path.join(subdir, target_name[:-4] + ".pyd")
        print("copy", target_file, importable_dll)
        shutil.copy(target_file, importable_dll)


def compile_pyx_files(args):
    subdir = args.subdir_name
    pyx_files = glob.glob(os.path.join(subdir, "*.pyx"))
    if not pyx_files:
        # No .pyx files to be compiled
        return None
    if not cythonize:
        print(
            f"cythonize not installed for this Python version,"
            f" cannot compile: {pyx_files}",
            file=sys.stderr,
        )
        return 1
    # -3: Use Python 3 syntax for source files
    # -i: Compile files "in-place" (put .so file alongside .pyx file)
    # -a: Put annotations in .html file
    cythonize_args = ["-3", "-i", "-a"] + pyx_files
    print(subprocess.list2cmdline(["cythonize"] + cythonize_args))
    return cythonize(cythonize_args)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("subdir_name")
    args = parser.parse_args()
    args.subdir_name = args.subdir_name.rstrip("/")
    try:
        _check_module_name(args.subdir_name)
    except ValueError as e:
        print(
            f"subdir_name '{args.subdir_name}' is not a valid Python module name: {e}",
            file=sys.stderr,
        )
        return 1
    rc = run_cargo_build(args)
    if rc:
        return rc
    copy_linux_artifacts(args)
    copy_windows_artifacts(args)
    rc = compile_pyx_files(args)
    if rc:
        return rc
    return 0


if __name__ == "__main__":
    sys.exit(main())
