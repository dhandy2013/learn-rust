#!/usr/bin/env python3
"""
Remove from a package directory artifacts created by build.py
"""
import argparse
from pathlib import Path
import re
import shutil
import sys


def _check_module_name(module_name):
    if not module_name:
        raise ValueError("Missing module_name")
    if not module_name == module_name.lower():
        raise ValueError("module_name must be all lowercase characters")
    if not re.match(r"[a-z_]\w+$", module_name):
        raise ValueError("module_name must be a valid identifier longer than 1 char")


def clean(args):
    subdir = Path(args.subdir_name)
    # Removing all Rust cargo artifacts is easy: just remove the target directory.
    target_dir = subdir / "target"
    if target_dir.exists():
        print(f"rm -f -r {target_dir}")
        shutil.rmtree(target_dir)
    # Remove any built/copied .so files
    for so_path in subdir.glob("*.so"):
        print(f"rm {so_path}")
        so_path.unlink()
    # Remove any .c or .html file with a corresponding .pyx file
    for pyx_path in subdir.glob("*.pyx"):
        c_path = subdir / (pyx_path.stem + ".c")
        if c_path.exists():
            print(f"rm {c_path}")
            c_path.unlink()
        html_path = subdir / (pyx_path.stem + ".html")
        if html_path.exists():
            print(f"rm {html_path}")
            html_path.unlink()
    # For consistency and safety, also remove Python build artifacts
    pycache_dir = subdir / "__pycache__"
    if pycache_dir.exists():
        print(f"rm -f -r {pycache_dir}")
        shutil.rmtree(pycache_dir)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("subdir_name", help="package directory to clean")
    args = parser.parse_args()
    args.subdir_name = args.subdir_name.rstrip("/")
    try:
        _check_module_name(args.subdir_name)
    except ValueError as e:
        print(
            f"subdir_name '{args.subdir_name}' is not a valid Python module name: {e}",
            file=sys.stderr,
        )
        return 1
    return clean(args)


if __name__ == "__main__":
    sys.exit(main())
