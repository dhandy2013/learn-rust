use ndarray::ShapeBuilder;
use pyo3::buffer::{Element, PyBuffer};
use pyo3::exceptions::{PyTypeError, PyValueError};
use pyo3::prelude::*;
use std::io;
use std::slice;

/// Print info about the Python buffer object
#[pyfunction]
fn buf_info(obj: &PyAny) -> PyResult<()> {
    if let Ok(a) = obj.extract::<PyBuffer<u8>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<u16>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<u32>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<u64>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<f64>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<i8>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<i16>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<i32>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<i64>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<f32>>() {
        buf_info_generic(a);
    } else if let Ok(a) = obj.extract::<PyBuffer<f64>>() {
        buf_info_generic(a);
    } else {
        return Err(PyTypeError::new_err("obj is not a supported buffer type"));
    }
    Ok(())
}

fn buf_info_generic<T: Element>(a: PyBuffer<T>) {
    let format: String = a.format().to_string_lossy().to_string();
    println!("Python buffer info visible from Rust:");
    println!("readonly: {}", a.readonly());
    println!("item_size: {}", a.item_size());
    println!("item_count: {}", a.item_count());
    println!("len_bytes: {}", a.len_bytes());
    println!("dimensions: {:?}", a.dimensions());
    println!("shape: {:?}", a.shape());
    println!("strides: {:?}", a.strides());
    println!("format: {:?}", format);
    println!("is_c_contiguous: {}", a.is_c_contiguous());
    println!("is_fortran_contiguous: {}", a.is_fortran_contiguous());
}

/// Return the sum of f64 numbers in the Python buffer object
#[pyfunction]
fn buf_f64_sum(py: Python<'_>, a: PyBuffer<f64>) -> PyResult<f64> {
    let a = a.as_slice(py).ok_or_else(|| {
        PyTypeError::new_err("buffer not compatible with slicing")
    })?;
    let mut sum = 0.0;
    for item in a.iter() {
        sum += item.get();
    }
    Ok(sum)
}

/// Hex dump contents of f64 buffer to stdout
#[pyfunction]
fn buf_f64_dump(py: Python<'_>, a: PyBuffer<f64>) -> PyResult<()> {
    let a = a.as_slice(py).ok_or_else(|| {
        PyTypeError::new_err("buffer not compatible with slicing")
    })?;
    hexdump::hexdump_slice(&a, &mut io::stdout())?;
    Ok(())
}

/// Sum f64 numbers in the buffer object using unsafe operations
#[pyfunction]
fn buf_f64_sum_scary(py: Python<'_>, buf: PyBuffer<f64>) -> PyResult<f64> {
    let array = arrayview_from_buf_f64(buf)?;
    // Let other Python threads run while we sum the numbers
    // UNSOUND? Python might let other threads modify the underlying buffer.
    let result = { py.allow_threads(move || array.sum()) };
    Ok(result)
}

/// Hex dump contents of f64 buffer using unsafe operations
#[pyfunction]
fn buf_f64_dump_scary(buf: PyBuffer<f64>) -> PyResult<()> {
    let array = arrayview_from_buf_f64(buf)?;

    // Consistency check on array
    println!("ndim: {:?}", array.ndim());
    println!("shape: {:?}", array.shape());
    println!("strides: {:?}", array.strides());
    println!("is_standard_layout: {}", array.is_standard_layout());
    // Unfortunately the is_contiguous() method is private.
    // println!("is_contiguous: {}", array.is_contiguous());

    if let Some(a) = array.as_slice_memory_order() {
        hexdump::hexdump_slice(&a, &mut io::stdout())?;
    } else {
        println!("as_slice_memory_order() returned None");
    }

    Ok(())
}

/// Add a scalar value to every element in the buffer of floats
#[pyfunction]
fn buf_f64_add_scalar(buf: PyBuffer<f64>, n: f64) -> PyResult<()> {
    let mut array = arrayview_mut_from_buf_f64(buf)?;
    for element in array.iter_mut() {
        *element += n;
    }
    Ok(())
}

type OneDimF64View<'a> =
    ndarray::ArrayBase<ndarray::ViewRepr<&'a f64>, ndarray::Dim<[usize; 1]>>;

/// Create an ndarray view using unsafe pointer operations.
fn arrayview_from_buf_f64<'a>(
    buf: PyBuffer<f64>,
) -> PyResult<OneDimF64View<'a>> {
    check_buf_f64_contiguous(&buf)?;

    let array = {
        let item_count = buf.item_count();
        let s = unsafe {
            slice::from_raw_parts(buf.buf_ptr() as *const f64, item_count)
        };
        let v = ndarray::ArrayView::from_shape((item_count,).strides((1,)), s);
        match v {
            Ok(a) => a,
            Err(err) => {
                return Err(PyValueError::new_err(format!(
                    "ArrayView from f64 buffer: {}",
                    err
                )));
            }
        }
    };

    Ok(array)
}

/// Create a one-dimensional mutable array from a buffer
fn arrayview_mut_from_buf_f64<'a>(
    buf: PyBuffer<f64>,
) -> PyResult<ndarray::ArrayViewMut1<'a, f64>> {
    check_buf_f64_contiguous(&buf)?;

    let array = {
        let s = unsafe {
            slice::from_raw_parts_mut(
                buf.buf_ptr() as *mut f64,
                buf.item_count(),
            )
        };
        let v = ndarray::ArrayViewMut1::from_shape(
            (buf.item_count(),).strides((1,)),
            s,
        );
        match v {
            Ok(a) => a,
            Err(err) => {
                return Err(PyValueError::new_err(format!(
                    "ArrayViewMut1 from f64 buffer: {}",
                    err
                )));
            }
        }
    };

    Ok(array)
}

fn check_buf_f64_contiguous(a: &PyBuffer<f64>) -> PyResult<()> {
    if !(a.is_c_contiguous() || a.is_fortran_contiguous()) {
        Err(PyTypeError::new_err("buffer is not contiguous"))
    } else {
        Ok(())
    }
}

/// Fun With Rust and numpy
#[pymodule]
fn _fwnumpy(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(buf_info, m)?).unwrap();
    m.add_function(wrap_pyfunction!(buf_f64_sum, m)?).unwrap();
    m.add_function(wrap_pyfunction!(buf_f64_dump, m)?).unwrap();
    m.add_function(wrap_pyfunction!(buf_f64_sum_scary, m)?)
        .unwrap();
    m.add_function(wrap_pyfunction!(buf_f64_dump_scary, m)?)
        .unwrap();
    m.add_function(wrap_pyfunction!(buf_f64_add_scalar, m)?)
        .unwrap();
    Ok(())
}
