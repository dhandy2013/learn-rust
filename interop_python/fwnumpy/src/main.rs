//! Examples of using Rust ndarray crate
use ndarray::prelude::*;

fn main() {
    let a = array![[1., 2., 3.], [4., 5., 6.]];
    println!("a = {:?}", a);

    let b = Array::linspace(0., 5., 4);
    println!("b = {:?}", b);
}
