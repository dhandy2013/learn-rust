// This is the old rust-cpython version of this library.
// It is no longer being compiled.
#[macro_use]
extern crate cpython;
use cpython::buffer::PyBuffer;
use cpython::{exc, PyErr, PyObject, PyResult, Python};
use hexdump;
use ndarray;
use ndarray::ShapeBuilder;
use std::fmt::Display;
use std::io;
use std::slice;

// The Python-importable name of this module is _fwnumpy
py_module_initializer!(_fwnumpy, init_fwnumpy, PyInit__fwnumpy, |py, m| {
    m.add(py, "__doc__", "Fun With Rust and numpy")?;
    m.add(py, "buf_info", py_fn!(py, buf_info(a: &PyObject)))?;
    m.add(py, "buf_f64_sum", py_fn!(py, buf_f64_sum(a: &PyObject)))?;
    m.add(py, "buf_f64_dump", py_fn!(py, buf_f64_dump(a: &PyObject)))?;
    m.add(
        py,
        "buf_f64_sum_scary",
        py_fn!(py, buf_f64_sum_scary(a: &PyObject)),
    )?;
    m.add(
        py,
        "buf_f64_dump_scary",
        py_fn!(py, buf_f64_dump_scary(a: &PyObject)),
    )?;
    m.add(
        py,
        "buf_f64_add_scalar",
        py_fn!(py, buf_f64_add_scalar(a: &PyObject, n: f64)),
    )?;
    Ok(())
});

/// Print info about the Python buffer object
fn buf_info(py: Python, a: &PyObject) -> PyResult<PyObject> {
    let a = PyBuffer::get(py, a)?;
    let format: String = a.format().to_string_lossy().to_string();
    println!("Python buffer info visible from Rust:");
    println!("readonly: {}", a.readonly());
    println!("item_size: {}", a.item_size());
    println!("item_count: {}", a.item_count());
    println!("len_bytes: {}", a.len_bytes());
    println!("dimensions: {:?}", a.dimensions());
    println!("shape: {:?}", a.shape());
    println!("strides: {:?}", a.strides());
    println!("format: {:?}", format);
    println!("is_c_contiguous: {}", a.is_c_contiguous());
    println!("is_fortran_contiguous: {}", a.is_fortran_contiguous());
    Ok(py.None())
}

/// Return the sum of f64 numbers in the Python buffer object
fn buf_f64_sum(py: Python, a: &PyObject) -> PyResult<f64> {
    let a = PyBuffer::get(py, a)?;
    check_buf_f64(py, &a)?;
    if let Some(a) = a.as_slice::<f64>(py) {
        let mut sum = 0.0;
        for item in a.iter() {
            sum += item.get();
        }
        return Ok(sum);
    } else {
        return Err(PyErr::new::<exc::Exception, _>(
            py,
            "Could not slice buffer",
        ));
    }
}

/// Convert a generic Result to a PyResult
fn to_pyresult<T, E: Display>(
    py: Python,
    operation: &str,
    result: Result<T, E>,
) -> PyResult<T> {
    match result {
        Ok(return_val) => PyResult::Ok(return_val),
        Err(err) => Err(PyErr::new::<exc::Exception, _>(
            py,
            format!("{}: {}", operation, err),
        )),
    }
}

/// Hex dump contents of f64 buffer to stdout
fn buf_f64_dump(py: Python, a: &PyObject) -> PyResult<PyObject> {
    let a = PyBuffer::get(py, a)?;
    check_buf_f64(py, &a)?;
    if let Some(a) = a.as_slice::<f64>(py) {
        to_pyresult(
            py,
            "hexdump_slice to stdout",
            hexdump::hexdump_slice(&a, &mut io::stdout()),
        )?;
        Ok(py.None())
    } else {
        Err(PyErr::new::<exc::Exception, _>(
            py,
            "Could not slice buffer",
        ))
    }
}

type OneDimF64View<'a> =
    ndarray::ArrayBase<ndarray::ViewRepr<&'a f64>, ndarray::Dim<[usize; 1]>>;

fn arrayview_from_buf_f64<'a>(
    py: Python,
    buf: &'a PyBuffer,
) -> PyResult<OneDimF64View<'a>> {
    check_buf_f64(py, &buf)?;

    let array = {
        let s = unsafe {
            slice::from_raw_parts(buf.buf_ptr() as *const f64, buf.item_count())
        };
        let v = ndarray::ArrayView::from_shape(
            (buf.item_count(),).strides((1,)),
            s,
        );
        match v {
            Ok(a) => a,
            Err(err) => {
                return Err(PyErr::new::<exc::Exception, _>(
                    py,
                    format!("ArrayView from f64 buffer: {}", err),
                ));
            }
        }
    };

    Ok(array)
}

/// Return the sum of f64 numbers in the buffer object using unsafe operations
fn buf_f64_sum_scary(py: Python, obj: &PyObject) -> PyResult<f64> {
    let buf = PyBuffer::get(py, obj)?;
    let array = arrayview_from_buf_f64(py, &buf)?;
    // Let other Python threads run while we sum the numbers
    let result = { py.allow_threads(move || array.sum()) };
    Ok(result)
}

/// Hex dump contents of f64 buffer using unsafe operations
fn buf_f64_dump_scary(py: Python, obj: &PyObject) -> PyResult<PyObject> {
    let buf = PyBuffer::get(py, obj)?;
    let array = arrayview_from_buf_f64(py, &buf)?;

    // Consistency check on array
    println!("ndim: {:?}", array.ndim());
    println!("shape: {:?}", array.shape());
    println!("strides: {:?}", array.strides());
    println!("is_standard_layout: {}", array.is_standard_layout());
    // Unfortunately the is_contiguous() method is private.
    // println!("is_contiguous: {}", array.is_contiguous());

    if let Some(a) = array.as_slice_memory_order() {
        to_pyresult(
            py,
            "hexdump_slice to stdout",
            hexdump::hexdump_slice(&a, &mut io::stdout()),
        )?;
    } else {
        println!("as_slice_memory_order() returned None");
    }

    Ok(py.None())
}

/// Create a one-dimensional mutable array from a buffer
fn arrayview_mut_from_buf_f64<'a>(
    py: Python,
    buf: &'a mut PyBuffer,
) -> PyResult<ndarray::ArrayViewMut1<'a, f64>> {
    check_buf_f64(py, &buf)?;

    let array = {
        let s = unsafe {
            slice::from_raw_parts_mut(
                buf.buf_ptr() as *mut f64,
                buf.item_count(),
            )
        };
        let v = ndarray::ArrayViewMut1::from_shape(
            (buf.item_count(),).strides((1,)),
            s,
        );
        match v {
            Ok(a) => a,
            Err(err) => {
                return Err(PyErr::new::<exc::Exception, _>(
                    py,
                    format!("ArrayViewMut1 from f64 buffer: {}", err),
                ));
            }
        }
    };

    Ok(array)
}

fn buf_f64_add_scalar(
    py: Python,
    obj: &PyObject,
    n: f64,
) -> PyResult<PyObject> {
    let mut buf = PyBuffer::get(py, obj)?;
    let mut array = arrayview_mut_from_buf_f64(py, &mut buf)?;
    for element in array.iter_mut() {
        *element += n;
    }
    Ok(py.None())
}

/// Verify that the PyBuffer reference contains f64 values
fn check_buf_f64(py: Python, a: &PyBuffer) -> PyResult<()> {
    check_buf(py, &a, "d", 8)
}

/// Verify that the PyBuffer contains items of the expected format and size.
fn check_buf(
    py: Python,
    a: &PyBuffer,
    expected_format: &str,
    expected_item_size: usize,
) -> PyResult<()> {
    let format: String = a.format().to_string_lossy().to_string();
    if format != expected_format {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Need buffer of format '{}' not '{}'",
                expected_format, format
            ),
        ));
    }
    if a.item_size() != expected_item_size {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Need buffer of item size {} not {}",
                expected_item_size,
                a.item_size()
            ),
        ));
    }
    if !(a.is_c_contiguous() || a.is_fortran_contiguous()) {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            "Need contiguous buffer",
        ));
    }
    if a.shape().len() != a.strides().len() {
        return Err(PyErr::new::<exc::TypeError, _>(
            py,
            format!(
                "Length of shape {:?} doesn't match length of strides {:?}",
                a.shape(),
                a.strides()
            ),
        ));
    }
    Ok(())
}
