import sys

import numpy as np

from . import _fwnumpy


def main():
    a = np.arange(1.0, 10.0 + 1.0)
    print(a)
    _fwnumpy.buf_info(a)

    print()
    print("buf_f64_sum(a):", _fwnumpy.buf_f64_sum(a))
    print("buf_f64_dump(a):")
    _fwnumpy.buf_f64_dump(a)

    print()
    print("buf_f64_sum_scary(a):", _fwnumpy.buf_f64_sum_scary(a))
    print("buf_f64_dump_scary(a):")
    _fwnumpy.buf_f64_dump_scary(a)

    print()
    n = 7.5
    print(f"Adding {n} to array a")
    _fwnumpy.buf_f64_add_scalar(a, n)
    print(f"result: {a}")


if __name__ == '__main__':
    sys.exit(main())
