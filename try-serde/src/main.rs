//! Rust JSON demo/utility
//! Read JSON from a file or from stdin, transform it, then write to a file or
//! to stdout. To enable logging, set this environment variable: RUST_LOG=info
use log::info;
use serde::{Serialize, Deserialize};
use serde_json::Value;
use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use structopt::StructOpt;

#[derive(StructOpt)]
struct Cli {
    #[structopt(parse(from_os_str))]
    input_filename: std::path::PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let args = Cli::from_args();
    let json: Value = {
        let reader: Box<dyn Read> = {
            if let Some("-") = args.input_filename.to_str() {
                info!("Reading JSON from standard input");
                Box::new(std::io::stdin())
            } else {
                info!("Reading JSON from file: {:?}", args.input_filename);
                Box::new(File::open(args.input_filename)?)
            }
        };
        serde_json::from_reader(BufReader::new(reader))?
    };
    {
        let mut writer = BufWriter::new(std::io::stdout());
        serde_json::to_writer_pretty(std::io::stdout(), &json)?;
        writeln!(&mut writer, "")?;
    }
    Ok(())
}

use std::f64::consts::PI;

#[derive(Serialize, Deserialize, Debug)]
struct Point {
    x: f64,
    y: f64,
    z: f64,
}

#[allow(dead_code)]
fn try_json_from_str() {
    let point = Point { x: 1.0, y: 2.0, z: PI };
    let serialized = serde_json::to_string(&point).unwrap();
    println!("serialized = {}", serialized);
    let deserialized: Point = serde_json::from_str(&serialized).unwrap();
    println!("deserialized = {:?}", deserialized);
}
