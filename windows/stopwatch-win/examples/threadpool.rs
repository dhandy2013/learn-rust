//! Example of using the Windows threadpool API
//!
//! See:
//! https://kennykerr.ca/rust-getting-started/calling-your-first-windows-api.html
use windows::{
    core::Result, Win32::Foundation::WIN32_ERROR, Win32::System::Threading::*,
};

static COUNTER: std::sync::RwLock<i32> = std::sync::RwLock::new(0);

fn main() -> Result<()> {
    println!("Hello, world!");

    unsafe {
        let work = CreateThreadpoolWork(Some(callback), None, None)?;

        extern "system" fn callback(
            _: PTP_CALLBACK_INSTANCE,
            _: *mut std::ffi::c_void,
            _: PTP_WORK,
        ) {
            let mut counter = COUNTER.write().unwrap();
            *counter += 1;
        }

        for _ in 0..10 {
            SubmitThreadpoolWork(work);
        }

        WaitForThreadpoolWorkCallbacks(work, false);
        CloseThreadpoolWork(work);
    }

    let counter = COUNTER.read().map_err(|_err| WIN32_ERROR(1))?;
    println!("counter: {}", *counter);
    Ok(())
}
