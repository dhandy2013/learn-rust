//! Read input in raw mode and dump input bytes in hex mode with timing
//!
//! Comprehensive list of terminal codes:
//! https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
//!
//! CSI: ESC [
//!
//! Mouse button press: CSI < button-value; Px; Py; M
//! Mouse button release: CSI < button-value; Px; Py; m
//! Mouse coordinates are character cell numbers with (1,1) being upper-left.
//!
//! button-value    meaning
//! ------------    ----------------------------------------------------------
//! 0               left button (MB1 in the xterm doc), no modifiers
//! 2               right button (MB3 in the xterm doc), no modifiers
//! 8               Alt + left button (== 0 + 8)
//! 10              Alt + right button (== 2 + 8)
//! 16              Ctrl + left button (== 0 + 16)
//! 18              Ctrl + right button (== 2 + 16)
//! 32              mouse drag with left button pressed (button press message)
//! 34              mouse drag with right button pressed (button press message)
//! 35              mouse move with no button pressed (button release message)
use std::{error::Error, io::Write, time::Instant};
use stopwatch_win::{
    format_elapsed_ms_bytes, format_hex_ascii_line, string_to_error,
    term::RawTerminal,
};
use windows::Win32::{
    Foundation::{WAIT_OBJECT_0, WAIT_TIMEOUT},
    System::Threading::WaitForSingleObject,
};

fn main() -> Result<(), Box<dyn Error>> {
    println!("Press keys on the keyboard to see their hex and ASCII bytes.");
    println!("Press Ctrl-C or Ctrl-D to exit.");

    let mut term = RawTerminal::new()?;

    let mut inbuf: [u8; 16] = [0; 16];
    let mut outbuf: Vec<u8> = Vec::with_capacity(82);
    let mut stop_flag = false;
    let timeout_ms = 2500;
    let start_time = Instant::now();
    while !(stop_flag && outbuf.is_empty()) {
        // Wait for input
        let result =
            unsafe { WaitForSingleObject(term.stdin_handle(), timeout_ms) };
        match result {
            WAIT_OBJECT_0 => {
                // Ok, proceed to read bytes
            }
            WAIT_TIMEOUT => {
                // Try again
                continue;
            }
            other => {
                Err(string_to_error(format!(
                    "WaitForSingleObject: unexpected result: {other:?}"
                )))?;
            }
        }
        // Read the current input bytes
        let n = term.read_bytes(&mut inbuf)?;
        let t = start_time.elapsed();
        if !stop_flag && (n > 0 || term.size_changed()) {
            // Trace the elapsed time since the input loop began
            let elapsed_ms = t.as_millis() as u32;
            format_elapsed_ms_bytes(elapsed_ms, &mut outbuf);
            if n > 0 {
                outbuf.extend(b" ");
                format_hex_ascii_line(&inbuf[..n], &mut outbuf)?;
            }
            if term.size_changed() {
                let (width, height) = term.console_size();
                let _ = write!(&mut outbuf, " size {width}x{height}");
            }
            outbuf.extend(b"\r\n");
            // Check for Ctrl-C (code 3) or Ctrl-D (code 4)
            if inbuf[..n].iter().any(|&byte| byte == 3 || byte == 4) {
                stop_flag = true;
            }
        }
        if !outbuf.is_empty() {
            // Process output
            let n = term.write_bytes(&outbuf)?;
            // Remove from output buffer bytes successfully written
            outbuf.copy_within(n.., 0);
            outbuf.truncate(outbuf.len() - n);
        }
    }
    drop(term);

    println!("Done.");
    Ok(())
}
