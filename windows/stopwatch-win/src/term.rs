//! Windows terminal handling
//!
//! References:
//! https://learn.microsoft.com/en-us/windows/console/getconsolemode
//! https://learn.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences
use std::io;
use windows::{
    core::{s, Error, Result},
    Win32::{
        Foundation::{CloseHandle, HANDLE},
        Globalization::{GetACP, GetCPInfoExA, CPINFOEXA, CP_UTF8},
        Security::SECURITY_ATTRIBUTES,
        Storage::FileSystem::{
            AreFileApisANSI, CreateFileA, SetFileApisToANSI, SetFileApisToOEM,
            WriteFile, FILE_FLAGS_AND_ATTRIBUTES, FILE_GENERIC_READ,
            FILE_GENERIC_WRITE, FILE_SHARE_READ, FILE_SHARE_WRITE,
            OPEN_EXISTING,
        },
        System::Console::*,
    },
};

pub struct RawTerminal {
    stdin_handle: HANDLE,
    stdout_handle: HANDLE,
    saved_stdin_mode: CONSOLE_MODE,
    saved_stdout_mode: CONSOLE_MODE,
    saved_input_cp: u32,
    saved_output_cp: u32,
    saved_file_apis_use_ansi: bool,
    console_size: (i16, i16),
    size_changed: bool,
}

impl RawTerminal {
    /// Put the terminal in "raw" non-echo mode and return an object that can
    /// be used to control the terminal. Only create one instance of this
    /// struct in a process at a time.
    ///
    /// Save the current console input and output code pages, then set them to
    /// UTF-8. Also tell Windows to use the OEM code page for file APIs, so that
    /// e.g. CreateFileA takes a UTF-8 filename.
    ///
    /// When this object is dropped, the terminal and code page settings are
    /// restored to what they were when this construtor was called.
    pub fn new() -> io::Result<Self> {
        // Opening CONIN$ guarantees direct access to console input even if
        // stdin had been redirected.
        let stdin_handle = unsafe {
            let desiredaccess = FILE_GENERIC_READ | FILE_GENERIC_WRITE;
            let sharemode = FILE_SHARE_READ;
            let securityattributes: Option<*const SECURITY_ATTRIBUTES> = None;
            let creationdisposition = OPEN_EXISTING;
            let flagsandattributes = FILE_FLAGS_AND_ATTRIBUTES(0); // ignored
            let templatefile = HANDLE(0); // ignored
            CreateFileA(
                s!("CONIN$"),
                desiredaccess.0,
                sharemode,
                securityattributes,
                creationdisposition,
                flagsandattributes,
                templatefile,
            )
            .map_err(|err| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("Opening CONIN$: {err}"),
                )
            })?
        };
        // Opening CONOUT$ guarantees direct access to console output even if
        // stdout had been redirected.
        let stdout_handle = unsafe {
            let desiredaccess = FILE_GENERIC_READ | FILE_GENERIC_WRITE;
            let sharemode = FILE_SHARE_WRITE;
            let securityattributes: Option<*const SECURITY_ATTRIBUTES> = None;
            let creationdisposition = OPEN_EXISTING;
            let flagsandattributes = FILE_FLAGS_AND_ATTRIBUTES(0); // ignored
            let templatefile = HANDLE(0); // ignored
            CreateFileA(
                s!("CONOUT$"),
                desiredaccess.0,
                sharemode,
                securityattributes,
                creationdisposition,
                flagsandattributes,
                templatefile,
            )
            .map_err(|err| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("Opening CONOUT$: {err}"),
                )
            })?
        };
        let saved_stdin_mode = get_console_mode(stdin_handle)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
        let saved_stdout_mode = get_console_mode(stdout_handle)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
        // Set and clear the desired input mode flags
        let mut new_stdin_mode = saved_stdin_mode.clone();
        new_stdin_mode &= !(ENABLE_ECHO_INPUT
            | ENABLE_INSERT_MODE
            | ENABLE_LINE_INPUT
            | ENABLE_PROCESSED_INPUT
            | ENABLE_QUICK_EDIT_MODE);
        new_stdin_mode |= ENABLE_VIRTUAL_TERMINAL_INPUT
            | ENABLE_MOUSE_INPUT
            | ENABLE_WINDOW_INPUT;
        let _ = unsafe { SetConsoleMode(stdin_handle, new_stdin_mode) };
        // Set the desired output mode flags
        let mut new_stdout_mode = saved_stdout_mode.clone();
        new_stdout_mode |= ENABLE_PROCESSED_OUTPUT
            | ENABLE_WRAP_AT_EOL_OUTPUT
            | ENABLE_VIRTUAL_TERMINAL_PROCESSING
            | DISABLE_NEWLINE_AUTO_RETURN;
        let _ = unsafe { SetConsoleMode(stdout_handle, new_stdout_mode) };
        let saved_input_cp = get_console_cp().unwrap_or(0);
        let saved_output_cp = get_console_output_cp().unwrap_or(0);
        let _ = set_console_cp(CP_UTF8);
        let _ = set_console_output_cp(CP_UTF8);
        let saved_file_apis_use_ansi = are_file_apis_ansi();
        if saved_file_apis_use_ansi {
            set_file_apis_to_oem();
        }
        // Do an initial zero-length read to flush unwanted input events
        let mut inbuf: [u8; 0] = [0; 0];
        let mut console_size_maybe: Option<(i16, i16)> = None;
        read_console_input(stdin_handle, &mut inbuf, &mut console_size_maybe)?;
        // Get the console size
        let console_size = match console_size_maybe {
            Some(console_size) => console_size,
            None => get_console_size(stdout_handle)?,
        };
        Ok(Self {
            stdin_handle,
            stdout_handle,
            saved_stdin_mode,
            saved_stdout_mode,
            saved_input_cp,
            saved_output_cp,
            saved_file_apis_use_ansi,
            console_size,
            size_changed: false,
        })
    }

    pub fn stdin_handle(&self) -> HANDLE {
        self.stdin_handle
    }

    pub fn stdout_handle(&self) -> HANDLE {
        self.stdout_handle
    }

    /// Return console (width, height) in character cells.
    /// The size was read when this object was first created, and is updated
    /// when console input events are processed by the `read_bytes` method.
    pub fn console_size(&self) -> (i16, i16) {
        self.console_size
    }

    /// Return the `size_changed` flag - true iff the console size changed
    /// during the most recent call to `read_bytes`.
    pub fn size_changed(&self) -> bool {
        self.size_changed
    }

    /// Clear the `sized_changed` flag and read bytes from console input into
    /// `buf`.  Return the number of bytes read, or an io::Error.
    /// If a WINDOW_BUFFER_SIZE_EVENT console input event is processed, the
    /// `console_size` field is updated and `size_changed` is set.
    ///
    /// If a wait function reported that there is input available on
    /// `stdin_handle`, this function could still return zero bytes for many
    /// reasons including:
    /// - the key pressed was Ctrl or Alt by itself and therefore unreadable
    /// - the key event was a key up event not a key down event
    /// - not a key event but a window size change event, a focus event, etc.
    ///   The `size_changed` flag may have been set.
    pub fn read_bytes(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut console_size_maybe: Option<(i16, i16)> = None;
        self.size_changed = false;
        let num_bytes_read = read_console_input(
            self.stdin_handle,
            buf,
            &mut console_size_maybe,
        )?;
        if let Some(new_console_size) = console_size_maybe {
            self.console_size = new_console_size;
            self.size_changed = true;
        }
        Ok(num_bytes_read)
    }

    /// Write the bytes in `buf` to stdout.
    /// Return the number of bytes written, or an io::Error.
    pub fn write_bytes(&self, buf: &[u8]) -> io::Result<usize> {
        let mut num_bytes_written: u32 = 0;
        if let Err(err) = unsafe {
            WriteFile(
                self.stdout_handle,
                Some(buf),
                Some(&mut num_bytes_written),
                None,
            )
        } {
            return Err(io::Error::new(io::ErrorKind::Other, err));
        }
        Ok(num_bytes_written as usize)
    }
}

impl Drop for RawTerminal {
    fn drop(&mut self) {
        // Restore the original console settings
        if let Err(err) =
            unsafe { SetConsoleMode(self.stdin_handle, self.saved_stdin_mode) }
        {
            eprintln!("Error restoring stdin console mode: {err}");
        }
        let _ = unsafe { CloseHandle(self.stdin_handle) };
        if let Err(err) = unsafe {
            SetConsoleMode(self.stdout_handle, self.saved_stdout_mode)
        } {
            eprintln!("Error restoring stdout console mode: {err}");
        }
        let _ = unsafe { CloseHandle(self.stdout_handle) };
        if self.saved_input_cp != 0 {
            if let Err(err) = set_console_cp(self.saved_input_cp) {
                eprintln!("Error restoring input codepage: {err}");
            }
        }
        if self.saved_output_cp != 0 {
            if let Err(err) = set_console_output_cp(self.saved_output_cp) {
                eprintln!("Error restoring output codepage: {err}");
            }
        }
        if self.saved_file_apis_use_ansi {
            set_file_apis_to_ansi();
        }
    }
}

/// Return (stdin_handle, stdout_handle)
pub fn get_console_stdin_stdout() -> Result<(HANDLE, HANDLE)> {
    let stdin_handle: HANDLE;
    let stdout_handle: HANDLE;
    unsafe {
        stdin_handle = GetStdHandle(STD_INPUT_HANDLE)?;
        stdout_handle = GetStdHandle(STD_OUTPUT_HANDLE)?;
    }
    Ok((stdin_handle, stdout_handle))
}

pub fn get_console_mode(console_handle: HANDLE) -> Result<CONSOLE_MODE> {
    let mut console_mode = CONSOLE_MODE::default();
    unsafe {
        GetConsoleMode(console_handle, &mut console_mode)?;
    }
    Ok(console_mode)
}

pub fn print_console_input_mode(console_mode: CONSOLE_MODE) {
    println!("Console input mode: {console_mode:?}");
    println!(
        "  ENABLE_ECHO_INPUT: {}",
        console_mode.contains(ENABLE_ECHO_INPUT)
    );
    println!(
        "  ENABLE_INSERT_MODE: {}",
        (console_mode & ENABLE_INSERT_MODE).0 != 0
    );
    println!(
        "  ENABLE_LINE_INPUT: {}",
        (console_mode & ENABLE_LINE_INPUT).0 != 0
    );
    println!(
        "  ENABLE_MOUSE_INPUT: {}",
        (console_mode & ENABLE_MOUSE_INPUT).0 != 0
    );
    println!(
        "  ENABLE_PROCESSED_INPUT: {}",
        (console_mode & ENABLE_PROCESSED_INPUT).0 != 0
    );
    println!(
        "  ENABLE_QUICK_EDIT_MODE: {}",
        (console_mode & ENABLE_QUICK_EDIT_MODE).0 != 0
    );
    println!(
        "  ENABLE_WINDOW_INPUT: {}",
        (console_mode & ENABLE_WINDOW_INPUT).0 != 0
    );
    println!(
        "  ENABLE_VIRTUAL_TERMINAL_INPUT: {}",
        (console_mode & ENABLE_VIRTUAL_TERMINAL_INPUT).0 != 0
    );
}

pub fn print_console_output_mode(console_mode: CONSOLE_MODE) {
    println!("Console output mode: {console_mode:?}");
    println!(
        "  ENABLE_PROCESSED_OUTPUT: {}",
        (console_mode & ENABLE_PROCESSED_OUTPUT).0 != 0
    );
    println!(
        "  ENABLE_WRAP_AT_EOL_OUTPUT: {}",
        (console_mode & ENABLE_WRAP_AT_EOL_OUTPUT).0 != 0
    );
    println!(
        "  ENABLE_VIRTUAL_TERMINAL_PROCESSING: {}",
        (console_mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING).0 != 0
    );
    println!(
        "  DISABLE_NEWLINE_AUTO_RETURN: {}",
        (console_mode & DISABLE_NEWLINE_AUTO_RETURN).0 != 0
    );
    println!(
        "  ENABLE_LVB_GRID_WORLDWIDE: {}",
        (console_mode & ENABLE_LVB_GRID_WORLDWIDE).0 != 0
    );
}

/// Get the input code page used by the console associated with the calling
/// process.
pub fn get_console_cp() -> Result<u32> {
    let code_page = unsafe { GetConsoleCP() };
    if code_page == 0 {
        Err(Error::from_win32())
    } else {
        Ok(code_page)
    }
}

/// Sets the input code page used by the console associated with the calling
/// process. Use code page 65001 for UTF-8.
pub fn set_console_cp(code_page: u32) -> Result<()> {
    unsafe { SetConsoleCP(code_page) }
}

/// Get the output code page used by the console associated with the calling
/// process.
pub fn get_console_output_cp() -> Result<u32> {
    let code_page = unsafe { GetConsoleOutputCP() };
    if code_page == 0 {
        Err(Error::from_win32())
    } else {
        Ok(code_page)
    }
}

/// Sets the output code page used by the console associated with the calling
/// process. Use code page 65001 for UTF-8.
pub fn set_console_output_cp(code_page: u32) -> Result<()> {
    unsafe { SetConsoleOutputCP(code_page) }
}

/// Get the string name of an integer code page returned by `get_console_cp`
/// or `get_console_output_cp`.
pub fn get_cp_name(code_page: u32) -> Result<String> {
    let mut cp_info = CPINFOEXA::default();
    unsafe {
        GetCPInfoExA(code_page, 0, &mut cp_info)?;
    }
    let cp_name = &cp_info.CodePageName[..];
    // Find index of first zero byte in codepage name
    let name_end_index = cp_name
        .iter()
        .position(|&byte| byte == 0)
        .unwrap_or(cp_name.len());
    let cp_name_trimmed = &cp_name[..name_end_index];
    Ok(String::from_utf8_lossy(cp_name_trimmed).to_string())
}

pub fn print_console_settings() -> Result<()> {
    let (stdin_handle, stdout_handle) = get_console_stdin_stdout()?;
    let stdin_mode = get_console_mode(stdin_handle)?;
    let stdout_mode = get_console_mode(stdout_handle)?;
    print_console_input_mode(stdin_mode);
    print_console_output_mode(stdout_mode);
    let input_cp = get_console_cp()?;
    println!("Input code page: {}", get_cp_name(input_cp)?);
    let output_cp = get_console_output_cp()?;
    println!("Output code page: {}", get_cp_name(output_cp)?);
    println!("Windows system ANSI code page: {}", get_cp_name(get_acp())?);
    println!("File APIs use ANSI code page? {}", are_file_apis_ansi());
    Ok(())
}

/// Retrieves the current Windows ANSI code page identifier for the operating
/// system.
///
/// A "code page" is a scheme for translating between unicode characters and
/// bytes. Windows has two different code pages active at any given time:
/// The ANSI code page and the OEM code page. The console APIs use the OEM
/// code page and the file and most other APIs use the ANSI code page.
///
/// References:
/// https://learn.microsoft.com/en-us/windows/win32/api/winnls/nf-winnls-getacp
/// https://learn.microsoft.com/en-us/windows/console/console-application-issues
pub fn get_acp() -> u32 {
    unsafe { GetACP() }
}

/// Determines whether the file I/O functions are using the ANSI or OEM
/// character set code page. This function is useful for 8-bit console input and
/// output operations.
///
/// I believe in this context "OEM codepage" means the current process console
/// codepage that is retrieved by GetConsoleCP and GetConsoleOutputCP and is
/// set by SetConsoleCP and SetConsoleOutputCP.
///
/// References:
/// https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-arefileapisansi
/// https://learn.microsoft.com/en-us/windows/console/console-application-issues
pub fn are_file_apis_ansi() -> bool {
    unsafe { AreFileApisANSI().as_bool() }
}

/// Causes the file I/O functions for the process to use the OEM character set
/// code page. This function is useful for 8-bit console input and output
/// operations.
///
/// References:
/// https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-setfileapistooem
/// https://learn.microsoft.com/en-us/windows/console/console-application-issues
pub fn set_file_apis_to_oem() {
    unsafe { SetFileApisToOEM() }
}

/// Causes the file I/O functions to use the ANSI character set code page for
/// the current process. This function is useful for 8-bit console input and
/// output operations.
///
/// References:
/// https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-setfileapistoansi
/// https://learn.microsoft.com/en-us/windows/console/console-application-issues
pub fn set_file_apis_to_ansi() {
    unsafe { SetFileApisToANSI() }
}

/// Read input bytes from a console into an input buffer.
/// If console size change events are read, update the console size.
/// Return the number of bytes read, or an error.
///
/// If a wait function reported that there is input available on
/// `stdin_handle`, this function could still return zero bytes for many
/// reasons including:
/// - the key pressed was Ctrl or Alt by itself and therefore unreadable
/// - the key event was a key up event not a key down event
/// - not a key event but a window size change event, a focus event, etc.
///
/// This function will read all available console input events until the input
/// buffer is full, and then it will continue to read and process any remaining
/// events that are not readable key down events. This function will never
/// block.  Calling this function with an empty input buffer will read and
/// process all available events up to the first readable key down event.
fn read_console_input(
    stdin_handle: HANDLE,
    inbuf: &mut [u8],
    console_size: &mut Option<(i16, i16)>,
) -> io::Result<usize> {
    let mut num_events: u32 = 0;
    unsafe {
        GetNumberOfConsoleInputEvents(stdin_handle, &mut num_events)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    }
    let mut records = [INPUT_RECORD::default()]; // exactly 1 record
    let mut num_records_read: u32 = 0;
    let mut num_bytes_read: usize = 0;
    for _ in 0..num_events {
        let peeking_at_input_events = unsafe {
            if num_bytes_read >= inbuf.len() {
                PeekConsoleInputA(
                    stdin_handle,
                    &mut records,
                    &mut num_records_read,
                )
                .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
                true
            } else {
                ReadConsoleInputA(
                    stdin_handle,
                    &mut records,
                    &mut num_records_read,
                )
                .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
                false
            }
        };
        if num_records_read < 1 {
            // For some reason GetNumberOfConsoleInputEvents reported
            // more records than were actually available. Just stop reading
            // records.
            break;
        }
        let record = &records[0];
        match record.EventType as u32 {
            KEY_EVENT => {
                // SAFETY: We ensured the event type is a KEY_EVENT.
                let key_event = unsafe { record.Event.KeyEvent };
                // Only read key down events, key up events are duplicates
                if key_event.bKeyDown.as_bool() {
                    // SAFETY: ReadConsoleInputA returns bytes (AKA AsciiChar)
                    let input_byte: u8 = unsafe { key_event.uChar.AsciiChar };
                    let control_state = key_event.dwControlKeyState;
                    if input_byte == 0 && control_state != 0 {
                        // This is a non-readable modifier key (Ctrl, Alt, etc)
                        continue;
                    }
                    if num_bytes_read >= inbuf.len() {
                        // Buffer is full, we should be peeking not reading
                        assert!(peeking_at_input_events);
                        // Stop here, leave this key down event in the queue
                        break;
                    }
                    // Room in the buffer, we should be reading not peeking
                    assert!(!peeking_at_input_events);
                    inbuf[num_bytes_read] = input_byte;
                    num_bytes_read += 1;
                }
            }
            WINDOW_BUFFER_SIZE_EVENT => {
                // SAFETY: We ensured the event type is a buffer size event
                let buffer_size_event =
                    unsafe { record.Event.WindowBufferSizeEvent };
                let buffer_size = buffer_size_event.dwSize;
                *console_size = Some((buffer_size.X, buffer_size.Y));
            }
            _ => {
                // Discard all other event types
            }
        }
        if peeking_at_input_events {
            // Remove peek'ed record from the queue
            unsafe {
                ReadConsoleInputA(
                    stdin_handle,
                    &mut records,
                    &mut num_records_read,
                )
                .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
            }
        }
    }
    Ok(num_bytes_read)
}

/// Return console (width, height) in character cells or an error.
fn get_console_size(stdout_handle: HANDLE) -> io::Result<(i16, i16)> {
    let mut csbi = CONSOLE_SCREEN_BUFFER_INFO::default();
    unsafe {
        GetConsoleScreenBufferInfo(stdout_handle, &mut csbi).map_err(|err| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("GetConsoleScreenBufferInfo: {err}"),
            )
        })?
    }
    let width = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    let height = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
    Ok((width, height))
}
