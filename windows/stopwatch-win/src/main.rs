//! Stopwatch Program - Windows version
//! This is exprimental code researching how to get asynchronouse console input
//! and output on Windows without doing blocking I/O in threads.
//!
//! StackOverflow article listing lots of problems with asynchronous console
//! input on Windows:
//! https://stackoverflow.com/questions/19955617/win32-read-from-stdin-with-timeout
//!
//! Experience report:
//!
//! `WaitForSingleObject` on the console input handle -
//! At first, `WaitForSingleObject` returns immediately,
//! but then a call to `ReadConsole` or `ReadFile` blocks waiting for input.
//! This behavior continues as long as only keyboard input comes in.
//! If I click more move the mouse in the console, `WaitForSingleObject`
//! starts working correctly and times out when no input comes in. If I
//! press a key, then it goes back to the original behavior of blocking
//! `ReadConsole` or `ReadFile`, until the next time I click or move the mouse.
//!
//! Getting this to work the way I want required use of:
//! - `GetNumberOfConsoleInputEvents`
//! - `PeekConsoleInput`
//! - `ReadConsoleInput` (different from `ReadConsole`)
use std::io;
use stopwatch_win::{string_to_error, term::RawTerminal};
use windows::{
    core::Result,
    Win32::Foundation::{WAIT_OBJECT_0, WAIT_TIMEOUT},
    Win32::System::{
        Console::{PeekConsoleInputA, INPUT_RECORD},
        Threading::WaitForSingleObject,
    },
};

fn main() -> Result<()> {
    println!("Creating raw terminal!");
    let mut term = RawTerminal::new().unwrap();
    trace_console_input(&mut term).unwrap();
    drop(term);
    Ok(())
}

/// Trace console input behavior
///
/// In a loop:
/// - WaitForSingleObject on the console input handle
/// - PeekConsoleInputA and trace console input events
/// - RawTerminal.read_bytes and trace bytes read
/// - Trace timeouts and errors if any
/// - Quit on Ctrl-C (input byte 0x03)
fn trace_console_input(term: &mut RawTerminal) -> io::Result<()> {
    let mut inbuf: [u8; 16] = [0; 16];
    let mut input_records: Vec<INPUT_RECORD> = Vec::with_capacity(16);
    for _ in 0..input_records.capacity() {
        input_records.push(INPUT_RECORD::default());
    }
    let _ = term.write_bytes(
        "Here is a Christmas tree to demonstrate UTF-8: 🎄\r\n".as_bytes(),
    );
    let _ = term.write_bytes(
        "Press keys or move the mouse. Press Ctrl-C to exit.\r\n".as_bytes(),
    );
    let timeout_ms = 2500;
    loop {
        let result =
            unsafe { WaitForSingleObject(term.stdin_handle(), timeout_ms) };
        match result {
            WAIT_OBJECT_0 => {
                let mut num_events: u32 = 0;
                unsafe {
                    PeekConsoleInputA(
                        term.stdin_handle(),
                        &mut input_records[..],
                        &mut num_events,
                    )?;
                }
                let _ = term.write_bytes(
                    format!("PeekConsoleInputA: {num_events} events\r\n")
                        .as_bytes(),
                );
                let input_record_slice =
                    &input_records[..(num_events as usize)];
                for record in input_record_slice.iter() {
                    let event_type = event_type_string(record.EventType);
                    let event_info = key_event_string(record);
                    let _ = term.write_bytes(
                        format!("INPUT_RECORD: {event_type} {event_info}\r\n")
                            .as_bytes(),
                    );
                }
                let n = term.read_bytes(&mut inbuf)?;
                let _ = term.write_bytes(
                    format!("Read {n} bytes: {:?}\r\n", &inbuf[..n]).as_bytes(),
                );
                if inbuf[..n].iter().any(|&byte| byte == 3) {
                    break Ok(());
                }
            }
            WAIT_TIMEOUT => {
                let _ = term.write_bytes(
                    format!("WAIT_TIMEOUT after {timeout_ms}ms\r\n").as_bytes(),
                );
            }
            other => {
                break Err(string_to_error(format!(
                    "WaitForSingleObject: unexpected result: {other:?}"
                )));
            }
        }
    }
}

fn event_type_string(input_record_type: u16) -> String {
    match input_record_type {
        0x0010 => "FOCUS_EVENT".to_string(),
        0x0001 => "KEY_EVENT".to_string(),
        0x0008 => "MENU_EVENT".to_string(),
        0x0002 => "MOUSE_EVENT".to_string(),
        0x0004 => "WINDOW_BUFFER_SIZE_EVENT".to_string(),
        other => format!("Uknown event type: 0x{other:04x}"),
    }
}

fn key_event_string(record: &INPUT_RECORD) -> String {
    if record.EventType != 1 {
        // Not a KEY_EVENT
        return String::new();
    }
    // SAFETY: We ensured above that the event type is a KEY_EVENT.
    let key_event = unsafe { record.Event.KeyEvent };
    // SAFETY: RawTerminal put the console in "ANSI" mode so we expect bytes
    let key_char: u8 = unsafe { record.Event.KeyEvent.uChar.AsciiChar };
    format!(
        "Down {0}, KeyCode {1:04x}, ScanCode {2:04x}, Char {3:02x}, ControlKeyState {4:08x}",
        key_event.bKeyDown.as_bool(),
        key_event.wVirtualKeyCode,
        key_event.wVirtualScanCode,
        key_char,
        key_event.dwControlKeyState
    )
}
