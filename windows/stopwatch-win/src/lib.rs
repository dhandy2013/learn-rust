//! Windows support functions for terminal stopwatch programs
use std::io::{self, Write as _};

pub mod term;

/// Given a number of elapsed milliseconds, append it to a byte vector as ASCII
/// characters in the format HH:MM:SS.mmm. A 32-bit unsigned integer can hold
/// enough milliseconds for 49.7 days, which is enough.
pub fn format_elapsed_ms_bytes(elapsed_ms: u32, out: &mut Vec<u8>) {
    let millis = elapsed_ms % 1000;
    let total_seconds = elapsed_ms / 1000;
    let seconds = total_seconds % 60;
    let total_minutes = total_seconds / 60;
    let minutes = total_minutes % 60;
    let total_hours = total_minutes / 60;
    let hours = total_hours % 60;
    let _ = write!(
        out,
        "{hours:02}:{minutes:02}:{seconds:02}.{millis:03}",
        hours = hours,
        minutes = minutes,
        seconds = seconds,
        millis = millis
    );
}

/// Write a single line of a dump of bytes in hex and ASCII format.
/// CR and LF characters are *not* appended to the line automatically.
/// The optimum size of bytes to dump is 16, resulting in 67 bytes output.
/// Return the actual number of bytes output.
pub fn format_hex_ascii_line(
    inbuf: &[u8],
    out: &mut dyn io::Write,
) -> io::Result<usize> {
    const MIN_CHUNK_SIZE: usize = 16;
    let mut num_output_bytes: usize = 0;
    for &byte in inbuf.iter() {
        write!(out, "{byte:02X} ")?;
    }
    num_output_bytes += inbuf.len() * 3;
    if inbuf.len() < MIN_CHUNK_SIZE {
        let num_pad_spaces = (MIN_CHUNK_SIZE - inbuf.len()) * 3;
        for _ in 0..num_pad_spaces {
            let n = out.write(b" ")?;
            num_output_bytes += n;
        }
    }
    let n = out.write(b" |")?;
    num_output_bytes += n;
    for &byte in inbuf.iter() {
        let display_byte = if byte >= 32 && byte < 127 { byte } else { b'.' };
        let n = out.write(&[display_byte])?;
        num_output_bytes += n;
    }
    let n = out.write(b"|")?;
    num_output_bytes += n;
    Ok(num_output_bytes)
}

pub fn string_to_error(msg: String) -> io::Error {
    io::Error::new(io::ErrorKind::Other, msg)
}
