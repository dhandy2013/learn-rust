fn main() {
    println!("Trying loops with break labels and values");
    let a = 'outer: loop {
        println!("Entered the outer loop");
        let mut i = 0;
        'inner: loop {
            println!("Entered the inner loop...");
            i += 1;
            if i >= 5 {
                break 'outer i
            }
        }
    };
    println!("a = {}", a);
}
