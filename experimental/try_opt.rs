// Try out the Option type in the standard library

fn main() {
    let mut s = Some(String::from("abc"));
    let t = s.take();
    drop(s);
    println!("t = {:?}", t);
}

/// See: https://rust-unofficial.github.io/patterns/idioms/option-iter.html
#[allow(dead_code)]
fn iterate_over_opt() {
    let turing = Some("Turing".to_owned());
    let mut logicians =
        vec!["Curry".to_owned(), "Kleene".to_owned(), "Markov".to_owned()];

    logicians.extend(turing);
    println!("logicians: {:?}", logicians);

    // Can't do this - .extend() consumed the Option
    // println!("turing: {:?}", turing);
}
