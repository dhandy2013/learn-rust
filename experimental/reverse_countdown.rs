#!./run-rust.py
/// Reverse a range

fn main() {
    for i in (0..10).rev() {
        println!("{i}");
    }
}
