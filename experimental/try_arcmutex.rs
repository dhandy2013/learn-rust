#!./run-rust.py
use std::any::Any;
use std::error::Error;
use std::fmt;
use std::sync::{Arc, Mutex};

#[derive(Debug)]
struct Data {
    x: f64,
    y: f64,
}

impl Data {
    fn distance(&self) -> f64 {
        self.x.hypot(self.y)
    }
}

trait AnyDebuggable: Any + fmt::Debug {
    fn say_hello(&self) {
        println!("AnyDebuggable says hello");
    }

    fn as_any_mut(&mut self) -> &mut dyn Any;
}

impl AnyDebuggable for Data {
    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let data =
        Arc::new(Mutex::new(Box::new(Data { x: 3.0, y: 4.0 })
            as Box<dyn AnyDebuggable + 'static>));
    println!("In the beginning: data: {:?}", data);

    {
        let data_ptr = Arc::clone(&data);

        // Lock the data to get a reference to it
        if let Ok(mut locked_data) = data_ptr.try_lock() {
            // Type of locked_data is MutexGuard<'_, Box<dyn AnyDebuggable>>
            println!("Un-casted locked_data: {:?}", locked_data);
            locked_data.say_hello();

            // Explicitly convert MutexGuard to mutable Box reference
            let locked_box_mut: &mut Box<dyn AnyDebuggable> = &mut *locked_data;

            // Convert mutable Box reference to mutable dynamic object
            let locked_dyn_mut: &mut dyn AnyDebuggable = &mut **locked_box_mut;

            // Must call a pre-arranged method on the mutable dynamic object
            // that returns a mutable dynamic Any reference. The dynamic
            // reference trait type must be exactly "Any", it can't be some
            // other trait for which Any is a supertrait; otherwise you won't be
            // able to call Any::downcast_ref() nor Any::downcast_mut() on it.
            // (I found this out by painful trial and error. There is no other
            // way.)
            let locked_any_mut: &mut dyn Any = locked_dyn_mut.as_any_mut();

            // Convert mutable dynamic Any reference to a mutable reference to
            // a specific concrete type that we know it really is.
            if let Some(downcasted_data) =
                locked_any_mut.downcast_mut::<Data>()
            {
                // Now we do things with our reference to the concrete object.
                downcasted_data.x = 12.1;
                println!("distance: {}", downcasted_data.distance());
            }

            // Try locking the Mutex when it is already locked.
            // It will faile with error "WouldBlock".
            println!("Locking again ...");
            match data_ptr.try_lock() {
                Ok(_locked_data2) => println!("Wow I'm surprised that worked!"),
                Err(err) => println!("Lock failed with this error: {:?}", err),
            }
        }; // this semicolon is necessary, try removing it!
    }

    println!("At the end: data: {:?}", data);

    Ok(())
}
