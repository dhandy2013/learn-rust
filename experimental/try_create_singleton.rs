//! Experiment with functions to create singleton objects,
//! including un-boxed singleton trait objects.
//!
//! Compiler error:
//! error[E0401]: can't use generic parameters from outer function
//!
//! ... which is too bad because that's exactly what I had hoped to do!
#![allow(unused_imports)]
#![allow(dead_code)]
use core::fmt::Debug;
use core::sync::atomic::{AtomicUsize, Ordering};

#[derive(Default, Debug)]
struct ImportantInfo<I: Default + Debug> {
    info: I,
}

trait ImportantTrait: Debug {
    fn some_number(&self) -> u32;
}

impl<I: Default + Debug> ImportantTrait for ImportantInfo<I> {
    fn some_number(&self) -> u32 {
        42
    }
}

fn create_singleton<I: Default + Debug>(
) -> Result<&'static dyn ImportantTrait, &'static str> {
    static COUNTER: AtomicUsize = AtomicUsize::new(0);
    static mut IMPORTANT_INFO: Option<ImportantInfo<I>> = None;
    if COUNTER.fetch_add(1, Ordering::SeqCst) > 1 {
        // We are not the first caller.
        // Decrement counter to avoid any possibility of overflow.
        COUNTER.fetch_sub(1, Ordering::SeqCst);
        return Err("create_singleton() already called");
    }
    // SAFETY: For the rest of this function it is Ok to read and write
    // to the mutable static variable IMPORTANT_INFO because we are the only
    // caller that can do so, guaranteed by the code above.
    unsafe {
        // Because we are the first caller, IMPORTANT_INFO had better be None.
        assert!(IMPORTANT_INFO.is_none());

        IMPORTANT_INFO.replace(I::default());

        Ok(IMPORTANT_INFO.as_mut().unwrap())
    }
}

fn main() {
    println!("Hello from try_create_singleton.rs");
    let s = create_singleton::<i32>().unwrap();
    println!("{:?}", s);
}
