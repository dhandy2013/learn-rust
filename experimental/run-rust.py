#!/usr/bin/python3
"""
Compile and run a single Rust source file.
"""
import argparse
import os
import subprocess
import sys
import tempfile

DEFAULT_EDITION = '2021'


def main():
    # Set up some platform-dependent variables
    if os.name == 'nt':
        # Filename extension for compiled programs on Windows
        bin_ext = '.exe'
    else:
        # On Linux/Mac default is no extension for compiled executable files
        bin_ext = ''

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        '-v',
        '--verbose',
        help="Print compile and run command lines before executing.",
        action='store_true',
    )
    parser.add_argument(
        '--out-dir',
        help="Output directory for compiled binary. "
        "Default is a temporary directory that is removed afterwards.",
    )
    parser.add_argument(
        '--no-run',
        help="Do not run the program after successful compile.",
        action='store_true',
    )
    parser.add_argument(
        '--release',
        help="Compile in release mode instead of debug mode",
        action='store_true',
    )
    parser.add_argument(
        '-C',
        help="argument passed as -C <arg> to rustc",
        dest='codegen',
        action='append',
    )
    parser.add_argument(
        '--edition',
        default=DEFAULT_EDITION,
        help=f"Rust edition (2015, 2018, 2021). Default: {DEFAULT_EDITION}",
    )
    parser.add_argument(
        'sourcefile',
        help="Rust source file, .rs extension is added if necessary",
    )
    parser.add_argument(
        'progargs',
        help="Arguments passed to compiled Rust program",
        nargs=argparse.REMAINDER,
    )
    args = parser.parse_args()

    sourcefile = args.sourcefile
    if not sourcefile.endswith('.rs'):
        sourcefile += '.rs'

    bin_base = os.path.basename(args.sourcefile)
    if bin_base.endswith('.rs'):
        bin_base = bin_base[:-3]
    bin_name = bin_base + bin_ext

    if args.out_dir is not None:
        temp_dir = None
        out_dir = args.out_dir
    else:
        temp_dir = tempfile.TemporaryDirectory(prefix=f"rust-{bin_base}-")
        out_dir = temp_dir.name
    out_path = os.path.join(out_dir, bin_name)

    compile_cmd = [
        'rustc',
        '--crate-type', 'bin',
        '--edition', args.edition,
        '-g',   # include debug info
        '-o', out_path,
    ]
    if args.release:
        compile_cmd.extend(['-C', 'opt-level=3'])
    if args.codegen:
        for codegen in args.codegen:
            compile_cmd.extend(['-C', codegen])
    compile_cmd.append(sourcefile)
    if args.verbose:
        print(subprocess.list2cmdline(compile_cmd))

    compile_rc = subprocess.run(compile_cmd).returncode
    if args.no_run or compile_rc != 0:
        if temp_dir is not None:
            temp_dir.cleanup()
        return compile_rc

    run_cmd = [out_path] + args.progargs
    if args.verbose:
        print(subprocess.list2cmdline(run_cmd))

    run_rc = subprocess.run(run_cmd).returncode
    if temp_dir is not None:
        temp_dir.cleanup()
    return run_rc

if __name__ == '__main__':
    sys.exit(main())
