#!./run-rust.py
//! Demonstration of why you can have only one writer at a time to the same
//! chunk of memory.
use std::sync::{Arc, Mutex};
use std::thread;

static mut UNSAFE_COUNTER: u32 = 0;

fn demo_unsafe_loop() {
    const N: u32 = 10_000_000;
    let t1 = thread::spawn(|| { unsafe_loop(N / 2) });
    let t2 = thread::spawn(|| { unsafe_loop(N / 2) });
    t1.join().expect("Error in thread t1");
    t2.join().expect("Error in thread t2");
    println!("Unsafe: After {} loops the counter value is {}",
             N, unsafe { UNSAFE_COUNTER });
}

fn unsafe_loop(n: u32) {
    for _i in 0..n {
        unsafe { UNSAFE_COUNTER += 1; }
    }
}

fn demo_safe_loop() {
    const N: u32 = 10_000_000;
    let counter = Arc::new(Mutex::new(0));
    let (c1, c2) = (counter.clone(), counter.clone());
    let t1 = thread::spawn(move || { safe_loop(c1, N / 2) });
    let t2 = thread::spawn(move || { safe_loop(c2, N / 2) });
    t1.join().expect("Error in thread t1");
    t2.join().expect("Error in thread t2");
    println!("Safe  : After {} loops the counter value is {}",
             N, counter.lock().unwrap());
}

fn safe_loop(counter: Arc<Mutex<u32>>, n: u32) {
    for _i in 0..n {
        let mut value_inside_mutex = counter.lock().unwrap();
        *value_inside_mutex += 1;
    }
}

fn main() {
    demo_unsafe_loop();
    demo_safe_loop();
}
