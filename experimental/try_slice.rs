#!./run-rust.py
//! Try doing things with Rust vectors, arrays, and slices.
//! https://doc.rust-lang.org/std/primitive.slice.html
use std::error::Error;
use std::io::{self, Read};

fn main() -> Result<(), Box<dyn Error>> {
    println!("Rust slice examples!");

    try_byte_slice_as_reader()?;
    try_byte_slice_with_invalid_indexes();

    Ok(())
}

fn try_byte_slice_as_reader() -> io::Result<()> {
    println!("\nByte slice as reader");

    // The std::io::Read trait is implemented for a slice of bytes.
    let mut input_bytes = &b"Blah blah blee"[..];
    println!("input_bytes.len() = {}", input_bytes.len());

    // This is the buffer into which we will receive data
    let mut buf = [0u8; 8];
    println!("buf.len() = {}", buf.len());

    let mut n;
    
    n = input_bytes.read(&mut buf)?;
    println!("Read {} bytes from byte slice: {:?}", n, &buf[..n]);
    
    // This is the same code as the implementation of read() for &[u8]
    n = {
        let amt = std::cmp::min(buf.len(), input_bytes.len());
        let (a, b) = input_bytes.split_at(amt);
        buf[..amt].copy_from_slice(a);
        input_bytes = b;
        amt
    };
    println!("Read {} bytes from byte slice: {:?}", n, &buf[..n]);
    println!("Remaining in input buffer: {:?}", input_bytes);

    Ok(())
}

#[allow(unused_variables)]
fn try_byte_slice_with_invalid_indexes() {
    println!("\nSlice using invalid indices");

    let byte_array = b"This is an array of 29 bytes.";
    let byte_slice = &byte_array;
    println!("{} bytes in byte slice", byte_slice.len());
    assert_eq!(byte_slice.len(), 29);

    // This line would panic:
    //let s1 = &byte_slice[5..3];
    //println!("byte slice with indices backwards: {:?}", s1);

    // This line would panic:
    //let s2 = &byte_slice[99..105];
    //println!("byte slice with index range past end: {:?}", s2);

    let s3 = &byte_slice[26..29];
    println!("Last three bytes of slice: {:?}", s3);

    // This line would panic:
    //let s4 = &byte_slice[26..30];
    //println!("One past end of slice: {:?}", s4);
}
