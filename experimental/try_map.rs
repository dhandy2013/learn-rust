#!./run-rust.py
/// Try Rust standard library mapping collections
use std::borrow::Cow;
use std::collections::{BTreeMap, HashMap};

fn main() {
    increment_map_values();
    nullable_string_key();
}

/// Create a map of static string to signed 32-bit integer.
/// Then increment all values in the map with keys beginning
/// with "a".
fn increment_map_values() {
    let mut m1: BTreeMap<&'static str, i32> = BTreeMap::new();
    m1.insert("apple", 1);
    m1.insert("banana", 2);
    for (key, value) in &m1 {
        println!("{key}: {value}", key = key, value = value);
    }
    println!("{:?}", m1);

    // Now increment values with keys beginning with "a"
    for (key, value) in m1.iter_mut() {
        if key.starts_with("a") {
            *value += 1;
        }
    }
    println!("{:?}", m1);
}

/// Map nullable string to some value (in this case u32).
/// See discussion at:
/// https://users.rust-lang.org/t/querying-hashmap-when-borrow-t-cant-be-satisfied/82682
fn nullable_string_key() {
    nullable_string_key_0();
    nullable_string_key_1();
    nullable_string_key_2();
}

/// This solution requires allocating a String when looking up values
fn nullable_string_key_0() {
    let mut map0: HashMap<Option<String>, u32> = HashMap::new();
    map0.insert(None, 0);
    map0.insert(Some("abc".to_string()), 100);
    println!("{:?}", map0.get(&None));
    println!("{:?}", map0.get(&Some("abc".to_string())));
}

/// Solution using copy-on-write, or "clone-on-write" as the docs say:
/// https://doc.rust-lang.org/std/borrow/enum.Cow.html
fn nullable_string_key_1() {
    let mut map1: HashMap<Option<Cow<'static, str>>, u32> = HashMap::new();
    map1.insert(None, 0);
    map1.insert(Some(Cow::Owned("abc".to_owned())), 100);
    println!("{:?}", map1.get(&None));
    println!("{:?}", map1.get(&Some(Cow::from("abc"))));
}

/// Solution that wraps HashMap and the value for the null key.
fn nullable_string_key_2() {
    struct HashMapNullableStringKey<T>(HashMap<String, T>, Option<T>);
    impl<T> HashMapNullableStringKey<T> {
        fn new() -> Self {
            Self(HashMap::new(), None)
        }
        fn insert(&mut self, k: Option<String>, v: T) -> Option<T> {
            match k {
                Some(k) => self.0.insert(k, v),
                None => self.1.replace(v),
            }
        }
        fn get(&self, k: &Option<&str>) -> Option<&T> {
            match *k {
                Some(k) => self.0.get(k),
                None => self.1.as_ref(),
            }
        }
    }
    let mut map = HashMapNullableStringKey::<u32>::new();
    map.insert(None, 0);
    map.insert(Some("abc".to_string()), 100);
    println!("{:?}", map.get(&None));
    println!("{:?}", map.get(&Some("abc")));
    println!("{:?}", map.get(&Some(&"abc".to_string())));
}
