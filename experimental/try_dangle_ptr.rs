/// Deliberately cause a dangling pointer and use-after-free,
/// and see if we can detect the problem using external tools.
/// See: https://doc.rust-lang.org/std/ffi/struct.CStr.html#method.as_ptr
use std::ffi::CString;

fn main() {
    // This commentede-out code would *not* cause a dangling pointer, because
    // the first (shadowed) ``ptr`` variable keeps the CString alive.
    // let ptr = CString::new("Hello").expect("CString::new failed");
    // let ptr = ptr.as_ptr();

    let ptr = CString::new("Hello").expect("CString::new failed").as_ptr();
    unsafe {
        // ``ptr`` is a dangling pointer
        let c = *ptr;
        println!("First byte in (dangling) C string: {}", c);
    }
}
