#!./run-rust.py
//! Try the Rust time API, check out time resolution.
use std::thread;
use std::time::{Duration, Instant};

fn try_elapsed_time() {
    let t0 = Instant::now();
    println!("{:9}ns - first elapsed time", t0.elapsed().as_nanos());
    println!("{:9}ns - second elapsed time", t0.elapsed().as_nanos());
    let t1 = t0.elapsed();
    let t2 = t0.elapsed();
    println!("{:9}ns - minimum delta time", (t2 - t1).as_nanos());
}

fn try_sleep() {
    // Sleep for several different dirations - nanoseconds, powers of 10
    let sleep_durations = vec![
        Duration::new(0, 1),
        Duration::new(0, 10),
        Duration::new(0, 100),
        Duration::new(0, 1_000),
        Duration::new(0, 10_000),
        Duration::new(0, 100_000),
        Duration::new(0, 1_000_000),
        Duration::new(0, 10_000_000),
        Duration::new(0, 100_000_000),
        Duration::new(0, 1_000_000_000),
    ];
    println!("Sleep times");
    println!("Nominal      Actual");
    println!("------------ ------------");
    let t0 = Instant::now();
    for sleep_duration in sleep_durations.iter() {
        let t1 = t0.elapsed();
        thread::sleep(*sleep_duration);
        let t2 = t0.elapsed();
        println!(
            "{:10}ns {:10}ns",
            sleep_duration.as_nanos(),
            (t2 - t1).as_nanos()
        );
    }
}

fn main() {
    try_elapsed_time();
    try_sleep();
}
