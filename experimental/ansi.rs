#!/usr/bin/env run-rust.py
//! Simple ANSI text control
//!
//! ASCII and ISO-8850-1 (latin-1) characters:
//!     https://www.ascii-code.com/ISO-8859-1
//! Control codes supported by "yaft" minimal termal program (small):
//!     http://uobikiemukot.github.io/yaft/escape.html
//! Control codes supported by the Linux console (medium):
//!     https://man7.org/linux/man-pages/man4/console_codes.4.html
//! Control codes supported by xterm (huge):
//!     https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
//!
//! Explanations of some codes used:
//!
//! ESC = ASCII code 27 (0x1b)  Escape
//! CSI = ESC [                 Control Sequence Introducer
//! CSI 0 m                     Normal text (reset all text attributes)
//! CSI 1 m                     Bold text (increased intensity)
//! CSI 2 m                     Faint text (decreased intensity)
//! CSI 3 m                     Italicized text
//! CSI 4 m                     Underlined text
//! CSI 5 m                     Blinking text (can show up as bold)
//! CSI 7 m                     Inverse text
//! CSI 9 m                     Strikethrough text
//! CSI 2 2 m                   Normal intensity (neither bold nor faint)
//! CSI 2 4 m                   Not underlined
//! CSI 2 7 m                   Not inverse
//! CSI 2 9 m                   Not invisible
use std::{io::{self, Write}, thread, time::Duration};

mod codes {
    #![allow(dead_code)]
    pub const BEL: char = '\x07';
    pub const VT: char = '\x0b';
    pub const FF: char = '\x0c';
    pub const ESC: char = '\x1b';
    pub const CSI: &str = "\x1b[";
    pub const RESET: &str = "\x1b[0m";
    pub const BOLD: &str = "\x1b[1m";
    pub const BLINK: &str = "\x1b[5m";
    pub const BLINK_OFF: &str = "\x1b[25m";
    pub const NORMAL: &str = "\x1b[22m";
    pub const UNDERLINE: &str = "\x1b[4m";
    pub const UNDERLINE_OFF: &str = "\x1b[24m";

    pub const CLEAR_SCREEN: &str = "\x1b[2J";
    pub const HOME: &str = "\x1b[1;1H";

    pub const LED_NUM_LOCK_ON: &str = "\x1b[1q";
    pub const LED_NUM_LOCK_OFF: &str = "\x1b[21q";
    pub const LED_SCROLL_LOCK_ON: &str = "\x1b[3q";
    pub const LED_SCROLL_LOCK_OFF: &str = "\x1b[23q";
}
use codes::*;

fn main() {
    println!("Clearing the screen in 1 second...");
    thread::sleep(Duration::from_millis(1000));
    print!("{CLEAR_SCREEN}{HOME}{BEL}");

    println!("This is {BOLD}bold{NORMAL} text!");
    println!("This is {UNDERLINE}underlined{UNDERLINE_OFF} text!");
    println!("This is {BLINK}blinking{BLINK_OFF} text!");

    println!("CSI parameter byte range:");
    for n in 0x30..=0x3F {
        let c = char::from_u32(n).unwrap();
        print!("{c}");
    }
    println!();

    // Standard 8 colors in background
    for n in 40..=47 {
        print!("{CSI}{n}m ");
    }
    println!("{RESET}");

    // Standard 8 colors in foreground
    for n in 30..=37 {
        print!("{CSI}{n}m*");
    }
    println!("{RESET}");

    // "DEC screen alignment test - fill screen with E's"
    // This actually "works"...and is real annoying.
    if false {
        print!("{ESC}#8");
    }

    // Alas, LED controls don't work
    if false {
        print!("Num lock LED on...{LED_NUM_LOCK_ON}");
        let _ = io::stdout().flush();
        thread::sleep(Duration::from_millis(1000));
        println!("Num lock LED off.{LED_NUM_LOCK_OFF}");
    }

    println!("{RESET}Done.");
}
