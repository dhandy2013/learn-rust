#!./run-rust.py
//! Experiment with error reporting and handling.
//! Minimum Supported Rust Version: 1.65 (uses backtrace)
pub use crate::error::Error;
use std::sync::{Arc, Mutex};

/// Example error module for a library
mod error {
    use std::backtrace::Backtrace;
    use std::fmt;
    use std::sync::{MutexGuard, TryLockError};

    /// Error returned from this library.
    /// If an error is constructed from just a string message, it assumed
    /// to be catastrophic and a backtrace is included.
    #[non_exhaustive]
    pub enum Error {
        String(String, Backtrace),
        Str(&'static str, Backtrace),
        NotSoBad,
    }

    impl Error {
        /// Get just the error message not the backtrace
        pub fn msg(&self) -> &str {
            use Error::*;
            match self {
                String(msg, _) => &*msg,
                Str(msg, _) => msg,
                NotSoBad => "not so bad",
            }
        }
    }

    impl fmt::Debug for Error {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            use Error::*;
            match self {
                String(msg, bt) => write!(f, "{:?}\n{}", msg, bt),
                Str(msg, bt) => write!(f, "{:?}\n{}", msg, bt),
                NotSoBad => write!(f, "{}", self.msg()),
            }
        }
    }

    impl fmt::Display for Error {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            use Error::*;
            match self {
                String(msg, bt) => write!(f, "{}\n{}", msg, bt),
                Str(msg, bt) => write!(f, "{}\n{}", msg, bt),
                NotSoBad => write!(f, "{}", self.msg()),
            }
        }
    }

    impl std::error::Error for Error {}

    /// Conversion of String to Error with message and backtrace
    impl From<String> for Error {
        fn from(value: String) -> Error {
            Error::String(value, Backtrace::force_capture())
        }
    }

    /// Conversion of static string slice to Error with message and backtrace
    impl From<&'static str> for Error {
        fn from(value: &'static str) -> Error {
            Error::Str(value, Backtrace::force_capture())
        }
    }

    /// Conversion from error returned by .try_lock()
    impl<T: 'static + ?Sized> From<TryLockError<MutexGuard<'_, T>>> for Error {
        fn from(value: TryLockError<MutexGuard<'_, T>>) -> Error {
            Error::String(
                format!(
                    "trying to lock smart pointer to {}: {}",
                    std::any::type_name::<T>(),
                    value
                ),
                Backtrace::force_capture(),
            )
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    if let Err(err) = do_something_that_could_fail() {
        println!("The function failed with this message: {}", err.msg());
    }

    println!("**************************************");
    println!("The following stack trace is expected.");
    println!("**************************************");
    cause_duplicate_lock_failure()?;

    Ok(())
}

fn do_something_that_could_fail() -> Result<(), crate::Error> {
    Err(crate::Error::from("I'm melting! I'm melting!"))
}

fn cause_duplicate_lock_failure() -> Result<(), crate::Error> {
    // Create a dynamic smart pointer to a new object.
    let dyn_ptr: DynObjPtr =
        Arc::new(Mutex::new(ConcreteObj { x: 3.0, y: 4.0 }));

    // Create first lock on dynamic object
    let _obj_guard1 = dyn_ptr.try_lock()?;

    // Attempt to create second lock (will fail)
    let _obj_guard2 = dyn_ptr.try_lock()?;

    Ok(())
}

/// A dynamic object trait
trait DynObj {
    fn x(&self) -> f64;
    fn y(&self) -> f64;
}

type DynObjPtr = Arc<Mutex<dyn DynObj>>;

/// A concrete structure
struct ConcreteObj {
    x: f64,
    y: f64,
}

impl DynObj for ConcreteObj {
    fn x(&self) -> f64 {
        self.x
    }

    fn y(&self) -> f64 {
        self.y
    }
}
