#!./run-rust.py
use std::collections::btree_map::BTreeMap;

fn main() {
    println!("Trying BTreeMap");
    let mut map = BTreeMap::new();
    map.insert("a", 1);
    map.insert("b", 2);
    map.insert("c", 3);
    for (&key, &value) in map.range(.."c") {
        println!("{}: {}", key, value);
    }
}
