#!./run-rust.py
//! See: https://doc.rust-lang.org/std/any/trait.Any.html#method.downcast_mut
use std::any::Any;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let mut x = 10u32;
    let mut s = "starlord".to_string();

    modify_if_u32(&mut x);
    modify_if_u32(&mut s);

    assert_eq!(x, 42);
    assert_eq!(&s, "starlord");

    let boxed_dyn_any = Box::new(11u32) as Box<dyn Any>;

    let any_ref: &dyn Any = &*boxed_dyn_any;
    if let Some(concrete_ref) = any_ref.downcast_ref::<u32>() {
        println!("Box contents: {:?}", concrete_ref);
    }

    Ok(())
}

fn modify_if_u32(s: &mut dyn Any) {
    if let Some(num) = s.downcast_mut::<u32>() {
        *num = 42;
    }
}
