use std::{io, thread};

/*
 * Question:
 * When a thread closure returns a value, does that value get returned to the
 * function that calls `.join()` on the thread handle?
 * Answer:
 * Yes, verified by `try_thread_return()` below.
 *
 * Question: When a thread panics in rust:
 * - Can the main program carry on safely?
 * - In the panicking thread, do custom drop() methods get called as the stack
 *   unwinds?
 * Answer:
 * Yes to both, verified by `try_panic_unwind()` below.
 * No if the program is compiled with .e.g. `-C panic=abort`
 * However, if panic=abort, you can still see a stack trace from a panicking
 * child thread if you set RUST_BACKTRACE=1. But drop methods are not called.
 */

fn main() -> io::Result<()> {
    println!("Hello from exp_rs_thread!");
    try_thread_return()?;
    try_panic_unwind()?;
    Ok(())
}

/// See if we can return a value from a thread function and receive that value
/// in the thread that calls join().
fn try_thread_return() -> io::Result<()> {
    let h: thread::JoinHandle<_> = thread::spawn(|| {
        let x: f64 = 26.0;
        (x - 1.0).sqrt()
    });
    let result = h.join().expect("Error in thread");
    println!("try_thread_return: thread result: {}", result);
    Ok(())
}

/// Make sure that when a thread panics, the main thread can recover, and the
/// `.drop()` method is called on objects owned by the panicking thread as the
/// child thread exits.
fn try_panic_unwind() -> io::Result<()> {
    let h = thread::Builder::new()
        .name("child-thread".to_string())
        .spawn(|| -> i32 { do_something_with_data() })?;
    match h.join() {
        Ok(result) => {
            println!("try_panic_unwind: thread result: {}", result);
        }
        Err(err) => {
            println!("try_panic_unwind: it panicked: {:?}", err);
        }
    };
    Ok(())
}

fn do_something_with_data() -> i32 {
    let scd = StructContainingData::new();
    println!("{:?}", scd);
    scd.access_data(42)
}

#[derive(Debug)]
struct StructContainingData {
    data: Vec<i32>,
}

impl StructContainingData {
    fn new() -> Self {
        Self {
            data: vec![1, 2, 3],
        }
    }

    fn access_data(&self, i: u32) -> i32 {
        self.data[i as usize]
    }
}

impl Drop for StructContainingData {
    fn drop(&mut self) {
        println!("Dropping a StructContainingData");
    }
}
