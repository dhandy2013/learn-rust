#!./run-rust.py
// https://quinedot.github.io/rust-learning/dont-hide.html
#![allow(dead_code)]
#![deny(elided_lifetimes_in_paths)]

struct Foo<'a>(&'a str);

impl<'a> Foo<'a> {
    fn new(s: &str) -> Foo<'_> {
        Foo(s)
    }
}

/// Try to create a shared reference, then a mutable reference, then drop the
/// mutable reference, then try to re-use the original shared reference.
fn cannot_reuse_shared_reference() {
    let mut local = "Hello".to_string();

    // Creating and using a shared reference
    let x = &local;
    println!("{x}");

    {
        // Creating an exclusive reference immediately and permanently
        // invalidates all shared references such as `x`.
        let y = &mut local;
        // Because you could do something like this, which would re-allocate
        // the storage behind `local` and cause `x` to be a dangling pointer.
        y.push_str(", world!");
        // However, even if you comment-out the above line the last line of this
        // function will not compile because creating the mutable reference cuts
        // short the lifetimes of any shared references to that same variable.

        // You can re-borrow the mutable reference as shared but that won't
        // give you back the `x` reference.
        println!("{y}");
    }

    // Trying to use the shared reference again will not compile.
    //println!("{x}");
    // But we can create a *new* shared reference and use it.
    let z = &local;
    println!("{z}");
}

fn foo(s: &mut String) -> &str {
    let _ms: &mut str = &mut *s;
    let rs: &str = &*s;
    rs
}

fn main() {
    println!("Lifetimes.");
}
