#!./run-rust.py
//! Experiment with Rust tuples and tuple structs.

mod color {
    use std::fmt;

    #[derive(Clone, Copy)]
    pub struct Color(u32);

    impl From<(u8, u8, u8)> for Color {
        fn from(value: (u8, u8, u8)) -> Color {
            Color(
                ((value.0 as u32) << 24) |  // red
                ((value.1 as u32) << 16) |  // green
                ((value.2 as u32) << 8) |   // blue
                0x000000ff, // default alpha value is fully opaque
            )
        }
    }

    impl fmt::Debug for Color {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "Color(0x{:08x})", self.0)
        }
    }
}

use color::Color;

fn main() {
    // Compile error:
    // cannot initialize a tuple struct which contains private fields
    //let color = Color(0x00ffa0);

    let color = Color::from((0x00, 0xff, 0xa0));

    println!("color = {:?}", color);
}
