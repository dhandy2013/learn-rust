#!./run-rust.py -v
// For experiments with the Rust standard library
use std::collections::HashMap;
use std::env;
use std::io;
use std::mem;
use std::str;

fn main() -> Result<(), io::Error> {
    // Note: As per the Rust book section 12.1, if the any command-line
    // argument contains bytes that are not valid UTF-8 characters, then
    // std::env::args will panic. std::env:args_os avoids that problem but
    // introduces another problem, which is that it returns items of type
    // OsString instead of String.
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);

    println!("i32::max_value(): {}", i32::max_value());
    println!("i32::min_value(): {}", i32::min_value());

    let umax_str = format!("{}", usize::max_value());
    println!(
        "usize max: {} ({} digits)",
        usize::max_value(),
        umax_str.len()
    );

    let u128_str = format!("{}", u128::max_value());
    println!(
        "u128 max: {} ({} digits)",
        u128::max_value(),
        u128_str.len()
    );

    let here =
        format!("{} ({}:{}:{})", module_path!(), file!(), line!(), column!());
    println!("We are here: {}", here);

    // &str to i32 map
    let mut m1 = HashMap::new();
    m1.insert("a", 1);
    m1.insert("b", 2);
    m1.insert("c", 3);
    println!("m1: {:?}", m1);
    println!("m1.get(\"b\"): {:?}", m1.get("b"));
    println!("m1.get(\"blah\"): {:?}", m1.get("blah"));
    println!("m1.remove(\"c\"): {:?}", m1.remove("c"));
    println!("m1.remove(\"c\"): {:?}", m1.remove("c"));
    println!("m1: {:?}", m1);

    // i32 to &str map
    // Bizarrely, get() requires "&" in front of literal integer keys!
    let mut m2 = HashMap::new();
    m2.insert(1, "a");
    m2.insert(2, "b");
    m2.insert(3, "c");
    println!("m2: {:?}", m2);
    println!("m2.get(&2): {:?}", m2.get(&2));
    println!("m2.get(&9): {:?}", m2.get(&9));
    println!("m2.remove(&3): {:?}", m2.remove(&3));
    println!("m2.remove(&3): {:?}", m2.remove(&3));
    println!("m2: {:?}", m2);

    // I guess it is consistent; I have to do it for regular functions too.
    let x = 5;
    println!("f({:?}) -> {:?}", &x, f(&x));
    println!("f({:?}) -> {:?}", &1, f(&1));

    // Demonstrating floating point decimal imprecision
    let a = 0.1;
    let b = 0.2;
    println!("{:?} + {:?} = {:?}", a, b, a + b);

    println!("size of bool: {:?}", mem::size_of::<bool>());
    println!("size of String: {:?}", mem::size_of::<String>());
    println!("size of Vec<String>: {:?}", mem::size_of::<Vec<String>>());
    println!("size of Option<i32>: {:?}", mem::size_of::<Option<i32>>());

    //
    // Figuring out how pass an iterator over a string vector
    //
    // First create a vector of strings
    let argv: Vec<String> = vec!["blah", "blee", "bloo"]
        .iter()
        .map(|i| i.to_string())
        .collect();
    println!("argv = {:?}", argv);
    // Create an iterator over that vector
    let argv_iter = argv.iter();
    println!("argv_iter = {:?}", argv_iter);
    // Pass that iterator as a parameter to a function
    let count = func_that_takes_iterator(argv_iter);
    println!("count = {:?}", count);

    // Conversion from numeric character values to UTF-8 string
    // When casting a single u8 integer to a char, Rust treats it like an
    // ISO-8859-1 character. So values > 127 are valid.
    let c169: u8 = 169; // code point of copyright symbol
    let nbsp: u8 = 0xA0; // non-breaking space
    let c_c7: u8 = 0xC7; // code point of capital C with doohickey
    let c_ad: u8 = 0xAD; // soft hyphen
    let mut s = String::new();
    s.push(c169 as char);
    s.push(nbsp as char);
    s.push(c_ad as char);
    s.push(c_c7 as char);
    println!("s = '{}'", s);

    // Writing stuff to a buffer

    // Growable Vec<u8> buffer
    let mut out1 = Vec::<u8>::new();
    f1(&mut out1)?;
    let s1 = str::from_utf8(&out1).unwrap(); // trust it is valid utf-8
    println!("out1 = {:?}", s1);

    // Fixed-size byte array buffer
    const OUT2_CAP: usize = 10;
    let mut out2 = [0u8; OUT2_CAP];
    let mut b2 = &mut out2[..]; // type of b2 is: &mut [u8]
    f1(&mut b2)?;
    let size2_after = OUT2_CAP - b2.len();
    let s2 = str::from_utf8(&out2[0..size2_after]).unwrap(); // better be utf-8
    println!("out2 = {:?}", s2);

    // Normal exit
    Ok(())
}

// Function that takes a writer
fn f1(out: &mut dyn io::Write) -> Result<(), io::Error> {
    write!(out, "Hello!\n")?;
    Ok(())
}

fn f(x: &i32) -> i32 {
    // Using the "*" operator to dereference a reference
    *x
}

fn func_that_takes_iterator<'a>(
    args: impl Iterator<Item = &'a String>,
) -> usize {
    let mut count: usize = 0;
    for item in args {
        println!("Item #{:?}: {:?}", count, item);
        count += 1;
    }
    count
}
