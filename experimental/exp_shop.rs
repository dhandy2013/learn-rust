// Example program demonstrating "classes" and "objects" in Rust

struct Store {
    name: String,
    apples_price: f64,
    apples: i32,
    cheese_price: f64,
    cheese: i32,
}

impl Store {
    pub fn new(name: &str) -> Store {
        Store {
            name: name.to_string(),
            apples_price: 5.00,
            apples: 10,
            cheese_price: 7.00,
            cheese: 10,
        }
    }

    pub fn open(&self) {
        println!("Welcome to {}!", self.name);
        println!("We have {} apples at ${:.2}",
                 self.apples, self.apples_price);
        println!("We have {} cheese wheels at ${:.2}",
                 self.cheese, self.cheese_price);
    }
}

struct Shopper {
    name: String,
    balance: f64,
}

impl Shopper {
    pub fn new(name: &str) -> Shopper {
        Shopper {
            name: name.to_string(),
            balance: 20.00,
        }
    }

    pub fn purchase(&mut self, store: &Store, product: &str) {
        println!("{} has a balance of ${:.2}", self.name, self.balance);
        let price = match product {
            "apple" => store.apples_price,
            "cheese" => store.cheese_price,
            _ => {
                println!("Sorry, {} does not carry {}", store.name, product);
                return;
            }
        };
        self.balance -= price;
        println!("{} purchased {} for ${:.2} and has ${:.2} left",
                 self.name, product, price, self.balance);
    }
}

fn main() {
    let store1 = Store::new("Whole Foods");
    store1.open();
    let mut dave = Shopper::new("Dave");
    dave.purchase(&store1, "apple");
    dave.purchase(&store1, "cheese");
    dave.purchase(&store1, "pizza");
}
