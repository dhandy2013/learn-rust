#!./run-rust.py

// I allow dead_code to avoid warnings due to unused enum variants.
#[allow(dead_code)]
#[derive(Copy, Clone, Debug)]
#[repr(usize)]
enum LuckyNumbers {
    One = 1,
    Five = 5,
    Seven = 7,
    Twelve = 12,
}

fn main() {
    let five = LuckyNumbers::Five;
    println!("five = {:?}", five);
    println!("five + 1 = {:?}", five as usize + 1);
}
