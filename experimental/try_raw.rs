fn main() {
    println!("Trying out raw identifiers");
    r#d953787cc4b747478f64bfca1a8b36d7();
    // r#4dfd0d2bd0d6490290b8b688bc20c5ec();
}

fn r#d953787cc4b747478f64bfca1a8b36d7() {
    println!("Hello from a function named d953787cc4b747478f64bfca1a8b36d7");
}

/*
 * Unfortunately this didn't work.
 * You can't have a function name begin with a numeric digit.
fn r#4dfd0d2bd0d6490290b8b688bc20c5ec() {
    println!("Hello from a function named 4dfd0d2bd0d6490290b8b688bc20c5ec");
}
*/
