enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) -> String {
        match self {
            Message::Quit => "Quit".to_string(),
            // You can use ".." catch all matching inside the { }
            Message::Move { x, y } => { format!("Move(x:{}, y:{})", x, y) },
            Message::ChangeColor(r, g, b) =>
                format!("ChangeColor({}, {}, {})", r, g, b),
            Message::Write(s) => format!("Write({})", s),
        }
    }
}

// Linked List implementation using Option for pointers that can be null
// Yes I know this is sub-optimial, this is early experimentation while I'm
// still learning this language.

// ... my first attempts would compile at all, then I found this tutorial:
// https://rust-unofficial.github.io/too-many-lists/first-layout.html

/*
struct Node {
    value: String,
    next: Option<&mut Node>,
}

struct LinkedList {
    head: Option<&mut Node>,
}

impl LinkedList {
    fn len(&self) -> usize {
        let count: usize = 0;
        let ptr = &self.head;
        loop {
            match ptr {
                Some(node) => {
                    count += 1;
                    ptr = node.next;
                }
                None => {
                    break
                },
            }
        }
        count
    }
}
*/

fn main() {
    println!("Fun with Rust enums");
    println!("Testing format: {}", format!("integer {}", 1));
    let _home = IpAddr::V4(127, 0, 0, 1);
    let _loopback = IpAddr::V6(String::from("::1"));
    let msg_array = [
        Message::Write("hello".to_string()),
        Message::Move {x: 0, y: 0}, 
        Message::ChangeColor(128, 128, 64),
        Message::Move {x: -1, y: -3},
        Message::Quit,
    ];
    for (i, msg) in msg_array.iter().enumerate() {
        println!("Result of calling message #{}: {}", i, msg.call());
    }

    let a: Option<i32> = Some(3);
    let b: Option<i32> = None;
    let c = if let Some(3) = a { "three" } else { "not three" };
    let d = if let None = b { "none" } else { "not none" };
    println!("c = {}", c);
    println!("d = {}", d);
}
