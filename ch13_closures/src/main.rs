use std::collections::HashMap;
use std::thread;
use std::time::Duration;

/// Struct holding "callback" closures called when events happen
struct ZapHandler<'a> {
    handler1: &'a mut dyn FnMut() -> u32,

    // You just can't call a stored FnOnce() handler if it is a trait object
    // https://users.rust-lang.org/t/object-polymorphism-moving-box-dyn-t/42572/23
    // "You simply can't call by-value methods on trait objects."
    // The solution is to store a Box instead of a reference. The Box trait object
    // can be consumed in-place whereas you can't drop something behind a reference.
    // Also see this: https://github.com/rust-lang/book/issues/2208
    //
    // The Option is not optional. Calling a FnOnce closure consumes it,
    // and you can't consume a field of a struct without moving it out of the
    // struct, and you can't move a field out of struct that implements Drop.
    // https://doc.rust-lang.org/error-index.html#E0509
    // The solution is to wrap the field in an Option and `.take()` it before
    // consuming it.
    handler2: Option<Box<dyn FnOnce() -> u32>>,
}

impl<'a> ZapHandler<'a> {
    fn zap1(&mut self) -> u32 {
        (self.handler1)()
    }

    fn zap2(mut self) -> u32 {
        println!("ZapHandler:zap2: enter");
        if let Some(handler2) = self.handler2.take() {
            let r = (handler2)();
            println!("ZapHandler:zap2: leave after calling handler");
            r
        } else {
            println!("ZapHandler:zap2: leave, there was no handler");
            0
        }
    }
}

impl<'a> Drop for ZapHandler<'a> {
    fn drop(&mut self) {
        println!("ZapHandler:drop() I'm melting! I'm melting!");
    }
}

fn main() {
    let mut count: u32 = 0;
    let mut handler1 = || -> u32 {
        count += 1;
        count
    };

    let final_value: u32 = 1243;
    let moved_value = "moved string".to_owned();
    let handler2 = move || -> u32 {
        println!("Side-effect in handler2: {}", moved_value);
        final_value
    };

    let mut zh = ZapHandler {
        handler1: &mut handler1,
        handler2: Some(Box::new(handler2)),
    };

    // We can't call handler1 here because it is mutably borrowed by zh.
    // (handler1)();

    println!("Zap 1: {}", zh.zap1());
    println!("Zap 2: {}", zh.zap1());
    println!("Final zap: {}", zh.zap2());

    // We can call handler1 now because zh was consumed by ZapHandler::zap2().
    (handler1)();

    // Rust does not allow us to call handler2 any more, it has been moved.
    // (handler2)();

    // Rust cleverly allows us to re-borrow ``count`` from handler1.
    println!("Final count: {}", count);

    // But trying to use handler1 again here is a bridge too far.
    // Uncommenting this line makes the println!() above fail to compile.
    // (handler1)();

    println!("Done.");
}

#[allow(dead_code)]
fn ch_13() {
    println!("Ch13: Iterators and Closures");
    ch_13_1();
    ch_13_2();
}

fn ch_13_1() {
    println!("\nCh13.1: Closures");
    let simulated_user_specified_value = 10;
    let simulated_random_number = 7;

    let workout = generate_workout(
        simulated_user_specified_value,
        simulated_random_number,
    );
    println!("{}", workout);

    let x = 4;
    let equal_to_x = |z| z == x;
    let y = 4;
    assert!(equal_to_x(y));

    fn f(x: i32) {
        println!("Inside f(): x == {:?}", x);
    }
    f(x);
    println!("Outside f(): x == {:?}", x);

    ex_closure_move();
}

fn ex_closure_move() {
    let x = vec![1, 2, 3];
    let equal_to_x = move |z| z == x;
    // println!("Can't use x here: {:?}", x);

    let y = vec![1, 2, 3];
    assert!(equal_to_x(y));
}

fn generate_workout(intensity: u32, random_number: u32) -> String {
    let mut expensive_result = Cacher::new(|num| {
        println!("Calculating slowly...");
        // (slow is a relative term)
        thread::sleep(Duration::from_millis(2));
        num
    });
    if intensity < 25 {
        format!(
            "Today, do {} pushups!\nNext, do {} situps!",
            expensive_result.value(intensity),
            expensive_result.value(intensity)
        )
    } else {
        if random_number == 3 {
            format!("Take a break today! Remember to stay hydrated!")
        } else {
            format!(
                "Today, run for {} minutes",
                expensive_result.value(intensity)
            )
        }
    }
}

struct Cacher<T>
where
    T: Fn(u32) -> u32,
{
    calculation: T,
    values: HashMap<u32, u32>,
}

impl<T> Cacher<T>
where
    T: Fn(u32) -> u32,
{
    fn new(calculation: T) -> Cacher<T> {
        Cacher {
            calculation,
            values: HashMap::new(),
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.values.get(&arg) {
            Some(v) => *v,
            None => {
                let v = (self.calculation)(arg);
                self.values.insert(arg, v);
                v
            }
        }
    }
}

fn ch_13_2() {
    println!("\nCh 13.2: Iterators");
    let v1 = vec![1, 2, 3];

    let mut v1_iter = v1.iter(); // mut required by .next()

    assert_eq!(v1_iter.next(), Some(&1));

    for val in v1_iter {
        println!("Item: {:?}", val);
    }

    let v2: Vec<_> = v1.iter().map(|x| x + 1).collect();
    println!("v2 = {:?}", v2);

    let n: i32;
    n = 12;
    let v3: Vec<i32> = (0..n).filter(|i| i % 2 == 0).collect();
    println!("v3 = {:?}", v3);

    let v4: Vec<i32> = divisible_by_vec(0..n, 3);
    println!("v4 = {:?}", v4);

    let v5: Vec<i32> = divisible_by_iter(0..n, 3).collect();
    println!("v5 = {:?}", v5);
}

fn divisible_by_vec<I>(numbers: I, d: i32) -> Vec<i32>
where
    I: Iterator<Item = i32>,
{
    numbers.filter(|i| i % d == 0).collect()
}

// Note: You have to put "move" in front of the "|i|" or you get an error:
// "borrowed value does not live long enough".
//
// Also see this StackOverflow thread for several different ways to return
// an iterator from a function: https://stackoverflow.com/a/27535594
fn divisible_by_iter<I>(numbers: I, d: i32) -> impl Iterator<Item = i32>
where
    I: Iterator<Item = i32>,
{
    numbers.filter(move |i| i % d == 0)
}

pub struct Counter {
    count: u32,
    limit: u32,
}

impl Counter {
    pub fn new(limit: u32) -> Counter {
        Counter { count: 0, limit }
    }
}

// I think this Iterator does pretty much what the ".." range operator does.
impl Iterator for Counter {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count < self.limit {
            let result = Some(self.count);
            self.count += 1;
            result
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn call_with_different_values() {
        let mut c = Cacher::new(|a| a);
        let v1 = c.value(1);
        let v2 = c.value(2);
        assert_eq!(v1, 1);
        assert_eq!(v2, 2);
    }

    #[test]
    fn call_with_same_values() {
        let mut c = Cacher::new(|a| a);
        let v1 = c.value(5);
        let v2 = c.value(5);
        assert_eq!(v1, 5);
        assert_eq!(v2, 5);
    }

    #[test]
    fn iterator_sum() {
        let v1 = vec![1, 2, 3];
        let v1_iter = v1.iter();
        let total: i32 = v1_iter.sum();
        assert_eq!(total, 6);
        // This does not compile because the sum() method takes ownership
        // of the iterator on which it is called, preventing use of the
        // iterator again.
        // assert_eq!(v1_iter.next(), None);
    }

    #[test]
    fn calling_next_directly() {
        let mut counter = Counter::new(5);
        assert_eq!(counter.next(), Some(0));
        assert_eq!(counter.next(), Some(1));
        assert_eq!(counter.next(), Some(2));
        assert_eq!(counter.next(), Some(3));
        assert_eq!(counter.next(), Some(4));
        assert_eq!(counter.next(), None);
        assert_eq!(counter.next(), None);
    }

    #[test]
    fn using_other_iterator_trait_methods() {
        let sum: u32 = Counter::new(5)
            .zip(Counter::new(5).skip(1))
            .map(|(a, b)| a * b)
            .filter(|x| x % 3 == 0)
            .sum();
        assert_eq!(18, sum);
    }
}
