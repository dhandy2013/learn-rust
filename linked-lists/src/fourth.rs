//! "A Bad but Safe Doubly-Linked Deque"
//! https://rust-unofficial.github.io/too-many-lists/fourth.html
use std::rc::Rc;
use std::cell::{Ref, RefCell, RefMut};

pub struct List<T> {
    head: Link<T>,
    tail: Link<T>,
}

type Link<T> = Option<Rc<RefCell<Node<T>>>>;

struct Node<T> {
    elem: T,
    next: Link<T>,
    prev: Link<T>,
}

pub struct IntoIter<T>(List<T>);

impl<T> Node<T> {
    fn new(elem: T) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            elem,
            prev: None,
            next: None,
        }))
    }
}

impl<T> List<T> {
    pub fn new() -> Self {
        Self {
            head: None,
            tail: None,
        }
    }

    pub fn push_front(&mut self, elem: T) {
        let new_head_rc = Node::new(elem);
        match self.head.take() {
            Some(old_head_rc) => {
                // Non-empty list
                old_head_rc.borrow_mut().prev = Some(new_head_rc.clone());
                new_head_rc.borrow_mut().next = Some(old_head_rc);
                self.head = Some(new_head_rc);
            },
            None => {
                // Empty list
                self.head = Some(new_head_rc.clone());
                self.tail = Some(new_head_rc);
            },
        }
    }

    pub fn push_back(&mut self, elem: T) {
        let new_tail_rc = Node::new(elem);
        match self.tail.take() {
            Some(old_tail_rc) => {
                // Non-empty list
                old_tail_rc.borrow_mut().next = Some(new_tail_rc.clone());
                new_tail_rc.borrow_mut().prev = Some(old_tail_rc);
                self.tail = Some(new_tail_rc);
            },
            None => {
                // Empty list
                self.head = Some(new_tail_rc.clone());
                self.tail = Some(new_tail_rc);
            },
        }
    }

    pub fn pop_front(&mut self) -> Option<T> {
        self.head.take().map(|old_head_rc| {
            match old_head_rc.borrow_mut().next.take() {
                Some(new_head_rc) => {
                    new_head_rc.borrow_mut().prev = None;
                    self.head = Some(new_head_rc);
                },
                None => {
                    // List is now empty
                    self.tail = None;
                },
            }
            Rc::try_unwrap(old_head_rc).ok().unwrap().into_inner().elem
        })
    }

    pub fn pop_back(&mut self) -> Option<T> {
        self.tail.take().map(|old_tail_rc| {
            match old_tail_rc.borrow_mut().prev.take() {
                Some(new_tail_rc) => {
                    new_tail_rc.borrow_mut().next = None;
                    self.tail = Some(new_tail_rc);
                },
                None => {
                    // List is now empty
                    self.head = None;
                },
            }
            Rc::try_unwrap(old_tail_rc).ok().unwrap().into_inner().elem
        })
    }

    pub fn peek_front_ref(&self) -> Option<Ref<T>> {
        self.head.as_ref().map(|node_rc| {
            Ref::map(node_rc.borrow(), |node| &node.elem)
        })
    }

    pub fn peek_front_ref_mut(&mut self) -> Option<RefMut<T>> {
        self.head.as_ref().map(|node_rc| {
            RefMut::map(node_rc.borrow_mut(), |node| &mut node.elem)
        })
    }

    pub fn peek_back_ref(&self) -> Option<Ref<T>> {
        self.tail.as_ref().map(|node_rc| {
            Ref::map(node_rc.borrow(), |node| &node.elem)
        })
    }

    pub fn peek_back_ref_mut(&mut self) -> Option<RefMut<T>> {
        self.tail.as_ref().map(|node_rc| {
            RefMut::map(node_rc.borrow_mut(), |node| &mut node.elem)
        })
    }

    pub fn into_iter(self) -> IntoIter<T> {
        IntoIter(self)
    }
}

impl<T: Copy> List<T> {
    pub fn peek_front_copy(&self) -> Option<T> {
        self.head.as_ref().map(|node_rc| {
            node_rc.borrow().elem
        })
    }

    pub fn peek_back_copy(&self) -> Option<T> {
        self.tail.as_ref().map(|node_rc| {
            node_rc.borrow().elem
        })
    }
}

impl<T: std::fmt::Debug> List<T> {
    pub fn print_items_forward(&self) {
        let mut cur: Link<T> = self.head.as_ref().map(
            |head_rc| head_rc.clone());
        let mut i = 0;
        loop {
            match cur {
                None => { break; }
                Some(cur_rc) => {
                    let cur_node = cur_rc.borrow();
                    println!("#{}: strong_count={} weak_count={} {:?}",
                             i,
                             Rc::strong_count(&cur_rc),
                             Rc::weak_count(&cur_rc),
                             cur_node.elem);
                    cur = cur_node.next.as_ref().map(|next_rc| next_rc.clone());
                    i += 1;
                }
            }
        }
    }

    pub fn print_items_reverse(&self) {
        let mut cur: Link<T> = self.tail.as_ref().map(
            |tail_rc| tail_rc.clone());
        let mut i = 0;
        loop {
            match cur {
                None => { break; }
                Some(cur_rc) => {
                    let cur_node = cur_rc.borrow();
                    println!("#{}: strong_count={} weak_count={} {:?}",
                             i,
                             Rc::strong_count(&cur_rc),
                             Rc::weak_count(&cur_rc),
                             cur_node.elem);
                    cur = cur_node.prev.as_ref().map(|prev_rc| prev_rc.clone());
                    i += 1;
                }
            }
        }
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        while self.pop_front().is_some() {}
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        self.0.pop_front()
    }
}

impl<T> DoubleEndedIterator for IntoIter<T> {
    fn next_back(&mut self) -> Option<T> {
        self.0.pop_back()
    }
}

#[cfg(test)]
mod test {
    use super::List;

    #[test]
    fn print_items() {
        let mut list = List::new();
        println!("Pushing first item on list.");
        list.push_front(1);
        println!("Items in list from head to tail:");
        list.print_items_forward();
        println!("Pushing second item on list.");
        list.push_front(2);
        println!("Items in list from head to tail:");
        list.print_items_forward();
        println!("Pushing third item on list.");
        list.push_front(3);
        println!("Items in list from head to tail:");
        list.print_items_forward();
        println!("Items in list from tail to head:");
        list.print_items_reverse();
    }

    #[test]
    fn push_front_pop_front() {
        let mut list = List::new();
        assert!(list.head.is_none());
        assert!(list.tail.is_none());
        list.push_front(('a', 1));
        assert!(list.head.is_some());
        assert!(list.tail.is_some());
        let item = list.pop_front().unwrap();
        assert_eq!(item, ('a', 1));
        assert!(list.head.is_none());
        assert!(list.tail.is_none());
    }

    #[test]
    fn basics() {
        let mut list = List::new();
        // Check empty list behaves right
        assert_eq!(list.pop_front(), None);

        // Populate list
        list.push_front(1);
        list.push_front(2);
        list.push_front(3);

        // Check normal removal
        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(list.pop_front(), Some(2));

        // Push some more just to make sure nothing's corrupted
        list.push_front(4);
        list.push_front(5);

        // Check normal removal
        assert_eq!(list.pop_front(), Some(5));
        assert_eq!(list.pop_front(), Some(4));

        // Push to the back
        list.push_back(8);
        list.push_back(9);
        assert_eq!(*list.peek_back_ref().unwrap(), 9);
        assert_eq!(list.peek_back_copy().unwrap(), 9);
        assert_eq!(list.pop_back(), Some(9));
        assert_eq!(*list.peek_back_ref().unwrap(), 8);
        *list.peek_back_ref_mut().unwrap() = 7;
        assert_eq!(list.pop_back(), Some(7));

        // Now exactly one item in the list: 1
        assert_eq!(list.peek_front_copy().unwrap(), 1);
        *list.peek_front_ref_mut().unwrap() = 0;
        assert_eq!(list.peek_front_copy().unwrap(), 0);

        // Check exhaustion
        assert_eq!(list.pop_front(), Some(0));
        assert_eq!(list.pop_front(), None);

        // Check exhaustion from back
        list.push_front(6);
        assert_eq!(list.pop_back(), Some(6));
        assert_eq!(list.pop_back(), None)
    }

    #[test]
    fn peek() {
        let mut list = List::new();
        list.push_front(1);
        list.push_front(2);
        list.push_front(3);
        assert_eq!(*list.peek_front_ref().unwrap(), 3);
        assert_eq!(list.peek_front_copy().unwrap(), 3);
    }

    #[test]
    fn into_iter() {
        let mut list = List::new();
        list.push_front(1);
        list.push_front(2);
        list.push_front(3);
        for (i, v) in list.into_iter().enumerate() {
            println!("#{}: {}", i, v);
            match i {
                0 => assert_eq!(v, 3),
                1 => assert_eq!(v, 2),
                2 => assert_eq!(v, 1),
                _ => unreachable!(),
            }
        }
    }
}
