//! "An Unsafe Queue"
//! https://rust-unofficial.github.io/too-many-lists/fifth.html
use std::ptr;

pub struct List<T> {
    head: Link<T>,
    tail: *mut Node<T>, // DANGER! Unsafe!
}

type Link<T> = Option<Box<Node<T>>>;

struct Node<T> {
    elem: T,
    next: Link<T>,
}

impl<T> List<T> {
    pub fn new() -> Self {
        List {
            head: None,
            tail: ptr::null_mut(),
        }
    }

    /// Push an element on the back of the list
    pub fn push(&mut self, elem: T) {
        let mut new_tail_box = Box::new(Node {
            elem,
            next: None,
        });
        let new_tail_mut_ptr: *mut Node<T> = &mut *new_tail_box;

        if !self.tail.is_null() {
            unsafe { (*self.tail).next = Some(new_tail_box); }
        } else {
            self.head = Some(new_tail_box);
        }
        self.tail = new_tail_mut_ptr;
    }

    /// Pop one item from the front of the list
    pub fn pop(&mut self) -> Option<T> {
        self.head.take().map(|head_box| {
            let head_node = *head_box;
            self.head = head_node.next;

            if self.head.is_none() {
                self.tail = ptr::null_mut();
            }

            head_node.elem
        })
    }
}

mod test {
    #[allow(unused_imports)]
    use super::List;

    #[test]
    fn basics() {
        let mut list = List::new();

        // Empty list works Ok
        assert_eq!(list.pop(), None);

        list.push(1);
        list.push(2);
        list.push(3);

        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), Some(2));

        list.push(4);

        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(4));
        assert_eq!(list.pop(), None);
    }
}
