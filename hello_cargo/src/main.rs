fn main() {
    demo_type_inference();
    println!("");

    demo_ownership();
    println!("");

    demo_borrowing();
    println!("");

    demo_immutable();
    println!("");

    demo_borrow_checker();
}

fn demo_type_inference() {
    let a = 1; // inferred u8
    let b = 2; // inferred u8
    let (c, s) = add_8bits(a, b);
    println!("{} + {} == {} {}", a, b, c, s);
    // 1 + 2 == 3 booyeah
}

fn add_8bits(a: u8, b: u8) -> (u8, String) {
    let result = a + b;
    (result, "booyeah".to_string())
}

fn demo_ownership() {
    let a = Acorn::new(42);
    acorn_taker(a);
    println!("After acorn_taker()");
    // a.identify(); <-- compile error!
}

struct Acorn {
    id: u32,
}

impl Acorn {
    fn new(id: u32) -> Acorn {
        Acorn { id }
    }

    fn identify(&self) {
        println!("I am acorn #{}", self.id);
    }
}

impl Drop for Acorn {
    fn drop(&mut self) {
        println!("I'm falling!");
    }
}

fn acorn_taker(acorn: Acorn) {
    acorn.identify();
    // Acorn gets dropped here!
}

fn demo_borrowing() {
    let a = Acorn::new(42);
    acorn_borrower(&a);
    println!("After acorn_borrower()");
}

fn acorn_borrower(acorn: &Acorn) {
    acorn.identify();
}

fn demo_immutable() {
    let a = 1;
    // a +=1; <-- compile error!
    let mut b = 2;
    b += 1;
    println!("a={}, b={}", a, b);
}

#[allow(unused_variables)]
fn demo_borrow_checker() {
    let a = 1;
    let ref1_a = &a;
    let ref2_a = &a;

    let mut b = 2;
    let ref1_b = &mut b;
    // let ref2_b = &mut b; <-- compile error
    *ref1_b += 1;
}
