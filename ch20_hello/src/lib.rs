use std::sync::{Arc, mpsc, Mutex};
use std::thread;

pub type ReturnCode = i32; // Send + 'static

trait FnBox {
    fn call_box(self: Box<Self>) -> ReturnCode;
}

impl<F: FnOnce() -> ReturnCode> FnBox for F {
    fn call_box(self: Box<F>) -> ReturnCode {
        (*self)()
    }
}

#[derive(Debug)]
struct Worker {
    id: usize,
    thread_handle: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread_handle = thread::Builder::new()
            .name(format!("worker-{:02}", id).into())
            .spawn(move || {
                loop {
                    let message = receiver.lock().unwrap().recv().unwrap();
                    if !Worker::handle_message(id, message) {
                        break;
                    }
                }
            })
            .unwrap();
        Worker {
            id,
            thread_handle: Some(thread_handle),
        }
    }

    /// Return true to continue loop, false to exit
    fn handle_message(id: usize, message: Message) -> bool {
        match message {
            Message::NewJob(job) => {
                println!("Worker {:02} starting job", id);
                let rc = job.call_box();
                println!("Worker {:02} finished job -> {}", id, rc);
                true
            },
            Message::Terminate => {
                println!("Worker {:02} terminated", id);
                false
            },
        }
    }
}

type Job = Box<dyn FnBox + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

pub struct ThreadPool {
    sender: mpsc::Sender<Message>,
    workers: Vec<Worker>,
}

impl ThreadPool {
    /// Create a new ThreadPool
    ///
    /// pool_size: Number of workers in the pool.
    ///
    /// # Panics
    /// `new` will panic if `pool_size` is zero.
    pub fn new(pool_size: usize) -> ThreadPool {
        assert!(pool_size > 0);
        let (sender, receiver) = mpsc::channel::<Message>();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(pool_size);

        for id in 0..pool_size {
            workers.push(Worker::new(id, receiver.clone()));
        }

        ThreadPool { sender, workers }
    }

    pub fn execute<F>(&self, threadfunc: F)
        where F: FnOnce() -> ReturnCode + Send + 'static  {
        let job = Box::new(threadfunc);
        let message = Message::NewJob(job);
        self.sender.send(message).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending Terminate message to {} workers", self.workers.len());
        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down {} workers", self.workers.len());
        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);
            if let Some(thread_handle) = worker.thread_handle.take() {
                thread_handle.join().unwrap()
            }
        }
    }
}
