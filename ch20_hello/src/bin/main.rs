use std::fs;
use std::io::prelude::*;
use std::net::TcpStream;
use std::net::TcpListener;
use std::thread;
use std::time::Duration;

use ch20_hello::{ReturnCode, ThreadPool};

fn main() {
    let listen_addr = "127.0.0.1:9000";
    let listener = TcpListener::bind(listen_addr).unwrap();
    println!("Listening for connections to {}", listen_addr);
    let pool = ThreadPool::new(4);

    // .take(2) makes it do a graceful shutdown after two requests
    for stream in listener.incoming().take(2) {
        let stream = stream.unwrap();
        println!("\nConnection from {:?}\n", stream.peer_addr().unwrap());
        pool.execute(|| {
            handle_connection(stream)
        });
    }
    println!("Shutting down listener.");

    fn handle_connection(mut stream: TcpStream) -> ReturnCode {
        let mut buffer = [0; 512];
        let n = stream.read(&mut buffer).unwrap();
        println!("Request:\n{}", String::from_utf8_lossy(&buffer[..n]));

        let get_root = b"GET / HTTP/1.1\r\n";
        let get_sleep = b"GET /sleep HTTP/1.1\r\n";
        let (rc, header, content) = if buffer.starts_with(get_root) {
            let header = "HTTP/1.1 200 OK\r\n\r\n";
            let content = fs::read_to_string("hello.html").unwrap();
            (200, header, content)
        } else if buffer.starts_with(get_sleep) {
            thread::sleep(Duration::from_secs(5));
            let header = "HTTP/1.1 200 OK\r\n\r\n";
            let content = fs::read_to_string("hello.html").unwrap();
            (200, header, content)
        } else {
            let header = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
            let content = fs::read_to_string("404.html").unwrap();
            (404, header, content)
        };

        let content = content.as_bytes();
        println!("Response:\n{:?} ({} bytes content)", header, content.len());

        stream.write(header.as_bytes()).unwrap();
        stream.write(content).unwrap();
        stream.flush().unwrap();
        rc
    }
}
