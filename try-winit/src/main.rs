//! Graphics experiments with winit and wgpu
use try_winit::run;

fn main() {
    println!("try-winit");
    pollster::block_on(run());
}
