// Ch. 12 of the Rust Book: minigrep project
use std::error::Error;
use std::env;

use minigrep::{Config, run};

fn main() -> Result<(), Box<dyn Error>> {
    // Note: Even though the env::args() iterator is consumed by the parse()
    // method, env::args() can be called again to get a fresh iterator that
    // still has the original command-line arguments.

    let argv: Vec<String> = env::args().collect();
    let config = Config::parse(argv.iter())?;
    run(&config)?;

    Ok(())
}
