//! Implements a simple file searcher and pattern matcher
use std::env;
use std::error::Error;
use std::fs;

#[derive(Debug)]
pub struct Config {
    query: String,
    filename: String,
    case_sensitive: bool,
}

impl Config {
    pub fn parse<'a>(mut args: impl Iterator<Item=&'a String>)
        -> Result<Config, &'static str>
    {
        args.next();  // Consume the program name
        let query = match args.next() {
            Some(arg) => arg.clone(),
            None => return Err("Missing query parameter"),
        };
        let filename = match args.next() {
            Some(arg) => arg.clone(),
            None => return Err("Missing filename parameter"),
        };
        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();
        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
}

pub fn run(config: &Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename.as_str())?;

    let results = if config.case_sensitive {
        search_case_sensitive(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };

    for line in results {
        println!("{}", line);
    }

    Ok(())
}

/// Perform a case-sensitive search on lines of text in a str
///
/// # Examples
/// ```
/// use minigrep;
/// let s = "One by one\nthey go\nonto the blue water";
/// assert_eq!(minigrep::search_case_insensitive("on", s),
///            vec!["One by one", "onto the blue water"]);
/// ```
pub fn search_case_sensitive<'a>(query: &str, contents: &'a str)
    -> Vec<&'a str>
{
    contents.lines()
        .filter(|line| line.contains(query))
        .collect()
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str)
    -> Vec<&'a str>
{
    let query_lc = query.to_lowercase();
    let query_lc_str = query_lc.as_str();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(query_lc_str) {
            results.push(line);
        }
    }

    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_err_if_missing_arg() {
        let argv: Vec<String> = vec!["a".to_string(), "b".to_string()];
        let result = Config::parse(argv.iter());
        println!("result = {:?}", result);
        match result {
            Err(e) => assert_eq!(e, "Missing filename parameter"),
            Ok(_) => panic!("Too few args; should have caused error"),
        }
    }

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(
            vec!["safe, fast, productive."],
            search_case_sensitive(query, contents)
        );
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
