const MESSAGE: &'static str = "
This project has several small example programs.
Look in the 'examples' directory for the list of Rust source files.
To run: cargo run --example <filename-without-path-or-.rs-extension>
";

fn main() {
    print!("{}", MESSAGE);
}
