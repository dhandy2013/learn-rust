//! Try the rust_decimal library
//! https://docs.rs/rust_decimal/latest/rust_decimal/
use rust_decimal_macros::dec;

fn main() {
    println!("Trying out the rust_decimal library.");
    let d1 = dec!(1.23);
    let d2 = dec!(1.230);
    println!("Is {d1} == {d2}? {}", d1 == d2);
    println!("{d1} + {d2} == {}", d1 + d2);
}
