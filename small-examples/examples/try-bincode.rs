//! Try the bincode library
//! https://docs.rs/bincode/latest/bincode/
use std::{env, io};

const USAGE: &'static str = "
try-bincode: Save or load data in bincode format

Usage:
    try-bincode <command-name>

List of commands:
    test1   Encode and decode a struct
    test2   Try to encode a struct into too little space
    test3   Try to decode a struct from a bytes with trailing garbage
    test4   Attempt to deserialize garbage bytes into a struct (it 'works')
    test5   Write and read struct with validation number
    test6   Verify that validation number prevents deserializing from garbage
    test7   Serialize unsigned 32-bit integer
";

// Define some nested data structures to serialize/deserialize
#[derive(bincode::Encode, bincode::Decode, Debug)]
struct Asteroid {
    pos: Vec2,
    vel: Vec2,
}

#[derive(bincode::Encode, bincode::Decode, Debug)]
struct Vec2 {
    x: f64,
    y: f64,
}

impl HasValidationNum for Asteroid {
    fn validation_num() -> u32 {
        0xfeedf00d
    }
}

/// Implement this trait on structs that also implement bincode::Encode and
/// bincode::Decode in order to give instances of this struct a stable
/// "type-code" that is encoded along with the struct data, to validate that the
/// byte stream contains a real instance of the struct.
pub trait HasValidationNum {
    /// Return a constant, random 32-bit unsigned integer identifying the struct
    fn validation_num() -> u32;
}

impl<T> HasValidationNum for &T
where
    T: HasValidationNum,
{
    fn validation_num() -> u32 {
        T::validation_num()
    }
}

fn main() -> io::Result<()> {
    let mut command = String::new();
    for (i, arg) in env::args().enumerate() {
        match i {
            0 => continue, // skip program name
            1 => {
                command = arg;
            }
            _ => {
                eprint!("{}", USAGE);
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Too many arguments",
                ));
            }
        }
    }
    match command.as_str() {
        "" | "help" | "--help" => {
            print!("{}", USAGE);
            Ok(())
        }
        "test1" => test1(),
        "test2" => test2(),
        "test3" => test3(),
        "test4" => test4(),
        "test5" => test5(),
        "test6" => test6(),
        "test7" => test7(),
        other => {
            eprint!("{}", USAGE);
            Err(io::Error::new(
                io::ErrorKind::Other,
                format!("Unknown command: {other}"),
            ))
        }
    }
}

/// Write an Asteroid struct into a Vec and report its size and contents.
/// Read the Asteroid from a byte slice and verify that the slice size is
/// automatically updated to remove the bytes read.
fn test1() -> io::Result<()> {
    let a = Asteroid {
        pos: Vec2 { x: 0.0, y: 0.0 },
        vel: Vec2 { x: 3.0, y: 4.0 },
    };
    println!("Original value: {a:#?}");

    let mut writebuf = Vec::<u8>::with_capacity(100);
    let config = bincode::config::standard();
    let n = bincode::encode_into_std_write(&a, &mut writebuf, config)
        .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    println!("Encoded into {n} bytes");
    println!("Write buffer contents: {writebuf:?}");

    let mut readslice = &writebuf[..];
    let b =
        bincode::decode_from_std_read::<Asteroid, _, _>(&mut readslice, config)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    println!("Decoded value: {b:#?}");
    println!("Leftover bytes after decoding: {readslice:?}");

    Ok(())
}

/// Write an Asteroid struct into a buffer just too small to receive it.
/// bincode correctly reports the error, but the buffer is filled with a
/// partial write.
fn test2() -> io::Result<()> {
    let a = Asteroid {
        pos: Vec2 { x: 0.0, y: 0.0 },
        vel: Vec2 { x: 3.0, y: 4.0 },
    };
    println!("Original value: {a:#?}");

    let mut writebuf = [0xFFu8; 31]; // one byte too short!
    let mut writeslice = &mut writebuf[..];
    let config = bincode::config::standard();
    let result = bincode::encode_into_std_write(&a, &mut writeslice, config);
    match result {
        Ok(n) => {
            println!("Encoded into {n} bytes");
        }
        Err(err) => {
            println!("Error while encoding: {err}");
            println!("Write buffer contents: {writebuf:?}");
        }
    }

    Ok(())
}

/// Write a struct into a buffer with extra trailing 0xFF bytes.
/// This correctly deserializes the struct and reports the trailing bytes.
/// This means you can use bincode to frame messages.
fn test3() -> io::Result<()> {
    let a = Asteroid {
        pos: Vec2 { x: 0.0, y: 0.0 },
        vel: Vec2 { x: 3.0, y: 4.0 },
    };
    println!("Original value: {a:#?}");

    let mut writebuf = [0xffu8; 37];
    let mut writeslice = &mut writebuf[..];
    let config = bincode::config::standard();
    let n = bincode::encode_into_std_write(&a, &mut writeslice, config)
        .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    println!("Encoded into {n} bytes");

    let mut readslice = &writebuf[..];
    let b =
        bincode::decode_from_std_read::<Asteroid, _, _>(&mut readslice, config)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    println!("Decoded value: {b:#?}");
    println!("Leftover bytes: {readslice:?}");

    Ok(())
}

/// Deserialize a buffer full of 0xFF bytes into an Asteroid struct.
/// Beware! It "works" but all of the floating point values are NaN.
fn test4() -> io::Result<()> {
    let writebuf = [0xffu8; 37]; // a buffer full of garbage bytes

    let mut readslice = &writebuf[..];
    let config = bincode::config::standard();
    let b =
        bincode::decode_from_std_read::<Asteroid, _, _>(&mut readslice, config)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    println!("Decoded value: {b:#?}");
    println!("Leftover bytes: {readslice:?}");

    Ok(())
}

/// Write an an Asteroid object with its known validation "magic number".
/// Measure how many bytes that takes, and read it back again.
fn test5() -> io::Result<()> {
    let a = Asteroid {
        pos: Vec2 { x: 0.0, y: 0.0 },
        vel: Vec2 { x: 3.0, y: 4.0 },
    };
    println!("Original value: {a:#?}");

    let mut writebuf = Vec::<u8>::with_capacity(100);
    let n = write_validated_struct(&mut writebuf, &a)?;
    println!("Encoded into {n} bytes");
    println!("Write buffer contents: {writebuf:?}");
    assert_eq!(n, writebuf.len());

    let mut readslice = &writebuf[..];
    let b = read_validated_struct::<Asteroid, _>(&mut readslice)?;
    println!("Decoded value: {b:#?}");
    println!("Leftover bytes after decoding: {readslice:?}");

    Ok(())
}

/// Verify that reading and writing an Asteroid along with its validation number
/// won't allow deserialization of garbage bytes not containing the magic number
/// in the right position.
fn test6() -> io::Result<()> {
    let writebuf = [0u8; 37]; // a buffer full of garbage bytes

    let mut readslice = &writebuf[..];
    let b = read_validated_struct::<Asteroid, _>(&mut readslice)?;
    println!("Decoded value: {b:#?}");
    println!("Leftover bytes: {readslice:?}");

    Ok(())
}

/// Why does the validation number, which is an unsigned 32-bit integer,
/// take 5 bytes instead of 4?
fn test7() -> io::Result<()> {
    let mut writebuf = Vec::<u8>::with_capacity(100);
    let config = bincode::config::standard();
    let code: u32 = 0xdeadbeef;
    let n = bincode::encode_into_std_write(code, &mut writebuf, config)
        .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    println!(
        "Serializing the 32-bit unsigned integer 0x{code:04x}: {n} bytes:"
    );
    println!("{writebuf:?}");
    Ok(())
}

fn write_validated_struct<
    D: bincode::Encode + HasValidationNum,
    W: io::Write,
>(
    dst: &mut W,
    obj: D,
) -> io::Result<usize> {
    let config = bincode::config::standard();
    let validation_num = D::validation_num();
    let n1 = bincode::encode_into_std_write(validation_num, dst, config)
        .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    let n2 = bincode::encode_into_std_write(obj, dst, config)
        .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    Ok(n1 + n2)
}

fn read_validated_struct<D: bincode::Decode + HasValidationNum, R: io::Read>(
    src: &mut R,
) -> io::Result<D> {
    let config = bincode::config::standard();
    let received_validation_num: u32 =
        bincode::decode_from_std_read(src, config)
            .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
    if received_validation_num != D::validation_num() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "Validation number mismatch: \
                received hex {received_validation_num:04x}, expected {:04x}",
                D::validation_num()
            ),
        ));
    }
    bincode::decode_from_std_read::<D, _, _>(src, config)
        .map_err(|err| io::Error::new(io::ErrorKind::Other, err))
}
