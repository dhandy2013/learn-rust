extern crate proc_macro;

use crate::proc_macro::TokenStream;
use quote::quote;
use syn;
use syn::punctuated::Pair;
use syn::Ident;

#[proc_macro_derive(HelloMacro)]
pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    // Create Rust syntax tree
    let ast = syn::parse(input)
        .expect("Error parsing token string while deriving HelloMacro");

    // Build the trait implementation
    impl_hello_macro(&ast)
}

fn impl_hello_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl HelloMacro for #name {
            fn hello_macro() {
                println!("Hello, Macro! My name is {}",
                         stringify!(#name));
            }
        }
    };
    gen.into()
}

// Docs:
// https://doc.rust-lang.org/reference/procedural-macros.html#attribute-macros
// Also see:
// Brief, incomplete example: https://stackoverflow.com/a/52593373
// https://blog.rust-lang.org/2018/12/21/Procedural-Macros-in-Rust-2018.html
#[proc_macro_attribute]
pub fn timeit(_attr: TokenStream, item: TokenStream) -> TokenStream {
    // The _attr TokenStream is the parameter to the attribute macro itself.
    // e.g. if "#[timeit(x)]" appears in the source, _attr contains x.
    // println!("proc_macro_attribute: timeit: _attr: {}", _attr.to_string());

    // The item TokenStream is what comes after the attribute marker.
    // e.g. for "#[timeit] fn f() {}", item contains "fn f() {}".
    println!("proc_macro_attribute: timeit: item: {}", item.to_string());

    let input_fn =
        if let syn::Item::Fn(fn_data) =
            syn::parse_macro_input!(item as syn::Item) {
        fn_data
    } else {
        panic!("The timeit attribute can only be applied to functions")
    };

    let sig = &input_fn.sig;
    let args = get_func_args(&sig);
    for arg in args.iter() {
        println!("proc_macro_attribute: timeit: arg: {}", arg);
    }

    let outer_name = &sig.ident;
    let inner_name = format!("_timeit_{}", outer_name);
    let inner_ident = syn::Ident::new(&inner_name, outer_name.span());
    let block = &input_fn.block;
    let mut new_sig = sig.clone();
    new_sig.ident = inner_ident.clone();
    let format_str = format!("{} took {{}} ns", outer_name);

    // Emits the exact same code as the original function
    /*
    let generated_code = quote! {
        #input_fn
    };
    */

    // Replaces the original function with one that just returns 42
    /*
    let generated_code = quote! {
        #sig { 42 }
    };
    */

    // Replace the original function with one that gets the starting time in t0,
    // calls a new inner function with the original parameters, stores the
    // result, prints the elapsed time, and returns the result.
    // Then it defines the inner function with a generated name based on the
    // original function name, with the same signature and body.
    // This technique is *not* reliable, because it can only handle very simple
    // function signatures, and not patterns that destructure tuples, etc.
    /*
    let generated_code = quote! {
        #sig {
            let t0 = ::std::time::Instant::now();
            let result = #inner_ident( #(#args ,)* );
            let ns = t0.elapsed().as_nanos();
            println!( #format_str, ns );
            result
        }
        #new_sig
        #block
    };
    */

    // Replace the original function with one just like it, except the original
    // function body is wrapped in a { block } with the result stored in a
    // variable. The starting time is saved before the block is executed, and
    // afterwards the elapsed time is printed, then the result is returned.
    // This should be completely reliable.
    let generated_code = quote! {
        #sig {
            let t0 = ::std::time::Instant::now();
            let result = #block ;
            let ns = t0.elapsed().as_nanos();
            println!( #format_str, ns );
            result
        }
    };

    // Return the generated code, turned into a token stream
    generated_code.into()
}

fn get_func_args(sig: &syn::Signature) -> Vec<Ident> {
    let args = sig.inputs.clone().into_pairs().filter_map(|arg| {
        let arg = match arg {
            Pair::Punctuated(t, _p) => t,
            Pair::End(t) => t,
        };
        let arg = if let syn::FnArg::Typed(pat_type) = arg {
            pat_type.pat
        } else {
            return None;
        };
        let arg = if let syn::Pat::Ident(pat_ident) = *arg {
            pat_ident.ident
        } else {
            return None;
        };
        Some(arg)
    });
    args.collect()
}

// The official docs for proc macros, unfortunately not with complete useful
// examples:
// https://doc.rust-lang.org/reference/procedural-macros.html#function-like-procedural-macros
//
// A bunch of example proc macros:
// https://github.com/dtolnay/proc-macro-workshop
//
// Actual code in the wild declaring proc attribute macros:
// https://github.com/chuck-flowers/attribution/blob/master/attribution-macros/src/lib.rs
