use ch19_hello_macro::HelloMacro;
use ch19_hello_macro_derive::{timeit, HelloMacro};
use core::marker::PhantomData;
use paste::paste;
use quote::quote;

#[derive(HelloMacro)]
struct Pancakes;

/* This is the output of the above macro invocation.
 * It gets emitted right after "struct Pancakes;".
impl HelloMacro for Pancakes {
    fn hello_macro() {
        println!("Hello, Macro! My name is Pancakes!");
    }
}
*/

fn main() {
    println!("Ch19.5 Macros - Procedural Macros");
    try_quote_macro();
    println!("");
    Pancakes::hello_macro();
    println!("");
    let result = arithmetic_function(0);
    println!("arithmetic_function(0) -> {}", result);
    println!("");
    try_simple_vec();
    println!("");
    try_dynamic_pwm_slice();
}

fn try_quote_macro() {
    println!("\nTrying out the quote! macro");

    let a = "blah";
    let b = 2;
    let s = quote! {
        A #a in hand is worth #b in the bush.
    };
    println!("s = {:?}", s);
}

// The timeit macro will destroy this function and replace it with another
#[timeit]
fn arithmetic_function(a: i32) -> i32 {
    let mut result = a;
    let loop_count = 10000;
    for i in 0..loop_count {
        result += loop_count - i;
    }
    result
}

macro_rules! simple_vec {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}

fn try_simple_vec() {
    let v1 = simple_vec!["a", "b", "c"];
    println!("v1 = {:?}", v1);
    let v2 = simple_vec!(1, 2, 3);
    println!("v2 = {:?}", v2);
}

/// This is some experimental code trying to solve a problem
/// with repetitive code in an embedded environment.
fn try_dynamic_pwm_slice() {
    let mut pwm0: DynamicPwmSlice = Slice::to_dyn_pwm(Slice::<Pwm0>::new());
    pwm0.enable_interrupt();

    let mut pwm1: DynamicPwmSlice = Slice::to_dyn_pwm(Slice::<Pwm1>::new());
    pwm1.enable_interrupt();

    let mut pwm2: DynamicPwmSlice = Slice::to_dyn_pwm(Slice::<Pwm2>::new());
    pwm2.enable_interrupt();
}

trait SliceId {
    fn num() -> u32;
}

#[derive(Copy, Clone)]
enum Pwm0 {}
impl SliceId for Pwm0 {
    fn num() -> u32 {
        0
    }
}

#[derive(Copy, Clone)]
enum Pwm1 {}
impl SliceId for Pwm1 {
    fn num() -> u32 {
        1
    }
}

#[derive(Copy, Clone)]
enum Pwm2 {}
impl SliceId for Pwm2 {
    fn num() -> u32 {
        2
    }
}

struct Slice<I>
where
    I: SliceId,
{
    _marker: PhantomData<I>,
}

impl<I> Slice<I>
where
    I: SliceId,
{
    pub fn new() -> Self {
        Self {
            _marker: PhantomData::<I>,
        }
    }
    pub fn enable_interrupt(&mut self) {
        println!("Enabling interrupts on slice {}", I::num());
    }
}

enum DynamicPwmSlice {
    Slice0(Slice<Pwm0>),
    Slice1(Slice<Pwm1>),
    Slice2(Slice<Pwm2>),
}

trait ToDynamicPwmSlice {
    fn to_dyn_pwm(self) -> DynamicPwmSlice;
}

macro_rules! impl_dyn_pwm_slice {
    ( $( $NUM:literal ),* ) => {
        paste! {
            $(
                impl ToDynamicPwmSlice for Slice<[<Pwm $NUM>]> {
                    fn to_dyn_pwm(self) -> DynamicPwmSlice {
                        DynamicPwmSlice::[<Slice $NUM>](self)
                    }
                }
            )*

            impl DynamicPwmSlice {
                pub fn enable_interrupt(&mut self) {
                    match self {
                        $(
                            Self::[<Slice $NUM>](pwm) => pwm.enable_interrupt(),
                        )*
                    }
                }
            }
        }
    };
}

impl_dyn_pwm_slice![0, 1, 2];
