use std::sync::{Arc, Mutex, mpsc};
use std::thread;
use std::time::Duration;

fn main() {
    println!("Ch16: Fearless Concurrency");
    ch16_1_threads();
    ch16_2_message_passing();
    ch16_3_shared_state();
}

fn ch16_1_threads() {
    println!("\nCh16.1 Using Threads to Run Code Simultaneously");
    ch16_1_a_spawn();
    ch16_1_b_move_closures();
}

fn ch16_1_a_spawn() {
    println!("\nCh16.1.a Creating a New Thread with ``spawn``");
    let handle = thread::spawn(|| {
        for i in 0..10 {
            println!("Message #{:03} from the spawned thread", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 0..5 {
        println!("Message #{:03} from the main thread", i);
        thread::sleep(Duration::from_millis(1));
    }
    handle.join().unwrap();
    println!("Done.");
}

fn ch16_1_b_move_closures() {
    println!("\nCh16.1.b Using move Closures with Threads");
    let v = vec![1, 2, 3];
    // Must put ``move`` here or compile error E0373
    let handle = thread::spawn(move || {
        println!("Vector v: {:?}", v);
    });
    handle.join().unwrap();
}

fn ch16_2_message_passing() {
    println!("\nCh16.2 Message Passing");

    let num_msgs = 5;
    let num_senders = 2;
    let (tx0, rx) = mpsc::channel();
    // This ends up creating one extra sender besides the senders used by the
    // threads. But I guess this is Ok because tx0 is available to spawn new
    // receivers on demand.
    for i in 0..num_senders {
        ch16_2_send_thread(mpsc::Sender::clone(&tx0), i, num_msgs);
    }

    // Fun fact: The receive loop continues to run until ALL senders are
    // dropped.  So I have to drop tx0 or the for loop will hang.
    drop(tx0);
    for msg in rx {
        println!("Received: {}", msg);
        thread::sleep(Duration::from_millis(50));
    }
}

fn ch16_2_send_thread(tx: mpsc::Sender<String>, id: u32, num_msgs: u32) {
    // Experiments prove that send finishes immediately even
    // when the receiving thread is sleeping. So there must be some sort
    // of internal queue/buffer. How big is it? What happens when it is
    // full?
    thread::spawn(move || {
        for i in 0..num_msgs {
            let msg = format!("From sender {}: Message #{}", id, i);
            tx.send(msg).unwrap();
            println!("Sender {}: sent #{}", id, i);
        }
    });
}

fn ch16_3_shared_state() {
    println!("\nCh16.3 Shared State");
    ch16_3_a_mutexes();
}

fn ch16_3_a_mutexes() {
    println!("\nCh16.3.a Mutexes");

    // In the same way we used RefCell<T> in Chapter 15 to allow us to mutate
    // contents inside an Rc<T>, we use Mutex<T> to mutate contents inside an
    // Arc<T>.
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }
    println!("Result: {}", *counter.lock().unwrap());
}
