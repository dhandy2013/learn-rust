use ch11_testing;

#[cfg(test)]
#[test]
fn test_main() {
    println!("Hey, you can stick tests in a binary crate too!");
    assert!(true);
}

fn main() {
    println!("Ch11: Testing");
    println!("{}", ch11_testing::greeting("user"));
}
