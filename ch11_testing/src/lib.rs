#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    // You can return an Err result to indicate test failure
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("wrong answer!"))
        }
    }

    #[test]
    // Or you can use one of the assert macros that panics on failure
    fn greeting_contains_name() {
        let result = greeting("Tom");
        assert!(result.contains("Tom"));
    }

    #[test]
    fn largest_int() {
        let number_list1 = vec![34, 50, 25, 100, 65];
        let largest1 = *largest(&number_list1);
        println!("The largest number in list 1 is {}", largest1);
        assert_eq!(largest1, 100);
    }

    #[test]
    fn largest_float() {
        let number_list2 = vec![102.7, 34.5, 6000.1, 89.9, 54.0, 2.0, 43.0];
        let largest2 = *largest(&number_list2);
        println!("The largest number in list 2 is {}", largest2);
        assert_eq!(largest2, 6000.1);
    }

    #[test]
    #[should_panic(expected = "Cannot find largest element of empty list")]
    fn largest_panic_on_empty_slice() {
        let a: Vec<i32> = vec![];
        println!("This causes panic: {}", largest(&a));
    }

    #[test]
    fn internal_adder_works() {
        assert_eq!(internal_adder(3, 5), 8);
    }
}

pub fn add_two(a: i32) -> i32 {
    internal_adder(a, 2)
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

pub fn greeting(name: &str) -> String {
    format!("Hello {}!", name)
}

// Return the largest item in the list.
// Panic if the list is empty.
pub fn largest<T: PartialOrd>(list: &[T]) -> &T {
    if list.len() == 0 {
        panic!("Cannot find largest element of empty list");
    }
    let mut largest_index: usize = 0;
    for i in 1..list.len() {
        if list[i] > list[largest_index] {
            largest_index = i;
        }
    }
    &list[largest_index]
}
