use ch11_testing;

mod common;

#[test]
fn adds_two_correctly() {
    common::setup();
    assert_eq!(4, ch11_testing::add_two(2));
}
