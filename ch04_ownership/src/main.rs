fn main() {
    f1();
    f2();
    f3();
    f4();
    f5();
    f6_more_references();
    f7_slices();
}

fn f1() {
    let mut s = String::from("hello");
    s.push_str(", world!");
    println!("{}", s);
}

fn f2() {
    let s1 = String::from("hello");
    let s2 = s1.clone();  // must use clone because String is on the heap
    println!("s1 = {}, s2 = {}", s1, s2);

    let x = 5;
    let y = x;  // i32 has the Copy trait so no need to clone
    println!("x = {}, y = {}", x, y);
}

fn f3() {
    let s = String::from("hello");
    takes_ownership(s);
    //println!("I say {}", s); // compile error because s is no longer valid

    let x = 5;
    makes_copy(x);
    println!("x = {}", x);  // this works because x is copied not moved
}

fn takes_ownership(s: String) {
    println!("s = {}", s);
}

fn makes_copy(n: i32) {
    println!("n = {}", n);
}

fn f4() {
    let s1 = gives_ownership();
    let s2 = String::from("f4() says: I like 🍕");
    let s3 = takes_and_gives_back(s2);
    println!("s1 = {}", s1);
    //println!("s2 = {}", s2); // compile error because s2 got moved
    println!("s3 = {}", s3);
    let s4 = String::from("🍕");
    let len = calculate_length(&s4); // explicit pass by reference
    println!("The length of '{}' is {}", s4, len);
}

fn gives_ownership() -> String {
    let some_string = String::from("hello from gives_ownership");
    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn calculate_length(s: &String) -> usize {
    s.len()
}

fn f5() {
    let mut s = String::from("abc");
    change(&mut s);
    println!("f5: s = '{}'", s);
    let r1 = &mut s;
    println!("f5: r1 = '{}'", r1);
    // r1 "effectively" out of scope because it isn't used any more
    // so we can create a new mutable reference to s
    let r2 = &mut s;
    println!("f5: r2 = '{}'", r2);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

fn f6_more_references() {
    let ref_returned_from_func = make_ref();
    println!("f6: {}", ref_returned_from_func);
}

// This won't compile
// fn make_ref() -> &String {
//     let s = String::from("abc");
//     &s
// }
fn make_ref() -> String {
    let s = String::from("abc");
    s
}

fn f7_slices() {
    let s = String::from("North Carolina");
    let i = first_word_end(&s[..]);
    println!("f7: Index of end of first word: {}", i);
    let s2 = first_word(&s[..]);
    println!("f7: First word returned as slice: {}", s2);

    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    println!("f7: array slize: {:?}", slice);
}

fn first_word_end(s: &str) -> usize {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    s.len()
}

fn first_word(s: &str) -> &str {
    let i = first_word_end(s);
    &s[..i]
}
