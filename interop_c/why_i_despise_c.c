/*
 * From "The C Programming Language", 2nd edition, p. 156
 *
 * This program was written by the inventors of the C programming language and
 * included as an example in their book describing the language to beginners.
 * The book had been in print for ten years and was on its second edition. And
 * yet it has an easily-triggered bug that causes references to out-of-bounds
 * memory, indicated by this program printing "asdfasdfasdfdeadbeef" at the
 * end.
 *
 * This is an example of why I choose Rust over C/C++ in all new projects.
 */
#include <stdarg.h>
#include <stdio.h>

/* minprintf:  minimal printf with variable argument list */
void minprintf(char *fmt, ...)
{
    va_list ap;   /* points to each unnamed arg in turn */
    char *p, *sval;
    int ival;
    double dval;

    va_start(ap, fmt); /* make ap point to 1st unnamed arg */
    for (p = fmt; *p; p++) {
        if (*p != '%') {
            putchar(*p);
            continue;
        }
        switch (*++p) {
        case 'd':
            ival = va_arg(ap, int);
            printf("%d", ival);
            break;
        case 'f':
            dval = va_arg(ap, double);
            printf("%f", dval);
            break;
        case 's':
            for (sval = va_arg(ap, char *); *sval; sval++)
                putchar(*sval);
            break;
        default:
            putchar(*p);
            break;
        }
    }
    va_end(ap);   /* clean up when done */
}

int main(void)
{
    char formats[][38] = {
        "Hello, %s\n",
        "a = %d\n",
        "pi = %f\n",
        "Oops, percent char at end of format!%", /* 37 chars plus null byte */
        "asdfasdfasdfdeadbeef\n",
    };

    printf("Testing minprintf()\n\n");

    minprintf(formats[0], "world");
    minprintf(formats[1], 1);
    minprintf(formats[2], 3.141592653589793);
    minprintf(formats[3], "goodbye");
    // minprintf("String instead of float: %f\n", "abc");
    // minprintf("Float instead of string: %s\n", 9.5);
    return 0;
}
