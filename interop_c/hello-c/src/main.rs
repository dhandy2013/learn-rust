/// Reproduce the minprintf() example function from:
///     "The C Programming Language", 2nd edition, p. 156
/// Rust doesn't have functions with a variable number of parameters,
/// so we'll emulate it by passing a slice of Param objects.
use std::io;

pub enum Param<'a> {
    Int(i64),
    Float(f64),
    String(&'a str),
}

fn _minprintf(
    out: &mut dyn io::Write,
    format: &str,
    params: &[Param],
) -> io::Result<()> {
    let mut chars = format.chars();
    let mut params = params.iter();
    while let Some(c) = chars.next() {
        if c != '%' {
            write!(out, "{}", c)?;
            continue;
        }
        if let Some(param) = params.next() {
            // Check the placeholder type
            // Ignore parameters of the wrong type
            match chars.next() {
                Some('d') => { 
                    if let Param::Int(i) = param {
                        write!(out, "{}", i)?;
                    } else {
                        write!(out, "%d")?;
                    }
                },
                Some('f') => {
                    if let Param::Float(f) = param {
                        write!(out, "{}", f)?;
                    } else {
                        write!(out, "%f")?;
                    }
                },
                Some('s') => {
                    if let Param::String(s) = param {
                        write!(out, "{}", s)?;
                    } else {
                        write!(out, "%s")?;
                    }
                },
                Some(c) => {
                    write!(out, "%{}", c)?;
                },
                None => {
                    write!(out, "%")?;
                },
            }
        } else {
            // No more parameters, just output the placeholder
            write!(out, "%{}", c)?;
        }
    }
    Ok(())
}

pub fn minprintf(format: &str, params: &[Param]) -> io::Result<()> {
    _minprintf(&mut io::stdout(), &format, &params)
}

fn main() -> io::Result<()> {
    let formats = [
        "Hello, %s\n",
        "a = %d\n",
        "pi = %f\n",
        "Oops, percent char at end of format!%",
        "asdfasdfasdfdeadbeef\n",
    ];

    println!("Testing minprintf()\n");

    minprintf(formats[0], &[Param::String("world")])?;
    minprintf(formats[1], &[Param::Int(1)])?;
    minprintf(formats[2], &[Param::Float(3.141592653589793)])?;
    minprintf(formats[3], &[Param::String("goodbye")])?;
    // minprintf("String instead of float: %f\n", &[Param::String("abc")])?;
    // minprintf("Float instead of string: %s\n", &[Param::Float(9.5)])?;
    Ok(())
}
