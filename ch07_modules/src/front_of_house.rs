// Everything in this file is inside module crate::front_of_house

// This "imports" the contents of front_of_house/hosting.rs as if it was all
// declared right here inside "pub mod hosting { ... }".
pub mod hosting;
