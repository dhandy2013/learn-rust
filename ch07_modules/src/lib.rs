#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

// This "imports" the contents of front_of_house.rs as if it was declared right
// here inside of "mod front_of_house { ... }".
mod front_of_house;

mod util {
    pub fn util_function() {
        println!("Called util_function");
    }
}

mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }

        pub fn eat(&self) -> String {
            format!("Yummy {} toast with {}", self.toast, self.seasonal_fruit)
        }
    }
}

pub fn eat_at_restaurant() {
    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    //meal.seasonal_fruit = String::from("blueberries");

    println!("{}", meal.eat());
}

pub fn walk_into_restaurant() {
    // absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // relative path
    front_of_house::hosting::add_to_waitlist();
}

// This is idiomatic
//use self::front_of_house::hosting;

// This works too but is not idiomatic
use self::front_of_house::hosting::add_to_waitlist as get_in_line;

pub fn walk_into_restaurant2() {
    get_in_line();
}
