fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

    let x: u16 = 65535;
    println!("The value of x is: {}", x);

    let x: f64 = 3.1415927;
    println!("The value of x is: {}", x);

    let x: bool = true;
    println!("The value of x is: {}", x);

    let heart_eyed_cat = '😻'; // single character (Unicode Scalar Value)
    println!("The cat said: {}", heart_eyed_cat);

    let tup = (500, 6.4, 1);
    println!("The value of tup is: {:#?}", tup);
    println!("The value of tup.0 is: {}", tup.0);
    let (x, y, z) = tup;
    println!("The value of x, y, z is: {}, {}, {}", x, y, z);

    let a = [10, 20, 30, 40, 50];
    println!("2nd item: {}", a[1]);
    let i = 5;
    if i < 5 {
        println!("Item #{} is {}:", i, a[i]);
    } else {
        println!("i is too big for array a");
    }

    println!("Calling func1(): {}", func1(3));

    println!("Calling func2(): {:?}", func2(3, 4));

    try_loop0(3);
    try_loop1();
    try_loop2();
}

fn func1(x: i32) -> i32 {
    x + 1
}

fn func2(x: i32, y: i32) -> (i32, i32) {
    (y, x)
}

fn try_loop0(n: i32) {
    let mut count = n;
    loop {
        println!("{}!", count);
        count -= 1;
        if count <= 0 {
            break
        }
    }
    println!("LIFTOFF!!!");
}

fn try_loop1() {
    let mut number = 3;

    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }

    println!("LIFTOFF!!!");
}

fn try_loop2() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
