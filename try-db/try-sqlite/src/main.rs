use clap::Parser;
use rusqlite::{self, types::FromSql, Connection, Result};
use std::path::PathBuf;

#[derive(Parser)]
struct Cli {
    path: Option<PathBuf>,
}

fn main() -> Result<()> {
    let args = Cli::parse();
    println!("sqlite3 version: {}", rusqlite::version());
    let conn = if let Some(path) = &args.path {
        Connection::open(path)?
    } else {
        Connection::open_in_memory()?
    };
    // Just to prove we have modern_sqlite feature enabled.
    println!("conn.transaction_state: {:?}", conn.transaction_state(None));

    println!("\nsqlite3 compile options:");
    conn.pragma_query(None, "compile_options", |row| {
        let compile_option = row.get::<_, String>(0)?;
        println!("{compile_option}");
        Ok(())
    })?;

    println!("\nOther PRAGMA values:");
    // https://sqlite.org/pragma.html#pragma_encoding
    // Database text encoding. The only *sane* value is UTF-8, but UTF-16 is
    // possible. Once it is set for a database it cannot be changed.  An attempt
    // to ATTACH a database with a different text encoding from the "main"
    // database will fail.
    println!(
        "encoding = {}",
        get_global_pragma::<String>(&conn, "encoding")?
    );
    // Boolean indicating whether foreign key constraints are enforced for this
    // database connection.
    println!(
        "foreign_keys = {}",
        get_global_pragma::<bool>(&conn, "foreign_keys")?
    );
    // https://sqlite.org/pragma.html#pragma_trusted_schema
    // "... all applications are encouraged to switch this setting off on every
    // database connection as soon as that connection is opened."
    println!(
        "trusted_schema = {}",
        get_global_pragma::<bool>(&conn, "trusted_schema")?
    );

    // PRAGMA table_list is new in sqlite 3.37.0 (2021-11-27)
    println!("\nTable list:");
    conn.pragma_query(None, "table_list", |row| {
        let schema = row.get::<_, String>("schema")?;
        let name = row.get::<_, String>("name")?;
        let objtype = row.get::<_, String>("type")?;
        let strict = row.get::<_, bool>("strict")?;
        let fullname = format!("{schema}.{name}");
        println!("{fullname:30} type={objtype} strict={strict}");
        Ok(())
    })?;

    Ok(())
}

fn get_global_pragma<T: FromSql>(
    conn: &Connection,
    pragma_name: &str,
) -> Result<T> {
    conn.pragma_query_value(None, pragma_name, |row| row.get::<_, T>(0))
}
