fn main() {
    println!("Ch18: Patterns and Matching");
    ch18_1_if_let();
    ch18_1_while_let();
    ch18_1_for_loops();
    ch18_1_let_statements();
    ch18_1_function_parameters();
    ch18_3_pattern_syntax();
}

fn ch18_1_if_let() {
    println!("\nCh18.1: if let");
    let favorite_color: Option<&str> = Some("yellow"); // or None;
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    let chosen_color = if let Some(color) = favorite_color {
        println!("Using favorite color {}", color);
        color
    } else if is_tuesday {
        println!("Tuesday is green day");
        "green"
    } else if let Ok(age) = age {
        if age > 30 {
            println!("Over 30, using purple");
            "purple"
        } else {
            println!("Under 30, using orange");
            "orange"
        }
    } else {
        println!("If all else fails, use blue");
        "blue"
    };
    println!("The final chosen color is: {}", chosen_color);

    // warning: irrefutable if-let pattern
    // note: #[warn(irrefutable_let_patterns)] on by default
    /*
    if let _x = 12 {
        println!("Yes it is 12!");
    }
    */
}

fn ch18_1_while_let() {
    println!("\nCh18.1: while let");
    let mut stack = Vec::new();
    for i in 0..5 {
        stack.push(i);
    }
    while let Some(item) = stack.pop() {
        println!("{}", item);
    }
}

fn ch18_1_for_loops() {
    println!("\nCh18.1: for loops");
    let v = vec!['a', 'b', 'c'];
    for (index, value) in v.iter().enumerate() {
        println!("#{}: {}", index, value);
    }
}

fn ch18_1_let_statements() {
    println!("\nCh18.1: let statements");
    let (x, y, _) = (1, 2, 3);
    println!("x = {}, y = {}", x, y);
    // error[E0005]: refutable pattern in local binding: `None` not covered
    // let Some(_z) = Some(9);
}

fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Location: x: {}, y: {}", x, y);
}

fn ch18_1_function_parameters() {
    println!("\nCh18.1: function parameters");
    let point = (7, 9);
    print_coordinates(&point);
}

fn ch18_3_pattern_syntax() {
    println!("\nCh18.3: pattern syntax");
    ch18_3_a_matching_literals();
    ch18_3_b_variable_shadowing();
    ch18_3_c_multiple_patterns();
    ch18_3_d_dot_dot_dot();
    ch18_3_e_destructuring_structs();
    ch18_3_f_destructuring_enums();
    ch18_3_g_match_guards();
    ch18_3_h_at_bindings();
}

fn ch18_3_a_matching_literals() {
    println!("\nCh18.3a matching literals");
    let x = 1;
    match x {
        1 => println!("one"),
        2 => println!("two"),
        _ => println!("many"),
    }
}

fn ch18_3_b_variable_shadowing() {
    println!("\nCh18.3b pattern matching and variable shadowing");
    let x = Some(5);
    let y = 10;
    match x {
        Some(50) => println!("Matched, value was exactly 50"),
        Some(y) => println!("Matched, y={:?}", y),
        _ => println!("Default, x={:?}", x),
    }
    println!("After all is said and done, x={:?}, y={:?}", x, y);
}

fn ch18_3_c_multiple_patterns() {
    println!("\nCh18.3c matching multiple patterns");
    let x = 1;
    match x {
        1 | 2 => println!("one or two"),
        3 => println!("three"),
        _ => println!("otherwise"),
    }

    // What about ``|`` in  bitwise operations?
    let a: u8 = 0x0f;
    let b: u8 = 0xf0;
    let y = a | b;
    println!("{:02x} | {:02x} == {:02x}", a, b, y);
    // This works, so I guess we can't have general numeric expressions
    // for match patterns. ``|`` behaves differently in a match pattern than in
    // an arithmetic/logical expression.
}

fn ch18_3_d_dot_dot_dot() {
    println!("\nCh13.8d dot dot dot pattern matching");
    let x = 5;
    match x {
        // You can remove the spaces on either side of "..." and it will
        // compile, but that is not the style used in "The Book".
        1 ... 5 => println!("one through five inclusive"),
        // two dots is not an option, gets a compile error
        // 6 .. 10 => println!("six through ten not including ten"),
        _ => println!("otherwise"),
    }

    // dot dot is a range not a pattern
    // You can put spaces on either side of ".." but that is not the style used
    // in "The Book".
    for i in 0..4 {
        println!("i = {:?}", i);
    }

    // You can match on Unicode Scalar Values similar to matching numbers
    let s = String::from("A bird in Hand is better than 2 in the bush!");
    for c in s.as_str().chars() {
        match c {
            'A' ... 'Z' => println!("{:?}: uppercase letter", c),
            'a' ... 'z' => println!("{:?}: lowercase letter", c),
            '0' ... '9' => println!("{:?}: ASCII digit", c),
            _ => println!("{:?}: some non-alphanumeric character", c),
        }
    }
}

struct Point {
    x: i32,
    y: i32,
    z: f64,
}

fn ch18_3_e_destructuring_structs() {
    println!("\nCh18.3e destructuring structs (and tuples)");

    let p1 = Point { x: 0, y: 7, z: -1.0 };
    let Point { x: a, y: b, z: _ } = p1;
    assert_eq!(0, a);
    assert_eq!(7, b);
    // To make "field is never used" compiler warning go away:
    assert_eq!(-1.0, p1.z);

    let p2 = Point { x: 2, y: 4, z: 12.5 };
    let Point { x, y, .. } = p2;
    println!("x = {}, y = {}", x, y);

    let p3 = Point { x: 0, y: 7, z: -9.7 };
    match p3 {
        Point { x, y: 0, .. } => println!("On the x-axis at {}", x),
        Point { x: 0, y, .. } => println!("On the y-axis at {}", y),
        Point { x, y, .. } => println!("On the plane at {:?}", (x, y)),
    }

    let numbers = (2, 4, 8, 16, 32);
    match numbers {
        (first, .., last) => {
            println!("first number: {}, last number: {}", first, last);
        },
    }
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
    Hello { id: i32 },
}

fn ch18_3_f_destructuring_enums() {
    println!("\nCh18.3.f destructuring enums");

    let messages = [
        Message::Move { x: 160, y: 100 },
        Message::ChangeColor(0, 180, 0),
        Message::Write("hello".to_string()),
        Message::Quit,
    ];

    for msg in messages.iter() {
        match msg {
            Message::Quit => {
                println!("Quit message has no data");
            },
            Message::Move { x, y } => {
                println!("Move by {} in x and by {} in y direction", x, y);
            },
            Message::Write(text) => println!("Text message: {}", text),
            Message::ChangeColor(r, g, b) => {
                println!("Change color to red {}, green {}, blue {}", r, g, b);
            }
            _ => println!("Unknown message type."),
        }
    }

    // You have to take a reference to avoid moving the data out of the array.
    // I guess iter() does that automatically?
    if let Message::Write(s) = &messages[2] {
        println!("The third message was: {}", s);
    }

    let mut setting_value = Some(5);
    let new_setting_value = Some(10);
    match (setting_value, new_setting_value) {
        (Some(_), Some(_)) => {
            println!("Can't overwrite an existing non-None value");
        }
        _ => {
            setting_value = new_setting_value;
        }
    }
    println!("setting_value == {:?}", setting_value);
}

fn ch18_3_g_match_guards() {
    println!("\nCh18.3g match guards");
    let num: Option<i32> = None;
    match num {
        Some(x) if x < 5 => println!("An x less thatn five: {}", x),
        Some(x) => println!("An x value: {}", x),
        None => println!("None"),
    }

    let x = 4;
    let y = false;
    match x {
        // the "if" applies to the whole "4 | 5 | 6" pattern
        4 | 5 | 6 if y => println!("yes"),
        _ => println!("no"),
    }
}

fn ch18_3_h_at_bindings() {
    println!("\nCh18.3h @bindings");
    let msg = Message::Hello { id: 5 };
    match msg {
        // Note three different ways to match ID inside Hello message
        // Reminder: "..." is an inclusive range operator, includes 3 and 7
        Message::Hello { id: id_variable @ 3...7 } => {
            println!("First range matched, Hello id: {}", id_variable);
        },
        Message::Hello { id: 10...12 } => {
            // ``id`` field is not available here!
            println!("Second range matched");
        }
        Message::Hello { id } => {
            println!("No range matched, Hello id: {}", id);
        }
        _ => {
            println!("Not a Hello message.");
        }
    }
}
