/// Cross casting of object safe trait objects
/// Adapted from this helpful post by SkiFire13 on the Rust users forum:
/// https://users.rust-lang.org/t/cross-casting-of-object-safe-trait-objects/100235/20
use std::{any, rc};

const TRACE_TYPES: bool = false;

pub trait DynCast {
    /// Clients don't implement or call this method directly.
    /// This method is implemented by the `impl_dyncast!` macro.
    /// It is called by the `dyn_cast` method on the DynCastExt trait which is
    /// automatically implmented for all types that implement this trait.
    ///
    /// `out` is supposed to point to an `Option<Rc<dyn Trait>>` which `self`
    /// will be `dyn_cast`ed to. `Self` is something like `dyn SomeTrait`.
    fn dyn_cast_raw(self: rc::Rc<Self>, out: &mut dyn any::Any);
}

/// Non-object safe trait automatically implemented for all `DynCast` that gives
/// `DynCast` objects a friendlier `dyn_cast` method.
pub trait DynCastExt {
    fn dyn_cast<T: ?Sized + 'static>(self: rc::Rc<Self>) -> Option<rc::Rc<T>>;
}

impl<D: ?Sized + DynCast> DynCastExt for D {
    // This method is always statically dispatched.
    // Sometimes Self is the original concrete type, sometimes it is `dyn SourceTrait`.
    // `T` is always `dyn TargetTrait`.
    fn dyn_cast<T: ?Sized + 'static>(self: rc::Rc<Self>) -> Option<rc::Rc<T>> {
        if TRACE_TYPES {
            let d_type_name = any::type_name::<D>();
            let t_type_name = any::type_name::<T>();
            let self_type_name = any::type_name::<Self>();
            println!("{d_type_name}::dyn_cast: T={t_type_name} Self={self_type_name}");
        }
        let mut out: Option<rc::Rc<T>> = None;
        // If `Self` is `dyn TargetTrait`, this method is dynamically dispatched.
        // But once inside the implemetation of `dyn_cast_raw`, `Self` is the original
        // concrete type. That is what makes this casting technique work.
        self.dyn_cast_raw(&mut out);
        out
    }
}

#[allow(unused_macros)]
macro_rules! impl_dyncast {
    ($source:ty : $($target:path),+ $(,)?) => {
        impl DynCast for $source {
            // `Self` is the concrete type implementing `DynCast`.
            // `out` is actually `&mut Option<rc::Rc<dyn TargetTrait>>`.
            fn dyn_cast_raw(self: rc::Rc<Self>, out: &mut dyn any::Any) {
                if TRACE_TYPES {
                    let self_type_name = any::type_name::<Self>();
                    println!("DynCast::dyn_cast_raw: Self={self_type_name}");
                }
                $(
                    if let Some(out2) = out.downcast_mut() {
                        *out2 = Some(self as rc::Rc<dyn $target>);
                        return;
                    }
                )+
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    trait Aspect0: DynCast {
        fn method0(&self) -> i32;
    }

    trait Aspect1: DynCast {
        fn method1(&self) -> i32;
    }

    trait Aspect2: DynCast {
        fn method2(&self) -> i32;
    }

    struct Object;

    impl Object {
        fn new() -> Self {
            Self
        }

        fn rc() -> rc::Rc<Self> {
            rc::Rc::new(Self::new())
        }
    }

    impl Aspect0 for Object {
        fn method0(&self) -> i32 {
            0
        }
    }

    impl Aspect1 for Object {
        fn method1(&self) -> i32 {
            1
        }
    }

    impl_dyncast!(Object: Aspect0, Aspect1);

    #[test]
    fn positive() {
        let o = Object::rc();
        let o = o.dyn_cast::<dyn Aspect0>().unwrap();
        assert_eq!(o.method0(), 0);
        let o = o.dyn_cast::<dyn Aspect1>().unwrap();
        assert_eq!(o.method1(), 1);
    }

    #[test]
    fn negative() {
        let o = Object::rc();
        let o = o.dyn_cast::<dyn Aspect2>();
        assert!(o.is_none());
    }
}
