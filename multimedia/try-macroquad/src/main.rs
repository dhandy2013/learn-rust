use crate::gameobj::{Drawable, GameObject, Updatable};
use gameobj::Intersectable;
use macroquad::audio;
use macroquad::color::colors;
use macroquad::file::set_pc_assets_folder;
use macroquad::input::{is_mouse_button_pressed, mouse_position, MouseButton};
use macroquad::logging::info;
use macroquad::math::DVec2;
use macroquad::shapes::{draw_circle, draw_line, draw_rectangle};
use macroquad::text::draw_text;
use macroquad::time;
use macroquad::window::{
    clear_background, next_frame, screen_height, screen_width,
};
mod crosscast;
mod gameobj;

struct CommonData {
    pos: DVec2,
    vel: DVec2,
}

impl CommonData {
    fn update(&mut self, delta_t: f64) {
        self.pos += self.vel * delta_t;
        let width = screen_width() as f64;
        let height = screen_height() as f64;
        if self.pos.x < 0.0 {
            self.pos.x = width;
        } else if self.pos.x >= width {
            self.pos.x = 0.0;
        }
        if self.pos.y < 0.0 {
            self.pos.y = height;
        } else if self.pos.y > height {
            self.pos.y = 0.0;
        }
    }
}

struct MovingBall {
    common: CommonData,
    r: f64,
}

impl GameObject for MovingBall {
    fn as_update_mut(&mut self) -> Option<&mut dyn Updatable> {
        Some(self)
    }
    fn as_draw_ref(&self) -> Option<&dyn Drawable> {
        Some(self)
    }
    fn as_interesect_ref(&self) -> Option<&dyn Intersectable> {
        Some(self)
    }
}

impl Updatable for MovingBall {
    fn update(&mut self, delta_t: f64) {
        self.common.update(delta_t);
    }
}

impl Drawable for MovingBall {
    fn draw(&self) {
        draw_circle(
            self.common.pos.x as f32,
            self.common.pos.y as f32,
            self.r as f32,
            colors::YELLOW,
        );
    }
}

impl Intersectable for MovingBall {
    fn intersect_pt(&self, point: DVec2) -> bool {
        self.common.pos.distance(point) < self.r
    }
}

struct MovingBox {
    common: CommonData,
    w: f64,
    h: f64,
}

impl GameObject for MovingBox {
    fn as_update_mut(&mut self) -> Option<&mut dyn Updatable> {
        Some(self)
    }
    fn as_draw_ref(&self) -> Option<&dyn Drawable> {
        Some(self)
    }
    fn as_interesect_ref(&self) -> Option<&dyn Intersectable> {
        Some(self)
    }
}

impl Updatable for MovingBox {
    fn update(&mut self, delta_t: f64) {
        self.common.update(delta_t);
    }
}

impl Drawable for MovingBox {
    fn draw(&self) {
        draw_rectangle(
            self.common.pos.x as f32,
            self.common.pos.y as f32,
            self.w as f32,
            self.h as f32,
            colors::GREEN,
        )
    }
}

impl Intersectable for MovingBox {
    fn intersect_pt(&self, point: DVec2) -> bool {
        let normalized_point = point - self.common.pos;
        normalized_point.x >= 0.0
            && normalized_point.x <= self.w
            && normalized_point.y >= 0.0
            && normalized_point.y <= self.h
    }
}

/// Convert a pixel point to a location in global coordinates
fn point_to_pos(x: f32, y: f32) -> DVec2 {
    DVec2 {
        x: x as f64,
        y: y as f64,
    }
}

#[macroquad::main("TryMacroquad")]
async fn main() {
    info!("Loading assets");
    set_pc_assets_folder("assets");
    let ding1khz = audio::load_sound("ding-1khz.wav")
        .await
        .expect("Could not load sound");

    info!("Creating objects");
    let mut display_list: Vec<Box<dyn GameObject + 'static>> = Vec::<_>::new();
    display_list.push(Box::new(MovingBox {
        common: CommonData {
            pos: (screen_width() as f64 / 2.0 - 60.0, 100.0).into(),
            vel: (10.0, 42.0).into(),
        },
        w: 120.0,
        h: 60.0,
    }));
    display_list.push(Box::new(MovingBall {
        common: CommonData {
            pos: (screen_width() as f64 - 30.0, screen_height() as f64 - 30.0)
                .into(),
            vel: (24.4, 23.8).into(),
        },
        r: 15.0,
    }));

    info!("Starting main loop");
    let mut frame_count: u32 = 0;
    loop {
        let delta_t: f64 = time::get_frame_time() as f64;
        for obj in display_list
            .iter_mut()
            .filter_map(|obj| obj.as_update_mut())
        {
            obj.update(delta_t);
        }
        frame_count += 1;

        clear_background(colors::RED);

        draw_line(40.0, 40.0, 100.0, 200.0, 15.0, colors::BLUE);

        for obj in display_list.iter().filter_map(|obj| obj.as_draw_ref()) {
            obj.draw();
        }

        if is_mouse_button_pressed(MouseButton::Left) {
            let (mouse_x, mouse_y) = mouse_position();
            let pos = point_to_pos(mouse_x, mouse_y);
            let mut hit_moving_obj = false;
            for obj in display_list
                .iter()
                .filter_map(|obj| obj.as_interesect_ref())
            {
                if obj.intersect_pt(pos) {
                    hit_moving_obj = true;
                    draw_circle(mouse_x, mouse_y, 30.0, colors::WHITE);
                    audio::stop_sound(&ding1khz);
                    audio::play_sound_once(&ding1khz);
                    break;
                }
            }
            if !hit_moving_obj {
                draw_circle(mouse_x, mouse_y, 30.0, colors::BLACK);
            }
        }

        let msg = format!(
            "Frame count: {:8} Time since startup: {:.3}",
            frame_count,
            time::get_time()
        );
        draw_text(&msg, 20.0, 20.0, 30.0, colors::DARKGRAY);
        draw_text(
            format!("FPS: {}", time::get_fps()).as_str(),
            0.0,
            screen_height() - 30.0,
            30.0,
            colors::WHITE,
        );

        next_frame().await;
    }
}
