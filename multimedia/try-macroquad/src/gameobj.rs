//! The traits describing the game objects that bind the universe together.
use macroquad::math::DVec2;
use std::num::NonZeroU32;

/// Generic `GameOject` trait implemented for all objects in the game universe.
///
/// The main job of this trait is to cast to one of the other more specific traits
/// that a game object can implement. Not all game objects implement all traits.
pub trait GameObject {
    /// Every game object knows its own unique ID if it has one
    fn id(&self) -> Option<NonZeroU32> {
        None
    }

    fn as_update_mut(&mut self) -> Option<&mut dyn Updatable> {
        None
    }

    fn as_draw_ref(&self) -> Option<&dyn Drawable> {
        None
    }

    fn as_interesect_ref(&self) -> Option<&dyn Intersectable> {
        None
    }
}

pub trait Updatable {
    /// Update the state of the object by the given time step
    fn update(&mut self, delta_t: f64);
}

pub trait Drawable {
    /// Draw the object
    fn draw(&self);
}

pub trait Intersectable {
    /// Return true iff the point is inside the object
    fn intersect_pt(&self, point: DVec2) -> bool;
}
