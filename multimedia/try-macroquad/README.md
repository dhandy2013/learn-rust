# Trying out the macroquad game and graphics Rust library

[macroquad](https://github.com/not-fl3/macroquad) is a simple and easy to use
game library for Rust programming language. It supports Linux, Windows, Web,
Android, and IOS platforms with the same code.

Unlike many other Rust frame buffer graphics libraries it doesn't burn lots of
CPU drawing a simple set of shapes.

## Running in debug mode on Windows computer

```
cargo run
```

## Running in debug mode in a web browser

One-time setup:
```
rustup target add wasm32-unknown-unknown
cargo install --locked basic-http-server
```

To build webapp and start webserver:
```
cargo build --target wasm32-unknown-unknown
basic-http-server .
```
The `basic-http-server` mini-webserver runs on port 4000 by default:
http://localhost:4000/

## Useful links

[macroquad API docs](https://docs.rs/macroquad/latest/macroquad/index.html)
