#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]
use std::{any::Any, fmt::Debug, rc::Rc};

#[derive(Debug)]
struct Data {
    a: i32,
    b: i32,
}

trait Sum {
    fn sum(&self) -> i32;
}

impl Sum for Data {
    fn sum(&self) -> i32 {
        self.a + self.b
    }
}

fn main() {
    println!("Try dynamic casting");
    let d = Rc::new(Data { a: 1, b: 2 });
    let s = d.sum();
    println!("d == {d:?} s == {s}");

    let mut maybe_dyn_sum: Option<Rc<dyn Sum>> = None;
    if let Some(out) = (&mut maybe_dyn_sum as &mut dyn Any).downcast_mut() {
        *out = Some(d.clone() as Rc<dyn Sum>);
    }
    let dyn_sum =
        maybe_dyn_sum.expect("cast to Rc<dyn Sum> should have worked");
    println!("sum: {}", dyn_sum.sum());

    let mut maybe_dyn_debug: Option<Rc<dyn Debug>> = None;
    if let Some(out) = (&mut maybe_dyn_debug as &mut dyn Any).downcast_mut() {
        *out = Some(d.clone() as Rc<dyn Debug>); // d is not quite what I wanted
    }
    let dyn_debug =
        maybe_dyn_debug.expect("cast to Rc<dyn Debug> should have worked");
    println!("debug: {dyn_debug:?}");
}
