Multimedia - graphics, sound, and input with Rust
=================================================

Sub-directories of this directory contain Rust code that does sound, graphics,
and input (keyboard, mouse, joystick) in Rust.

See: https://github.com/Rust-SDL2/rust-sdl2

sdl2-demo: This sub-directory contains all of the rust-sdl2 example programs
except for audio-whitenoise.rs, which requires the nightly toolchain.

try-macroquad: This sub-directory contains example code and programs based
on the `macroquad` library. This library is notable in that it supports sound
and graphics for programs that run on on the desktop or in the browser, without
code changes or cfg attributes.
