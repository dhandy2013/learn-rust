fn main() {
    println!("Fun with collections");
    if false {
        f_8_1_1();
        f_8_1_2();
        f_8_1_3();
        f_8_1_4();
    }
    if false {
        f_8_2_1();
        f_8_2_2();
    }
    f_8_3_1();
    f_8_3_2();
}

// Section 8.1 of "the book", first bit of example code
fn f_8_1_1() {
    let v1 = vec![1, 2, 3];
    println!("Size of Vec v1: {}", v1.len());

    let mut v2: Vec<i32> = Vec::new();
    for i in 5..9 {
        v2.push(i);
    }
    println!("Size of Vec v2: {}", v2.len());
}

// Section 8.1 of "the book", second bit of example code, etc.
fn f_8_1_2() {
    let v = vec![1, 2, 3, 4, 5];
    // let v = vec![1, 2];  // this could cause panic if I'm not careful

    if v.len() >= 3 {
        let third: &i32 = &v[2];
        println!("The third element is {}", third);
    }

    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element."),
    }
}

fn f_8_1_3() {
    let mut v = vec![100, 32, 57];
    for i in &mut v {
        print!("i before= {}, ", i);
        *i += 50;
        println!("i after = {}", i);
    }
    println!("v = {:?}", v);
}

#[derive(Debug)]
enum SpreadsheetCell {
    Number(f64),
    Text(String),
}

fn f_8_1_4() {
    let row = vec![
        SpreadsheetCell::Text("This is a text cell".to_string()),
        SpreadsheetCell::Number(123.5),
    ];
    println!("row = {:#?}", row);
}

fn f_8_2_1() {
    let mut s = String::from("abc");
    let s2 = "def";
    s.push_str(s2);
    println!("s = {:?}", s);
    println!("s2 = {:?}", s2);

    s.push('!');
    println!("s = {:?}", s);
    s.push('🍕');
    println!("s = {:?}", s);
}

fn f_8_2_2() {
    // This illustrates the difference between Unicode Scalar Values (numerical
    // character values) and Grapheme Clusters (visible characters). This Hindi
    // string is 6 code points characters but only four visible characters.
    // The .chars() iterator returns code points, not characters, and some of
    // them are not visible when printed.
    for c in "नमस्ते".chars() {
        println!("{}", c);
    }
}

use std::collections::HashMap;

fn f_8_3_1() {
    let mut scores = HashMap::new();
    scores.insert(String::from("Tigers"), 7);
    scores.insert(String::from("Bears"), 3);
    println!("scores = {:?}", scores);

    // Overwrite existing value
    scores.insert("Bears".to_string(), 4);

    // Insert only if it doesn't exist
    scores.entry("Bears".to_string()).or_insert(5);  // won't change value
    scores.entry("Dragons".to_string()).or_insert(12);

    // Iterate over all keys and values in HashMap
    for (key, value) in &scores {
        println!("{} = {}", key, value);
    }

    // Initialize the HashMap from parallel vectors
    // Note: HashMap type is different than above, keys are &String not String,
    // which can get annoying when trying to .insert().
    let teams = vec!["Cougars".to_string(), "Giants".to_string()];
    let score_numbers = vec![10, 50];
    // zip() creates a vector of tuples; collect() turns it into a HashMap
    let scores: HashMap<_, _> = teams.iter().zip(score_numbers.iter()).collect();
    println!("scores = {:?}", scores);
    // This gets a compile error because it wants a &String key not String:
    // scores.insert("Giants".to_string(), 49);

    let field_name = "Favorite color".to_string();
    let field_value = "Green".to_string();
    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // Compile error here because key and value got "moved" into the HashMap
    // println!("field_name = {:?}", field_name);

    let key = "Favorite color";
    println!("map.get({:?}) == {:?}", key, map.get(key));
}

fn f_8_3_2() {
    let text = "hello world wide world";
    let mut map = HashMap::new();
    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("word count: {:?}", map);
}
