//! Exercise from the end of chapter 8 of "The Book":
//!
//! Given a list of integers, use a vector and return the median (when sorted,
//! the value in the middle position) and mode (the value that occurs most
//! often; a hash map will be helpful here) of the list.
//!
//! This program will print a 1-line notice, then read the list of integers
//! from standard input, space separated, until end-of-file.
//!
//! It will print "Median: " and then the median value followed by newline.
//!
//! Then it will print "Mode: " and the mode values separated by spaces followed
//! by newline.
use std::collections::HashMap;
use std::io;

fn main() -> io::Result<()> {
    println!("Reading integers from standard input");
    let mut buf = String::with_capacity(256);
    let mut numbers = Vec::<i32>::with_capacity(64);
    while io::stdin().read_line(&mut buf)? > 0 {
        for item in buf.split_ascii_whitespace() {
            let n = item
                .parse::<i32>()
                .map_err(|err| io::Error::new(io::ErrorKind::Other, err))?;
            numbers.push(n);
        }
        buf.clear();
    }
    if numbers.len() == 0 {
        return Ok(());
    }

    // Calculate and print the median
    numbers.sort_unstable();
    let i = numbers.len() / 2;
    if numbers.len() % 2 == 0 {
        // Even number of numbers, median is average of middle two.
        let median = (numbers[i - 1] as f32 + numbers[i] as f32) / 2.0;
        println!("Median: {median}");
    } else {
        // Odd number of numbers, median is the middle one
        let median = numbers[i];
        println!("Median: {median}");
    }

    // Calculate and print the mode values.
    let mut frequencies = HashMap::<i32, usize>::new();
    for item in numbers.into_iter() {
        frequencies
            .entry(item)
            .and_modify(|counter| *counter += 1)
            .or_insert(1);
    }
    let mut counts: Vec<(i32, usize)> = frequencies.drain().collect();
    counts.sort_by(|(a_value, a_count), (b_value, b_count)| {
        // Sort by count descending then by item value ascending
        match b_count.cmp(a_count) {
            std::cmp::Ordering::Equal => a_value.cmp(b_value),
            order => order,
        }
    });
    let mut prev_count_maybe = None;
    print!("Mode:");
    for (value, count) in counts {
        if let Some(prev_count) = prev_count_maybe {
            if prev_count != count {
                break;
            }
        } else {
            prev_count_maybe = Some(count);
        }
        print!(" {value}");
    }
    println!("");

    Ok(())
}
