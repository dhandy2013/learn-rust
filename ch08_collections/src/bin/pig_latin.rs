//! Exercise from the end of chapter 8 of "The Book":
//!
//! Convert strings to pig latin. The first consonant of each word is moved to
//! the end of the word and “ay” is added, so “first” becomes “irst-fay.” Words
//! that start with a vowel have “hay” added to the end instead (“apple” becomes
//! “apple-hay”). Keep in mind the details about UTF-8 encoding!
//!
//! Modification: For a "word" consisting of a single ASCII consonant, just add
//! "ay" to the end. Example: "f" becomes "fay". These aren't real English words
//! but can appear in text for example when initials are used.
//!
//! This program prints a 1-line notice to standard error, then reads lines of
//! text from standard input until end-of-file is encountered. Each line is
//! converted to "pig latin" according to the instructions above, then printed
//! to standard output.  The number of words convered to pig latin is printed to
//! standard error.
use std::io::{self, Write};

fn main() -> io::Result<()> {
    eprintln!("Reading text from standard input and converting to pig latin");
    let mut line = String::with_capacity(256);
    let mut output_buf = String::with_capacity(256);
    let mut word_count: usize = 0;
    while io::stdin().read_line(&mut line)? > 0 {
        word_count += convert_input_to_pig_latin(&line, &mut output_buf);
        print!("{output_buf}");
        line.clear();
        output_buf.clear();
    }
    io::stdout().flush()?;
    eprintln!("Number of words converted to pig latin: {word_count}");
    Ok(())
}

/// Convert `input` to "pig-latin" and copy it to `output_buf`.
///
/// `input` is scanned for runs of 1 or more ASCII letters, and those are
/// converted to pig latin according to the rules in the module docs.
/// All other characters in `input` are copied to `output_buf` unchanged.
///
/// Return the number of words converted to pig latin.
fn convert_input_to_pig_latin(input: &str, output_buf: &mut String) -> usize {
    let mut converter = PigLatinConverter::new();

    for c in input.chars() {
        if c.is_ascii_alphabetic() {
            converter.add_letter(c);
        } else {
            converter.convert(output_buf);
            output_buf.push(c);
        }
    }

    // Flush any remaining characters in the converter
    converter.convert(output_buf);

    converter.count()
}

struct PigLatinConverter {
    word_chars: Vec<char>,
    word_count: usize,
}

impl PigLatinConverter {
    fn new() -> Self {
        Self {
            word_chars: Vec::with_capacity(20),
            word_count: 0,
        }
    }

    /// Add an ASCII letter to the pig latin converter.
    /// Panic if character `letter` is not an ASCII letter.
    fn add_letter(&mut self, letter: char) {
        assert!(letter.is_ascii_alphabetic());
        self.word_chars.push(letter);
    }

    /// Convert the buffered characters to pig latin according to the rules in
    /// the doc comment for this module. If the character buffer is empty, do
    /// nothing.  Otherwise, append the word converted to pig latin to
    /// `output_buf`, empty the character buffer, and increment the word count.
    fn convert(&mut self, output_buf: &mut String) {
        if self.word_chars.len() == 0 {
            return;
        }

        match self.word_chars[0] {
            'A' | 'E' | 'I' | 'O' | 'U' | 'a' | 'e' | 'i' | 'o' | 'u' => {
                // starts with a vowel
                for &c in self.word_chars.iter() {
                    output_buf.push(c);
                }
                output_buf.push_str("-hay");
            }
            first_letter if self.word_chars.len() > 1 => {
                // starts with consonant
                for (i, &c) in (&self.word_chars[1..]).iter().enumerate() {
                    if i == 0 && first_letter.is_ascii_uppercase() {
                        output_buf.push(c.to_ascii_uppercase());
                    } else {
                        output_buf.push(c);
                    }
                }
                output_buf.push('-');
                output_buf.push(first_letter.to_ascii_lowercase());
                output_buf.push_str("ay");
            }
            only_letter => {
                // 1-letter word consisting of only a consonant.
                // This case isn't covered by the docs, so I'll make up a new
                // rule for it. Just add "ay" to the end of the word, e.g. "f"
                // => "fay".
                output_buf.push(only_letter);
                output_buf.push_str("ay");
            }
        }

        self.word_chars.clear();
        self.word_count += 1;
    }

    /// Return the number of words converted to pig latin.
    fn count(&self) -> usize {
        self.word_count
    }
}
