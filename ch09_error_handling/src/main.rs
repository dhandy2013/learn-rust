use std::error::Error;
use std::fs;
use std::fs::File;
use std::io;
use std::io::ErrorKind;
use std::io::Read;
use std::net::IpAddr;

fn main() -> Result<(), Box<dyn Error>> {
    println!("Chapter 9 - Fun with error handling");
    // panic!("Head for the hills!");
    // Note: panic!() is how tests are marked as failing.

    if false {
        let v = vec![1, 2, 3];
        // This would panic if allowed to execute
        println!("{}", v[99]);
    }

    let filename = "hello.txt";

    let _f = match File::open(filename){
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create(filename) {
                Ok(fc) => fc,
                Err(e) => panic!("Error creating file {:?}: {:?}", filename, e),
            },
            other_err => panic!("Error opening file {:?}: {:?}", filename,
                                other_err),
        },
    };

    // A better way to do error checking using closures
    let _f = File::open(filename).unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create(filename).unwrap_or_else(|error| {
                panic!("Error creating file {:?}: {:?}", filename, error);
            })
        } else {
            panic!("Error opening file {:?}: {:?}", filename, error);
        }
    });

    // Panics if the open fails
    let _f = File::open(filename).unwrap();

    // Panics with a more specific message if open fails.
    // But unfortunately it is inflexible - the message can't be a
    // formatted string, has to be a &str (like a string literal).
    let _f = File::open(filename).expect("Error opening the hello file");

    // Chaining .unwrap() so compiler doesn't complain about unhandled error.
    // Don't do this in production! Because .unwrap() panics with a generic
    // message on any error.
    read_username_from_file_1().unwrap();
    read_username_from_file_2().unwrap();
    read_username_from_file_3().unwrap();
    read_username_from_file_4().unwrap();

    // .unwrap() is acceptable here because we're parsing a string literal
    // that is guaranteed to be in the right format.
    let _home: IpAddr = "127.0.0.1".parse().unwrap();

    // Can't use the "?" operator inside main() unless we change the return
    // type to be Result<(), Box<dyn Error>>
    let _f = File::open(filename)?;

    Ok(())
}

// Read file contents, return string or error, v1
fn read_username_from_file_1() -> Result<String, io::Error> {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

// Read file contents, return string or error, v2
// The "?" operator extracts the Ok value if the value is Ok,
// else does a return Err(e), exiting the containing function.
// The error value goes through a "from" function to adapt it's type.
fn read_username_from_file_2() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

// Read file contents, return string or error, v3
// Same as before but more concise due to more method chaining.
fn read_username_from_file_3() -> Result<String, io::Error> {
    let mut s = String::new();

    File::open("hello.txt")?.read_to_string(&mut s)?;

    Ok(s)
}

// Read file contents, return string or error, v4
// It turns out that fs::read_to_string() already reads a whole file.
fn read_username_from_file_4() -> Result<String, io::Error> {
    fs::read_to_string("hello.txt")
}
