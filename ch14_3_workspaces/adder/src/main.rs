use rand;
use rand::Rng;

use add_one;

fn main() {
    println!("Ch14.3: Cargo Workspaces");
    let num = 10;
    println!("{} plus one is {}", num, add_one::add_one(num));

    // Fun with random numbers
    // https://rust-random.github.io/book/guide-values.html
    let mut rng = rand::thread_rng();

    // Calling random() with type annotation
    let f1 = rand::random::<f64>();
    println!("Here's a random f64 number: {}", f1);

    // Deriving the random type from the variable type
    let n1: i32 = rand::random();
    println!("Here's a random i32 number: {}", n1);

    // Using the previously-initialized random number generater
    let n2: u8 = rng.gen();
    println!("Here's a random u8 number: {}", n2);
    let f2: f64 = rng.gen();
    println!("Here's another radom f64: {}", f2);
}
