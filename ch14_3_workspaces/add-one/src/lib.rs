use rand;
// use rand_core;  // This fails because Cargo.toml references version 0.3.x

pub fn add_one(x: i32) -> i32 {
    x + 1
}

// Oooh, this could panic if x plus the random number is out of i32 range!
pub fn add_rand(x: i32) -> i32 {
    x + rand::random::<i32>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(3, add_one(2));
    }
}
