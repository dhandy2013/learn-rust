use std::cell::RefCell;
use std::fmt;
use std::mem;
use std::rc::{Rc, Weak};

use ch15_smartptrs::{hexdump_ref, hexdump_slice, MyBox};
use ch15_smartptrs::List::{Cons, Nil};
use ch15_smartptrs::List2 as L2;
use ch15_smartptrs::List3 as L3;
use ch15_smartptrs::List4 as L4;
use ch15_smartptrs::Node;

fn main() {
    println!("Ch15: Smart Pointers");
    ch_15_1();
    ch_15_2();
    ch_15_3();
    ch_15_4();
    ch_15_5();
    ch_15_6();
}

fn ch_15_1() {
    println!("\nCh15.1: Using Box<T> to Point to Data on the Heap");

    if false {
        let b = Box::new(5);
        println!("b = {}", b);
    }

    let list = Cons(1,
        Box::new(Cons(2,
            Box::new(Cons(3,
                Box::new(Nil))))));
    println!("Dumping list:");
    hexdump_ref(&list);

    let mut s = String::new();
    for c in 0x00u8..0x80u8 {
        s.push(c as char);
    }
    println!("Dumping String:");
    hexdump_ref(&s);
    println!("Dumping String content:");
    hexdump_slice(s.as_bytes());
}

fn ch_15_2() {
    println!("\nCh15.2 the Deref trait");
    ch_15_2_a();
    ch_15_2_b();
    ch_15_2_b2();
    ch_15_2_c();
    ch_15_2_d();
}

fn ch_15_2_a() {
    let x = 2;
    let y = &x;
    // Notice that y is auto-derefed
    // Question: Is that due to "implicit deref coercion", or is it due to some
    // other Rust rule that I don't know about yet?
    let z = x + y;

    assert_eq!(2, x);
    // Must explicitly deref y here or compile error:
    // no implementation for `{integer} == &{integer}`
    assert_eq!(2, *y);

    // Notice that y is auto-derefed again
    println!("{} + {} = {}", x, y, z);
}

fn ch_15_2_b() {
    let x = 5;
    let y = Box::new(x);
    // Rust doesn't auto-deref Box<i32> like it did &i32, so this gets a
    // compile error:
    // let z = x + y;
    // I have to explicitly dereference y like this:
    let z = x + *y;

    // I don't have to dereference y here:
    println!("{} + {} = {}", x, y, z);
}

fn ch_15_2_b2() {
    // Implicit deref coercion at work
    // You can pass a &Box<i32> as if it were &i32
    // But you can't pass Box<i32> as if it were i32 - for some reason Rust
    // isn't smart enough to know you can just copy the i32 out of the Box
    // in order to pass it as a parameter.

    let x = 5;
    let y = Box::new(x);
    let z = my_add(&x, &y);
    println!("{} + {} = {}", x, y, z);
}

fn my_add(x: &i32, y: &i32) -> i32 {
    x + y
}

fn ch_15_2_c() {
    let x = 6;
    let y = MyBox::new(x);
    // All is well as long as we explicitly deref y
    let z = x + *y;
    println!("{} + {} = {}", x, *y, z);
}

fn ch_15_2_d() {
    let m = MyBox::new(String::from("Alvin"));
    // "Implicit deref coercion" at work here allowing MyBox<String> to be
    // passed in place of &str
    hello(&m);
}

fn hello(name: &str) {
    println!("Hello, {}!", name);
}

fn ch_15_3() {
    println!("\nCh15.3 the Drop trait");
    let _c = CustomSmartPointer { data: String::from("data c") };
    let _d = CustomSmartPointer { data: String::from("data d") };
    println!("CustomSmartPointers c and d created");
    mem::drop(_d);
    println!("CustomSmartPointer d dropped");
}

struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer with data: '{}'", self.data);
    }
}

fn ch_15_4() {
    println!("\nCh15.4 Rc<T>, the reference counted smart pointer");

    let a = Rc::new( L2::Cons(5, Rc::new(L2::Cons(10, Rc::new(L2::Nil)))));
    println!("Initial a refcount: {}", Rc::strong_count(&a));
    println!("a = {}", a);

    let b = L2::Cons(3, Rc::clone(&a));
    println!("a refcount after cloning b: {}", Rc::strong_count(&a));
    println!("b = {}", b);

    {
        let c = L2::Cons(4, Rc::clone(&a));
        println!("a refcount after cloning c: {}", Rc::strong_count(&a));
        println!("c = {}", c);
        println!("a refcount after printing c: {}", Rc::strong_count(&a));
    }
    println!("a refcount after c out of scope: {}", Rc::strong_count(&a));

    println!("Passing a as a parameter to func f5");
    f5(&a);
    println!("a refcount after returning from f5: {}", Rc::strong_count(&a));
}

fn f5<T: fmt::Display>(param: &Rc<T>) {
    println!("Inside func f5: param refcount: {}", Rc::strong_count(&param));
    println!("param = {}", param);
}

fn ch_15_5() {
    println!("\nCh15.5 RefCell<T> and the Interior Mutability Pattern");
    // See tests in lib.rs

    let value = Rc::new(RefCell::new(5));

    let a = Rc::new(L3::Cons(Rc::clone(&value), Rc::new(L3::Nil)));
    let b = L3::Cons(Rc::new(RefCell::new(6)), Rc::clone(&a));
    let c = L3::Cons(Rc::new(RefCell::new(10)), Rc::clone(&a));
    println!("a = {}", a);
    println!("b = {}", b);
    println!("c = {}", c);

    *value.borrow_mut() += 10;
    println!("After mutating the inner value:");
    println!("a = {}", a);
    println!("b = {}", b);
    println!("c = {}", c);
}

fn ch_15_6() {
    println!("\nCh15.6 Reference cycles can leak memory");
    ch_15_6_a();
    ch_15_6_b();
}

fn ch_15_6_a() {
    println!("\nCh15.6a Creating a Reference Cycle");

    let a = Rc::new(L4::Cons(5, RefCell::new(Rc::new(L4::Nil))));

    println!("a == {}", a);
    println!("a: initial rc count = {}", Rc::strong_count(&a));
    println!("a: next item = {:?}", a.tail());

    let b = Rc::new(L4::Cons(10, RefCell::new(Rc::clone(&a))));

    println!("b == {}", b);
    println!("a: rc count after creating b = {}", Rc::strong_count(&a));
    println!("b: initial rc count = {}", Rc::strong_count(&b));
    println!("b: next item = {:?}", b.tail());

    if let Some(link) = a.tail() {
        *link.borrow_mut() = Rc::clone(&b);
    }

    println!("b: rc count after changing a = {}", Rc::strong_count(&b));
    println!("a: rc count after changing a = {}", Rc::strong_count(&a));

    // Uncomment the next line to cause a stack overflow due to cycle
    // println!("a: next item = {:?}", a.tail());
}

fn ch_15_6_b() {
    println!("\nCh15.6b Preventing Reference Cycles: Rc<T> -> Weak<T>");

    let leaf = Rc::new(Node {
        value: 3,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![]),
    });
    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
    println!("leaf strong_count = {}, weak_count = {}",
             Rc::strong_count(&leaf),
             Rc::weak_count(&leaf));

    {
        let branch = Rc::new(Node {
            value: 5,
            parent: RefCell::new(Weak::new()),
            children: RefCell::new(vec![Rc::clone(&leaf)]),
        });
        println!("branch = {:?}", branch);

        *leaf.parent.borrow_mut() = Rc::downgrade(&branch);
        println!("branch strong_count = {}, weak_count = {}",
                 Rc::strong_count(&branch),
                 Rc::weak_count(&branch));
        println!("leaf strong_count = {}, weak_count = {}",
                 Rc::strong_count(&leaf),
                 Rc::weak_count(&leaf));
    }

    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
    println!("leaf strong_count = {}, weak_count = {}",
             Rc::strong_count(&leaf),
             Rc::weak_count(&leaf));
}
