use std::cell::RefCell;
use std::fmt;
use std::mem;
use std::ops::Deref;
use std::rc::{Rc, Weak};

pub enum List {
    Cons(i32, Box<List>),
    Nil,
}

pub struct MyBox<T>(T);

impl<T> MyBox<T> {
    pub fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

/// Print the memory contents of an object reference in hex format
pub fn hexdump_ref<T>(obj: &T) {
    // p is a byte pointer to the start of memory to be dumped
    let p = ((obj as *const T) as usize) as *const u8;
    let size = mem::size_of::<T>() as isize;
    _hexdump_ptr(p, size);
}

/// Print the memory contents of a slice in hex format
pub fn hexdump_slice(slice: &[u8]) {
    let p = slice.as_ptr();
    let size = slice.len() as isize;
    _hexdump_ptr(p, size);
}

// INTERNAL FUNCTION
// p: pointer to a valid memory block
// size: number of valid addressable bytes in the entire block
// Panic if size < 0
// UNDEFINED BEHAVIOR if less than size bytes are actually valid!
pub fn _hexdump_ptr(p: *const u8, size: isize) {
    let mut offset: isize = 0;
    loop {
        let (n, line) = _format_hexdump_line(p, offset, size);
        print!("{}", line);
        if n <= 0 {
            break;
        }
        offset += n;
    }
}

// INTERNAL FUNCTION
// Format up to 16 bytes as a line in classic "hexdump -C" style.
// p: pointer to a valid memory block
// offset: offset within memory block of first byte to dump
// size: number of valid addressable bytes in the entire block
// Return (num_bytes_dumped, line)
// Panic if offset < 0 or size < 0
// UNDEFINED BEHAVIOR if less than size bytes are actually valid!
//
// TODO:
// Avoid allocating a new string via format!() for each byte dumped. The output
// size is fixed and known, so we should be able to re-use a buffer.
fn _format_hexdump_line(p: *const u8, offset: isize, size: isize)
    -> (isize, String)
{
    if offset < 0 {
        panic!("Cannot allow offset < 0");
    }
    if size < 0 {
        panic!("Cannot allow size < 0");
    }
    let mut line = String::with_capacity(79);  // 78 ASCII chars + newline
    line.push_str(format!("{:08x}", offset).as_str());

    // Two blocks of 8 bytes each in hex format
    let mut cur_offset = offset;
    if cur_offset < size {
        for _ in 0..2 {
            line.push_str(" ");
            for _ in 0..8 {
                if cur_offset < size {
                    let b = unsafe {
                        *p.offset(cur_offset)
                    };
                    let byte_repr = format!(" {:02x}", b);
                    line.push_str(byte_repr.as_str());
                    cur_offset += 1;
                } else {
                    line.push_str("   ");
                }
            }
        }
    }

    // ASCII/ISO-8859-1 representation
    if offset < size {
        line.push_str("  |");
        for i in 0..16 {
            if offset + i >= size {
                break;
            }
            let c = unsafe {
                *p.offset(offset + i)
            };
            if c >= 0x20 && c < 0x7f {
                // It is a printable ASCII char
                line.push(c as char);
            } else {
                // Represent non-printable char
                line.push('.');
            }
        }
        line.push_str("|");
    }

    // Newline at the end
    line.push('\n');
    (cur_offset - offset, line)
}

pub enum List2 {
    Cons(i32, Rc<List2>),
    Nil,
}

impl fmt::Display for List2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut first = true;
        let mut p = self;
        loop {
            match p {
                List2::Cons(n, next_ptr) => {
                    if !first {
                        write!(f, ".")?;
                    } else {
                        first = false;
                    }
                    write!(f, "{}", n)?;
                    p = next_ptr;
                },
                List2::Nil => {
                    if first {
                        write!(f, "(empty list)")?;
                    }
                    break;
                },
            }
        }
        Ok(())
    }
}

pub enum List3 {
    Cons(Rc<RefCell<i32>>, Rc<List3>),
    Nil,
}

impl fmt::Display for List3 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut first = true;
        let mut p = self;
        loop {
            match p {
                List3::Cons(n, next_ptr) => {
                    if !first {
                        write!(f, ".")?;
                    } else {
                        first = false;
                    }
                    write!(f, "{}", n.borrow())?;
                    p = next_ptr;
                },
                List3::Nil => {
                    if first {
                        write!(f, "(empty list)")?;
                    }
                    break;
                },
            }
        }
        Ok(())
    }
}

// Ch15.06

#[derive(Debug)]
pub enum List4 {
    Cons(i32, RefCell<Rc<List4>>),
    Nil,
}

impl List4 {
    pub fn tail(&self) -> Option<&RefCell<Rc<List4>>> {
        match self {
            List4::Cons(_, item) => Some(item),
            List4::Nil => None,
        }
    }

    pub fn shallow_copy(&self) -> List4 {
        match self {
            List4::Cons(n, next_cell) => {
                let list = next_cell.borrow();
                // Q: Why is n an &i32 here but an i32 down below?
                List4::Cons(*n, RefCell::new(Rc::clone(&list)))
            },
            List4::Nil => List4::Nil,
        }
    }
}

// I had to create the auxiliary method shallow_copy() to get this to compile.
// It had to be named something other than clone() or else Rc::clone() ended up
// being called instead.
// TODO: Find a better way.
impl fmt::Display for List4 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut first = true;
        let mut p: List4 = self.shallow_copy();
        loop {
            match p {
                List4::Cons(n, next_cell) => {
                    if !first {
                        write!(f, ".")?;
                    } else {
                        first = false;
                    }
                    write!(f, "{}", n)?;
                    let next_list = next_cell.borrow();
                    p = next_list.shallow_copy();
                },
                List4::Nil => {
                    if first {
                        write!(f, "(empty list)")?;
                    }
                    break;
                },
            }
        }
        Ok(())
    }
}

// Ch15.5 - interior mutability

pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
    where T: Messenger
{
    pub fn new(messenger: &T, max: usize) -> LimitTracker<T> {
        LimitTracker {
            messenger,
            value: 0,
            max,
        }
    }

    pub fn set_value(&mut self, value: usize) {
        self.value = value;

        let percentage_of_max = self.value as f64 / self.max as f64;

        if percentage_of_max >= 1.0 {
            self.messenger.send("Error: You are over your quota!");
        } else if percentage_of_max >= 0.9 {
             self.messenger.send("Urgent warning: You've used up over 90% of your quota!");
        } else if percentage_of_max >= 0.75 {
            self.messenger.send("Warning: You've used up over 75% of your quota!");
        }
    }
}

// Ch15.6

#[derive(Debug)]
pub struct Node {
    pub value: i32,
    pub parent: RefCell<Weak<Node>>,
    pub children: RefCell<Vec<Rc<Node>>>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger { sent_messages: RefCell::new(vec![]) }
        }

        // This will panic every time
        fn double_borrow(&self, message: &str) {
            let mut b1 = self.sent_messages.borrow_mut();
            let mut b2 = self.sent_messages.borrow_mut();
            b1.push(String::from(message));
            b2.push(String::from(message));
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.borrow_mut().push(String::from(message));
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning() {
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }

    #[test]
    #[should_panic(expected = "already borrowed")]
    fn blows_up_due_to_double_mut_borrow() {
        let mm = MockMessenger::new();
        mm.double_borrow("This will panic");
    }
}
