use super::*;

#[test]
fn it_works() {
    assert_eq!(2 + 2, 4);
}

#[test]
fn can_summarize_news_article() {

    let news = NewsArticle {
        headline: "Man Bites Dog!".to_string(),
        location: "Columbus, OH".to_string(),
        author: "Rumple Stiltskin".to_string(),
        content: "This is a test of news article content...".to_string(),
    };

    notify(&news);

    println!("{}", news.summarize_more());
}
