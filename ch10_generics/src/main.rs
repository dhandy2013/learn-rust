use std::fmt::Display;

// See lib.rs for trait definitions, etc.
// See tests.rs for tests
//use ch10_generics::NewsArticle;
use ch10_generics::Pair;
use ch10_generics::Summary; // this is the trait
use ch10_generics::Tweet;

fn main() {
    println!("Ch10: Fun with Generic Types, Traits, and Lifetimes");
    f_10();
    f_10_1();
    f_10_2();
    f_10_3();
}

fn f_10() {
    let number_list1 = vec![34, 50, 25, 100, 65];
    let number_list2 = vec![102.7, 34.5, 6000.1, 89.9, 54.0, 2.0, 43.0, 8.0];

    let largest1 = largest(&number_list1);
    println!("The largest number in list 1 is {}", largest1);

    let largest2 = largest(&number_list2);
    println!("The largest number in list 2 is {}", largest2);

    println!("Another largest number: {}", largest(&vec![1, 2, 3]));
    //println!("This causes panic: {}", largest(&vec![]));
}

// Return the largest item in the list.
// Panic if the list is empty.
fn largest<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest_index: usize = 0;
    for i in 1..list.len() {
        if list[i] > list[largest_index] {
            largest_index = i;
        }
    }
    &list[largest_index]
}

#[derive(Debug)]
struct Point<T: Copy> {
    x: T,
    y: T,
}

impl<T: Copy> Point<T> {
    fn x(&self) -> T {
        self.x
    }
    fn y(&self) -> T {
        self.y
    }
}

impl Point<f64> {
    fn length(&self) -> f64 {
        ((self.x * self.x) + (self.y * self.y)).sqrt()
    }
    // Have to give new names to these specialized getter methods
    // or I get error[E0592]: duplicate definitions with name
    //
    // Note that these getters only have a reference to self yet then can pass
    // a copy of the members out of the function without getting error[E0507]
    // that I got below with generic types. So I guess it helps that it knows
    // f64 implements Copy.
    fn get_x(&self) -> f64 {
        self.x
    }
    fn get_y(&self) -> f64 {
        self.y
    }
}

fn f_10_1_a() {
    let p = Point { x: 5, y: 3 };
    println!("p = {:#?}", p);
    println!("p.x() = {}, p.y() = {}", p.x(), p.y());

    let p2 = Point { x: 3.0, y: 4.0 };
    println!("p2.get_x() = {}, p2.get_y() = {}", p2.get_x(), p2.get_y());
    println!("Magnitude of p2: {}", p2.length());
}

// Point that can have different types for each coordinate field
// The "where" clause is required to make it possible to pass instances of this 
// by reference. See more explanation below.
#[derive(Debug)]
struct WeirdPoint<T, U>
    where T: Copy,
          U: Copy,
{
    x: T,
    y: U,
}

impl<T, U> WeirdPoint<T, U>
    where T: Copy,
          U: Copy,
{
    // Note: I fixed the original example code for this method in section
    // 10.1 of the book. The original version did not accept references
    // for the self and other parameters, therefore making this method
    // consume those parameters! I fixed that by adding the "where" clauses
    // that made it possible to accept references and copy those fields out
    // of the borrowed reference.
    fn mixup<V, W>(&self, other: &WeirdPoint<V, W>) -> WeirdPoint<T, W>
        where W: Copy,
              V: Copy,
    {
        WeirdPoint {
            x: self.x,
            y: other.y,
        }
    }
}

fn f_10_1_b() {
    let wp1 = WeirdPoint { x: "abc", y: 4.5 };
    let wp2 = WeirdPoint { x: 5, y: (1, 2) };
    let wp3 = wp1.mixup(&wp2);
    println!("wp1 = {:?}", wp1);
    println!("wp2 = {:?}", wp2);
    println!("wp3 = {:?}", wp3);
}

fn f_10_1() {
    println!("\nCh10.1: Fun with generic data types");
    f_10_1_a();
    f_10_1_b()
}

fn f_10_2() {
    println!("\nCh10.2: Fun with traits");
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}\n{}", tweet.summarize(), tweet.summarize_more());

    // See tests for code that exercises NewsArticle

    let p1 = Pair::new(3, 12);
    let p2 = Pair::new(8, 5);
    println!("p1 comparison: {}", p1.cmp_display());
    println!("p2 comparison: {}", p2.cmp_display());
}

fn longest_str<'a>(s1: &'a str, s2: &'a str) -> &'a str {
    if s1.len() >= s2.len() {
        s1
    } else {
        s2
    }
}

struct TextExcerpt<'a> {
    part: &'a str,
}

impl<'a> TextExcerpt<'a> {
    fn level(&self) -> i32 {
        9
    }
}

fn longest_with_announce<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
    where T: Display
{
    println!("Be advised: {}", ann);
    if x.len() >= y.len() {
        x
    } else {
        y
    }
}

fn f_10_3() {
    println!("\nCh10.3: Validating references with lifetimes");
    
    let x = 7;
    let r;
    r = &x;

    println!("r = {}", r);

    let s1 = String::from("Loooooooooooooooooong string");

    {
        let s2 = "shortstr";
        let result = longest_str(&s1.as_str(), &s2);
        println!("Longest str: {}", result);
    }

    let long_text = String::from("Once upon a time there was a dog named...");
    let word = long_text.split(' ').next().expect("expected at least 1 space");
    let excerpt = TextExcerpt { part: word };
    println!("Text excerpt: {:?} (level {})", excerpt.part, excerpt.level());

    let n = longest_with_announce(&s1.as_str(), "abc", "ta-daaa");
    println!("n = {}", n);
}
