use std::fmt::Display;

#[cfg(test)]
mod tests;

pub trait Summary {
    fn summarize(&self) -> String;

    // Default implementation
    fn summarize_more(&self) -> String {
        String::from("(Read more...)")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
    // Override default implementation
    fn summarize_more(&self) -> String {
        format!("{} {}", self.summarize(), "Read more...")
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

pub struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    pub fn cmp_display(&self) -> String {
        if self.x >= self.y {
            return format!("Wider than tall: {} > {}", self.x, self.y);
        } else {
            return format!("Taller than wide: {} > {}", self.y, self.x);
        }
    }
}
