use std::io;
use std::mem;
use std::slice;

/// Maximum number of bytes dumped per output line
pub const CHUNK_SIZE: usize = 16;
pub const NUM_COLS: usize = 2;  // must evenly divide CHUNK_SIZE

/// Print the memory contents of an object reference in hex format
pub fn hexdump_ref<T>(obj: &T, out: &mut dyn io::Write) -> io::Result<()> {
    // p is a byte pointer to the start of memory to be dumped
    let p = ((obj as *const T) as usize) as *const u8;
    let size = mem::size_of::<T>();
    let bytes = unsafe { slice::from_raw_parts(p, size) };
    hexdump_bytes(bytes, out)
}

/// Print the contents of a slice's buffer to stdout in hex format
pub fn hexdump_slice<T>(slice: &[T], out: &mut dyn io::Write)
    -> io::Result<()> {
    let p = slice.as_ptr() as *const u8;
    let size = mem::size_of::<T>() * slice.len();
    let bytes = unsafe { slice::from_raw_parts(p, size) };
    hexdump_bytes(bytes, out)
}

/// Print the contents of a chunk of memory at a pointer in hex format (unsafe!)
pub unsafe fn hexdump_ptr(p: *const u8, size: usize, out: &mut dyn io::Write)
    -> io::Result<()> {
    let bytes =  slice::from_raw_parts(p, size);
    hexdump_bytes(bytes, out)
}

/// Print the memory contents of a byte slice in hex format
pub fn hexdump_bytes(slice: &[u8], out: &mut dyn io::Write) -> io::Result<()> {
    let mut offset: usize = 0;
    for chunk in slice.chunks(CHUNK_SIZE) {
        format_hexdump_line(offset, chunk, out)?;
        offset += chunk.len();
    }
    format_hexdump_line(offset, &[], out)
}

/// Read bytes on input stream and dump them as hex/ASCII on output stream.
pub fn hexdump_stream(
    inp: &mut dyn io::Read,
    out: &mut dyn io::Write,
) -> io::Result<()> {
    let mut offset: usize = 0;
    let mut chunk = [0u8; CHUNK_SIZE];
    loop {
        // Read until CHUNK_SIZE bytes have been acquired or EOF is encountered,
        // whichever happens first.
        let mut p: usize = 0;
        while p < CHUNK_SIZE {
            let n = inp.read(&mut chunk[p..])?;
            if n == 0 {
                break;
            }
            p += n;
        }
        format_hexdump_line(offset, &chunk[..p], out)?;
        if p == 0 {
            break;
        }
        offset += p;
    }
    Ok(())
}

/// Format up to CHUNK_SIZE bytes as a line in classic "hexdump -C" style.
/// offset:
///     Offset within memory of start of slice, for printing purposes only.
/// bytes:
///     Slice of bytes, only first CHUNK_SIZE bytes will be looked at.
///     It's Ok if the slice size is less than CHUNK_SIZE, including zero.
/// out:
///     Output writer object
/// Return:
///     An io::Result - empty if no error, else an io::Error
pub fn format_hexdump_line(
    offset: usize,
    bytes: &[u8],
    out: &mut dyn io::Write,
) -> io::Result<()> {
    write!(out, "{:08x}", offset)?;
    if bytes.len() == 0 {
        // Early exit if no bytes to dump
        write!(out, "\n")?;
        return Ok(());
    }

    let mut cur_pos: usize = 0;
    for _ in 0..NUM_COLS {
        write!(out, " ")?;
        for _ in 0..(CHUNK_SIZE / NUM_COLS) {
            match bytes.get(cur_pos) {
                Some(&b) => {
                    write!(out, " {:02x}", b)?;
                    cur_pos += 1;
                }
                None => {
                    write!(out, "   ")?;
                }
            }
        }
    }

    // ASCII representation
    write!(out, "  |")?;
    for i in 0..CHUNK_SIZE {
        match bytes.get(i) {
            Some(&c) => {
                if c >= 0x20 && c < 0x7f {
                    // It is a printable ASCII char
                    write!(out, "{}", c as char)?;
                } else {
                    // Represent non-printable/non-ASCII char as dot
                    write!(out, ".")?;
                }
            }
            None => {
                break;
            }
        }
    }
    write!(out, "|")?;

    // Write ending newline
    write!(out, "\n")?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io;

    #[test]
    fn bench_test() -> io::Result<()> {
        // No asserts, just look at the output and see if it looks reasonable
        let mut s = String::with_capacity(64);
        s.push_str("abc123");
        hexdump_ref(&s, &mut io::stdout())?;
        hexdump_bytes(s.as_bytes(), &mut io::stdout())?;
        Ok(())
    }

    #[test]
    fn test_format_hexdump_line() -> io::Result<()> {
        const LINE_SIZE: usize = 80;
        let mut outbuf = Vec::<u8>::with_capacity(LINE_SIZE);
        let inbytes = b"abc123";
        format_hexdump_line(1023, inbytes, &mut outbuf)?;
        let s = std::str::from_utf8(&outbuf).unwrap();
        assert_eq!(
    "000003ff  61 62 63 31 32 33                                 |abc123|\n",
        s);
        Ok(())
    }
}
