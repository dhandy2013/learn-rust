use hexdump;
use std::io;

fn main() -> io::Result<()> {
    // Read stdin and dump it to stdout
    hexdump::hexdump_stream(&mut io::stdin(), &mut io::stdout())
}
