# Welcome to cpif-web!

This crate is for making simple single-page web applications for games, demos,
and animated sketches using HTML, CSS, and the Rust programming language.  A
tiny bit of JavaScript is necessary to load and run the compiled Rust
WebAssembly code.

The main focus is 2D graphics in one or more HTML canvases, without needing to
use either `js_sys` nor `web_sys` directly. There is also support for generating
sounds and sound effects.

For examples of how to use this crate, see the companion `cpif-web-demo` package.

Minimum Supported Rust Version: 1.65

To run the tests:
```
wasm-pack test --firefox --headless
```

To run just the graphics module tests:
```
wasm-pack test --firefox --headless . -- graphics::tests
```

To speed things up after the first test run, pass `--mode no-install` after
`--headless`.

To see the graphical output of the tests, omit `--headless` and point your
browser to http://127.0.0.1:8000/
