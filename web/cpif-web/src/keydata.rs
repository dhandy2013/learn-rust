//! Keyboard event data
#![allow(non_upper_case_globals)]
use bitflags::bitflags;
use std::num::NonZeroU8;

bitflags! {
    /// Keyboard Modifier flags.
    /// I'm only recording the most commonly used modifiers.
    pub struct KeyModifiers: u8 {
        const Alt   = 0b00000001;
        const Ctrl  = 0b00000010;
        const Shift = 0b00000100;
    }
}

pub type FunctionKeyId = NonZeroU8;

#[derive(Clone, Debug)]
pub struct KeyData {
    key_id: KeyId,
    modifiers: KeyModifiers,
    caps_lock: bool,
}

impl KeyData {
    pub(crate) fn from_web_sys_keyboard_event(
        key_event: &web_sys::KeyboardEvent,
    ) -> Self {
        let key_name = key_event.key();
        let caps_lock = key_event.get_modifier_state("CapsLock");

        let mut modifiers = KeyModifiers::empty();
        if key_event.alt_key() {
            modifiers.insert(KeyModifiers::Alt);
        }
        if key_event.ctrl_key() {
            modifiers.insert(KeyModifiers::Ctrl);
        }
        if key_event.shift_key() {
            modifiers.insert(KeyModifiers::Shift);
        }

        Self {
            key_id: KeyId::from_string(key_name),
            modifiers,
            caps_lock,
        }
    }

    /// Return the key ID enum for the key
    pub fn key_id(&self) -> &KeyId {
        &self.key_id
    }

    /// Return the set of key modifiers active
    pub fn modifiers(&self) -> KeyModifiers {
        self.modifiers
    }

    /// Return true iff the Caps Lock key was active
    pub fn caps_lock(&self) -> bool {
        self.caps_lock
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
#[non_exhaustive]
pub enum KeyId {
    Alt,
    AltGraph,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
    ArrowUp,
    Backspace,
    BrowserBack,
    BrowserForward,
    CapsLock,
    Control,
    Delete,
    End,
    Enter,
    Escape,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    Home,
    Insert,
    Meta,
    NumLock,
    OS,
    PageDown,
    PageUp,
    ScrollLock,
    Shift,
    Spacebar,
    Tab,
    Other(String),
}

impl KeyId {
    pub fn from_string(key_name: String) -> Self {
        match Self::from_known_str(key_name.as_str()) {
            Some(key) => key,
            None => Self::Other(key_name),
        }
    }

    /// Return a KeyId for the given str, but only if it is a known
    /// enumerated key (not "Other"). Otherwise return None.
    ///
    /// The list of key values the browser could provide is here:
    /// https://developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_key_values
    /// Not all of these are implemented in this module.
    fn from_known_str(key_name: &str) -> Option<Self> {
        let key = match key_name {
            "Alt" => Self::Alt,
            "AltGraph" => Self::AltGraph,
            "ArrowDown" => Self::ArrowDown,
            "ArrowLeft" => Self::ArrowLeft,
            "ArrowRight" => Self::ArrowRight,
            "ArrowUp" => Self::ArrowUp,
            "Backspace" => Self::Backspace,
            "BrowserBack" => Self::BrowserBack,
            "BrowserForward" => Self::BrowserForward,
            "CapsLock" => Self::CapsLock,
            "Control" => Self::Control,
            "Delete" => Self::Delete,
            "End" => Self::End,
            "Enter" => Self::Enter,
            "Escape" => Self::Escape,
            "F1" => Self::F1,
            "F2" => Self::F2,
            "F3" => Self::F3,
            "F4" => Self::F4,
            "F5" => Self::F5,
            "F6" => Self::F6,
            "F7" => Self::F7,
            "F8" => Self::F8,
            "F9" => Self::F9,
            "F10" => Self::F10,
            "F11" => Self::F11,
            "F12" => Self::F12,
            "Home" => Self::Home,
            "Insert" => Self::Insert,
            "Meta" => Self::Meta,
            "NumLock" => Self::NumLock,
            "OS" => Self::OS,
            "PageDown" => Self::PageDown,
            "PageUp" => Self::PageUp,
            "Scroll" => Self::ScrollLock, // IE9 and IE11
            "ScrollLock" => Self::ScrollLock,
            "Shift" => Self::Shift,
            // According to MDN, older browsers including IE11 return
            // "Spacebar" instead of " ".
            "Spacebar" => Self::Spacebar,
            // This is the only case where the enum variant name doesn't exactly
            // match the official the key value.
            // (Can't have a Rust identifier named " ".)
            " " => Self::Spacebar,
            "Tab" => Self::Tab,
            _ => return None,
        };
        Some(key)
    }

    /// If the key is a function key, return the key number 1..=12.
    /// Otherwise return None.
    pub fn function_key_num(&self) -> Option<FunctionKeyId> {
        match self {
            Self::F1 => FunctionKeyId::new(1),
            Self::F2 => FunctionKeyId::new(2),
            Self::F3 => FunctionKeyId::new(3),
            Self::F4 => FunctionKeyId::new(4),
            Self::F5 => FunctionKeyId::new(5),
            Self::F6 => FunctionKeyId::new(6),
            Self::F7 => FunctionKeyId::new(7),
            Self::F8 => FunctionKeyId::new(8),
            Self::F9 => FunctionKeyId::new(9),
            Self::F10 => FunctionKeyId::new(10),
            Self::F11 => FunctionKeyId::new(11),
            Self::F12 => FunctionKeyId::new(12),
            _ => None,
        }
    }
}
