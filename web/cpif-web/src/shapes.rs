//! Library of shapes drawable on a DrawingCanvas
use crate::color::Color;
use crate::error::Error;
use crate::geometry::{AngleDeg, Length, PathCommand, Point, TransformStyle};
use crate::graphics::DrawingCanvas;

/// Draw an arrow from `p1` to `p2`.
///
/// The arrow head is on point `p2` and has a head with barbs coming out at
/// 45 degree angles. `head_size` is the distance from the tip of the arrow
/// to the shadow drop of the barb onto the arrow shaft (the barb length
/// times about 0.707).
///
/// The arrow color will be the current color set by `.set_stroke_color()`.
/// The arrow and barb width will be the current width set either by
/// `.set_line_width()` or by `.set_line_width_px()`.
pub fn arrow(
    dc: &mut DrawingCanvas,
    p1: Point,
    p2: Point,
    head_size: Length,
) -> Result<(), Error> {
    let arrow_length = (p2 - p1).hypot();
    let arrow_direction = (p2 - p1).angle_deg(dc.transform_style());
    let down_direction = match dc.transform_style() {
        TransformStyle::Pixel => 1.0,
        TransformStyle::Math => -1.0,
    };
    dc.with_save_restore(|dc| {
        // Transform coordinate space so that arrow goes "up" along y axis
        dc.transform(p1, arrow_direction, 1.0)?;

        // Draw arrow shaft
        dc.line(Point::new(0.0, 0.0), Point::new(0.0, arrow_length))?;

        // Draw barbs
        dc.lines(&[
            Point::new(-head_size, arrow_length + head_size * down_direction),
            Point::new(0.0, arrow_length),
            Point::new(head_size, arrow_length + head_size * down_direction),
        ])?;

        Ok(())
    })
}

/// Draw a resistor electrical symbol horizontally in an area of shape
/// (2.0, 0.5) centered at `loc`, and then scaled by `scale`.
pub fn draw_resistor(
    dc: &mut DrawingCanvas,
    loc: Point,
    scale: f64,
) -> Result<(), Error> {
    let points = [
        Point::new(-1.0, 0.0),
        Point::new(-0.6, 0.0),
        Point::new(-0.5, 0.4),
        Point::new(-0.3, -0.4),
        Point::new(-0.1, 0.4),
        Point::new(0.1, -0.4),
        Point::new(0.3, 0.4),
        Point::new(0.5, -0.4),
        Point::new(0.6, 0.0),
        Point::new(1.0, 0.0),
    ];
    dc.with_save_restore(|dc| {
        dc.set_stroke_color(Color::WHITE);
        dc.transform(loc, AngleDeg::from(0.0), scale)?;
        dc.set_line_width(0.01 / scale);
        dc.lines(&points[..])
    })
}

pub fn draw_capacitor(
    dc: &mut DrawingCanvas,
    loc: Point,
    scale: f64,
) -> Result<(), Error> {
    let points1 = [
        Point::new(-1.0, 0.0),
        Point::new(-0.25, 0.0),
        Point::new(-0.25, 0.4),
        Point::new(-0.25, -0.4),
    ];
    let points2 = [
        Point::new(1.0, 0.0),
        Point::new(0.25, 0.0),
        Point::new(0.25, 0.4),
        Point::new(0.25, -0.4),
    ];
    dc.with_save_restore(|dc| {
        dc.set_stroke_color(Color::WHITE);
        dc.transform(loc, AngleDeg::from(0.0), scale)?;
        dc.set_line_width(0.01 / scale);
        dc.lines(&points1[..])?;
        dc.lines(&points2[..])?;
        Ok(())
    })
}

pub fn draw_inductor(
    dc: &mut DrawingCanvas,
    loc: Point,
    scale: f64,
) -> Result<(), Error> {
    let tip_of_left_terminal = Point::new(-1.0, 0.0);
    let start_of_coil = Point::new(-0.6, 0.0);
    let path_commands = inductor_path_commands(start_of_coil);
    dc.with_save_restore(|dc| {
        dc.set_stroke_color(Color::WHITE);
        dc.transform(loc, AngleDeg::from(0.0), scale)?;
        dc.set_line_width(0.01 / scale);
        dc.path(
            tip_of_left_terminal,
            &path_commands,
            false,
            Color::TRANSPARENT,
            Color::WHITE,
        )
    })
}

fn inductor_path_commands(start_of_coil: Point) -> Vec<PathCommand> {
    let total_coil_x_distance = 1.2; // display units
    let ry = 0.40; // radius of coil in y direction
    let rx = 0.20; // radius of coil in x direction

    // There is one loop per "time unit".
    // The number of points per loop needs to be high enough that the bezier
    // curve is reasonably smooth. But it doesn't need to be too high.
    let points_per_coil_loop = 16;
    let delta_t = 1.0 / points_per_coil_loop as f64;

    let total_points = (points_per_coil_loop * 3) + points_per_coil_loop / 2;
    let total_t = total_points as f64 * delta_t; // total "time" == total turns
    let start_angle = AngleDeg::from(-90.0);

    // movement of center of coil in +x direction per turn of the coil
    let v_cx = (total_coil_x_distance - (2.0 * rx)) / total_t;

    // angular velocity of point on the coil: 1 turn per "time unit"
    let v_angle = AngleDeg::from(360.0);

    let mut path_commands: Vec<_> = Vec::<PathCommand>::new();
    use PathCommand::*;
    // Draw straight line from left terminal tip to start of coil
    path_commands.push(Line(start_of_coil));

    let ts = TransformStyle::Math;

    // Calculate an angle of rotation and point on the coil as a function of `t`
    let calc_angle_p = |t: f64| {
        let center =
            Point::new(start_of_coil.x() + rx + v_cx * t, start_of_coil.y());
        let angle = start_angle + v_angle * t;
        let p = center + angle.rotate_unit(ts).scale_xy(rx, ry);
        (angle, p)
    };

    // Calculate the angle of a line tangent to a point on the coil as a
    // function of the angle of rotation, with the tangent angle pointing in the
    // direction of rotation. The returned angle is in degrees clockwise from +y.
    let calc_tangent = |angle: AngleDeg| {
        let dx_dt = rx * angle.cos() * v_angle.as_rad() + v_cx;
        let dy_dt = -ry * angle.sin() * v_angle.as_rad();
        let a = Point::new(dx_dt, dy_dt).angle_deg(ts);
        a
    };

    for i in 0..total_points {
        let t = (i as f64) / points_per_coil_loop as f64;

        // Let `p` be the current point on the coil.
        // Let `angle` be the angle between the +y axis and line from the
        // current center of the coil and the current point on the coil.
        let (angle, p) = calc_angle_p(t);
        // Let `a` be the angle of the line tangent to `p`.
        let a = calc_tangent(angle);

        // Let `next_p` be the next point on the coil after `delta_t` more time.
        // Let `next_angle` be the angle between the +y axis and a line from the
        // next center of the coil and the next point on the coil.
        let (next_angle, next_p) = calc_angle_p(t + delta_t);
        // Let `next_a` be the angle of the line tangent to `next_p`.
        let next_a = calc_tangent(next_angle);

        // Let `d` be the distance between the current point and the next point.
        let d = (next_p - p).hypot();
        // Let `k` be a constant 0 < k <= 0.5 that controls the bezier curvature
        // by determining how far control points are from known points on the
        // coil, proportional to `d`.
        let k = 0.25;

        // Let `ctl_p1` be a point on the line tangent to `p` (the current
        // point), `d * k` units distant from `p` in the direction of rotation.
        let ctl_p1 = p + a.rotate_unit(ts).scale_xy(d * k, d * k);

        // Let `ctl_p2` be a point on the line tangent to `next_p` (the next
        // point on the coil), `d * k` units distant from `next_p` opposite the
        // direction of rotation.
        let ctl_p2 = next_p + next_a.rotate_unit(ts).scale_xy(-d * k, -d * k);

        // Add a cubic bezier curve to the path with `ctl_p1` and `ctl_p2` as
        // the control points and next_p as the end point.
        path_commands.push(CubicBezier(ctl_p1, ctl_p2, next_p));
    }

    // Draw straight line from end of coil to right terminal tip
    path_commands.push(Line(Point::new(1.0, 0.0)));

    path_commands
}

/// Like `DrawingCanvas.path()` but draws endpoints, control points, etc. to
/// help understand what the path method is trying to do.
///
/// A red dot is drawn at the starting location, control points are green dots,
/// end points are blue dots.
pub fn path_debug(
    dc: &mut DrawingCanvas,
    start_loc: Point,
    path_commands: &[PathCommand],
    close_path: bool,
) -> Result<(), Error> {
    let dot_radius = dc.pxsize() * 3.0;
    let arrow_head_size = dc.pxsize() * 5.0;
    let arrow_width = dc.pxsize() * 1.0;
    let arrow_color = Color::BRIGHT_WHITE;
    let start_pt_color = Color::RED;
    let control_pt_color = Color::GREEN;
    let end_pt_color = Color::BLUE;
    dc.with_save_restore(|dc| {
        dc.set_line_width(arrow_width);
        dc.set_stroke_color(arrow_color);
        dc.set_fill_color(start_pt_color);
        dc.dot(start_loc, dot_radius)?;
        if path_commands.len() == 0 {
            return Ok(());
        }
        let mut current_pt = start_loc;
        for path_command in path_commands.iter() {
            use PathCommand::*;
            match path_command {
                Line(p) => {
                    arrow(dc, current_pt, *p, arrow_head_size)?;
                    dc.set_fill_color(end_pt_color);
                    dc.dot(*p, dot_radius)?;
                    current_pt = *p;
                }
                QuadBezier(p0, p1) => {
                    arrow(dc, current_pt, *p0, arrow_head_size)?;
                    arrow(dc, *p0, *p1, arrow_head_size)?;
                    dc.set_fill_color(control_pt_color);
                    dc.dot(*p0, dot_radius)?;
                    dc.set_fill_color(end_pt_color);
                    dc.dot(*p1, dot_radius)?;
                    current_pt = *p1;
                }
                CubicBezier(p0, p1, p2) => {
                    arrow(dc, current_pt, *p0, arrow_head_size)?;
                    arrow(dc, *p0, *p1, arrow_head_size)?;
                    arrow(dc, *p1, *p2, arrow_head_size)?;
                    dc.set_fill_color(control_pt_color);
                    dc.dot(*p0, dot_radius)?;
                    dc.dot(*p1, dot_radius)?;
                    dc.set_fill_color(end_pt_color);
                    dc.dot(*p2, dot_radius)?;
                    current_pt = *p2;
                }
            }
        }
        if close_path {
            arrow(dc, current_pt, start_loc, arrow_head_size)?;
        }
        Ok(())
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::color::Color;
    use crate::geometry::{self, AngleDeg};
    use crate::graphics::CanvasOptions;
    use crate::test_util;
    use wasm_bindgen_test::*;

    wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn test_hello() {
        console_log!("Hello from the shapes tests module");
    }

    #[wasm_bindgen_test]
    fn test_arrows() -> Result<(), Error> {
        let mut dc =
            test_util::create_dc("test_arrows", CanvasOptions::default());
        dc.set_math_transform()?;
        dc.set_line_width(0.01);

        // Arrow pointing up
        dc.set_stroke_color(Color::WHITE);
        arrow(&mut dc, Point::new(0.0, 0.0), Point::new(0.0, 0.5), 0.1)?;

        // In upper-left quadrant draw crossed arrows pointing up
        dc.set_stroke_color(Color::WHITE);
        arrow(&mut dc, Point::new(-0.75, 0.0), Point::new(-0.25, 0.5), 0.1)?;
        arrow(&mut dc, Point::new(-0.25, 0.0), Point::new(-0.75, 0.5), 0.1)?;
        let p1 = geometry::intersect_points_angles(
            Point::new(-0.75, 0.0),
            AngleDeg::from(45.0),
            Point::new(-0.25, 0.0),
            AngleDeg::from(-45.0),
        )
        .expect("lines should intersect");
        dc.set_fill_color(Color::BLUE);
        dc.set_stroke_color(Color::TRANSPARENT);
        dc.circle(p1, 0.01)?;

        // In upper-right quadrant draw crassed arrows pointing down
        dc.set_stroke_color(Color::WHITE);
        arrow(&mut dc, Point::new(0.75, 0.5), Point::new(0.25, 0.0), 0.1)?;
        arrow(&mut dc, Point::new(0.25, 0.5), Point::new(0.75, 0.0), 0.1)?;
        let p2 = geometry::intersect_points_angles(
            Point::new(0.75, 0.5),
            AngleDeg::from(-135.0),
            Point::new(0.25, 0.5),
            AngleDeg::from(135.0),
        )
        .expect("lines should intersect");
        dc.set_fill_color(Color::BLUE);
        dc.set_stroke_color(Color::TRANSPARENT);
        dc.circle(p2, 0.01)?;

        Ok(())
    }

    #[wasm_bindgen_test]
    fn test_inductor() -> Result<(), Error> {
        wasm_logger::init(wasm_logger::Config::default());
        log::debug!("test_inductor");

        let mut dc =
            test_util::create_dc("test_inductor", CanvasOptions::default());
        dc.set_math_transform()?;

        draw_inductor(&mut dc, Point::new(0.0, 0.0), 1.0)?;

        let tip_of_left_terminal = Point::new(-1.0, 0.0);
        let start_of_coil = Point::new(-0.6, 0.0);
        let path_commands = inductor_path_commands(start_of_coil);
        path_debug(&mut dc, tip_of_left_terminal, &path_commands, false)?;

        Ok(())
    }

    #[wasm_bindgen_test]
    fn test_electrical_component_symbols() -> Result<(), Error> {
        let mut dc = test_util::create_dc(
            "test_electrical_component_symbols",
            CanvasOptions::default(),
        );

        // Use mathematical coordinates
        dc.set_math_transform()?;

        // Scale each component to be 1/3 natural size, so scaled from (width,
        // height) shape (2.0, 0.5) to (0.667, 0.167)
        let scale: f64 = 1.0 / 3.0;

        draw_resistor(&mut dc, Point::new(-2.0 * scale, 0.0), scale)?;
        draw_capacitor(&mut dc, Point::new(0.0, 0.0), scale)?;
        draw_inductor(&mut dc, Point::new(2.0 * scale, 0.0), scale)?;
        Ok(())
    }
}
