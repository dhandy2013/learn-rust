//! Utility functions for interacting with the web browser API.
//! Functions that have web_sys items in their signature or return
//! JsValue errors are meant to be internal to this crate. This is not a
//! hard-and-fast rule though because these functions can be useful to
//! other crates.
use crate::Error;
use log;
use std::io::Write;
use wasm_bindgen::{Clamped, JsCast, JsValue};

/// Return the global Window object, or panic.
pub fn get_window() -> web_sys::Window {
    web_sys::window().expect("global window should exist")
}

/// Return the document object for the current web page, or panic.
pub fn get_document() -> web_sys::Document {
    get_window()
        .document()
        .expect("document on global window should exist")
}

/// Return the performance object for the current web page, or panic.
/// It has a .now() method for performance timing.
pub fn get_performance() -> web_sys::Performance {
    get_window()
        .performance()
        .expect("window.performance should exist")
}

/// Return the HtmlCanvasElement with the given ID.
/// Return error if not found or not a canvas element.
pub fn get_canvas_by_id(
    canvas_id: &str,
) -> Result<web_sys::HtmlCanvasElement, Error> {
    let document = get_document();
    let element = document.get_element_by_id(canvas_id).ok_or_else(|| {
        let msg = format!("canvas #{} should exist", canvas_id);
        log::error!("{}", msg);
        Error::from(msg)
    })?;
    element
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .or_else(|element| {
            let msg = format!(
                "Element #{} is a {} not a canvas",
                canvas_id,
                element.tag_name()
            );
            log::error!("{}", msg);
            Err(Error::from(msg))
        })
}

/// Return the Element with the given ID if it exists,
/// or None if it does not exist in the document.
pub fn get_element_by_id(element_id: &str) -> Option<web_sys::Element> {
    let document = get_document();
    document.get_element_by_id(element_id)
}

/// Calculate the size of the whole available browser client area.
/// Panic in the unlikely case that your web document has no documentElement.
/// If width or height are zero, substitute 1 instead.
/// Return (width, height).
pub fn calc_full_browser_client_size() -> (u32, u32) {
    let document = get_document();
    let de = document.document_element().unwrap_or_else(|| {
        let msg = "Document has no documentElement";
        log::error!("{}", msg);
        panic!("{}", msg);
    });
    let client_width = de.client_width() as u32;
    let client_height = de.client_height() as u32;
    let canvas_width = if client_width > 0 { client_width } else { 1 };
    let canvas_height = if client_height > 0 { client_height } else { 1 };
    (canvas_width, canvas_height)
}

/// Create and return an HtmlCanvasElement with ID `canvas_id`, placed inside an
/// existing div element with ID `container_id`.
///
/// If the optional size tuple is given, create the canvas with dimensions
/// (`width`, `height`) pixels. Otherwise, if size is None, create the canvas
/// sized to fill the entire browser client area.
pub fn create_canvas(
    container_id: &str,
    canvas_id: &str,
    size: Option<(u32, u32)>,
) -> Result<web_sys::HtmlCanvasElement, JsValue> {
    // Calculate final canvas height and width
    let (canvas_width, canvas_height) = match size {
        Some((width, height)) => (width, height),
        None => calc_full_browser_client_size(),
    };

    // Create the canvas and set its ID and size
    let document = get_document();
    let canvas = document
        .create_element("canvas")?
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .expect("canvas should be an HtmlCanvasElement");
    canvas.set_id(canvas_id);
    canvas.set_width(canvas_width);
    canvas.set_height(canvas_height);

    if let Some(container) = document.get_element_by_id(container_id) {
        container.append_child(&canvas)?;
        Ok(canvas)
    } else {
        let msg = format!("Canvas container #{} does not exist", container_id);
        log::error!("{}", msg);
        Err(JsValue::from_str(&msg))
    }
}

/// Convert 32-bit unsigned integer representing an RGBA color to a JsValue
/// containing a CSS string representation of the color. The upper 8 bits
/// are the Red value, the next 8 bits Blue, the next 8 bits Green, and the
/// lower 8 bits are Alpha.
/// https://developer.mozilla.org/en-US/docs/Web/CSS/color_value
///
/// Example:
///     "brown": 0xaa5500ff = red(0xaa), green(0x55), blue(0x00), alpha(0xff)
///     Resulting JavaScript string: "#aaff00ff"
pub fn color_num_to_js_value(color_num: u32) -> JsValue {
    // By careful counting I have determined the exact number of ASCII bytes in
    // an RGBA color string is nine: The '#' character plus 8 hex digits.
    let mut color_buf = [0u8; 9];
    write!(&mut color_buf[..], "#{:08X}", color_num).unwrap();
    // SAFETY: these bytes are guaranteed to be ASCII.
    let color_str = unsafe { std::str::from_utf8_unchecked(&color_buf[..]) };
    JsValue::from_str(color_str)
}

/// Return the HtmlFormElement with the given ID.
/// Return an error if not found or not a form element.
pub fn get_form_by_id(
    form_id: &str,
) -> Result<web_sys::HtmlFormElement, Error> {
    let element = get_element_by_id(form_id).ok_or_else(|| {
        Error::from(format!("get_form_by_id({:?}): no such element", form_id))
    })?;
    element
        .dyn_into::<web_sys::HtmlFormElement>()
        .or_else(|element| {
            Err(Error::from(format!(
                "element #{} was {}, expected form",
                form_id,
                element.tag_name()
            )))
        })
}

/// Find an input element with the given ID.
/// Return None if it does not exist in the document or is not an input.
pub fn get_input_element(input_id: &str) -> Option<web_sys::HtmlInputElement> {
    let element = get_element_by_id(input_id)?;
    let input = match element.dyn_into::<web_sys::HtmlInputElement>() {
        Ok(input) => input,
        Err(_) => return None,
    };
    Some(input)
}

/// Get the string value of the input element with the given ID.
/// Return an error if not found or not an input element.
pub fn get_input_string(input_id: &str) -> Result<String, Error> {
    let input = get_input_element(input_id).ok_or_else(|| {
        Error::from(format!(
            "get_input_string({:?}): no such input element",
            input_id
        ))
    })?;
    Ok(input.value())
}

/// Get the boolean input value of a checkbox or radio button.
/// Return true iff the input is checked or selected.
/// Return an error if input not found or not an input element.
pub fn get_input_checked(input_id: &str) -> Result<bool, Error> {
    let input = get_input_element(input_id).ok_or_else(|| {
        Error::from(format!(
            "get_input_checked({:?}): no such input element",
            input_id
        ))
    })?;
    Ok(input.checked())
}

/// Set the string value of an input element.
/// Return an error if input not found or not an input element.
pub fn set_input_str(input_id: &str, value: &str) -> Result<(), Error> {
    let input = get_input_element(input_id).ok_or_else(|| {
        Error::from(format!(
            "set_input_str({:?}): no such input element",
            input_id
        ))
    })?;
    input.set_value(value);
    Ok(())
}

/// Set the HTML contents of the element with the given ID.
/// Return an error if element not found.
pub fn set_inner_html(element_id: &str, value: &str) -> Result<(), Error> {
    let element = get_element_by_id(element_id).ok_or_else(|| {
        Error::from(format!(
            "set_inner_html({:?}): no such element",
            element_id
        ))
    })?;
    element.set_inner_html(value);
    Ok(())
}

/// Return a 2D rendering context for the canvas, or return an error.
/// Pass alpha=false to speed up bitmap rendering.
pub fn get_render_context_2d(
    canvas: &web_sys::HtmlCanvasElement,
    alpha: bool,
) -> Result<web_sys::CanvasRenderingContext2d, JsValue> {
    let context_options = js_sys::Map::new();
    context_options
        .set(&JsValue::from_str("alpha"), &JsValue::from_bool(alpha));
    let maybe_ctx =
        canvas.get_context_with_context_options("2d", &context_options)?;
    if let Some(ctx) = maybe_ctx {
        Ok(ctx.dyn_into::<web_sys::CanvasRenderingContext2d>().unwrap())
    } else {
        let msg = "Canvas does not have a 2D rendering context";
        log::error!("{}", msg);
        Err(JsValue::from_str(&msg))
    }
}

/// Copy a pixel buffer to an external canvas.
/// Return Err if creating the ImageData or copying it to the canvas fails.
pub fn try_blit(
    pixel_bytes: &[u8],
    width: u32,
    height: u32,
    ctx: &web_sys::CanvasRenderingContext2d,
    x: f64,
    y: f64,
) -> Result<(), JsValue> {
    const BYTES_PER_PIXEL: usize = 4;
    let expected_bytes_len = width as usize * height as usize * BYTES_PER_PIXEL;
    if pixel_bytes.len() != expected_bytes_len {
        return Err(JsValue::from(&format!(
            "pixel_bytes len was {} but should be {}",
            pixel_bytes.len(),
            expected_bytes_len
        )));
    }
    let data = web_sys::ImageData::new_with_u8_clamped_array_and_sh(
        Clamped(pixel_bytes),
        width,
        height,
    )?;
    ctx.put_image_data(&data, x, y).map_err(|err| err.into())
}

/// Copy a pixel buffer to an external canvas.
/// Panic if creating the ImageData or copying it to the canvas fails.
pub fn blit(
    pixel_bytes: &mut [u8],
    width: u32,
    height: u32,
    ctx: &web_sys::CanvasRenderingContext2d,
    x: f64,
    y: f64,
) {
    if let Err(err) = try_blit(pixel_bytes, width, height, ctx, x, y) {
        panic!("Error copying pixel bytes to canvas: {:?}", err);
    }
}

/// Set a property value by name on the global object.
/// In main browser context the global object is the Window object,
/// in a web worker it is a WorkerGlobalScope-derived object.
/// Choose carefully what key to use, you can overwrite DOM values.
///
/// Good news: You can pass structs defined in Rust with the #[wasm_bindgen]
/// attribute, and they become accessible and usable from JavaScript.
/// Bad news: You cannot easly round-trip Rust structs *back* to Rust from
/// JavaScript.  See the documenation for `get_global_val()` for more details.
pub fn set_global_val(key: &str, value: JsValue) -> Result<(), JsValue> {
    let global = js_sys::global();
    let property_key = JsValue::from_str(key);
    // MDN docs say the return value from Reflect.set() is a boolean
    // "indicating whether or not setting the property was successful".
    // No hint is given as to why it would ever not be successful.
    let result = js_sys::Reflect::set(&global, &property_key, &value)?;
    if result {
        Ok(())
    } else {
        Err(JsValue::from_str(&format!(
            "unable to set {:?} property on global object",
            key
        )))
    }
}

/// Get a property value by name from the global object.
/// In main browser context the global object is the Window object,
/// in a web worker it is a WorkerGlobalScope-derived object.
///
/// Unfortunately, you cannot easily round-trip a Rust struct that has been
/// exported to a JsValue back to a Rust struct, if the Rust struct is not
/// something simple for which JsCast is already implemented, such as String.
/// See:
/// https://github.com/rustwasm/wasm-bindgen/issues/2231
/// https://docs.rs/wasm-bindgen-downcast/latest/wasm_bindgen_downcast/
pub fn get_global_val(key: &str) -> Result<JsValue, JsValue> {
    let property_key = JsValue::from_str(key);
    let global = js_sys::global();
    js_sys::Reflect::get(&global, &property_key)
}

/// Get a URL query parameter from the current web page.
/// Return None if no parameter of that name is set.
/// Return None if the page URL does not exist or is invalid.
pub fn get_page_param(name: &str) -> Option<String> {
    let window = get_window();
    let href_string = match window.location().href() {
        Ok(h) => h,
        Err(_) => return None,
    };
    let url = match web_sys::Url::new(&href_string) {
        Ok(u) => u,
        Err(_) => return None,
    };
    url.search_params().get(name)
}

/// Convert a Window or Element to an EventTarget to unify event handling.
/// Return an error if the parameter is not really an event target.
pub fn event_target_from_jsvalue(
    obj: JsValue,
) -> Result<web_sys::EventTarget, Error> {
    obj.dyn_into::<web_sys::EventTarget>().or_else(|obj| {
        Err(Error::from(format!(
            "object {:?} is not an event target",
            obj
        )))
    })
}

/// Return the HTML element with the given ID.
/// Return an error if the element does not exists or is not an HTML element
/// (e.g. if it is an SvgElement instead).
pub fn get_html_element(
    element_id: &str,
) -> Result<web_sys::HtmlElement, Error> {
    let element = get_element_by_id(element_id)
        .ok_or_else(|| {
            Error::from(format!(
                "get_html_element_by_id({:?}): no such element",
                element_id
            ))
        })?
        .dyn_into::<web_sys::HtmlElement>()
        .or_else(|element| {
            Err(Error::from(format!(
                "element #{} is a {} not an HTML element",
                element_id,
                element.tag_name()
            )))
        })?;
    Ok(element)
}

/// Return the title of the HTML element with the given ID.
/// Return an error if no such HTML element exists in the document.
pub fn get_element_title(element_id: &str) -> Result<String, Error> {
    let element = get_html_element(element_id)?;
    let title = element.title();
    Ok(title)
}

/// Set the title of the HTML element with the given ID.
/// Return an error if no such HTML element exists in the document.
pub fn set_element_title(element_id: &str, title: &str) -> Result<(), Error> {
    let element = get_html_element(element_id)?;
    element.set_title(title);
    Ok(())
}

/// Set a new image href on a button containing an SVG image.
pub fn set_svg_button_image(
    button_id: &str,
    image_href: &str,
) -> Result<(), JsValue> {
    let button = get_element_by_id(button_id)
        .ok_or_else(|| {
            let msg = format!(
                "set_svg_button_image({:?}, {:?}): no such button",
                button_id, image_href
            );
            log::error!("{}", msg);
            JsValue::from_str(&msg)
        })?
        .dyn_into::<web_sys::HtmlButtonElement>()
        .or_else(|element| {
            let msg = format!(
                "element #{} tag is {:?} not button",
                button_id,
                element.tag_name(),
            );
            log::error!("{}", msg);
            Err(JsValue::from_str(&msg))
        })?;

    let svg = button
        .first_element_child()
        .ok_or_else(|| {
            let msg = format!("button #{} is empty, missing svg", button_id);
            log::error!("{}", msg);
            JsValue::from_str(&msg)
        })?
        .dyn_into::<web_sys::SvgElement>()
        .or_else(|element| {
            let msg = format!(
                "button #{} first child tag is {:?} not svg",
                button_id,
                element.tag_name(),
            );
            log::error!("{}", msg);
            return Err(JsValue::from_str(&msg));
        })?;

    let image = svg
        .first_element_child()
        .ok_or_else(|| {
            let msg = format!(
                "button #{} svg element is empty, missing image",
                button_id
            );
            log::error!("{}", msg);
            JsValue::from_str(&msg)
        })?
        .dyn_into::<web_sys::SvgImageElement>()
        .or_else(|element| {
            let msg = format!(
                "button #{} svg element first child tag is {:?} not image",
                button_id,
                element.tag_name(),
            );
            log::error!("{}", msg);
            return Err(JsValue::from_str(&msg));
        })?;

    svg.remove_child(&image)?;
    let document = get_document();
    let new_image = document
        .create_element_ns(Some("http://www.w3.org/2000/svg"), "image")?;
    new_image.set_attribute("href", image_href)?;
    svg.append_child(&new_image)?;
    Ok(())
}

/// Add CSS class `class_name` to an element.
/// Do nothing if that CSS class is already set on that element.
/// Return an error if an element with ID `element_id` does not exist.
pub fn add_css_class(element_id: &str, class_name: &str) -> Result<(), Error> {
    let element = get_element_by_id(element_id).ok_or_else(|| {
        Error::from(format!(
            "web_util::add_css_class: no element with ID {:?}",
            element_id
        ))
    })?;
    element.class_list().add_1(class_name)?;
    Ok(())
}

/// Remove CSS class `class_name` from an element.
/// Do nothing if that CSS class was not set on that element.
/// Return an error if an element with that ID does not exist.
pub fn remove_css_class(
    element_id: &str,
    class_name: &str,
) -> Result<(), Error> {
    let element = get_element_by_id(element_id).ok_or_else(|| {
        Error::from(format!(
            "web_util::remove_css_class: no element with ID {:?}",
            element_id
        ))
    })?;
    element.class_list().remove_1(class_name)?;
    Ok(())
}

/// If CSS class `class_name` was set on an element, remove it.  Otherwise, add
/// it.
/// Return true iff the CSS class is on the element after the call.
/// Return an error if an element with that ID does not exist.
pub fn toggle_css_class(
    element_id: &str,
    class_name: &str,
) -> Result<bool, Error> {
    let element = get_element_by_id(element_id).ok_or_else(|| {
        Error::from(format!(
            "web_util::toggle_css_class: no element with ID {:?}",
            element_id
        ))
    })?;
    let result = element.class_list().toggle(class_name)?;
    Ok(result)
}

/// Return true iff the CSS class is on the element.
/// Return an error if an element with that ID does not exist.
pub fn contains_css_class(
    element_id: &str,
    class_name: &str,
) -> Result<bool, Error> {
    let element = get_element_by_id(element_id).ok_or_else(|| {
        Error::from(format!(
            "web_util::contains_css_class: no element with ID {:?}",
            element_id
        ))
    })?;
    let result = element.class_list().contains(class_name);
    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;
    use wasm_bindgen_test::*;

    // Configure wasm_bindgen_test to always run tests in browser, not Node.js.
    // I don't want to have to install Node.js just to run tests.
    wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn test_hello() {
        console_log!("Hello from the web_util tests module");
    }

    #[wasm_bindgen_test]
    fn test_basic_functions_dont_panic() {
        let _window = get_window();
        let _document = get_document();
        let _performance = get_performance();
    }
}
