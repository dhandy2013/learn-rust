//! Dynamic objects of any type, that you can cast to the concrete type.
use crate::error::Error;
use std::any::Any;
use std::sync::{Arc, Mutex};

/// Object-safe trait for storing hetrogenous objects in a collection,
/// or dynamically passing them to a different application layer.
pub trait DynAny: Any {
    /// Return an Any reference to this object.
    /// Someday I will create a derive macro for this, but for now implement
    /// it like this:
    ///
    /// fn any_ref(&self) -> &dyn Any {
    ///     let any_ref: &dyn Any = &*self;
    ///     any_ref
    /// }
    fn any_ref(&self) -> &dyn Any;

    /// Return a mutable Any reference to this object.
    /// Someday I will create a derive macro for this, but for now implement
    /// it like this:
    ///
    /// fn any_mut(&mut self) -> &mut dyn Any {
    ///     let any_mut: &mut dyn Any = &mut *self;
    ///     any_mut
    /// }
    fn any_mut(&mut self) -> &mut dyn Any;
}

/// Smart pointer to dynamically-typed object.
pub type DynAnyPtr = Arc<Mutex<dyn DynAny + Send + Sync>>;

pub fn dyn_cast_ref<T: DynAny>(any_ref: &dyn Any) -> Result<&T, Error> {
    any_ref.downcast_ref::<T>().ok_or_else(|| {
        let type_name = std::any::type_name::<T>();
        let msg =
            format!("Could not cast DynAny to ref {:?}: wrong type", type_name);
        Error::from(msg)
    })
}

pub fn dyn_cast_mut<T: DynAny>(any_mut: &mut dyn Any) -> Result<&mut T, Error> {
    any_mut.downcast_mut::<T>().ok_or_else(|| {
        let type_name = std::any::type_name::<T>();
        let msg =
            format!("Could not cast DynAny to mut {:?}: wrong type", type_name);
        Error::from(msg)
    })
}
