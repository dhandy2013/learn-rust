//! The the main application data and related interfaces.
//! An app contains one model and one view instance which are supplied by the
//! user of this model. Internally it also contains an animator instance and other
//! helper objects.
use crate::animation::Animator;
use crate::dynany::{dyn_cast_mut, DynAny};
use crate::error::Error;
use crate::events::{
    Closures, Event, EventHandler, EventListenerID, EventSignal, EventTarget,
    EventType,
};
use crate::geometry::Point;
use crate::graphics::{
    CanvasOptions, DrawingCanvas, DrawingCanvasCallback, DrawingCanvasPtr,
};
use crate::sound::{SoundController, SoundID};
use std::any::Any;
use std::collections::{hash_map::Entry, HashMap};
use std::sync::{Arc, Mutex};
use wasm_bindgen::{closure::Closure, JsCast};

lazy_static! {
    static ref APP_REGISTRY: Mutex<HashMap<String, DynAppPtr>> =
        Mutex::new(HashMap::new());
}

/// Object-safe application trait so that we can store multiple applications
/// of different types in the global app registry.
pub trait DynApp: DynAny + Send + Sync {}
pub type DynAppPtr = Arc<Mutex<dyn DynApp>>;

impl<M: Model, V: View<M>> DynApp for App<M, V> {}

/// Interface for managing graphics and sound-related resources.
///
/// This trait must be object safe and therefore does not reference generic
/// types such as the model or the view.
pub trait ResourceManager {
    /// The application ID
    fn id(&self) -> &str;

    /// The main canvas ID is the application ID with the standard drawing
    /// canvas ID suffix appended.
    fn main_canvas_id(&self) -> Result<String, Error>;

    /// Create an HTML canvas inside of an existing div element.
    /// The div element must have the same ID as the application.
    /// Register a DrawingCanvas object for this new HTML canvas.
    fn create_main_canvas(
        &mut self,
        options: CanvasOptions,
    ) -> Result<(), Error>;

    /// Create and register a DrawingCanvas object for an existing
    /// HTML canvas element with ID `canvas_id`.
    fn create_canvas_with_id(
        &mut self,
        canvas_id: &str,
        options: CanvasOptions,
    ) -> Result<(), Error>;

    /// Do some action with the main DrawingCanvas object
    /// (the one that was created by calling `.create_main_canvas()`.)
    fn with_main_canvas(
        &self,
        doit: &mut dyn DrawingCanvasCallback,
    ) -> Result<(), Error>;

    /// Do some action with a DrawingCanvas object that was previously
    /// created by calling `.create_canvas_with_id(canvas_id)`.
    fn with_canvas(
        &self,
        canvas_id: &str,
        doit: &mut dyn DrawingCanvasCallback,
    ) -> Result<(), Error>;

    /// Update all registered DrawingCanvas objects with sizes and scales
    /// to match the current sizes of their respective HTML canvas elements.
    /// This is automatically called by the app when the window is resized.
    fn resize_all_canvases(&self) -> Result<(), Error>;

    /// Start the animation interval timer.
    fn start(&mut self, frame_time_sec: f64) -> Result<(), Error>;

    /// Stop the animation interval timer.
    fn stop(&mut self);

    /// Return true iff the the animation interval timer is current running.
    fn is_running(&self) -> bool;

    /// Call `window.requestAnimationFrame()` with a callback parameter that
    /// will call the view `.draw()` method.
    fn cause_redraw(&mut self) -> Result<(), Error>;

    /// Un-register an event listener that had previously been registered by
    /// calling `app.add_event_listener()`.
    fn del_event_listener(&mut self, listener_id: EventListenerID);

    /// Return a mutable reference to the sound controller object.
    ///
    /// The SoundController is created the first time this method is invoked.
    /// It contains an AudioContext, which must not be created until first
    /// user interaction with the browser page (e.g. clicking something.)
    /// If this method gets called before then, you will get this error:
    ///
    /// "An AudioContext was prevented from starting automatically. It must be
    /// created or resumed after a user gesture on the page."
    fn sound_controller(&mut self) -> Result<&mut SoundController, Error>;
}

pub trait Model: 'static + Send + Sync {
    /// Advance the model state by one step in time.
    /// Return true if something changed such that the view should redraw.
    /// Return false if nothing changed that affects the view.
    /// Return an error if something went wrong that should be reported.
    fn step(&mut self, dt: f64) -> Result<bool, Error>;
}

type ModelPtr<M> = Arc<Mutex<M>>;

pub trait View<M>: 'static + Send + Sync
where
    M: Model,
{
    /// Redraw everything given the view, app, and read-only model.
    fn draw(
        &mut self,
        rm: &dyn ResourceManager,
        model: &M,
    ) -> Result<(), Error>;
}

type ViewPtr<V> = Arc<Mutex<V>>;

/// Application object. Contains a model, view, and helpers.
/// Type parameters: M is the model type, V is the view type.
pub struct App<M: Model, V: View<M>> {
    id: String,
    /// Smart pointer to the Model object provided by the client.
    ///
    /// Note to future me: No, you cannot rip out the smart pointer and replace
    /// it with a Box or regular ownership. You have tried this several times
    /// and it did not work. Using a smart pointer here overcomes gnarly
    /// lifetime and borrowing issues when you try to pass the model to callback
    /// functions.
    model: ModelPtr<M>,
    /// smart pointer to the View object provided by the client.
    ///
    /// Note to future me: See the note on the `model` field above.
    view: ViewPtr<V>,
    animator: Animator,
    closures: Closures,
    canvases: Vec<DrawingCanvasPtr>,
    interval_callback:
        fn(&mut Self, &mut M, &mut V, f64) -> Result<bool, Error>,
    resize_callback: Option<fn(&mut Self, &mut M, &mut V) -> Result<(), Error>>,
    sound_end_callback:
        Option<fn(&mut V, &mut M, &mut Self, SoundID) -> Result<(), Error>>,
    sound_controller: Option<SoundController>,
}

impl<M: Model, V: View<M>> DynAny for App<M, V> {
    fn any_ref(&self) -> &dyn Any {
        let any_ref: &dyn Any = &*self;
        any_ref
    }

    fn any_mut(&mut self) -> &mut dyn Any {
        let any_mut: &mut dyn Any = &mut *self;
        any_mut
    }
}

impl<M: Model, V: View<M>> ResourceManager for App<M, V> {
    fn id(&self) -> &str {
        &self.id
    }

    fn main_canvas_id(&self) -> Result<String, Error> {
        let canvas_id = DrawingCanvas::canvas_id_for_div_id(self.id());
        match self.get_canvas_maybe(&canvas_id) {
            Ok(dc_maybe) => match dc_maybe {
                Some(_) => Ok(canvas_id),
                None => Err(Error::from(format!(
                    "app {:?}: main_canvas_id: canvas {:?} not created yet",
                    self.id(),
                    canvas_id,
                ))),
            },
            Err(err) => Err(err),
        }
    }

    fn create_main_canvas(
        &mut self,
        options: CanvasOptions,
    ) -> Result<(), Error> {
        let canvas_id = DrawingCanvas::canvas_id_for_div_id(self.id());
        if self.get_canvas_maybe(&canvas_id)?.is_some() {
            return Err(Error::from(format!(
                "app {:?}: create_main_canvas: canvas {:?} already exists",
                self.id(),
                canvas_id,
            )));
        }
        let dc_ptr = DrawingCanvas::new_ptr_in_div(self.id(), options)?;
        self.canvases.push(dc_ptr.clone());
        Ok(())
    }

    fn create_canvas_with_id(
        &mut self,
        canvas_id: &str,
        options: CanvasOptions,
    ) -> Result<(), Error> {
        if self.get_canvas_maybe(canvas_id)?.is_some() {
            return Err(Error::from(format!(
                "app {:?}: canvas {:?} already exists",
                self.id(),
                canvas_id,
            )));
        }
        let dc_ptr = DrawingCanvas::new_ptr(canvas_id, options)?;
        self.canvases.push(dc_ptr.clone());
        Ok(())
    }

    fn with_main_canvas(
        &self,
        doit: &mut dyn DrawingCanvasCallback,
    ) -> Result<(), Error> {
        let dc_ptr = self.get_main_canvas()?;
        self.with_canvas_ptr(dc_ptr, doit)
    }

    fn with_canvas(
        &self,
        canvas_id: &str,
        doit: &mut dyn DrawingCanvasCallback,
    ) -> Result<(), Error> {
        let dc_ptr = self.get_canvas(canvas_id)?;
        self.with_canvas_ptr(dc_ptr, doit)
    }

    fn resize_all_canvases(&self) -> Result<(), Error> {
        for dc_ptr in self.canvases.iter() {
            let mut dc_guard = dc_ptr.try_lock()?;
            dc_guard.resize()?;
        }
        Ok(())
    }

    fn start(&mut self, frame_time_sec: f64) -> Result<(), Error> {
        self.animator.start(frame_time_sec)
    }

    fn stop(&mut self) {
        self.animator.stop();
    }

    fn is_running(&self) -> bool {
        self.animator.is_running()
    }

    fn cause_redraw(&mut self) -> Result<(), Error> {
        self.animator.cause_redraw()
    }

    fn del_event_listener(&mut self, listener_id: EventListenerID) {
        self.closures.del_event_listener(listener_id);
    }

    fn sound_controller(&mut self) -> Result<&mut SoundController, Error> {
        if self.sound_controller.is_none() {
            self.create_sound_controller()?;
        }
        Ok(self
            .sound_controller
            .as_mut()
            .expect("sound controller should exist"))
    }
}

impl<M: Model, V: View<M>> App<M, V> {
    /// Create a new App and save it in the global app registry.
    ///
    /// Parameters
    /// `app_id`: the key for this App in the global app registry.
    /// `model`: the Model for this App. It will be owned by the App.
    /// `view`: the View for this App. It will be owned by the App.
    /// `setup`: initialization callback
    ///
    /// The `setup` callback is passed a mutable reference to the DynApp
    /// interface of this App instance, a mutable reference to the model, and a
    /// mutable reference to the view. During setup clients typically will:
    /// - Create one or more canvases (`app.create_main_canvas()`, etc.)
    /// - Set event handlers (`app.add_element_event_listener()`, etc.)
    /// After `setup` returns, the framework will automatically schedule the
    /// first call to View::draw().
    ///
    /// Return an error if there are any problems creating or initializing the
    /// App.
    ///
    /// This create method does not return the App instance itself because
    /// clients do not need direct access to it. Clients will be passed a
    /// mutable reference to the app in the setup closure and in the event
    /// handler callbacks.
    pub fn create<F>(
        app_id: &str,
        model: M,
        view: V,
        setup: F,
    ) -> Result<(), Error>
    where
        F: FnOnce(&mut Self, &mut M, &mut V) -> Result<(), Error>,
    {
        // Wrap the model and view in smart pointers.
        let model_ptr = Arc::new(Mutex::new(model));
        let view_ptr = Arc::new(Mutex::new(view));

        // Create the animator and hook it up to the app
        let animation_closure = {
            let moved_app_id = app_id.to_owned();
            Closure::new(move |timestamp: f64| {
                if let Err(err) =
                    Self::on_animation_frame(&moved_app_id, timestamp)
                {
                    log::error!(
                        "App {:?}: animation_closure: {:?}",
                        moved_app_id,
                        err
                    );
                }
            })
        };
        let timer_closure = {
            let moved_app_id = app_id.to_owned();
            Closure::new(move || {
                if let Err(err) = Self::on_timer(&moved_app_id) {
                    log::error!(
                        "App {:?}: timer_closure: {:?}",
                        moved_app_id,
                        err
                    );
                }
            })
        };
        let animator = Animator::new(animation_closure, timer_closure);

        // Create the Application
        let app = Arc::new(Mutex::new(App {
            id: app_id.to_owned(),
            model: model_ptr.clone(),
            view: view_ptr.clone(),
            animator,
            closures: Closures::new(app_id),
            canvases: Vec::new(),
            interval_callback: Self::on_interval_default,
            resize_callback: None,
            sound_end_callback: None,
            sound_controller: None,
        })) as DynAppPtr;

        // Register this Application in the global app registry.
        match APP_REGISTRY.try_lock() {
            Ok(mut registry) => match registry.entry(app_id.to_owned()) {
                Entry::Vacant(entry) => {
                    entry.insert(app.clone());
                    Ok(())
                }
                Entry::Occupied(_) => Err(Error::from(format!(
                    "APP_REGISTRY entry already exists: {:?}",
                    app_id
                ))),
            },
            Err(err) => Err(Error::from(format!(
                "Could not access APP_REGISTRY: {:?}",
                err
            ))),
        }?;

        // Lock the smart pointers so we can use them.
        let mut dynapp_guard = app.try_lock()?;
        let any_mut = dynapp_guard.any_mut();
        let app_mut = any_mut.downcast_mut::<Self>().ok_or_else(|| {
            let msg = format!(
                "app ptr is not of type {:?}",
                std::any::type_name::<Self>()
            );
            log::error!("{}", msg);
            Error::from(msg)
        })?;
        let mut model_guard = model_ptr.try_lock()?;
        let mut view_guard = view_ptr.try_lock()?;

        // Set up resize event handler
        app_mut.add_event_listener(
            EventTarget::Window,
            EventType::Resize,
            Self::on_resize,
        )?;

        // Perform client app setup.
        // This will typically create one or more drawing canvases
        // and set up event handlers.
        setup(app_mut, &mut *model_guard, &mut *view_guard)?;

        // Schedule the first call to View::draw()
        app_mut.cause_redraw()?;

        // Finally done with App creation and initialization.
        Ok(())
    }

    fn create_sound_controller(&mut self) -> Result<(), Error> {
        // This must only be called once
        assert!(self.sound_controller.is_none());

        let sound_end_closure: Closure<dyn FnMut(web_sys::Event)> = {
            let moved_app_id = self.id().to_owned();
            Closure::wrap(Box::new(move |event| {
                let result = Self::on_sound_end(moved_app_id.as_str(), event);
                if let Err(err) = result {
                    log::error!(
                        "App {:?}: sound_end_closure: {}",
                        moved_app_id,
                        err
                    );
                }
            }))
        };

        self.sound_controller = Some(SoundController::new(sound_end_closure)?);
        Ok(())
    }

    /// Default interval timer handler, can be overridden by
    /// `.set_interval_callback()`.
    pub fn on_interval_default(
        &mut self,
        model: &mut M,
        _view: &mut V,
        dt: f64,
    ) -> Result<bool, Error> {
        model.step(dt)
    }

    /// Set a custom function to be called whenever the interval timer fires.
    ///
    /// The interval timer will not start until `App::start()` is called.
    /// The callback function will be called with mutable references to the app,
    /// the model, and the view, and a `dt` parameter containing the nominal
    /// time interval in seconds.
    /// The callback function should return `true` if the app should be redrawn.
    ///
    /// The default interval timer behavior is to call `model.step(dt)`. Setting
    /// a custom interval callback by calling this method will override that
    /// behavior. You can call `app.on_interval_default(dt)` from your custom
    /// interval callback to retain that default behavior and then do additional
    /// things.
    pub fn set_interval_callback(
        &mut self,
        interval_callback: fn(
            &mut Self,
            &mut M,
            &mut V,
            f64,
        ) -> Result<bool, Error>,
    ) {
        self.interval_callback = interval_callback;
    }

    /// Set a custom function to be called whenever the browser window is
    /// resized, or None to remove any such function previously set.
    pub fn set_resize_callback(
        &mut self,
        resize_callback: Option<
            fn(&mut Self, &mut M, &mut V) -> Result<(), Error>,
        >,
    ) {
        self.resize_callback = resize_callback;
    }

    fn on_resize(
        &mut self,
        model: &mut M,
        view: &mut V,
        _event: Event,
    ) -> Result<EventSignal, Error> {
        self.resize_all_canvases()?;
        if let Some(resize_callback) = self.resize_callback {
            resize_callback(self, model, view)?;
        }
        self.cause_redraw()?;
        Ok(EventSignal::default())
    }

    fn on_animation_frame(app_id: &str, timestamp: f64) -> Result<(), Error> {
        Self::with_app_and_model_and_view(app_id, |app, model, view| {
            if app.animator.on_animation_frame(timestamp)? {
                view.draw(app, model)
            } else {
                Ok(())
            }
        })
    }

    fn on_timer(app_id: &str) -> Result<(), Error> {
        Self::with_app_and_model_and_view(app_id, |app, model, view| {
            let dt = app.animator.frame_time_sec();
            let force_redraw = (app.interval_callback)(app, model, view, dt)?;
            app.animator.on_timer(force_redraw)
        })
    }

    fn on_sound_end(app_id: &str, event: web_sys::Event) -> Result<(), Error> {
        let event_target = event.target().ok_or_else(|| {
            Error::from("sound end event should have a target")
        })?;
        let audio_node = event_target
            .dyn_into::<web_sys::AudioNode>()
            .or_else(|err| {
                Err(Error::from(format!(
                    "sound end event target should be audio node: {:?}",
                    err,
                )))
            })?;

        let dynany_ptr = Self::get(app_id)?;
        let mut dynany_guard = dynany_ptr.try_lock()?;
        let any_mut = dynany_guard.any_mut();
        let app_mut = dyn_cast_mut::<Self>(any_mut)?;

        let sound_id_maybe =
            app_mut.sound_controller()?.on_audio_node_stop(audio_node);

        let sound_end_callback = match app_mut.sound_end_callback {
            Some(cb) => cb,
            None => return Ok(()),
        };

        let sound_id = match sound_id_maybe {
            Some(id) => id,
            None => return Ok(()),
        };

        let model_ptr = app_mut.model();
        let mut model_guard = model_ptr.try_lock()?;

        let view_ptr = app_mut.view();
        let mut view_guard = view_ptr.try_lock()?;

        sound_end_callback(
            &mut *&mut view_guard,
            &mut *&mut model_guard,
            app_mut,
            sound_id,
        )
    }

    pub fn set_sound_end_callback(
        &mut self,
        sound_end_callback: Option<
            fn(&mut V, &mut M, &mut Self, SoundID) -> Result<(), Error>,
        >,
    ) {
        self.sound_end_callback = sound_end_callback;
    }

    fn model(&self) -> ModelPtr<M> {
        self.model.clone()
    }

    fn view(&self) -> ViewPtr<V> {
        self.view.clone()
    }

    /// Fetch a dynamic app smart pointer from the global app registry
    /// or return an error.
    pub fn get(app_id: &str) -> Result<DynAppPtr, Error> {
        match Self::get_maybe(app_id) {
            Some(app_ptr) => Ok(app_ptr),
            None => Err(Error::from(format!("No app with ID {:?}", app_id))),
        }
    }

    /// Fetch a dynamic app smart pointer from the global app registry.
    /// Returns:
    /// Some(app) if an app with that ID is registered,
    /// None if there is no app registered with that ID,
    /// Panics if there is a problem accessing the global app registry.
    pub fn get_maybe(app_id: &str) -> Option<DynAppPtr> {
        match Self::try_get(app_id) {
            Ok(maybe_app) => maybe_app,
            Err(err) => {
                panic!(
                    "Could not fetch Application with ID {:?}: {:?}",
                    app_id, err
                )
            }
        }
    }

    /// Fetch dynamic app smart pointer from the global app registry.
    /// Returns:
    /// Ok(Some(app)) if an app with that ID is registered,
    /// Ok(None) if there is no app registered with that ID,
    /// Err(err) if there is a problem accessing the global app registry.
    pub fn try_get(app_id: &str) -> Result<Option<DynAppPtr>, Error> {
        match APP_REGISTRY.try_lock() {
            Ok(registry) => match registry.get(app_id) {
                Some(app) => Ok(Some(app.clone())),
                None => Ok(None),
            },
            Err(err) => Err(Error::from(format!(
                "Could not access APP_REGISTRY: {}",
                err
            ))),
        }
    }

    /// Given an application ID, do something with mutable references to
    /// the app, the model, and the view. Return a result or an error.
    fn with_app_and_model_and_view<F, R>(app_id: &str, f: F) -> Result<R, Error>
    where
        F: FnOnce(&mut Self, &mut M, &mut V) -> Result<R, Error>,
    {
        let dynany_ptr = Self::get(app_id)?;
        let mut dynany_guard = dynany_ptr.try_lock()?;
        let any_mut = dynany_guard.any_mut();
        let app_mut = dyn_cast_mut::<Self>(any_mut)?;

        let model_ptr = app_mut.model();
        let mut model_guard = model_ptr.try_lock()?;

        let view_ptr = app_mut.view();
        let mut view_guard = view_ptr.try_lock()?;

        f(app_mut, &mut *model_guard, &mut *&mut view_guard)
    }

    fn get_main_canvas(&self) -> Result<DrawingCanvasPtr, Error> {
        let canvas_id = DrawingCanvas::canvas_id_for_div_id(self.id());
        match self.get_canvas_maybe(&canvas_id) {
            Ok(dc_maybe) => match dc_maybe {
                Some(dc_ptr) => Ok(dc_ptr),
                None => Err(Error::from(format!(
                    "app {:?}: get_main_canvas: canvas {:?} not created yet",
                    self.id(),
                    canvas_id,
                ))),
            },
            Err(err) => Err(err),
        }
    }

    fn get_canvas(&self, canvas_id: &str) -> Result<DrawingCanvasPtr, Error> {
        match self.get_canvas_maybe(canvas_id)? {
            Some(dc_ptr) => Ok(dc_ptr),
            None => Err(Error::from(format!(
                "app {:?}: no canvas with ID {:?}",
                self.id(),
                canvas_id,
            ))),
        }
    }

    fn get_canvas_maybe(
        &self,
        canvas_id: &str,
    ) -> Result<Option<DrawingCanvasPtr>, Error> {
        for dc_ptr in self.canvases.iter() {
            let dc_guard = dc_ptr.try_lock()?;
            if dc_guard.canvas_id() == canvas_id {
                return Ok(Some(dc_ptr.clone()));
            }
        }
        Ok(None)
    }

    fn with_canvas_ptr(
        &self,
        dc_ptr: DrawingCanvasPtr,
        doit: &mut dyn DrawingCanvasCallback,
    ) -> Result<(), Error> {
        let mut dc_guard = dc_ptr.try_lock()?;

        if dc_guard.transform_stack_len() != 0 {
            let msg = format!(
                "App::with_canvas_ptr: DrawingCanvas {:?}: \
                non-zero transform stack len {} before calling doit",
                dc_guard.canvas_id(),
                dc_guard.transform_stack_len()
            );
            log::error!("{}", msg);
            return Err(Error::from(msg));
        }

        let result = dc_guard.with_save_restore(doit);

        if dc_guard.transform_stack_len() != 0 {
            let msg = format!(
                "App::with_canvas_ptr: DrawingCanvas {:?}: \
                non-zero transform stack len {} after doit returned {:?}",
                dc_guard.canvas_id(),
                dc_guard.transform_stack_len(),
                result,
            );
            log::error!("{}", msg);
            return Err(Error::from(msg));
        }

        result
    }

    pub fn add_event_listener(
        &mut self,
        event_target: EventTarget,
        event_type: EventType,
        callback: fn(
            &mut Self,
            &mut M,
            &mut V,
            event: Event,
        ) -> Result<EventSignal, Error>,
    ) -> Result<EventListenerID, Error> {
        let handler: EventHandler =
            Box::new(move |app_id: &str, event: Event| {
                let dynany_ptr = Self::get(app_id)?;
                let mut dynany_guard = dynany_ptr.try_lock()?;
                let any_mut = dynany_guard.any_mut();
                let app_mut = dyn_cast_mut::<Self>(any_mut)?;

                let model_ptr = app_mut.model();
                let mut model_guard = model_ptr.try_lock()?;

                let view_ptr = app_mut.view();
                let mut view_guard = view_ptr.try_lock()?;

                callback(
                    app_mut,
                    &mut *&mut model_guard,
                    &mut *&mut view_guard,
                    event,
                )
            });

        self.closures
            .add_event_listener(event_target, event_type, handler)
    }

    pub fn add_canvas_listener(
        &mut self,
        canvas_id: &str,
        event_type: EventType,
        callback: fn(
            &mut Self,
            &mut M,
            &mut V,
            event: Event,
        ) -> Result<EventSignal, Error>,
    ) -> Result<EventListenerID, Error> {
        let dc_ptr = self.get_canvas(canvas_id)?;
        let dc_guard = dc_ptr.try_lock()?;
        let canvas_id = dc_guard.canvas_id();
        self.add_event_listener(
            EventTarget::from_element_id(canvas_id),
            event_type,
            callback,
        )
    }

    pub fn add_main_canvas_listener(
        &mut self,
        event_type: EventType,
        callback: fn(
            &mut Self,
            &mut M,
            &mut V,
            event: Event,
        ) -> Result<EventSignal, Error>,
    ) -> Result<EventListenerID, Error> {
        let canvas_id = self.main_canvas_id()?;
        self.add_canvas_listener(&canvas_id, event_type, callback)
    }

    /// Register a function to be called when a mouse button is clicked over
    /// the main drawing canvas.
    pub fn on_main_canvas_mouse_start_call(
        &mut self,
        callback: fn(
            &mut V,             // the application View
            &mut M,             // the application Model
            &mut Self,          // the application
            Event,              // the mouse or touch event
            &mut DrawingCanvas, // the canvas over which the event occurred
            Point, // the event location in canvas tranformed coordinates
        ) -> Result<(), Error>,
    ) -> Result<EventListenerID, Error> {
        self.on_canvas_event_call(
            EventType::MouseDown,
            self.main_canvas_id()?,
            callback,
        )
    }

    /// Register a function to be called when an event happens over a
    /// a drawing canvas.
    fn on_canvas_event_call(
        &mut self,
        event_type: EventType,
        canvas_id: String,
        callback: fn(
            &mut V,             // the application View
            &mut M,             // the application Model
            &mut Self,          // the application
            Event,              // the mouse or touch event
            &mut DrawingCanvas, // the canvas over which the event occurred
            Point, // the event location in canvas tranformed coordinates
        ) -> Result<(), Error>,
    ) -> Result<EventListenerID, Error> {
        let moved_canvas_id = canvas_id.clone();
        let handler: EventHandler =
            Box::new(move |app_id: &str, event: Event| {
                let dynany_ptr = Self::get(app_id)?;
                let mut dynany_guard = dynany_ptr.try_lock()?;
                let any_mut = dynany_guard.any_mut();
                let app_mut = dyn_cast_mut::<Self>(any_mut)?;

                let model_ptr = app_mut.model();
                let mut model_guard = model_ptr.try_lock()?;

                let view_ptr = app_mut.view();
                let mut view_guard = view_ptr.try_lock()?;

                // This is a canvas-related event, expect to have a location
                let px = event.data().offset_loc().ok_or_else(|| {
                    Error::from(format!(
                        "App {:?}: canvas {:?} event has no offset_loc: {:?}",
                        app_id, &moved_canvas_id, event
                    ))
                })?;

                let dc_ptr = app_mut.get_canvas(&moved_canvas_id)?;
                let mut dc_guard = dc_ptr.try_lock()?;

                let pt = dc_guard.transform_px(px)?;

                let result = callback(
                    &mut *&mut view_guard,
                    &mut *&mut model_guard,
                    app_mut,
                    event.clone(),
                    &mut *&mut dc_guard,
                    pt,
                );

                match result {
                    Ok(()) => Ok(EventSignal::prevent_default()),
                    Err(err) => Err(err),
                }
            });

        let event_target = EventTarget::Element(canvas_id);
        self.closures
            .add_event_listener(event_target, event_type, handler)
    }
}
