//! Manage event handlers: Rust callbacks and JavaScript closures
use crate::error::Error;
use crate::geometry::PixelPoint;
use crate::keydata::KeyData;
use crate::web_util;
use uuid::Uuid;
use wasm_bindgen::{closure::Closure, JsCast, JsValue};

#[derive(Clone, Debug)]
#[non_exhaustive]
pub enum EventTarget {
    /// Target is an HTML element on a page identified by ID.
    Element(String),

    /// Target is the main browser window. Use for key events.
    Window,
}

impl EventTarget {
    /// Construct event target from HTML element ID
    pub fn from_element_id(element_id: &str) -> Self {
        Self::Element(element_id.to_owned())
    }

    fn as_web_event_target(&self) -> Result<web_sys::EventTarget, Error> {
        let target = match self {
            EventTarget::Element(element_id) => {
                let document = web_util::get_document();
                let element = document
                    .get_element_by_id(element_id.as_str())
                    .ok_or_else(|| {
                    Error::from(format!("Element #{} should exist", element_id))
                })?;
                web_util::event_target_from_jsvalue(JsValue::from(element))
            }
            EventTarget::Window => {
                let window = web_util::get_window();
                web_util::event_target_from_jsvalue(JsValue::from(window))
            }
        }?;
        Ok(target)
    }
}

#[derive(Copy, Clone, Debug)]
#[non_exhaustive]
pub enum EventType {
    Click,
    Resize,
    KeyDown,
    KeyPress,
    KeyUp,
    MouseDown,
    MouseMove,
    MouseUp,
    TouchStart,
    TouchMove,
    TouchEnd,
    TouchCancel,
}

impl EventType {
    /// Web-browser compatible str representation of the event type.
    ///
    /// NOTE 1: It is super-important to make these string representations
    /// exactly the same as the event type strings in this list:
    /// https://developer.mozilla.org/en-US/docs/Web/API/Event/type
    ///
    /// NOTE 2: When you add a new event type string here, add the exact same
    /// string to from_web_str() as well. Make sure the string matches the enum.
    fn as_web_str(&self) -> &'static str {
        match self {
            Self::Click => "click",
            Self::Resize => "resize",
            Self::KeyDown => "keydown",
            Self::KeyPress => "keypress",
            Self::KeyUp => "keyup",
            Self::MouseDown => "mousedown",
            Self::MouseMove => "mousemove",
            Self::MouseUp => "mouseup",
            Self::TouchStart => "touchstart",
            Self::TouchMove => "touchmove",
            Self::TouchEnd => "touchend",
            Self::TouchCancel => "touchcancel",
        }
    }

    fn from_web_str(event_type_str: &str) -> Result<Self, Error> {
        match event_type_str {
            "click" => Ok(Self::Click),
            "resize" => Ok(Self::Resize),
            "keydown" => Ok(Self::KeyDown),
            "keypress" => Ok(Self::KeyPress),
            "keyup" => Ok(Self::KeyUp),
            "mousedown" => Ok(Self::MouseDown),
            "mousemove" => Ok(Self::MouseMove),
            "mouseup" => Ok(Self::MouseUp),
            "touchstart" => Ok(Self::TouchStart),
            "touchmove" => Ok(Self::TouchMove),
            "touchend" => Ok(Self::TouchEnd),
            "touchcancel" => Ok(Self::TouchCancel),
            _ => Err(Error::from(format!(
                "Unknown event type str: {}",
                event_type_str
            ))),
        }
    }
}

/// Event information passed to event handlers
#[derive(Clone, Debug)]
pub struct Event {
    event_type: EventType,
    time_stamp: f64,
    data: EventData,
}

impl Event {
    fn from_web_event(web_event: &web_sys::Event) -> Result<Self, Error> {
        let event_type = EventType::from_web_str(&web_event.type_())?;
        let time_stamp = web_event.time_stamp();

        let data = if let Some(key_event) =
            web_event.dyn_ref::<web_sys::KeyboardEvent>()
        {
            EventData::Key(KeyData::from_web_sys_keyboard_event(key_event))
        } else if let Some(mouse_event) =
            web_event.dyn_ref::<web_sys::MouseEvent>()
        {
            EventData::Mouse(MouseData {
                client_loc: PixelPoint::new(
                    mouse_event.client_x().into(),
                    mouse_event.client_y().into(),
                ),
                offset_loc: PixelPoint::new(
                    mouse_event.offset_x().into(),
                    mouse_event.offset_y().into(),
                ),
                buttons: mouse_event.buttons(),
                ctrl_key: mouse_event.ctrl_key(),
                shift_key: mouse_event.shift_key(),
            })
        } else if let Some(touch_event) =
            web_event.dyn_ref::<web_sys::TouchEvent>()
        {
            let target_offset = if let Some(event_target) = touch_event.target()
            {
                if let Ok(element) =
                    event_target.dyn_into::<web_sys::HtmlElement>()
                {
                    PixelPoint::new(element.offset_left(), element.offset_top())
                } else {
                    PixelPoint::new(0, 0)
                }
            } else {
                PixelPoint::new(0, 0)
            };
            let touches = touch_event.changed_touches();
            if let Some(touch) = touches.item(0) {
                EventData::Touch(TouchData {
                    client_loc: PixelPoint::new(
                        touch.client_x().into(),
                        touch.client_y().into(),
                    ),
                    offset_loc: PixelPoint::new(
                        touch.page_x() - target_offset.x(),
                        touch.page_y() - target_offset.y(),
                    ),
                })
            } else {
                EventData::Other
            }
        } else {
            EventData::Other
        };

        Ok(Event {
            event_type,
            time_stamp,
            data,
        })
    }

    pub fn event_type(&self) -> EventType {
        self.event_type.clone()
    }

    pub fn time_stamp(&self) -> f64 {
        self.time_stamp
    }

    pub fn data(&self) -> EventData {
        self.data.clone()
    }
}

#[derive(Clone, Debug)]
#[non_exhaustive]
pub enum EventData {
    Key(KeyData),
    Mouse(MouseData),
    Touch(TouchData),
    Other,
}

impl EventData {
    /// If the event data contains mouse or touch data, return the "offset
    /// location" of the event, e.g. the canvas coordinates of an event if the
    /// target is a drawing canvas. Otherwise return None.
    pub fn offset_loc(&self) -> Option<PixelPoint> {
        match self {
            Self::Mouse(mouse_data) => Some(mouse_data.offset_loc()),
            Self::Touch(touch_data) => Some(touch_data.offset_loc()),
            _ => None,
        }
    }
}

#[derive(Clone, Debug)]
pub struct MouseData {
    client_loc: PixelPoint,
    offset_loc: PixelPoint,
    buttons: u16,
    ctrl_key: bool,
    shift_key: bool,
}

impl MouseData {
    /// Mouse event location relative to the top left of the browser viewport,
    /// not including any scroll effect.
    pub fn client_loc(&self) -> PixelPoint {
        self.client_loc
    }

    /// Mouse event location relative to the "padding edge of the target node".
    /// Use this for e.g. canvas coordinates.
    pub fn offset_loc(&self) -> PixelPoint {
        self.offset_loc
    }

    /// Indicates which mouse buttons were pressed at the time of the mouse
    /// event. The button numbers below are added together to form the result.
    /// 0: No button
    /// 1: Primary button (usually left)
    /// 2: Secondary button (usually right)
    /// 4: Auxilliary button (usually wheel or middle)
    /// 8: 4th button (typically "Browser Back")
    /// 16: 5th button (typically "Browser Forward")
    pub fn buttons(&self) -> u16 {
        self.buttons
    }

    /// True iff the Ctrl key was pressed when the mouse event occurred.
    pub fn ctrl_key(&self) -> bool {
        self.ctrl_key
    }

    /// True iff the shift key was pressed when the mouse event occurred.
    pub fn shift_key(&self) -> bool {
        self.shift_key
    }
}

#[derive(Clone, Debug)]
pub struct TouchData {
    client_loc: PixelPoint,
    offset_loc: PixelPoint,
}

impl TouchData {
    /// Touch event location relative to the top left of the browser viewport,
    /// not including any scroll effect.
    pub fn client_loc(&self) -> PixelPoint {
        self.client_loc
    }

    /// Touch event location relative to the "padding edge of the target node".
    /// Use this for e.g. canvas coordinates.
    pub fn offset_loc(&self) -> PixelPoint {
        self.offset_loc
    }
}

/// Return value from event handlers to signal the system what to do after
/// returning from an event listener custom handler function:
///
/// - `EventSignal::default()`: continue regular processing of the event
/// - `EventSignal::prevent_default()`: no more processing of this event
///
/// Use `prevent_default()` when your custom event handler has completely
/// processed the event and you don't want the system to continue on to do other
/// things with the event (such as default handling of keys, mouse, or touch
/// events).
#[derive(Clone, Copy, Debug)]
#[non_exhaustive]
pub struct EventSignal {
    action: EventSignalAction,
}

impl EventSignal {
    /// Prevent the system default event action, because
    /// the application code has already applied special handling.
    pub fn prevent_default() -> Self {
        Self {
            action: EventSignalAction::PreventDefault,
        }
    }
}

/// Return default EventSignal to allow the system to continue to apply the
/// regular processing for the event after the custom event handler returns.
impl Default for EventSignal {
    fn default() -> Self {
        Self {
            action: EventSignalAction::NoSignal,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum EventSignalAction {
    NoSignal,
    PreventDefault,
}

pub type EventResult = Result<EventSignal, Error>;

/// Holds JavaScript closures managed by DrawingCanvas
pub(crate) struct Closures {
    /// The ID of the Application to which these closures pertain.
    app_id: String,

    /// List of Closure objects created by
    /// Self::add_event_listener()
    event_listeners: Vec<EventListener>,
}

impl Closures {
    pub(crate) fn new(app_id: &str) -> Self {
        Self {
            app_id: app_id.to_owned(),
            event_listeners: Vec::new(),
        }
    }

    pub(crate) fn add_event_listener(
        &mut self,
        event_target: EventTarget,
        event_type: EventType,
        handler: EventHandler,
    ) -> Result<EventListenerID, Error> {
        let listener = EventListener::new(
            &self.app_id,
            event_target,
            event_type,
            handler,
        )?;
        let listener_id = listener.listener_id;
        self.event_listeners.push(listener);
        Ok(listener_id)
    }

    pub(crate) fn del_event_listener(&mut self, listener_id: EventListenerID) {
        self.event_listeners
            .retain(|listener| listener.listener_id != listener_id);
    }
}

// SAFETY: I believe this is safe because in reality the Closure methods
// will only be called from code run by JavaScript UI main thread.
unsafe impl Send for Closures {}
unsafe impl Sync for Closures {}

/// Holds enough information for an event handler such that the closure doesn't
/// get dropped while it is active and the listener can be removed later.
struct EventListener {
    /// A globally unique ID for this event listener to act as a handle
    /// for removing it later.
    listener_id: EventListenerID,

    /// The object on which the listener is registered.
    event_target: EventTarget,

    /// The ID of the event type to listen for.
    /// See: https://developer.mozilla.org/en-US/docs/Web/Events
    event_type: EventType,

    /// The JavaScript closure which wraps a Rust closure that gets called
    /// when the event is fired.
    closure: Closure<dyn FnMut(web_sys::Event) + 'static>,
}

impl EventListener {
    fn new(
        app_id: &str,
        event_target: EventTarget,
        event_type: EventType,
        mut handler: EventHandler,
    ) -> Result<Self, Error> {
        let closure = {
            // Copy app_id and other IDs to be moved inside the closure
            let moved_app_id = app_id.to_owned();
            let moved_event_target = event_target.clone();
            let moved_event_type = event_type.clone();
            Closure::wrap(Box::new(move |web_event| {
                let event = match Event::from_web_event(&web_event) {
                    Ok(e) => e,
                    Err(err) => {
                        log::error!(
                            "Event handler: app {:?} target {:?} type {:?}: {:?}",
                            moved_app_id,
                            moved_event_target,
                            moved_event_type,
                            err
                        );
                        return;
                    }
                };
                let result = handler(&moved_app_id, event);
                match result {
                    Err(err) => {
                        log::error!(
                            "Event handler: app {:?} target {:?} type {:?}: {:?}",
                            moved_app_id,
                            moved_event_target,
                            moved_event_type,
                            err
                        );
                    }
                    Ok(signal) => match signal {
                        EventSignal {
                            action: EventSignalAction::PreventDefault,
                        } => {
                            web_event.prevent_default();
                        }
                        _ => {}
                    },
                }
            })
                as Box<dyn FnMut(web_sys::Event) + 'static>)
        };

        // Register the listener on the target
        let target = event_target.as_web_event_target()?;
        target
            .add_event_listener_with_callback(
                event_type.as_web_str(),
                closure.as_ref().unchecked_ref(),
            )
            .or_else(|err| {
                let msg = format!(
                    "Registering listener for event {:?} on target {:?}: {:?}",
                    event_type, event_target, err
                );
                log::error!("{}", msg);
                Err(Error::from(msg))
            })?;

        let listener = Self {
            listener_id: EventListenerID::new(),
            event_target: event_target,
            event_type: event_type,
            closure: closure,
        };

        Ok(listener)
    }
}

impl Drop for EventListener {
    fn drop(&mut self) {
        // Un-register the listener from the target
        if let Ok(target) = self.event_target.as_web_event_target() {
            if let Err(err) = target.remove_event_listener_with_callback(
                self.event_type.as_web_str(),
                self.closure.as_ref().unchecked_ref(),
            ) {
                log::error!(
                "Un-registering listener for event {:?} on target {:?}: {:?}",
                self.event_type,
                self.event_target,
                err
            );
            }
        }
    }
}

/*
// I wish I could do something like this:
// https://github.com/rust-lang/rust/issues/41517
type EventHandler = FnMut(&str) -> EventResult
    + Sync
    + Send
    + 'static;
*/

pub type EventHandler = Box<
    dyn FnMut(&str, Event) -> EventResult
        //+ Sync
        //+ Send
        + 'static,
>;

/// A globally unique ID for an event listener to act as a handle
/// for removing it later.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct EventListenerID(Uuid);

impl EventListenerID {
    fn new() -> Self {
        Self(Uuid::new_v4())
    }
}
