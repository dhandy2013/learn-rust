//! Utilities just for testing
use crate::color::Color;
use crate::geometry::PixelPoint;
use crate::graphics::{CanvasOptions, DrawingCanvas};
use crate::web_util;

/// Create and initialize a new DrawingCanvas for test purposes.
///
/// - Create a new HTML div element with ID `test_name` and append it to the
/// page body.
/// - Create a new HTML canvas element inside the div element, sized at 800x600
/// pixels.
/// - Create a DrawingCanvas object for this HTML canvas.
/// - Fill the canvas with black (otherwise it is transparent)
/// - Draw the `test_name` text in Color::WHITE in the upper-left corner of the
/// canvas.
/// - Return the DrawingCanvas.
pub fn create_dc(test_name: &str, options: CanvasOptions) -> DrawingCanvas {
    let document = web_util::get_document();
    let div_element = document
        .create_element("div")
        .expect("should be able to create div");
    div_element
        .set_attribute("id", test_name)
        .expect("should be able to set id on div");
    let body = document.body().expect("HTML document should have a body");
    body.append_child(&div_element)
        .expect("append child should work");
    let options = options.builder_from().size(Some((800, 600))).build();
    let mut dc = DrawingCanvas::new_in_div(test_name, options)
        .expect("creating DrawingCanvas should work");
    dc.with_save_restore(|dc| {
        dc.fill(Color::BLACK)?;
        dc.set_fill_color(Color::WHITE);
        dc.draw_text_px(test_name, PixelPoint::new(0, 20))?;
        Ok(())
    })
    .expect("drawing test name on canvas should work");
    dc
}
