//! Highly customized button controls drawn on the DrawingCanvas canvas
use crate::{
    color::Color,
    error::Error,
    geometry::{Contains, Point, Rect},
    graphics::DrawingCanvas,
    keydata::KeyId,
};
use std::fmt;

pub struct Button<M: 'static, I: Eq + fmt::Debug + 'static = &'static str> {
    state: ButtonState,
    behavior: Box<dyn ButtonBehavior<M, I>>,
}

// We must implement fmt::Debug "manually" instead of using the derive macro
// because the derive macro would require M to also implement fmt::Debug.
impl<M: 'static, I: Eq + fmt::Debug + 'static> fmt::Debug for Button<M, I> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let id = self.id();
        f.debug_struct("Button")
            .field("id", &id)
            .field("state", &self.state)
            .field("behavior", &self.behavior)
            .finish()
    }
}

impl<M: 'static, I: Eq + fmt::Debug + 'static> Button<M, I> {
    pub fn new(
        bounding_box: Rect,
        key: Option<KeyId>,
        behavior: impl ButtonBehavior<M, I>,
    ) -> Self {
        Self {
            state: ButtonState {
                bounding_box,
                key,
                active: false,
            },
            behavior: Box::new(behavior) as Box<dyn ButtonBehavior<M, I>>,
        }
    }

    pub fn id(&self) -> I {
        self.behavior.id()
    }

    /// Return the bounding box in which this button is drawn.
    /// Also used to detect pointer hit on the mouse.
    pub fn bounding_box(&self) -> Rect {
        self.state.bounding_box
    }

    /// Modify the button's bounding box.
    pub fn set_bounding_box(&mut self, bounding_box: Rect) {
        self.state.bounding_box = bounding_box
    }

    /// Change the button anchor point without changing its size.
    /// The anchor point is the upper-left corner for pixel-style coordinates,
    /// and the lower-left corner for math-style coordinates.
    pub fn move_to(&mut self, p: Point) {
        self.state.bounding_box.move_to(p);
    }

    /// Return true iff this button is currently being clicked/tapped/pressed.
    pub fn active(&self) -> bool {
        self.state.active
    }

    /// Draw the button according to its current state,
    /// also taking into account model state if necessary.
    pub fn draw(&self, dc: &mut DrawingCanvas, model: &M) -> Result<(), Error> {
        self.behavior.draw(dc, &self.state, model)?;
        dc.with_save_restore(|dc| {
            let color = if self.state.active {
                Color::BRIGHT_WHITE
            } else {
                Color::WHITE
            };
            dc.set_stroke_color(color);
            dc.draw_rect(&self.state.bounding_box)
        })
    }

    /// Given mouse down or touch start at point `pt`,
    /// detect whether the point is inside the button bounding box.
    /// If it is, activate the button, taking appropriate actions, and return
    /// true. Otherwise return false.
    /// If the button activation/deactivation behavior returns an error, this
    /// method returns that error.
    pub fn on_pointer_start(
        &mut self,
        pt: Point,
        model: &mut M,
    ) -> Result<bool, Error> {
        if self.state.bounding_box.contains(pt) {
            self.activate(model)?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    /// Given mouse up or touch end/cancel at point `pt`,
    /// detect whether the point is inside the button bounding box.
    /// If it is, deactivate the button, taking appropriate actions, and return
    /// true. Otherwise return false.
    /// If the button deactivate behavior returns an error, this method
    /// returns that error.
    pub fn on_pointer_end(
        &mut self,
        pt: Point,
        model: &mut M,
    ) -> Result<bool, Error> {
        if self.state.bounding_box.contains(pt) {
            self.deactivate(model)?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    /// Given a key down event, detect whether this key is a command key for
    /// this button. (Not all buttons have a command key.)
    /// If it is, activate the button, taking appropriate actions, and return
    /// true. Otherwise return false.
    /// If the button `on_activate` behavior returns an error, this method
    /// returns that error.
    pub fn on_key_down(
        &mut self,
        key: &KeyId,
        model: &mut M,
    ) -> Result<bool, Error> {
        match self.state.key.as_ref() {
            Some(k) => {
                if *key != *k {
                    return Ok(false);
                }
            }
            None => return Ok(false),
        }

        // Deliberately throwing away boolean result of activate() because
        // caller is more interested in whether the key matched not whether
        // button had been pressed.
        self.activate(model)?;

        Ok(true)
    }

    /// Given a key up event, detect whether this key is a command key for
    /// this button. (Not all buttons have a command key.)
    /// If it is, deactivate the button, taking appropriate actions, and return
    /// true. Otherwise return false.
    /// If the button `on_deactivate` behavior returns an error, this method
    /// returns that error.
    pub fn on_key_up(
        &mut self,
        key: &KeyId,
        model: &mut M,
    ) -> Result<bool, Error> {
        match self.state.key.as_ref() {
            Some(k) => {
                if *key != *k {
                    return Ok(false);
                }
            }
            None => return Ok(false),
        }

        // Deliberately throwing away boolean result of deactivate() because
        // caller is more interested in whether the key matched not whether
        // button had been pressed.
        self.deactivate(model)?;

        Ok(true)
    }

    /// Force this button to be "active" (clicked/tapped/pressed).
    /// Return true iff the active flag changed state.
    /// Client is responsible for calling redraw after this.
    pub fn activate(&mut self, model: &mut M) -> Result<bool, Error> {
        if self.state.active {
            // Nothing to do.
            return Ok(false);
        }
        let (state, behavior) = (&mut self.state, &mut self.behavior);
        state.active = true;
        behavior.on_activate(state, model)?;
        Ok(true)
    }

    /// Force this button to be "inactive" (not clicked/tapped/pressed).
    /// Return true iff the active flag changed state.
    /// Client is responsible for calling redraw after this.
    pub fn deactivate(&mut self, model: &mut M) -> Result<bool, Error> {
        if !self.state.active {
            // Nothing to do.
            return Ok(false);
        }
        let (state, behavior) = (&mut self.state, &mut self.behavior);
        state.active = false;
        behavior.on_deactivate(state, model)?;
        Ok(true)
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct ButtonState {
    /// The bounding box used for checking if a point is inside the
    /// button and also for drawing the button boundary to show selection.
    bounding_box: Rect,

    /// The key ID, if any, that can activate this button.
    key: Option<KeyId>,

    /// True if this button is currently being pressed/clicked/tapped.
    active: bool,
}

impl ButtonState {
    pub fn bounding_box(&self) -> Rect {
        self.bounding_box
    }

    pub fn key(&self) -> Option<KeyId> {
        self.key.clone()
    }

    pub fn active(&self) -> bool {
        self.active
    }
}

pub trait ButtonBehavior<M, I = &'static str>:
    fmt::Debug + 'static + Send + Sync
where
    M: 'static,
    I: Eq + fmt::Debug + 'static,
{
    /// Return an ID for the button in order to find it in a list.
    fn id(&self) -> I;

    /// Draw the button.
    /// Use `state.bounding_box` to locate where to draw the button.
    /// The `model` parameter can be used for animation or other purposes.
    /// After this draw method is called, Button::draw will draw the bounding
    /// rectangle lighter or bolder according to whether the button is active.
    fn draw(
        &self,
        dc: &mut DrawingCanvas,
        state: &ButtonState,
        model: &M,
    ) -> Result<(), Error>;

    /// This method is called when button state `active` flag is changed
    /// from false to true via the Button::activate() method.
    ///
    /// The mutable `state` reference allows immediately deactivating the
    /// button without calling `on_deactivate` if that is the desired behavior.
    ///
    /// The mutable `model` reference allows the button to affect the
    /// application, which is kind of the whole point of having a button in the
    /// first place.
    fn on_activate(
        &mut self,
        state: &mut ButtonState,
        model: &mut M,
    ) -> Result<(), Error>;

    /// This method is called when button state `active` flag is changed
    /// from true to false via the Button::deactivate() method.
    ///
    /// See `.on_activate()` for the purpose of `state` and `model`.
    fn on_deactivate(
        &mut self,
        state: &mut ButtonState,
        model: &mut M,
    ) -> Result<(), Error>;
}
