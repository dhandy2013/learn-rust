//! Geometrical shapes objects: point, rectangle, angle, etc.
use std::f64::consts::PI;
use std::ops::{Add, AddAssign, Mul, Sub, SubAssign};

#[derive(Clone, Copy, Debug)]
pub enum TransformStyle {
    /// Standard graphics transformation
    Pixel,
    /// Math style transformation
    Math,
}

/// Length in coordinate units. May or may not be pixels.
pub type Length = f64;

/// (x, y) coordinates
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Point(Length, Length);

impl Point {
    /// Construct a Point from x and y coordinates
    pub const fn new(x: Length, y: Length) -> Self {
        Self(x, y)
    }

    /// Construct a Point from a Pixel.
    /// Only numeric type conversion is done, no scaling.
    pub fn from_px_loc(loc: PixelPoint) -> Self {
        Self(loc.x() as Length, loc.y() as Length)
    }

    pub const fn x(&self) -> Length {
        self.0
    }

    pub const fn y(&self) -> Length {
        self.1
    }

    /// Return the distance from the origin to the point.
    pub fn hypot(&self) -> Length {
        self.x().hypot(self.y())
    }

    /// Return angle in degrees from the positive y axis clockwise to the point
    pub fn angle_deg(&self, transform_style: TransformStyle) -> AngleDeg {
        match transform_style {
            TransformStyle::Pixel => {
                AngleDeg::from_rad(self.x().atan2(-self.y()))
            }
            TransformStyle::Math => {
                AngleDeg::from_rad(self.x().atan2(self.y()))
            }
        }
    }

    /// Return a copy of the point scaled in the x axis by `scalex` and in the y
    /// axis by `scaley`.
    pub fn scale_xy(&self, scalex: f64, scaley: f64) -> Point {
        Self(self.0 * scalex, self.1 * scaley)
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Self(self.0 - rhs.0, self.1 - rhs.1)
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl approx::AbsDiffEq for Point {
    type Epsilon = f64;

    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        f64::abs_diff_eq(&self.x(), &other.x(), epsilon)
            && f64::abs_diff_eq(&self.y(), &other.y(), epsilon)
    }
}

/// Length in pixels (most basic screen units)
/// This is the type returned for mouse event coordinates.
pub type Pixels = i32;

/// Pixel location
#[derive(Clone, Copy, Debug)]
pub struct PixelPoint(Pixels, Pixels);

impl PixelPoint {
    pub const fn new(x: Pixels, y: Pixels) -> Self {
        Self(x, y)
    }

    pub const fn x(&self) -> Pixels {
        self.0
    }

    pub const fn y(&self) -> Pixels {
        self.1
    }
}

/// (width, height) size of rectangular area
#[derive(Clone, Copy, Debug)]
pub struct RectSize(Length, Length);

impl RectSize {
    /// Construct a RectSize from width and height values.
    /// The `width` and `height` values are coerced to be non-negative.
    pub fn new(width: Length, height: Length) -> Self {
        let width = if width < 0.0 { -width } else { width };
        let height = if height < 0.0 { -height } else { height };
        Self(width, height)
    }

    pub const fn width(&self) -> Length {
        self.0
    }

    pub const fn height(&self) -> Length {
        self.1
    }
}

/// Rectangular area of a DrawingCanvas
#[derive(Clone, Copy, Debug)]
pub struct Rect {
    pub x: Length,
    pub y: Length,
    pub w: Length,
    pub h: Length,
}

impl Rect {
    /// Construct a Rect from a Point specifying a corner of the rectangle
    /// and a RectSize specifying the width and the height.
    ///
    /// For "pixel style" coordinates, `p` is the top-left corner.
    /// For "math style" coordinates, `p` is the bottom-left corner.
    pub fn new(p: Point, size: RectSize) -> Self {
        Self {
            x: p.x(),
            y: p.y(),
            w: size.width(),
            h: size.height(),
        }
    }

    /// Construct a Rect without checking any of the parameters, so that we can
    /// create Rects in a `const` context (can't do comparison and checking of
    /// floating-point values from a `const` function.)
    ///
    /// WARNING: If either `w` or `h` are negative, then Rect::contains() and
    /// the .top_left(), .top_right(), .bottom_right(), .bottom_left() methods
    /// will not work properly. Prefer Rect::new() and Rect::from_corners(). Use
    /// Rect::normalize() to fix up a Rect with possible negative width or
    /// height.
    pub const fn new_unchecked(
        x: Length,
        y: Length,
        w: Length,
        h: Length,
    ) -> Self {
        Self { x, y, w, h }
    }

    /// Construct a Rect from two opposite corners.
    /// The anchor corner is calculated such that the width and height of the
    /// Rect are guaranteed to be non-negative.
    pub fn from_corners(p1: Point, p2: Point) -> Rect {
        let (x, w) = (p1.x(), p2.x() - p1.x());
        let (y, h) = (p1.y(), p2.y() - p1.y());
        let mut r = Self { x, y, w, h };
        r.normalize();
        r
    }

    /// Fix a Rect such that its width and height are non-negative.
    /// Adjust the anchor of the Rect if necessary to meet this invariant.
    pub fn normalize(&mut self) {
        if self.w < 0.0 {
            self.x += self.w;
            self.w = -self.w;
        }
        if self.h < 0.0 {
            self.y += self.h;
            self.h = -self.h;
        }
    }

    /// Construct a Rect from opposite corners, with the corners specified
    /// as PixelLoc points instead of regular coordinate points. Only numeric
    /// type conversion is done, no scaling.
    pub fn from_px_corners(p1: PixelPoint, p2: PixelPoint) -> Self {
        let p1 = Point::from_px_loc(p1);
        let p2 = Point::from_px_loc(p2);
        Self::from_corners(p1, p2)
    }

    /// Change the anchor point of the Rect to `p` without changing its size.
    /// For "pixel style" coordinates, the anchor is the top-left corner.
    /// For "math style" coordinates, the anchor is the bottom-left corner.
    pub fn move_to(&mut self, p: Point) {
        self.x = p.x();
        self.y = p.y();
    }

    /// Return the anchor point of this Rect.
    /// For "pixel style" coordinates, the anchor is the top-left corner.
    /// For "math style" coordinates, the anchor is the bottom-left corner.
    pub fn anchor(&self) -> Point {
        Point::new(self.x, self.y)
    }

    /// Return the top-left corner point of this Rect.
    /// `ts` and the anchor point are used to calculate the return value.
    /// For TransformStyle::Pixel, the anchor is the top-left corner.
    /// For TransformStyle::Math, the anchor is the bottom-left corner.
    pub fn top_left(&self, ts: TransformStyle) -> Point {
        use TransformStyle::*;
        match ts {
            Pixel => Point::new(self.x, self.y),
            Math => Point::new(self.x, self.y + self.h),
        }
    }

    /// Return the top-right corner point of this Rect.
    /// `ts` and the anchor point are used to calculate the return value.
    /// For TransformStyle::Pixel, the anchor is the top-left corner.
    /// For TransformStyle::Math, the anchor is the bottom-left corner.
    pub fn top_right(&self, ts: TransformStyle) -> Point {
        use TransformStyle::*;
        match ts {
            Pixel => Point::new(self.x + self.w, self.y),
            Math => Point::new(self.x + self.w, self.y + self.h),
        }
    }

    /// Return the bottom-right corner point of this Rect.
    /// `ts` and the anchor point are used to calculate the return value.
    /// For TransformStyle::Pixel, the anchor is the top-left corner.
    /// For TransformStyle::Math, the anchor is the bottom-left corner.
    pub fn bottom_right(&self, ts: TransformStyle) -> Point {
        use TransformStyle::*;
        match ts {
            Pixel => Point::new(self.x + self.w, self.y + self.h),
            Math => Point::new(self.x + self.w, self.y),
        }
    }

    /// Return the bottom-left corner point of this Rect.
    /// `ts` and the anchor point are used to calculate the return value.
    /// For TransformStyle::Pixel, the anchor is the top-left corner.
    /// For TransformStyle::Math, the anchor is the bottom-left corner.
    pub fn bottom_left(&self, ts: TransformStyle) -> Point {
        use TransformStyle::*;
        match ts {
            Pixel => Point::new(self.x, self.y + self.h),
            Math => Point::new(self.x, self.y),
        }
    }
}

/// Trait for geometrical objects that can contain (overlap) other geometrical
/// objects.
pub trait Contains<T> {
    fn contains(&self, containee: T) -> bool;
}

impl Contains<Point> for Rect {
    fn contains(&self, p: Point) -> bool {
        if p.x() < self.x {
            return false;
        } else if p.x() > self.x + self.w {
            return false;
        }

        if p.y() < self.y {
            return false;
        } else if p.y() > self.y + self.h {
            return false;
        }

        true
    }
}

impl Contains<&Rect> for Rect {
    fn contains(&self, r: &Rect) -> bool {
        let p1 = Point::new(r.x, r.y);
        let p2 = Point::new(r.x + r.w, r.y + r.h);
        self.contains(p1) && self.contains(p2)
    }
}

/// An angle measured in degrees
#[derive(Clone, Copy, Debug)]
pub struct AngleDeg(f64);

impl AngleDeg {
    /// Construct an angle in degrees from radians.
    pub fn from_rad(rad: f64) -> Self {
        Self(rad * (180.0 / PI))
    }

    /// Return the angle as a number in radians.
    pub fn as_rad(&self) -> f64 {
        (self.0 * PI) / 180.0
    }

    /// Return the angle as a 64-bit floating-point number in degrees.
    pub fn as_f64(&self) -> f64 {
        self.0
    }

    /// Return a unit vector represented as a Point, rotated clockwise
    /// from the positive y axis by the amount of this angle.
    /// TODO: test with TransformStyle::Pixel
    pub fn rotate_unit(&self, transform_style: TransformStyle) -> Point {
        let (x, y) = self.as_rad().sin_cos();
        match transform_style {
            TransformStyle::Pixel => Point::new(x, -y),
            TransformStyle::Math => Point::new(x, y),
        }
    }

    pub fn sin(&self) -> f64 {
        self.as_rad().sin()
    }

    pub fn cos(&self) -> f64 {
        self.as_rad().cos()
    }
}

impl From<f64> for AngleDeg {
    fn from(value: f64) -> Self {
        Self(value)
    }
}

impl Add for AngleDeg {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

impl Sub for AngleDeg {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0)
    }
}

impl AddAssign for AngleDeg {
    fn add_assign(&mut self, other: Self) {
        *self = Self(self.0 + other.0)
    }
}

impl SubAssign for AngleDeg {
    fn sub_assign(&mut self, other: Self) {
        *self = Self(self.0 - other.0);
    }
}

impl Mul<f64> for AngleDeg {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self {
        Self(self.0 * rhs)
    }
}

/// Calculate the intersection between two lines defined by points and angles,
/// assuming points in mathematics-style coordinate system (x increasing right,
/// y increasing up) and clock-style angles (zero degrees is up, 90 degrees is
/// right.) Return None if the lines are parallel.
pub fn intersect_points_angles(
    p1: Point,
    a1: AngleDeg,
    p2: Point,
    a2: AngleDeg,
) -> Option<Point> {
    let dx = p2.x() - p1.x();
    let dy = p2.y() - p1.y();
    let sin_a1 = a1.as_rad().sin();
    let cos_a1 = a1.as_rad().cos();
    let sin_a2 = a2.as_rad().sin();
    let cos_a2 = a2.as_rad().cos();
    let r1 = sin_a1 * dy - cos_a1 * dx;
    let r2 = sin_a2 * dy - cos_a2 * dx;
    let (v1, v2) = {
        if r1 == 0.0 {
            if r2 == 0.0 {
                return None;
            } else {
                (1.0, 0.0)
            }
        } else if r2 == 0.0 {
            (0.0, 1.0)
        } else if r1.abs() >= r2.abs() {
            (r2 / r1, 1.0)
        } else {
            (1.0, r1 / r2)
        }
    };
    let q1 = sin_a1 * v1 - sin_a2 * v2;
    let q2 = cos_a1 * v1 - cos_a2 * v2;
    if q1 == 0.0 && q2 == 0.0 {
        return None;
    }
    let t = if q1.abs() >= q2.abs() {
        dx / q1
    } else {
        dy / q2
    };
    Some(if v1 == 1.0 {
        Point::new(p1.x() + sin_a1 * t, p1.y() + cos_a1 * t)
    } else {
        assert_eq!(v2, 1.0);
        Point::new(p2.x() + sin_a2 * t, p2.y() + cos_a2 * t)
    })
}

/// An item in a path to be drawn and/or filled.
#[non_exhaustive]
pub enum PathCommand {
    /// `Line(p)` means add a straight line to the path from the current
    /// location to point `p`.
    Line(Point),
    /// `QuadBezier(p0, p1)` means add a quadratic bezier curve to the path
    /// starting at the current location with control point `p0` and end point
    /// `p1`.
    QuadBezier(Point, Point),
    /// `CubicBezier(p0, p1, p2)` means add a cubic bezier curve to the path
    /// starting at the current location with control points `p0` and 'p1' and
    /// end point 'p2'.
    CubicBezier(Point, Point, Point),
}

#[cfg(test)]
mod tests {
    use super::*;
    use approx::assert_abs_diff_eq;
    use wasm_bindgen_test::*;

    wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn test_hello() {
        console_log!("Hello from the geometry tests module");
    }

    #[wasm_bindgen_test]
    fn test_intersect() {
        let p1 = intersect_points_angles(
            Point::new(-0.75, 0.0),
            AngleDeg::from(45.0),
            Point::new(-0.25, 0.0),
            AngleDeg::from(-45.0),
        )
        .expect("lines should intersect");
        assert_abs_diff_eq!(p1, Point::new(-0.5, 0.25), epsilon = 0.000001);

        let p2 = intersect_points_angles(
            Point::new(0.75, 0.5),
            AngleDeg::from(-135.0),
            Point::new(0.25, 0.5),
            AngleDeg::from(135.0),
        )
        .expect("lines should intersect");
        assert_abs_diff_eq!(p2, Point::new(0.5, 0.25), epsilon = 0.000001);
    }
}
