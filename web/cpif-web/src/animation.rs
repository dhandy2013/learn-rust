//! Animation and timing control.
//! Goal: Try to run with a set frame time between callbacks, but stop running
//! if the current browser tab goes into the background. This is done with a
//! combination of setInterval() and setTimeout() (using the Rust equivalent
//! of these JavaSript functions).
use crate::error::Error;
use crate::web_util;
use wasm_bindgen::{prelude::Closure, JsCast};

pub struct Animator {
    frame_time_sec: f64, // seconds between callback invocations
    is_running: bool,
    animation_closure: Closure<dyn FnMut(f64)>,
    animation_id: Option<i32>,
    last_animation_timestamp: Option<f64>,
    timer_closure: Closure<dyn FnMut()>,
    timer_id: Option<i32>,
    need_redraw: bool,

    /// Number of times that Self::on_timer() has been called.
    timer_count: usize,

    /// Last timer_count seen by Self::on_animation_frame()
    frame_count: usize,
}

// SAFETY: I believe this is safe because in reality the Animator objects
// will only be called from code run by JavaScript closures which are on the
// main UI thread.
unsafe impl Send for Animator {}
unsafe impl Sync for Animator {}

impl Animator {
    pub(crate) fn new(
        animation_closure: Closure<dyn FnMut(f64)>,
        timer_closure: Closure<dyn FnMut()>,
    ) -> Self {
        Self {
            frame_time_sec: 0.0,
            is_running: false,
            animation_closure,
            animation_id: None,
            last_animation_timestamp: None,
            timer_closure,
            timer_id: None,
            need_redraw: false,
            timer_count: 0,
            frame_count: 0,
        }
    }

    pub fn start(&mut self, frame_time_sec: f64) -> Result<(), Error> {
        self.frame_time_sec = frame_time_sec;
        self.is_running = true;
        self.request_animation_frame()
    }

    pub fn stop(&mut self) {
        self.is_running = false;
        if let Some(timer_id) = self.timer_id.take() {
            web_util::get_window().clear_timeout_with_handle(timer_id);
        }
    }

    pub fn frame_time_sec(&self) -> f64 {
        self.frame_time_sec
    }

    pub fn is_running(&self) -> bool {
        self.is_running
    }

    pub fn cause_redraw(&mut self) -> Result<(), Error> {
        self.need_redraw = true;
        self.request_animation_frame()
    }

    /// This method is called by the animation frame closure.
    /// Update the frame count to match the timer count.
    /// Return true iff the app should be re-drawn by the caller.
    pub(crate) fn on_animation_frame(
        &mut self,
        timestamp: f64,
    ) -> Result<bool, Error> {
        // Ignore duplicate timestamps
        match self.last_animation_timestamp {
            Some(last_animation_timestamp) => {
                if last_animation_timestamp == timestamp {
                    return Ok(false);
                }
            }
            None => self.last_animation_timestamp = Some(timestamp),
        }

        // Signal to the timer handler that the frame handler has responded to
        // the latest step result.
        self.frame_count = self.timer_count;

        let return_value = self.need_redraw;
        self.need_redraw = false;

        // Restart the timer if animation is running and timer not already started
        if self.is_running && self.timer_id.is_none() {
            self.set_interval(self.frame_time_sec)?;
        }

        Ok(return_value)
    }

    /// Start the timer, set to fire every `interval_sec` seconds.
    /// If there is an error starting the timer, return the error.
    /// Otherwise, set `self.timer_id` to the i32 ID for this timer
    /// instance so that it can be canceled later.
    fn set_interval(&mut self, interval_sec: f64) -> Result<(), Error> {
        let window = web_util::get_window();
        let delay_ms: i32 = (interval_sec * 1000.0) as i32;
        let timer_id = window
            .set_interval_with_callback_and_timeout_and_arguments_0(
                self.timer_closure.as_ref().unchecked_ref(),
                delay_ms,
            )?;
        self.timer_id = Some(timer_id);
        Ok(())
    }

    /// When the timer interval timer fires, do the following:
    /// - Check if the animation frame handler is getting called. If not, assume
    ///   the the browser tab is in the background and cancel the interval timer.
    /// - If force_redraw is true, set the need_redraw flag.
    /// - Schedule an animation frame.
    pub(crate) fn on_timer(&mut self, force_redraw: bool) -> Result<(), Error> {
        // Check to see if the timer has gotten ahead of the frame handler.
        if self.timer_count > self.frame_count {
            // .on_animation_frame() did not get called since the last
            // time this handler got called. Cancel the timer so we don't get
            // a pileup.
            log::debug!(
                "Animator::on_timer timer_count={} frame_count={} cancel timer",
                self.timer_count,
                self.frame_count
            );
            self.cancel_timer();
        }
        self.timer_count += 1;

        if force_redraw {
            self.need_redraw = true;
        }
        self.request_animation_frame()
    }

    fn request_animation_frame(&mut self) -> Result<(), Error> {
        let animation_id = web_util::get_window().request_animation_frame(
            self.animation_closure.as_ref().unchecked_ref(),
        )?;
        self.animation_id = Some(animation_id);
        self.last_animation_timestamp = None;
        Ok(())
    }

    /// Cancel the interval timer
    fn cancel_timer(&mut self) {
        if let Some(timer_id) = self.timer_id.take() {
            let _ = web_util::get_window().clear_timeout_with_handle(timer_id);
        }
    }

    /// Cancel the animation frame request
    fn cancel_animation(&mut self) {
        if let Some(animation_id) = self.animation_id.take() {
            let _ = web_util::get_window().cancel_animation_frame(animation_id);
        }
    }
}

impl Drop for Animator {
    fn drop(&mut self) {
        self.cancel_animation();
        self.cancel_timer();
    }
}
