//! Sound support
use crate::error::Error;
use uuid::Uuid;
use wasm_bindgen::{closure::Closure, JsCast};

pub struct SoundController {
    /// The main AudioContext. Most sound-related commands start here.
    ctx: web_sys::AudioContext,

    /// The main volume control. This is connected directly to the audio
    /// context destination. All other things must connect to this or you
    /// will not hear them!
    gain: web_sys::GainNode,

    /// One second of random noise, to be re-used in different ways
    noise_buf: web_sys::AudioBuffer,

    /// The currently running sounds.
    /// Remove these and drop them when they are stopped, because they cannot
    /// be restarted or reused.
    sounds: Vec<SoundData>,

    /// Javascript Closure to call whenever a sound stops playing.
    sound_end_closure: Closure<dyn FnMut(web_sys::Event)>,
}

impl SoundController {
    pub const ATTACK_TIME: f64 = 0.01;
    pub const DECAY_TIME: f64 = 0.01;

    /// Create a SoundController with a Closure to be called when any
    /// controlled sound ends.
    pub(crate) fn new(
        sound_end_closure: Closure<dyn FnMut(web_sys::Event)>,
    ) -> Result<Self, Error> {
        // Create the audio context that is key to all audio APIs
        let ctx = web_sys::AudioContext::new()?;

        // Create the master volume control for this audio context
        let gain = ctx.create_gain()?;
        gain.connect_with_audio_node(&ctx.destination())?;

        // Create a 1-second buffer of white noise
        let num_noise_channels: u32 = 1;
        let sample_rate = ctx.sample_rate();
        let num_noise_samples = sample_rate as u32;
        let mut noise_samples =
            Vec::<f32>::with_capacity(num_noise_samples as usize);
        for _ in 0..num_noise_samples as usize {
            let r = js_sys::Math::random() as f32;
            noise_samples.push((r * 2.0) - 1.0);
        }
        log::debug!(
            "cpif_web::sounds::SoundController::new: \
            num_noise_channels:{num_noise_channels} \
            num_noise_samples:{num_noise_samples} \
            sample_rate:{sample_rate}"
        );
        let noise_buf = ctx.create_buffer(
            num_noise_channels,
            num_noise_samples,
            sample_rate,
        )?;
        noise_buf.copy_to_channel(&noise_samples, 0)?;

        // Construct and return the SoundController
        Ok(Self {
            ctx,
            gain,
            noise_buf,
            sounds: Vec::<_>::new(),
            sound_end_closure,
        })
    }

    /// Called by sound_end_closure whenever a registered sound ends.
    /// Return the ID of the related sound object, if any.
    pub(crate) fn on_audio_node_stop(
        &mut self,
        audio_node: web_sys::AudioNode,
    ) -> Option<SoundID> {
        if let Some(mut sound) = self.del_sound_by_audio_node(&audio_node) {
            if let Err(err) = sound.stop() {
                log::error!(
                    "SoundController: error stopping sound {:?}: {:?}",
                    sound.id(),
                    err
                );
            }
            Some(sound.id())
        } else {
            log::debug!(
                "on_audio_node_stop: node not registered: {:?}",
                audio_node
            );
            None
        }
    }

    /// Find a sound for which audio_node is its tracking audio node.
    /// Remove the sound object from the list and return it, or return
    /// None if no matching sound object is found.
    fn del_sound_by_audio_node(
        &mut self,
        audio_node: &web_sys::AudioNode,
    ) -> Option<SoundData> {
        let sound_index = if let Some((i, _sound)) =
            self.sounds.iter().enumerate().find(|(_i, sound)| {
                js_sys::Object::is(audio_node, &sound.tracking_node)
            }) {
            i
        } else {
            return None;
        };
        let sound = self.sounds.swap_remove(sound_index);
        Some(sound)
    }

    /// Get the current main volume setting.
    /// Range is 0.0 through 1.0 inclusive.
    pub fn main_volume(&self) -> f32 {
        let volume = self.gain.gain().value();
        volume
    }

    /// Set the current main volume setting.
    /// Range is 0.0 through 1.0 inclusive.
    pub fn set_main_volume(&mut self, volume: f32) {
        self.gain.gain().set_value(volume);
    }

    /// Play a steady tone and then stop
    pub fn beep(
        &mut self,
        waveform: Waveform,
        tone: Tone,
    ) -> Result<SoundID, Error> {
        let sound = self.sound_from_beep_tracks(waveform, &[&[tone]])?;
        Ok(self.add_sound(sound))
    }

    /// Play a sequence of simple tones, all with the same waveform
    pub fn beep_tones(
        &mut self,
        waveform: Waveform,
        tones: &[Tone],
    ) -> Result<SoundID, Error> {
        let sound = self.sound_from_beep_tracks(waveform, &[tones])?;
        Ok(self.add_sound(sound))
    }

    /// Simultaneously play two sequences of simple tones, all with the same
    /// waveform
    pub fn beep_duet(
        &mut self,
        waveform: Waveform,
        tones0: &[Tone],
        tones1: &[Tone],
    ) -> Result<SoundID, Error> {
        let sound = self.sound_from_beep_tracks(waveform, &[tones0, tones1])?;
        Ok(self.add_sound(sound))
    }

    /// Play an electronic bell sound
    pub fn bell(
        &mut self,
        time_constant: f64,
        tone: Tone,
    ) -> Result<SoundID, Error> {
        let sound = self.sound_from_bell_tracks(time_constant, &[&[tone]])?;
        Ok(self.add_sound(sound))
    }

    /// Play a sequence of electronic bell sounds
    pub fn bell_tones(
        &mut self,
        time_constant: f64,
        tones: &[Tone],
    ) -> Result<SoundID, Error> {
        let sound = self.sound_from_bell_tracks(time_constant, &[tones])?;
        Ok(self.add_sound(sound))
    }

    /// Simultaneously play two sequences of electronic bell sounds
    pub fn bell_duet(
        &mut self,
        time_constant: f64,
        tones0: &[Tone],
        tones1: &[Tone],
    ) -> Result<SoundID, Error> {
        let sound =
            self.sound_from_bell_tracks(time_constant, &[tones0, tones1])?;
        Ok(self.add_sound(sound))
    }

    /// Play a burst of noise. The noise will have the filter center frequency,
    /// volume, and duration of the `tone` parameter.
    pub fn noise(&mut self, tone: Tone) -> Result<SoundID, Error> {
        let tracking_node = self.create_tracking_node()?;
        let mut data = SoundData::new(tracking_node);
        let t = self.ctx.current_time();

        let end_t = data.add_noise_tones(
            &self.ctx,
            &self.ctx.destination(),
            &self.noise_buf,
            &[tone],
            t,
        )?;

        data.start(end_t)?;
        Ok(self.add_sound(data))
    }

    /// Play a sequence of noise "tones" with different frequencies and volumes
    pub fn noise_tones(&mut self, tones: &[Tone]) -> Result<SoundID, Error> {
        let tracking_node = self.create_tracking_node()?;
        let mut data = SoundData::new(tracking_node);
        let t = self.ctx.current_time();

        let end_t = data.add_noise_tones(
            &self.ctx,
            &self.ctx.destination(),
            &self.noise_buf,
            tones,
            t,
        )?;

        data.start(end_t)?;
        Ok(self.add_sound(data))
    }

    /// Play a "whoosh" sound that smoothly changes the volume and frequency
    /// of the noise over time based on a sequence of Tone settings.
    ///
    /// Given tones = tone0, tone1, ..., toneN;
    ///
    /// For each i in 0..N:
    /// - Over the time period `tones[i].duration()`:
    ///   - frequency is ramped exponentially from `tones(i].freq()` to
    ///     `tones[i+1].freq()`
    ///   - volume is ramped linearly from `tones[i].volume()` to
    ///     `tones[i+1].volume()`
    ///
    /// Finally, over time period `tones[N].duration()`:
    /// - frequency set to `tones[N].freq()`
    /// - volume is set to `tones[N].volume()`
    pub fn whoosh(&mut self, tones: &[Tone]) -> Result<SoundID, Error> {
        let tracking_node = self.create_tracking_node()?;
        let mut data = SoundData::new(tracking_node);
        let t = self.ctx.current_time();

        let end_t = data.add_whoosh_tones(
            &self.ctx,
            &self.ctx.destination(),
            &self.noise_buf,
            tones,
            t,
        )?;

        data.start(end_t)?;
        Ok(self.add_sound(data))
    }

    /// Add a Sound object to the list of stored sounds and return its ID.
    fn add_sound(&mut self, sound: SoundData) -> SoundID {
        let sound_id = sound.id();
        self.sounds.push(sound);
        sound_id
    }

    /// Find a sound object in the list of stored sounds.
    /// Return the internal index of the sound object if it exists, None
    /// otherwise.
    fn find_sound(&self, sound_id: SoundID) -> Option<usize> {
        if let Some((i, _sound)) = self
            .sounds
            .iter()
            .enumerate()
            .find(|(_i, sound)| sound.id() == sound_id)
        {
            Some(i)
        } else {
            None
        }
    }

    /// Stop a currently-playing sound. Calling stop() on the same sound twice
    /// does not result in an error; it just has no effect.
    /// Return true if the sound was found and .stop() was called on it,
    /// false if the sound was not found, and error if sound.stop() errored.
    ///
    /// This method does not remove the sound from the list of sounds. It is
    /// expected that Self::on_audio_node_stop() will remove the sound from the
    /// list later when the sound end event fires.
    pub fn stop_sound(&mut self, sound_id: SoundID) -> Result<bool, Error> {
        match self.find_sound(sound_id) {
            Some(sound_index) => {
                let sound = &mut self.sounds[sound_index];
                sound.stop()?;
                Ok(true)
            }
            None => Ok(false),
        }
    }

    /// Stop playing all sounds.
    pub fn stop(&mut self) -> Result<(), Error> {
        for sound in self.sounds.iter_mut() {
            sound.stop()?;
        }
        Ok(())
    }

    /// Create a SoundData object to play sequences of plain beep tones
    fn sound_from_beep_tracks(
        &mut self,
        waveform: Waveform,
        tone_tracks: &[&[Tone]],
    ) -> Result<SoundData, Error> {
        let tracking_node = self.create_tracking_node()?;
        let mut data = SoundData::new(tracking_node);
        let t = self.ctx.current_time();
        let mut end_t = t;
        for tones in tone_tracks {
            let et = data.add_beep_tones(
                &self.ctx,
                &self.ctx.destination(),
                waveform,
                tones,
                t,
            )?;
            if et > end_t {
                end_t = et
            }
        }
        data.start(end_t)?;
        Ok(data)
    }

    /// Create a SoundData object to play sequences of electronic bell tones
    fn sound_from_bell_tracks(
        &mut self,
        time_constant: f64,
        tone_tracks: &[&[Tone]],
    ) -> Result<SoundData, Error> {
        let tracking_node = self.create_tracking_node()?;
        let mut data = SoundData::new(tracking_node);
        let t = self.ctx.current_time();
        let mut end_t = t;
        for tones in tone_tracks {
            let et = data.add_bell_tones(
                &self.ctx,
                &self.ctx.destination(),
                time_constant,
                tones,
                t,
            )?;
            if et > end_t {
                end_t = et
            }
        }
        data.start(end_t)?;
        Ok(data)
    }

    /// Create a "tracking node", an AudioNode whose sole purpose is to expire
    /// at a certain time and generate a sound end callback that can be used
    /// to clean up other related sound objects.
    fn create_tracking_node(
        &self,
    ) -> Result<web_sys::ConstantSourceNode, Error> {
        let tracking_node = web_sys::ConstantSourceNode::new(&self.ctx)?;
        tracking_node.add_event_listener_with_callback(
            "ended",
            self.sound_end_closure.as_ref().unchecked_ref(),
        )?;
        Ok(tracking_node)
    }
}

// Any JavaScript-ish thing such as an AudioContext is not Send nor Sync because
// inside its Rust wrapper it contains a *mut u8. I want to store my
// SoundController objects in a global registry controlled by a Mutex but cannot
// do that by default because it contains JavaScript objects. So here I am
// artificially forcing SoundController to be Send and Sync so I can store it in
// the global registry.
// SAFETY: I believe this is safe because in reality the SoundController objects
// will only be called from code run by JavaScript closures which are on the
// main UI thread.
unsafe impl Send for SoundController {}
unsafe impl Sync for SoundController {}

#[derive(Clone, Copy, Debug)]
#[non_exhaustive]
pub enum Waveform {
    Sine,
    Square,
    Sawtooth,
    Triangle,
}

impl From<Waveform> for web_sys::OscillatorType {
    fn from(waveform: Waveform) -> Self {
        use Waveform::*;
        match waveform {
            Sine => web_sys::OscillatorType::Sine,
            Square => web_sys::OscillatorType::Square,
            Sawtooth => web_sys::OscillatorType::Sawtooth,
            Triangle => web_sys::OscillatorType::Triangle,
        }
    }
}

struct SoundData {
    id: SoundID,
    tracking_node: web_sys::ConstantSourceNode,
    audio_nodes: Vec<web_sys::AudioNode>,
    #[allow(dead_code)]
    gain_list: Vec<web_sys::GainNode>,
}

impl SoundData {
    fn new(tracking_node: web_sys::ConstantSourceNode) -> Self {
        Self {
            id: SoundID::new(),
            tracking_node,
            audio_nodes: Vec::<_>::new(),
            gain_list: Vec::<_>::new(),
        }
    }

    fn id(&self) -> SoundID {
        self.id
    }

    /// Create oscillator and gain nodes to generate plain "beep" tones.
    /// Connect the nodes to each other and to audio output.
    /// Schedule the attack and decay envelops, and the final stop time.
    /// Return the end time in seconds of the sequence of tones.
    fn add_beep_tones(
        &mut self,
        ctx: &web_sys::AudioContext,
        destination: &web_sys::AudioNode,
        waveform: Waveform,
        tones: &[Tone],
        start_time: f64,
    ) -> Result<f64, Error> {
        let mut t = start_time;
        let osc = ctx.create_oscillator()?;
        osc.set_type(waveform.into());
        let gain = ctx.create_gain()?;
        let g = gain.gain();

        for tone in tones {
            // Set frequency of tone
            osc.frequency().set_value_at_time(tone.freq.0, t)?;

            let sustain_t = tone.duration
                - (SoundController::ATTACK_TIME + SoundController::DECAY_TIME);
            if sustain_t >= 0.0 {
                // Attack
                g.set_value_at_time(0.0, t)?;
                g.linear_ramp_to_value_at_time(
                    tone.volume,
                    t + SoundController::ATTACK_TIME,
                )?;
                // Sustain
                g.set_value_at_time(
                    tone.volume,
                    t + SoundController::ATTACK_TIME + sustain_t,
                )?;
                // Decay
                g.linear_ramp_to_value_at_time(0.0, t + tone.duration)?;
            } else {
                // Beep is too short for attack/sustain/decay
                g.set_value_at_time(tone.volume, t)?;
            }

            // Advance the time
            t += tone.duration;
        }

        osc.connect_with_audio_node(&gain)?
            .connect_with_audio_node(destination)?;

        let audio_node = osc
            .dyn_into::<web_sys::AudioNode>()
            .expect("an oscillator node *is* an audio node");
        self.audio_nodes.push(audio_node);
        self.gain_list.push(gain);
        Ok(t)
    }

    /// Create oscillator and gain nodes for ringing bell tones.
    /// Return the end time in seconds of the sequence of tones.
    /// `time_constant` is damping time constant in seconds:
    ///
    /// - 1 time constant: 63.2%
    /// - 2 time constants: 86.5%
    /// - 3 time constants: 95.0%
    /// - 4 time constants: 98.2%
    /// - 5 time constants: 99.3%
    ///
    /// However, the output is completely silenced at the end of the complete
    /// series of tones.
    fn add_bell_tones(
        &mut self,
        ctx: &web_sys::AudioContext,
        destination: &web_sys::AudioNode,
        time_constant: f64,
        tones: &[Tone],
        start_time: f64,
    ) -> Result<f64, Error> {
        let mut t = start_time;
        let osc = ctx.create_oscillator()?;
        osc.set_type(web_sys::OscillatorType::Sine);
        let gain = ctx.create_gain()?;
        let g = gain.gain();
        for tone in tones {
            // Set frequency of tone
            osc.frequency().set_value_at_time(tone.freq.0, t)?;

            // Attack
            g.set_value_at_time(0.0, t)?;
            g.linear_ramp_to_value_at_time(
                tone.volume,
                t + SoundController::ATTACK_TIME,
            )?;

            // Begin the amplitude decay
            g.set_target_at_time(
                0.0,
                t + SoundController::ATTACK_TIME,
                time_constant,
            )?;

            // Advance the time
            t += tone.duration;
        }

        osc.connect_with_audio_node(&gain)?
            .connect_with_audio_node(destination)?;

        let audio_node = osc
            .dyn_into::<web_sys::AudioNode>()
            .expect("an oscillator node *is* an audio node");
        self.audio_nodes.push(audio_node);
        self.gain_list.push(gain);
        Ok(t)
    }

    /// Create a series of noise bursts at a given filter frequencies and
    /// volumes. Return the ending time, which will be greater than or equal to
    /// the starting time.
    fn add_noise_tones(
        &mut self,
        ctx: &web_sys::AudioContext,
        destination: &web_sys::AudioNode,
        noise_buf: &web_sys::AudioBuffer,
        tones: &[Tone],
        start_time: f64,
    ) -> Result<f64, Error> {
        let noise_node = ctx.create_buffer_source()?;
        noise_node.set_buffer(Some(noise_buf));
        noise_node.set_loop(true);

        let gain = ctx.create_gain()?;
        let g = gain.gain();

        let filter = ctx.create_biquad_filter()?;
        filter.set_type(web_sys::BiquadFilterType::Bandpass);
        let frequency = filter.frequency();

        let mut t = start_time;
        for tone in tones.iter() {
            g.set_value_at_time(tone.volume(), t)?;
            frequency.set_value_at_time(tone.freq().0, t)?;
            if tone.duration() > 0.0 {
                t += tone.duration();
            }
        }

        noise_node
            .connect_with_audio_node(&filter)?
            .connect_with_audio_node(&gain)?
            .connect_with_audio_node(destination)?;

        let audio_node = noise_node
            .dyn_into::<web_sys::AudioNode>()
            .expect("an audio buffer source node *is* an audio node");
        self.audio_nodes.push(audio_node);
        self.gain_list.push(gain);

        Ok(t)
    }

    /// Create a "whoosh" sound with changing frequency and volume based on the
    /// tones. Return end time, which will be greater than or equal to
    /// `start_time`.
    fn add_whoosh_tones(
        &mut self,
        ctx: &web_sys::AudioContext,
        destination: &web_sys::AudioNode,
        noise_buf: &web_sys::AudioBuffer,
        tones: &[Tone],
        start_time: f64,
    ) -> Result<f64, Error> {
        let noise_node = ctx.create_buffer_source()?;
        noise_node.set_buffer(Some(noise_buf));
        noise_node.set_loop(true);

        let gain = ctx.create_gain()?;
        let g = gain.gain();

        let filter = ctx.create_biquad_filter()?;
        filter.set_type(web_sys::BiquadFilterType::Bandpass);
        let frequency = filter.frequency();

        let mut t = start_time;
        for (i, tone) in tones.iter().enumerate() {
            if i == 0 {
                g.set_value_at_time(tone.volume(), t)?;
                frequency.set_value_at_time(tone.freq().0, t)?;
            } else {
                g.linear_ramp_to_value_at_time(tone.volume(), t)?;
                frequency
                    .exponential_ramp_to_value_at_time(tone.freq().0, t)?;
            }
            if tone.duration() > 0.0 {
                t += tone.duration();
            }
        }

        noise_node
            .connect_with_audio_node(&filter)?
            .connect_with_audio_node(&gain)?
            .connect_with_audio_node(destination)?;

        let audio_node = noise_node
            .dyn_into::<web_sys::AudioNode>()
            .expect("an audio buffer source node *is* an audio node");
        self.audio_nodes.push(audio_node);
        self.gain_list.push(gain);

        Ok(t)
    }

    /// Start all the startable nodes associated with this sound.
    /// Have the tracking node stop at time end_t
    fn start(&mut self, end_t: f64) -> Result<(), Error> {
        for audio_node in self.audio_nodes.iter() {
            start_audio_node(audio_node)?;
        }
        self.tracking_node.start()?;
        self.tracking_node.stop_with_when(end_t)?;
        Ok(())
    }

    fn stop(&mut self) -> Result<(), Error> {
        for audio_node in self.audio_nodes.drain(..) {
            stop_audio_node(&audio_node)?;
        }
        self.tracking_node.stop()?;
        Ok(())
    }
}

/// Call the start() method on an AudioScheduledSourceNode represented as an
/// AudioNode. Apparently on Safari browsers the AudioScheduledSourceNode
/// interface doesn't exist so I have to use this workaround (as recommended
/// in the docs).
fn start_audio_node(audio_node: &web_sys::AudioNode) -> Result<(), Error> {
    if let Some(osc_node) = audio_node.dyn_ref::<web_sys::OscillatorNode>() {
        return Ok(osc_node.start()?);
    }
    if let Some(buf_node) =
        audio_node.dyn_ref::<web_sys::AudioBufferSourceNode>()
    {
        return Ok(buf_node.start()?);
    }
    if let Some(const_node) =
        audio_node.dyn_ref::<web_sys::ConstantSourceNode>()
    {
        return Ok(const_node.start()?);
    }
    Err(Error::from("audio_node does not have a start method"))
}

/// Call the stop() method on an AudioScheduledSourceNode represented as an
/// AudioNode. Apparently on Safari browsers the AudioScheduledSourceNode
/// interface doesn't exist so I have to use this workaround (as recommended
/// in the docs).
fn stop_audio_node(audio_node: &web_sys::AudioNode) -> Result<(), Error> {
    match audio_node.dyn_ref::<web_sys::OscillatorNode>() {
        Some(osc_node) => return Ok(osc_node.stop()?),
        None => {}
    }
    match audio_node.dyn_ref::<web_sys::AudioBufferSourceNode>() {
        Some(buf_node) => return Ok(buf_node.stop()?),
        None => {}
    }
    match audio_node.dyn_ref::<web_sys::ConstantSourceNode>() {
        Some(const_node) => return Ok(const_node.stop()?),
        None => {}
    }
    Err(Error::from("audio_node does not have a stop method"))
}

// Any JavaScript-ish thing such as an OscilatorNode is not Send nor Sync
// because inside its Rust wrapper it contains a *mut u8. I want to store my
// SoundData objects indirectly in a global registry controlled by a Mutex but
// cannot do that by default because it contains JavaScript objects. So here I
// am artificially forcing SoundData to be Send and Sync so I can store it in
// the global registry.
// SAFETY: I believe this is safe because in reality the SoundData objects will
// only be called from code run by JavaScript closures which are on the main UI
// thread.
unsafe impl Send for SoundData {}
unsafe impl Sync for SoundData {}

impl Drop for SoundData {
    fn drop(&mut self) {
        let _ = self.stop();
    }
}

/// A globally unique ID for an event listener to act as a handle
/// for removing it later.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct SoundID(Uuid);

impl SoundID {
    fn new() -> Self {
        Self(Uuid::new_v4())
    }
}

/// Frequency in Hz
#[derive(Clone, Copy, Debug)]
pub struct Frequency(f32);

impl From<f32> for Frequency {
    fn from(f: f32) -> Self {
        Self(f)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Tone {
    /// Frequency of the tone in Hz
    freq: Frequency,

    /// Duration of the time the tone will play in seconds
    duration: f64,

    /// Volume of the tone, use range 0.0..=1.0 for best results
    volume: f32,
}

impl Tone {
    /// `freq`: frequency in Hz
    /// `duration`: length of time tone is played, in seconds
    /// `volume`: loudness of tone, use range 0.0..=1.0 for best results
    pub const fn new(freq: Frequency, duration: f64, volume: f32) -> Self {
        Self {
            freq,
            duration,
            volume,
        }
    }

    pub fn freq(&self) -> Frequency {
        self.freq
    }

    pub fn duration(&self) -> f64 {
        self.duration
    }

    pub fn volume(&self) -> f32 {
        self.volume
    }
}

/// Generate a series of DTMF tones.
/// `tone_duration`: Time in seconds each tone will play.
/// `space_duration`: Time in seconds between each tone.
/// `volume`: Loudness of each tone, range 0.0..=1.0
/// `codes`: string containing the character code for each tone:
/// ```text
///                 High-group frequencies (Hz)
///                 1209 1336 1447 1633
///             697  1    2    3    A
/// Low-group   770  4    5    6    B
/// frequencies 852  7    8    9    C
/// (Hz)        941  *    0    #    D
/// ```
/// Characters are interpreted case-insensitive. `E` is interpreted as `*` and
/// `F` is interpreted as `#`, so all hexidecimal characters are supported.
/// Unsupported characters are ignored.
///
/// Return a pair of tone lists to be played simultaneously.
pub fn dtmf(
    tone_duration: f64,
    space_duration: f64,
    volume: f32,
    codes: &str,
) -> (Vec<Tone>, Vec<Tone>) {
    let mut lo_tones = Vec::<Tone>::new();
    let mut hi_tones = Vec::<Tone>::new();
    for c in codes.chars() {
        let (lo_freq, hi_freq) = match c.to_ascii_uppercase() {
            '1' => (697.0, 1209.0),
            '2' => (697.0, 1336.0),
            '3' => (697.0, 1447.0),
            'A' => (697.0, 1633.0),
            //
            '4' => (770.0, 1209.0),
            '5' => (770.0, 1336.0),
            '6' => (770.0, 1447.0),
            'B' => (770.0, 1633.0),
            //
            '7' => (852.0, 1209.0),
            '8' => (852.0, 1336.0),
            '9' => (852.0, 1447.0),
            'C' => (852.0, 1633.0),
            //
            '*' | 'E' => (941.0, 1209.0),
            '0' => (941.0, 1336.0),
            '#' | 'F' => (941.0, 1447.0),
            'D' => (941.0, 1633.0),
            //
            _ => {
                continue;
            }
        };
        lo_tones.push(Tone::new(
            Frequency::from(lo_freq),
            tone_duration,
            volume,
        ));
        lo_tones.push(Tone::new(Frequency::from(0.0), space_duration, 0.0));
        hi_tones.push(Tone::new(
            Frequency::from(hi_freq),
            tone_duration,
            volume,
        ));
        hi_tones.push(Tone::new(Frequency::from(0.0), space_duration, 0.0));
    }
    (lo_tones, hi_tones)
}
