//! Color abstraction for 2D vector graphics
use crate::web_util;
use std::fmt;
use wasm_bindgen::prelude::JsValue;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Color(u32);

impl Color {
    /// EGA palette colors
    pub const BLACK: Color = Color(0x000000ff);
    pub const BLUE: Color = Color(0x0000aaff);
    pub const GREEN: Color = Color(0x00aa00ff);
    pub const CYAN: Color = Color(0x00aaaaff);
    pub const RED: Color = Color(0xaa0000ff);
    pub const MAGENTA: Color = Color(0xaa00aaff);
    pub const BROWN: Color = Color(0xaa5500ff);
    pub const WHITE: Color = Color(0xaaaaaaff); // or "light gray"
    pub const GRAY: Color = Color(0x555555ff); // or "dark gray / bright black"
    pub const BRIGHT_BLUE: Color = Color(0x5555ffff);
    pub const BRIGHT_GREEN: Color = Color(0x55ff55ff);
    pub const BRIGHT_CYAN: Color = Color(0x55ffffff);
    pub const BRIGHT_RED: Color = Color(0xff5555ff);
    pub const BRIGHT_MAGENTA: Color = Color(0xff55ffff);
    pub const BRIGHT_YELLOW: Color = Color(0xffff55ff);
    pub const BRIGHT_WHITE: Color = Color(0xffffffff);

    /// Use this to draw without really drawing
    pub const TRANSPARENT: Color = Color(0x00000000);

    /// Return a JsValue containing a CSS string representation of the color.
    /// https://developer.mozilla.org/en-US/docs/Web/CSS/color_value
    pub fn as_js_color(&self) -> JsValue {
        web_util::color_num_to_js_value(self.0)
    }

    /// Return color as an array of 4 bytes: [red, green, blue, alpha]
    pub fn as_array4(&self) -> [u8; 4] {
        [
            ((self.0 & 0xff000000) >> 24) as u8, // red
            ((self.0 & 0x00ff0000) >> 16) as u8, // green
            ((self.0 & 0x0000ff00) >> 8) as u8,  // blue
            (self.0 & 0x000000ff) as u8,         // alpha
        ]
    }

    // Return color as an array of 3 bytes: [red, green, blue] (alpha truncated)
    pub fn as_array3(&self) -> [u8; 3] {
        [
            ((self.0 & 0xff000000) >> 24) as u8, // red
            ((self.0 & 0x00ff0000) >> 16) as u8, // green
            ((self.0 & 0x0000ff00) >> 8) as u8,  // blue
        ]
    }
}

impl fmt::Debug for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Color(0x{:08x})", self.0)
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let color_array4 = self.as_array4();
        write!(
            f,
            "Color(red=0x{:02x}, green=0x{:02x}, blue=0x{:02x}, alpha=0x{:02x})",
            color_array4[0], // red
            color_array4[1], // green
            color_array4[2], // blue
            color_array4[3], // alpha
        )
    }
}

/// Create a Color from a (red, green, blue, alpha) tuple.
impl From<(u8, u8, u8, u8)> for Color {
    fn from(value: (u8, u8, u8, u8)) -> Color {
        Color(
            ((value.0 as u32) << 24) |  // red
            ((value.1 as u32) << 16) |  // green
            ((value.2 as u32) << 8) |   // blue
            (value.3 as u32), // alpha
        )
    }
}

/// Create a Color from a (red, green, blue) tuple, alpha defaults to maximum.
impl From<(u8, u8, u8)> for Color {
    fn from(value: (u8, u8, u8)) -> Color {
        Color(
            ((value.0 as u32) << 24) |  // red
            ((value.1 as u32) << 16) |  // green
            ((value.2 as u32) << 8) |   // blue
            0x000000ff, // default alpha value is fully opaque
        )
    }
}
