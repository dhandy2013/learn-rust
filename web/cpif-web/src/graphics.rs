//! Graphics module to simplify drawing on an HTML canvas.
use crate::color::Color;
use crate::error::Error;
pub use crate::geometry::TransformStyle;
use crate::geometry::{
    AngleDeg, Length, PathCommand, PixelPoint, Pixels, Point, Rect, RectSize,
};
use crate::web_util;
use js_sys::Array;
use std::f64::consts::PI;
use std::fmt::Debug;
use std::io::Write;
use std::sync::{Arc, Mutex};
use wasm_bindgen::prelude::JsValue;
use web_sys::DomMatrix;

pub struct DrawingCanvas {
    /// The options with which this DrawingCanvas was originally created
    options: CanvasOptions,

    /// ID of the HTML canvas for this DrawingCanvas
    canvas_id: String,

    /// The HTML canvas on which all drawing will be rendered.
    canvas: web_sys::HtmlCanvasElement,

    /// The canvas' 2D rendering context.
    /// The DOM specifies that the canvas only has one rendering context, so it
    /// makes sense to cache it rather than repeatedly call JavaScript to fetch
    /// it.
    ctx: web_sys::CanvasRenderingContext2d,

    /// Canvas width, cached to avoid repeatedly calling canvas.width().
    width: u32,

    /// Canvas height, cached to avoid repeatedly calling canvas.height().
    height: u32,

    /// Current top-level transformation state
    transform_state: TransformState,

    /// Stack of previous transformation states
    transform_stack: Vec<TransformState>,
}

#[derive(Clone, Copy, Debug)]
struct TransformState {
    /// Pixels per coordinate unit
    scale: f64,

    /// Top-level transformation style
    style: TransformStyle,

    /// Default font size in px
    text_height_px: Pixels,
}

impl Default for TransformState {
    fn default() -> Self {
        Self {
            scale: 1.0,
            style: TransformStyle::Pixel,
            text_height_px: DrawingCanvas::DEFAULT_TEXT_HEIGHT_PX,
        }
    }
}

// Any JavaScript-ish thing such as a CanvasRenderingContext2d is not Send nor
// Sync because inside its Rust wrapper it contains a *mut u8. I want to store
// my DrawingCanvas objects in a global registry controlled by a Mutex but
// cannot do that by default because it contains JavaScript objects. So here I
// am artificially forcing DrawingCanvas to be Send and Sync so I can store it
// in the global registry.
// SAFETY: I believe this is safe because in reality the DrawingCanvas objects
// will only be called from code run by JavaScript closures which are on the
// main UI thread.
unsafe impl Send for DrawingCanvas {}
unsafe impl Sync for DrawingCanvas {}

pub type DrawingCanvasPtr = Arc<Mutex<DrawingCanvas>>;

impl DrawingCanvas {
    pub const CANVAS_ID_SUFFIX: &'static str = "-drawing-canvas";
    pub const DEFAULT_TEXT_HEIGHT_PX: Pixels = 20;

    /// Create DrawingCanvas canvas object for an existing HTMLCanvas element.
    ///
    /// `canvas_id`: The ID of an existing HTML Canvas element.
    ///
    /// `options`:
    ///     The canvas behavior options. Use CanvasOptions::default() to get
    ///     the default options, CanvasOptions::builder() to get a builder to
    ///     override any defaults.
    ///
    /// If `options.size()` is None (the default), resize the canvas to fill the
    /// entire browser client area, when the canvas is created and every time
    /// the browser window is resized. Otherwise make the initial canvas size
    /// the `(width, height)` tuple contained in the `options.size()` option.
    ///
    /// This method will also create a 2D rendering context for the canvas and
    /// configure it for the needs of this DrawingCanvas.
    pub fn new(canvas_id: &str, options: CanvasOptions) -> Result<Self, Error> {
        let canvas = web_util::get_canvas_by_id(canvas_id)?;

        // Pass alpha=false to (hopefully) speed up bitmap rendering.
        let ctx = web_util::get_render_context_2d(&canvas, false)?;

        let (width, height) = (canvas.width(), canvas.height());
        let mut transform_state = TransformState::default();

        // Pull text_height_px from options.
        // The `.type` and `.scale` fields will be set by either
        // `.set_pixel_transform()` or `.set_math_transform()` later in this
        // method.
        transform_state.text_height_px = options.text_height_px();

        let mut dc = Self {
            options,
            canvas_id: canvas_id.to_owned(),
            canvas,
            ctx,
            width,
            height,
            transform_state,
            transform_stack: Vec::<_>::new(),
        };

        // Finish setting up the canvas up according to the options
        use TransformStyle::*;
        match options.transform_style {
            Pixel => dc.set_pixel_transform(),
            Math => dc.set_math_transform(),
        }?;

        // Return the new DrawingCanvas
        Ok(dc)
    }

    /// Create a DrawingCanvas canvas inside a containing HTML div element.
    ///
    /// `div_id`:
    ///     The ID of an existing HTML div element. This method will create
    ///     an HTML canvas element inside this div with the ID returned by
    ///     `DrawingCanvas::get_canvas_id_for_div_id(div_id)`. Then this method
    ///     will create a DrawingCanvas for that HTML canvas.
    ///
    /// `options`:
    ///     The canvas behavior options. See `DrawingCanvas::new()` for details.
    pub fn new_in_div(
        div_id: &str,
        options: CanvasOptions,
    ) -> Result<Self, Error> {
        let canvas_id = Self::canvas_id_for_div_id(div_id);
        let _canvas =
            web_util::create_canvas(div_id, &canvas_id, options.size())?;
        Self::new(&canvas_id, options)
    }

    /// Create a DrawingCanvas and wrap it in a DrawingCanvasPtr smart pointer.
    /// See `DrawingCanvas::new()` for a description of the parameters.
    pub fn new_ptr(
        canvas_id: &str,
        options: CanvasOptions,
    ) -> Result<DrawingCanvasPtr, Error> {
        Ok(Arc::new(Mutex::new(Self::new(&canvas_id, options)?)))
    }

    /// Create a DrawingCanvas canvas inside a containing HTML div element.
    /// Return a smart pointer to this new DrawingCanvas, or an error if
    /// something went wrong. See `DrawingCanvas::new_in_div()` for a
    /// description of the parameters.
    pub fn new_ptr_in_div(
        div_id: &str,
        options: CanvasOptions,
    ) -> Result<DrawingCanvasPtr, Error> {
        Ok(Arc::new(Mutex::new(Self::new_in_div(div_id, options)?)))
    }

    /// Return the canvas ID generated from a div ID by appending
    /// DrawingCanvas::CANVAS_ID_SUFFIX ("-drawing-canvas").
    pub fn canvas_id_for_div_id(div_id: &str) -> String {
        format!("{}{}", div_id, Self::CANVAS_ID_SUFFIX)
    }

    /// The CanvasOptions that were used to create this DrawingCanvas
    pub fn options(&self) -> &CanvasOptions {
        &self.options
    }

    /// The ID of the HTML canvas element for this DrawingCanvas.
    pub fn canvas_id(&self) -> &str {
        &self.canvas_id
    }

    /// The canvas width in pixels
    pub fn width(&self) -> u32 {
        self.width
    }

    /// The canvas height in pixels
    pub fn height(&self) -> u32 {
        self.height
    }

    /// The number of coordinate units per pixel with the current scale
    pub fn pxsize(&self) -> f64 {
        let scale = self.transform_state.scale;
        if scale == 0.0 {
            1.0
        } else {
            1.0 / scale
        }
    }

    /// Set the coordinate transformation to "pixel style".
    ///
    /// This transformation is the usual computer graphics system: the origin is
    /// in the upper-left corner with x increasing to the right and y increasing
    /// downward. The scale is one coordinate unit per pixel.
    ///
    /// After the new scale has been applied, set the default line width to the
    /// number of units equivalent to one pixel, so we don't get surprised by
    /// really thin lines being drawn.
    pub fn set_pixel_transform(&mut self) -> Result<(), Error> {
        self.ctx.set_transform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0)?;
        self.transform_state = TransformState {
            scale: 1.0,
            style: TransformStyle::Pixel,
            ..self.transform_state
        };
        self.ctx.set_line_width(self.pxsize());
        Ok(())
    }

    /// Set the coordinate transformation to "math style".
    ///
    /// The origin is in the center, with x increasing to the right and y
    /// increasing upward. The scale is such that for the smallest canvas
    /// dimension the coordinate range is -1.0 to 1.0 inclusive. The larger
    /// canvas dimension is scaled to match. Call pxsize() to get the number of
    /// coordinate units per pixel.
    ///
    /// After the new scale has been applied, set the default line width to the
    /// number of units equivalent to one pixel, so we don't get surprised by
    /// really thick lines being drawn.
    pub fn set_math_transform(&mut self) -> Result<(), Error> {
        let half_width = self.width as f64 / 2.0;
        let half_height = self.height as f64 / 2.0;
        let min_dimension = half_width.min(half_height);
        let scale: f64 = if min_dimension == 0.0 {
            1.0
        } else {
            min_dimension
        };
        self.ctx.set_transform(
            scale,
            0.0,
            0.0,
            -scale,
            half_width,
            half_height,
        )?;
        self.transform_state = TransformState {
            scale,
            style: TransformStyle::Math,
            ..self.transform_state
        };
        self.ctx.set_line_width(self.pxsize());
        Ok(())
    }

    /// Set the text font size in px.
    /// The value will be clamped to the range 1..=999
    pub fn set_text_height_px(&mut self, text_height_px: Pixels) {
        let text_height_px = Pixels::max(1, Pixels::min(999, text_height_px));
        self.transform_state.text_height_px = text_height_px;
    }

    /// Modify the current drawing transformation matrix.
    /// `translate`: The new origin (in the current coordinate system.)
    /// `scale`: Scale X and Y coordinates by this amount.
    /// `rotate`: Rotate drawing this many degrees clockwise.
    /// The next call to self.pxsize() will reflect the new scale.
    pub fn transform(
        &mut self,
        translate: Point,
        rotate: AngleDeg,
        scale: f64,
    ) -> Result<(), Error> {
        self.ctx.translate(translate.x(), translate.y())?;
        self.ctx.scale(scale, scale)?;
        self.transform_state.scale *= scale;
        use TransformStyle::*;
        match self.transform_state.style {
            Pixel => self.ctx.rotate(rotate.as_rad()),
            Math => self.ctx.rotate(-rotate.as_rad()),
        }?;
        Ok(())
    }

    /// Translate a pixel point (e.g. mouse event coordinates) to a point in
    /// the current drawing canvas coordinate system.
    pub fn transform_px(&self, px: PixelPoint) -> Result<Point, Error> {
        let t = self.ctx.get_transform()?;
        let ti = t.invert_self();
        let pt = Self::transform_px_with_matrix(px, &ti);
        Ok(pt)
    }

    /// Transform a pixel point to a coordinate space point using the given
    /// transformation matrix. This is a private function because the goal is
    /// to avoid exposing any DOM-specific types in the public API.
    fn transform_px_with_matrix(px: PixelPoint, m: &DomMatrix) -> Point {
        let px_x = px.x() as Length;
        let px_y = px.y() as Length;
        let x = px_x * m.a() + px_y * m.c() + m.e();
        let y = px_x * m.b() + px_y * m.d() + m.f();
        Point::new(x, y)
    }

    /// Return a Rect that covers the entire DrawingCanvas.
    /// This can be used to find the size and position of the drawing area
    /// in the current coordinate system.
    pub fn get_canvas_rect(&self) -> Result<Rect, Error> {
        let t = self.ctx.get_transform()?;
        let ti = t.invert_self();
        let pt1 = Self::transform_px_with_matrix(PixelPoint::new(0, 0), &ti);
        let pt2 = Self::transform_px_with_matrix(
            PixelPoint::new(self.width as i32, self.height as i32),
            &ti,
        );
        Ok(Rect::from_corners(pt1, pt2))
    }

    /// Save current drawing state, run `doit`, then restore drawing state.
    pub fn with_save_restore<F, R>(&mut self, doit: F) -> Result<R, Error>
    where
        F: FnOnce(&mut Self) -> Result<R, Error>,
        R: Debug,
    {
        self.transform_stack.push(self.transform_state);
        self.ctx.save();
        let result = doit(self);
        self.ctx.restore();
        self.transform_state = self.transform_stack.pop().ok_or_else(|| {
            let msg = format!(
                "DrawingCanvas {:?} with_save_restore: \
                stack underflow after doit returned {:?}",
                self.canvas_id(),
                result,
            );
            log::error!("{}", msg);
            Error::from(msg)
        })?;
        result
    }

    pub(crate) fn transform_stack_len(&self) -> usize {
        self.transform_stack.len()
    }

    /// If `.options().size()` is None, resize the HTML canvas to the current
    /// size of the browser client area. Otherwise, assume the HTML canvas has
    /// been resized already according to CSS rules, etc. Then update the
    /// DrawingCanvas size and scale to match the current HTML canvas size.
    pub fn resize(&mut self) -> Result<(), Error> {
        let (old_width, old_height) = (self.width, self.height);
        let (new_width, new_height) = if self.options.size().is_none() {
            let (width, height) = web_util::calc_full_browser_client_size();
            self.canvas.set_width(width);
            self.canvas.set_height(height);
            (width, height)
        } else {
            (old_width, old_height)
        };
        self.width = new_width;
        self.height = new_height;

        log::trace!(
            "DrawingCanvas::resize: {}x{} -> {}x{}",
            old_width,
            old_height,
            new_width,
            new_height
        );

        // Update the top-level coordinate transformation to reflect the new
        // canvas dimensions.
        use TransformStyle::*;
        match self.transform_state.style {
            Pixel => self.set_pixel_transform(),
            Math => self.set_math_transform(),
        }
    }

    /// Return the current transformation style:
    /// TransformStyle::Pixel for x increasing to the right, y increasing down
    /// TransformStyle::Math for x increasing to the right, y increasing up
    pub fn transform_style(&self) -> TransformStyle {
        self.transform_state.style
    }

    /// Return the current text font height in px
    pub fn text_height_px(&self) -> Pixels {
        self.transform_state.text_height_px
    }

    /// Return the width-to-height ratio of characters in the default monospace
    /// font. This ratio was determined by experimentation and seems to hold
    /// true for all scales.
    ///
    /// Right now this returns a constant value (0.6) but in the future if I
    /// support multiple font families it will need to be computed.
    pub fn char_width_height_ratio(&self) -> f64 {
        0.6
    }

    //
    // From here on down are the drawing methods!
    //

    /// Set the width used for drawing lines, paths, and shape borders.
    /// `width` is in current coordinate units.
    pub fn set_line_width(&self, width: Length) {
        self.ctx.set_line_width(width);
    }

    /// Set the width used for drawing lines, paths, and shape borders.
    /// `width` is in pixels.
    pub fn set_line_width_px(&self, width: Pixels) {
        self.ctx.set_line_width(width as f64 * self.pxsize());
    }

    /// Set a new default stroke color (the color of lines and shape borders.)
    /// This color will be used by draw methods that don't take a color
    /// parameter.
    pub fn set_stroke_color(&self, color: Color) {
        self.ctx.set_stroke_style(&color.as_js_color());
    }

    /// Set a new default fill color (the color of the interior of shapes).
    /// This color will be used by fill methods that don't take a color
    /// parameter.
    pub fn set_fill_color(&self, color: Color) {
        self.ctx.set_fill_style(&color.as_js_color());
    }

    /// Fill the entire canvas with the given color.
    /// Does not change nor use the default fill color.
    pub fn fill(&self, color: Color) -> Result<(), Error> {
        let ctx = &self.ctx;

        // Save the original transformation matrix and fill color
        ctx.save();

        // Set to straight pixel transformation
        ctx.set_transform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0)
            .map_err(|err| {
                // In case of error, call ctx.restore() so the graphics
                // save/restore stack doesn't become unbalanced.
                ctx.restore();
                err
            })?;
        // Set the fill color
        ctx.set_fill_style(&color.as_js_color());
        // Fill the canvas
        ctx.fill_rect(0.0, 0.0, self.width as f64, self.height as f64);

        // Restore the original transformation and color
        ctx.restore();

        Ok(())
    }

    /// Draw a hollow rectangle with the current default stroke color
    /// and line width (default: 1 pixel).
    pub fn draw_rect(&self, rect: &Rect) -> Result<(), Error> {
        self.ctx.stroke_rect(rect.x, rect.y, rect.w, rect.h);
        Ok(())
    }

    /// Fill a rectangle with the current fill color.
    pub fn fill_rect(&self, rect: &Rect) -> Result<(), Error> {
        self.ctx.fill_rect(rect.x, rect.y, rect.w, rect.h);
        Ok(())
    }

    /// Draw a hollow rectangle with the given color.
    ///
    /// The line width will be the current width set either by
    /// `.set_line_width()` or by `.set_line_width_px()`.
    pub fn draw_rect_with_color(
        &self,
        rect: &Rect,
        color: Color,
    ) -> Result<(), Error> {
        let ctx = &self.ctx;

        ctx.save();
        ctx.set_stroke_style(&color.as_js_color());
        ctx.stroke_rect(rect.x, rect.y, rect.w, rect.h);
        ctx.restore();

        Ok(())
    }

    /// Draw a hollow rectangle with a dashed border with the given color.
    ///
    /// The line width will be the current width set either by
    /// `.set_line_width()` or by `.set_line_width_px()`.
    pub fn draw_rect_dashed_with_color(
        &self,
        rect: &Rect,
        color: Color,
    ) -> Result<(), Error> {
        let ctx = &self.ctx;

        let a = Array::new();
        a.set(0, JsValue::from_f64(0.01));
        a.set(1, JsValue::from_f64(0.01));
        ctx.save();
        match ctx.set_line_dash(&a) {
            Ok(_) => {
                ctx.set_stroke_style(&color.as_js_color());
                ctx.stroke_rect(rect.x, rect.y, rect.w, rect.h);
            }
            Err(err) => log::error!("set_line_dash: {:?}", err),
        }
        ctx.restore();

        Ok(())
    }

    /// Draw a polygon.
    ///
    /// The polygon will be filled in with the current color set by
    /// `.set_fill_color()`. Use `Color::TRANSPARENT` to not fill the polygon.
    /// The polygon border color will be the current color set by
    /// `.set_stroke_color()`. Use `Color::TRANSPARENT` to not draw a border on
    /// the polygon.
    ///
    /// The border width will be the current line width set by
    /// `.set_line_width()` or `.set_line_width_px()`.
    pub fn polygon(&mut self, vertices: &[Point]) -> Result<(), Error> {
        self.ctx.begin_path();
        for (i, vertex) in vertices.iter().enumerate() {
            if i == 0 {
                self.ctx.move_to(vertex.x(), vertex.y());
            } else {
                self.ctx.line_to(vertex.x(), vertex.y());
            }
        }
        self.ctx.close_path();
        self.ctx.fill();
        self.ctx.stroke();
        Ok(())
    }

    /// Draw a polygon at the given center point, rotation angle, and scale.
    /// Draw the polygon border with the given width in pixels.
    /// Use the current stroke color and fill color.
    pub fn draw_polygon_transformed(
        &mut self,
        vertices: &[Point],
        center: Point,
        angle: AngleDeg,
        scale: f64,
        border_width: Pixels,
    ) -> Result<(), Error> {
        self.with_save_restore(|dc| {
            dc.transform(center, angle, scale)?;
            let line_width = border_width as f64 * dc.pxsize();
            dc.ctx.set_line_width(line_width);
            dc.polygon(vertices)?;
            Ok(())
        })
    }

    /// Draw text at the given pixel location.
    /// The anchor point is the bottom-left corner of the drawn text.
    /// The current transformation is ignored; no translation, rotation, nor
    /// scaling are done on the text.
    /// The text color is the current color set by `.set_fill_color()`.
    /// The text font size is the current size set by `.set_text_height_px()`.
    pub fn draw_text_px(
        &self,
        text: &str,
        loc: PixelPoint,
    ) -> Result<(), Error> {
        let height_px = self.transform_state.text_height_px;
        let font = FontName::new(height_px as u32);

        let ctx = &self.ctx;
        ctx.save();
        let result = (|| {
            // Set to straight pixel transformation
            ctx.set_transform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0)?;
            ctx.set_font(font.as_str());
            for (i, line) in text.split('\n').enumerate() {
                let line_y = loc.y() as f64 + (i as f64 * height_px as f64);
                ctx.fill_text(line, loc.x() as f64, line_y)?;
            }
            Ok(())
        })();
        ctx.restore();

        result
    }

    /// Draw text at the given location and height.
    ///
    /// The text color is the current color set by `.set_fill_color()`.
    /// `loc` is the lower left corner of the text in coordinate units.
    /// `height` is the text height in scaled coordinate units.
    ///
    /// If the text contains newline characters then it will be drawn as
    /// multiple horizontal lines of text, one above the other, spaced
    /// `height` coordinate units apart.
    ///
    /// For the default "20px monosppace" font the character cell
    /// width-to-height ratio is consistently 0.6. In other words, if you draw a
    /// character with height of 0.1 the width is 0.06. Prefer this rule of
    /// thumb over the wildly-inaccurate `.measure_text()` method.
    pub fn draw_text(
        &mut self,
        text: &str,
        loc: Point,
        height: Length,
    ) -> Result<(), Error> {
        self.with_scaling_for_text(loc, height, |height_px| {
            for (i, line) in text.split('\n').enumerate() {
                let line_y = i as f64 * height_px as f64;
                self.ctx.fill_text(line, 0.0, line_y)?;
            }
            Ok(())
        })
    }

    /// Calculate measurements for rendering text using the Javascript
    /// `ctx.measureText()` method.
    ///
    /// `height` is the text height in scaled coordinate units.
    /// This method attempts to handle multi-line text.
    /// The returned width is the width of the largest line.
    /// The returned height is the total of all lines.
    /// Ascent and descent values are only for the first line.
    ///
    /// WARNING: This method is only for experimental purposes. With the default
    /// "20px monospace" font, the calculated bounding box of the text is
    /// inaccurate and the inaccuracy ratio increases with the size of the
    /// canvas.
    ///
    /// I'm keeping this method around only for further experiments. I may
    /// delete it in the future if I can't make it work more accurately.
    pub fn measure_text(
        &self,
        text: &str,
        height: Length,
    ) -> Result<TextMetrics, Error> {
        let mut largest_line_width: f64 = 0.0;
        let mut largest_bounding_box_left: f64 = 0.0;
        let mut largest_bounding_box_right: f64 = 0.0;
        let mut total_actual_height: f64 = 0.0;
        let mut total_font_height: f64 = 0.0;
        let mut actual_bounding_box_ascent: f64 = 0.0; // only for first line
        let mut actual_bounding_box_descent: f64 = 0.0; // only for first line
        let mut font_bounding_box_ascent: f64 = 0.0; // only for first line
        let mut font_bounding_box_descent: f64 = 0.0; // only for first line
        let mut tm = self.with_scaling_for_text(
            Point::new(0.0, 0.0),
            height,
            |_height_px| {
                for (i, line) in text.split('\n').enumerate() {
                    let tm = self.ctx.measure_text(line)?;
                    largest_line_width =
                        f64::max(largest_line_width, tm.width());
                    largest_bounding_box_left = f64::max(
                        largest_bounding_box_left,
                        tm.actual_bounding_box_left(),
                    );
                    largest_bounding_box_right = f64::max(
                        largest_bounding_box_right,
                        tm.actual_bounding_box_right(),
                    );
                    let actual_ascent = tm.actual_bounding_box_ascent();
                    let actual_descent = tm.actual_bounding_box_descent();
                    let font_ascent = tm.font_bounding_box_ascent();
                    let font_descent = tm.font_bounding_box_descent();
                    total_actual_height += actual_ascent + actual_descent;
                    total_font_height += font_ascent + font_descent;
                    if i == 0 {
                        actual_bounding_box_ascent = actual_ascent;
                        actual_bounding_box_descent = actual_descent;
                        font_bounding_box_ascent = font_ascent;
                        font_bounding_box_descent = font_descent;
                    }
                }
                Ok(TextMetrics {
                    // The correct pxsize needs to be filled in after the closure
                    // returns and we restore the regular transformation matrix.
                    pxsize: 1.0,
                    width_px: largest_line_width,
                    actual_height_px: total_actual_height,
                    font_height_px: total_font_height,
                    actual_bounding_box_ascent,
                    actual_bounding_box_descent,
                    font_bounding_box_ascent,
                    font_bounding_box_descent,
                })
            },
        )?;

        tm.pxsize = self.pxsize();
        Ok(tm)
    }

    /// Internal method to temporarily apply transformation and scaling to
    /// render text at whatever size is wanted, irrespective of the font pixel
    /// size. This is common code used by `.draw_text()` and by
    /// `.measure_text()`.
    ///
    /// `loc` is the lower left corner of the text in coordinate units.
    /// `height` is the text height in scaled coordinate units.
    /// `f` is a closure that will be called after proper scaling is applied,
    /// with parameters `|height_px|`:
    /// - `height_px` is the nominal font height in pixels, as an integer. This
    ///    is used to implement line breaks in text.
    fn with_scaling_for_text<F, R>(
        &self,
        loc: Point,
        height: Length,
        f: F,
    ) -> Result<R, Error>
    where
        F: FnOnce(i32) -> Result<R, Error>,
    {
        let height_px = Self::DEFAULT_TEXT_HEIGHT_PX;
        let font = FontName::new(height_px as u32);
        let (scale_x, scale_y) = {
            let scale = height / height_px as Length;
            use TransformStyle::*;
            match self.transform_style() {
                Pixel => (scale, scale),
                Math => (scale, -scale),
            }
        };

        let ctx = &self.ctx;
        ctx.save();
        let result = (|| {
            ctx.set_font(font.as_str());
            ctx.translate(loc.x(), loc.y())?;
            ctx.scale(scale_x, scale_y)?;
            f(height_px)
        })();
        ctx.restore();

        result
    }

    /// Draw centered text at the given location and height.
    /// The text color is the current color set by `.set_fill_color()`.
    /// `loc` is the center of the first line of text in coordinate units.
    /// `height` is the text height in coordinate units.
    ///
    /// To vertically center multiple lines of text use
    /// `.draw_text_centered_multiline()`.
    pub fn draw_text_centered(
        &mut self,
        text: &str,
        loc: Point,
        height: Length,
    ) -> Result<(), Error> {
        let text = text.trim();
        if text.len() == 0 {
            return Ok(());
        }
        self.ctx.save();
        let result = (|| {
            let ctx = &self.ctx;
            ctx.set_text_align("center");
            ctx.set_text_baseline("middle");
            self.draw_text(text, loc, height)
        })();
        self.ctx.restore();
        result
    }

    /// Draw text potentially containing multiple lines, centered at `loc`.
    /// The text color is the current color set by `.set_fill_color()`.
    /// `height` is the text height of each line in coordinate units.
    /// The total height of text is the number of lines times `height`. Each
    /// line of text is horizontally centered at `loc.x()`.
    ///
    /// A line is a sequence of characters ending in the newline character (code
    /// point 0x0A). Trailing newline characters are ignored. Leading and
    /// trailing ASCII whitespace characters on each line are ignored.
    pub fn draw_text_centered_multiline(
        &mut self,
        text: &str,
        loc: Point,
        height: Length,
    ) -> Result<(), Error> {
        let text = text.trim();
        let num_lines = text.lines().count();
        if num_lines == 0 {
            return Ok(());
        }
        let height_span = (num_lines - 1) as f64 * height;
        use TransformStyle::*;
        let direction = match self.transform_style() {
            Pixel => 1.0,
            Math => -1.0,
        };
        let mut y = loc.y() - ((height_span / 2.0) * direction);
        for line in text.lines() {
            self.draw_text_centered(line, Point::new(loc.x(), y), height)?;
            y += height * direction;
        }
        Ok(())
    }

    /// Draw a circle with a center at `center` and radius of `radius`.
    ///
    /// The circle will be filled in with the current color set by
    /// `.set_fill_color()`. Use `Color::TRANSPARENT` to not fill the circle.
    /// The circle border color will be the current color set by
    /// `.set_stroke_color()`. Use `Color::TRANSPARENT` to not draw a border on
    /// the circle.
    ///
    /// The border width will be the current line width set by
    /// `.set_line_width()` or `.set_line_width_px()`.
    pub fn circle(
        &mut self,
        center: Point,
        radius: Length,
    ) -> Result<(), Error> {
        const START_ANGLE: f64 = 0.0;
        const END_ANGLE: f64 = 2.0 * PI;
        self.ctx.begin_path();
        self.ctx
            .arc(center.x(), center.y(), radius, START_ANGLE, END_ANGLE)?;
        self.ctx.fill();
        self.ctx.stroke();
        Ok(())
    }

    /// Draw a dot (a filled-in circle with no border).
    ///
    /// The dot will be filled in with the current color set by
    /// `.set_fill_color()`.
    pub fn dot(&mut self, center: Point, radius: Length) -> Result<(), Error> {
        const START_ANGLE: f64 = 0.0;
        const END_ANGLE: f64 = 2.0 * PI;
        self.ctx.begin_path();
        self.ctx
            .arc(center.x(), center.y(), radius, START_ANGLE, END_ANGLE)?;
        self.ctx.fill();
        Ok(())
    }

    /// Draw a circular arc with a center at `center` and radius of `radius`.
    /// The arc is between `start_angle` and `end_angle`. The angles are
    /// measured in degrees clockwise from the positive y axis.
    ///
    /// The arc is always drawn clockwise.  For example, if `start_angle` is 0.0
    /// and `end_angle` is 90.0, then a one-quarter circle arc will be drawn. If
    /// `start_angle` is 90.0 and `end_angle` is 0.0, then a three-quarter
    /// circle arc will be drawn.
    ///
    /// The arc will be filled in with the current color set by
    /// `.set_fill_color()`. Use `Color::TRANSPARENT` to not fill the arc.  The
    /// arc border color will be the current color set by `.set_stroke_color()`.
    /// Use `Color::TRANSPARENT` to not draw a border on the arc.
    ///
    /// The border width will be the current line width set by
    /// `.set_line_width()` or `.set_line_width_px()`.
    pub fn arc(
        &mut self,
        center: Point,
        radius: Length,
        start_angle: AngleDeg,
        end_angle: AngleDeg,
    ) -> Result<(), Error> {
        let (clockwise, direction) = match self.transform_style() {
            TransformStyle::Pixel => (true, 1.0),
            TransformStyle::Math => (false, -1.0),
        };
        let a1 = (start_angle.as_rad() - (PI / 2.0)) * direction;
        let a2 = (end_angle.as_rad() - (PI / 2.0)) * direction;
        self.ctx.begin_path();
        self.ctx.arc_with_anticlockwise(
            center.x(),
            center.y(),
            radius,
            a1,
            a2,
            !clockwise,
        )?;
        self.ctx.fill();
        self.ctx.stroke();
        Ok(())
    }

    /// Draw an ellipse centered at `center` with horizontal radius `radius_x`
    /// and vertical radius `radius_y`.
    ///
    /// The ellipse will be filled in with the current color set by
    /// `.set_fill_color()`. Use `Color::TRANSPARENT` to not fill the ellipse.  The
    /// ellipse border color will be the current color set by `.set_stroke_color()`.
    /// Use `Color::TRANSPARENT` to not draw a border on the ellipse.
    ///
    /// The border width will be the current line width set by
    /// `.set_line_width()` or `.set_line_width_px()`.
    pub fn ellipse(
        &mut self,
        center: Point,
        radius_x: Length,
        radius_y: Length,
    ) -> Result<(), Error> {
        const ROTATION: f64 = 0.0;
        const START_ANGLE: f64 = 0.0;
        const END_ANGLE: f64 = 2.0 * PI;
        self.ctx.begin_path();
        self.ctx.ellipse(
            center.x(),
            center.y(),
            radius_x,
            radius_y,
            ROTATION,
            START_ANGLE,
            END_ANGLE,
        )?;
        self.ctx.fill();
        self.ctx.stroke();
        Ok(())
    }

    /// Draw an elliptical arc with a center at `center` and radius of `radius`.
    /// The arc is between `start_angle` and `end_angle`. The angles are
    /// measured in degrees clockwise from the positive y axis.
    ///
    /// The arc is always drawn clockwise.  For example, if `start_angle` is 0.0
    /// and `end_angle` is 90.0, then a one-quarter ellipse arc will be drawn. If
    /// `start_angle` is 90.0 and `end_angle` is 0.0, then a three-quarter
    /// ellipse arc will be drawn.
    ///
    /// The arc will be filled in with the current color set by
    /// `.set_fill_color()`. Use `Color::TRANSPARENT` to not fill the arc.  The
    /// arc border color will be the current color set by `.set_stroke_color()`.
    /// Use `Color::TRANSPARENT` to not draw a border on the arc.
    ///
    /// The border width will be the current line width set by
    /// `.set_line_width()` or `.set_line_width_px()`.
    pub fn ellipse_arc(
        &mut self,
        center: Point,
        radius_x: Length,
        radius_y: Length,
        start_angle: AngleDeg,
        end_angle: AngleDeg,
    ) -> Result<(), Error> {
        const ROTATION: f64 = 0.0;
        let (clockwise, direction) = match self.transform_style() {
            TransformStyle::Pixel => (true, 1.0),
            TransformStyle::Math => (false, -1.0),
        };
        let a1 = (start_angle.as_rad() - (PI / 2.0)) * direction;
        let a2 = (end_angle.as_rad() - (PI / 2.0)) * direction;
        self.ctx.begin_path();
        self.ctx.ellipse_with_anticlockwise(
            center.x(),
            center.y(),
            radius_x,
            radius_y,
            ROTATION,
            a1,
            a2,
            !clockwise,
        )?;
        self.ctx.fill();
        self.ctx.stroke();
        Ok(())
    }

    /// Draw a line from `p1` to `p2`.
    ///
    /// The line color will be the current color set by `.set_stroke_color()`.
    /// The line width will be the current width set either by
    /// `.set_line_width()` or by `.set_line_width_px()`.
    pub fn line(&mut self, p1: Point, p2: Point) -> Result<(), Error> {
        self.ctx.begin_path();
        self.ctx.move_to(p1.x(), p1.y());
        self.ctx.line_to(p2.x(), p2.y());
        self.ctx.stroke();
        Ok(())
    }

    /// Draw connected line segments between the points in `points`.
    ///
    /// The line color will be the current color set by `.set_stroke_color()`.
    /// The line width will be the current width set either by
    /// `.set_line_width()` or by `.set_line_width_px()`.
    pub fn lines(&mut self, points: &[Point]) -> Result<(), Error> {
        self.ctx.begin_path();
        for (i, point) in points.iter().enumerate() {
            if i == 0 {
                self.ctx.move_to(point.x(), point.y());
            } else {
                self.ctx.line_to(point.x(), point.y());
            }
        }
        self.ctx.stroke();
        Ok(())
    }

    /// Draw a path starting at point `start_loc` and continuing according to
    /// the `PathCommand` enum variants in `path_commands`. If `path_commands`
    /// is an empty slice then return Ok without doing anything.
    ///
    /// If `close_path` is true, make the path a closed loop from the ending
    /// location back to `start_loc`.
    ///
    /// If `close_path` is true and `fill_color` is not `Color::TRANSPARENT`,
    /// fill the path with that color and set that color as the next current
    /// fill color for this DrawingCanvas.
    ///
    /// If `stroke_color` is not `Color::TRANSPARENT` then draw along the path
    /// with that color and set the next current stroke color for this
    /// DrawingCanvas.  The width of the line drawn is the current width set by
    /// the latest call to either `.set_line_width()` or `.set_line_width_px()`.
    pub fn path(
        &mut self,
        start_loc: Point,
        path_commands: &[PathCommand],
        close_path: bool,
        fill_color: Color,
        stroke_color: Color,
    ) -> Result<(), Error> {
        if path_commands.len() == 0 {
            return Ok(());
        }
        let ctx = &self.ctx;
        ctx.begin_path();
        ctx.move_to(start_loc.x(), start_loc.y());
        for path_command in path_commands.iter() {
            use PathCommand::*;
            match path_command {
                Line(p) => ctx.line_to(p.x(), p.y()),
                QuadBezier(p0, p1) => {
                    ctx.quadratic_curve_to(p0.x(), p0.y(), p1.x(), p1.y())
                }
                CubicBezier(p0, p1, p2) => ctx.bezier_curve_to(
                    p0.x(),
                    p0.y(),
                    p1.x(),
                    p1.y(),
                    p2.x(),
                    p2.y(),
                ),
            }
        }
        if close_path {
            ctx.close_path();
            if fill_color != Color::TRANSPARENT {
                self.set_fill_color(fill_color);
                ctx.fill();
            }
        }
        if stroke_color != Color::TRANSPARENT {
            self.set_stroke_color(stroke_color);
            ctx.stroke();
        }
        Ok(())
    }
}

/// Dynamically generate a font name without creating a String on the heap. I
/// can do this because ctx.set_font(font_name: &str) does not take a String nor
/// a JsValue paremeter but a &str.
struct FontName {
    /// Buffer holding the font name
    buf: [u8; FONT_BUFSIZE],

    /// Length in bytes of the font name (within the buffer)
    n: usize,
}

const FONT_FAMILY: &'static str = "monospace";
const FONT_BUFSIZE: usize = 32; // should be big enough for size + font family

impl FontName {
    /// Create font name with a pixel hight embedded in it.
    /// The font height is in pixels and is clamped to the range 1..=999 .
    fn new(height_px: u32) -> Self {
        let height_px = u32::min(u32::max(height_px, 1), 999);
        let mut buf = [0u8; FONT_BUFSIZE];
        let mut ptr = &mut buf[..];
        write!(&mut ptr, "{}px {}", height_px, FONT_FAMILY)
            .expect("write should always succeed");
        let n = FONT_BUFSIZE - ptr.len();
        Self { buf, n }
    }

    fn as_str(&self) -> &str {
        // SAFETY: these bytes are guaranteed to be ASCII because they are
        // composed of decimal digits which are ASCII plus the contents of
        // FONT_FAMILY which is also ASCII.
        unsafe { std::str::from_utf8_unchecked(&self.buf[..self.n]) }
    }
}

pub trait DrawingCanvasCallback:
    FnMut(&mut DrawingCanvas) -> Result<(), Error>
{
}
impl<T> DrawingCanvasCallback for T where
    T: FnMut(&mut DrawingCanvas) -> Result<(), Error>
{
}

#[derive(Clone, Copy, Debug)]
pub struct CanvasOptions {
    transform_style: TransformStyle,
    text_height_px: Pixels,
    size: Option<(u32, u32)>,
}

impl CanvasOptions {
    /// Create canvas options builder starting from default options.
    /// Use this builder with chained method calls following the standard
    /// builder pattern, then call `.build()` to convert the builder into
    /// CanvasOptions object.
    pub fn builder() -> CanvasOptionsBuilder {
        CanvasOptionsBuilder {
            options: Self::default(),
        }
    }

    /// Create canvas options builder from this options object.
    pub fn builder_from(&self) -> CanvasOptionsBuilder {
        CanvasOptionsBuilder {
            options: self.clone(),
        }
    }

    pub fn transform_style(&self) -> TransformStyle {
        self.transform_style
    }

    pub fn text_height_px(&self) -> Pixels {
        self.text_height_px
    }

    pub fn size(&self) -> Option<(u32, u32)> {
        self.size
    }
}

impl Default for CanvasOptions {
    fn default() -> Self {
        let default_transform_state = TransformState::default();
        Self {
            transform_style: default_transform_state.style,
            text_height_px: default_transform_state.text_height_px,
            size: None,
        }
    }
}

pub struct CanvasOptionsBuilder {
    options: CanvasOptions,
}

impl CanvasOptionsBuilder {
    /// Convert the CanvasOptionsBuilder to CanvasOptions
    pub fn build(self) -> CanvasOptions {
        self.options
    }

    /// Set the canvas top-level transformation style to "pixel" or "math".
    /// Default is `TransformStyle::Pixel`
    pub fn transform_style(mut self, transform_style: TransformStyle) -> Self {
        self.options.transform_style = transform_style;
        self
    }

    /// Set the text font size in px.
    /// The value will be clamped to the range 1..=999
    /// Default is DrawingCanvas::DEFAULT_TEXT_HEIGHT_PX (20).
    pub fn text_height_px(mut self, text_height_px: Pixels) -> Self {
        let text_height_px = Pixels::max(1, Pixels::min(999, text_height_px));
        self.options.text_height_px = text_height_px;
        self
    }

    /// Set the `(width, height)` size in pixes of the canvas that will be
    /// created, or `None` to resize the canvas to fill the entire browser
    /// client area. The default is `None`.
    pub fn size(mut self, size: Option<(u32, u32)>) -> Self {
        self.options.size = size;
        self
    }
}

#[derive(Clone)]
pub struct TextMetrics {
    /// Number of coordinate units per pixel at time metrics were taken.
    /// This assumes the x and y dimensions are scaled evenly.
    pxsize: f64,

    width_px: f64,
    actual_height_px: f64,
    font_height_px: f64,
    actual_bounding_box_ascent: f64, // only for first line
    actual_bounding_box_descent: f64, // only for first line
    font_bounding_box_ascent: f64,   // only for first line
    font_bounding_box_descent: f64,  // only for first line
}

impl TextMetrics {
    /// The width of the text in pixels if it was rendered with the current font
    /// and transformation matrix.
    pub fn width_px(&self) -> f64 {
        self.width_px
    }

    /// The width of the text in coordinate units,
    /// according to scale at the time the text was measured.
    pub fn width(&self) -> f64 {
        self.width_px * self.pxsize
    }

    /// The bounding box (width and height) of the text in pixels
    pub fn bounding_box_px(&self) -> RectSize {
        RectSize::new(self.width_px, self.actual_height_px)
    }

    /// The bounding box (width and height) of the text in coordinate units,
    /// according to scale at the time the text was measured.
    pub fn bounding_box(&self) -> RectSize {
        RectSize::new(self.width(), self.actual_height_px * self.pxsize)
    }

    /// Distance from text baseline to top of text in pixels, first line only.
    pub fn ascent_px(&self) -> f64 {
        self.actual_bounding_box_ascent
    }

    /// Distance from text baseline to bottom of text in pixelst, first line
    /// only.
    pub fn descent_px(&self) -> f64 {
        self.actual_bounding_box_descent
    }

    // The bounding box (width and height) of the text font in pixels.
    // That is, the height is the height of the largest font used.
    //
    // WARNING: This is unreliable! The `fontBoundingBoxAscent` and
    // `fontBoundingBoxDescent` Javascript properties are not available by
    // default on Firefox. Unless you set
    // dom.textMetrics.fontBoundingBox.enabled to true, both of these property
    // values are `NaN` (not-a-number), resulting in this method returning `NaN`
    // for the height of the bounding box.
    #[allow(dead_code)]
    pub(crate) fn font_bounding_box_px(&self) -> RectSize {
        RectSize::new(self.width_px, self.font_height_px)
    }

    /// Distance from text baseline to top of font in pixels, first line only.
    ///
    /// WARNING: This is unreliable! See `.font_bounding_box_px()` for details.
    #[allow(dead_code)]
    pub(crate) fn font_ascent_px(&self) -> f64 {
        self.font_bounding_box_ascent
    }

    /// Distance from text baseline to bottom of text in pixelst, first line
    /// only.
    ///
    /// WARNING: This is unreliable! See `.font_bounding_box_px()` for details.
    #[allow(dead_code)]
    pub(crate) fn font_descent_px(&self) -> f64 {
        self.font_bounding_box_descent
    }

    /// Return true iff the font-related methods will return valid valids:
    /// `.font_bounding_box()`, `.font_ascent_px()`, and `.font_descent_px()`.
    /// See `.font_bounding_box_px()` for details.
    #[allow(dead_code)]
    pub(crate) fn font_metrics_valid(&self) -> bool {
        !(self.font_height_px.is_nan()
            || self.font_bounding_box_ascent.is_nan()
            || self.font_bounding_box_descent.is_nan())
    }
}

impl Debug for TextMetrics {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TextMetrics")
            .field("width", &self.width())
            .field("bounding_box", &self.bounding_box())
            .field("ascent_px", &self.ascent_px())
            .field("descent_px", &self.descent_px())
            .finish()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_util;
    use wasm_bindgen_test::*;

    wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn test_hello() {
        console_log!("Hello from the graphics tests module");
    }

    #[wasm_bindgen_test]
    fn test_circle() -> Result<(), Error> {
        let mut dc =
            test_util::create_dc("test_circle", CanvasOptions::default());
        dc.set_math_transform()?;
        dc.set_line_width(0.01);
        dc.set_stroke_color(Color::GREEN);
        dc.set_fill_color(Color::TRANSPARENT);
        dc.circle(Point::new(0.0, 0.0), 0.5)?;
        Ok(())
    }

    #[wasm_bindgen_test]
    fn draw_arcs() -> Result<(), Error> {
        let mut dc =
            test_util::create_dc("test_arcs", CanvasOptions::default());
        dc.set_math_transform()?;
        dc.set_line_width(0.01);
        // Array of (center, start_angle, end_angle)
        let arc_examples = [
            (
                Point::new(0.0, 0.25),
                AngleDeg::from(45.0),
                AngleDeg::from(135.0),
            ),
            (
                Point::new(0.0, 0.00),
                AngleDeg::from(45.0),
                AngleDeg::from(225.0),
            ),
            (
                Point::new(0.0, -0.25),
                AngleDeg::from(45.0),
                AngleDeg::from(315.0),
            ),
            (
                Point::new(0.25, 0.25),
                AngleDeg::from(135.0),
                AngleDeg::from(45.0),
            ),
            (
                Point::new(0.25, 0.00),
                AngleDeg::from(225.0),
                AngleDeg::from(45.0),
            ),
            (
                Point::new(0.25, -0.25),
                AngleDeg::from(315.0),
                AngleDeg::from(45.0),
            ),
        ];

        // Draw circular arcs
        let radius = 0.1;
        dc.set_fill_color(Color::BRIGHT_YELLOW);
        dc.set_stroke_color(Color::RED);
        dc.with_save_restore(|dc| {
            dc.transform(Point::new(0.125, 0.0), AngleDeg::from(0.0), 1.0)?;
            for (center, start, end) in arc_examples {
                dc.arc(center, radius, start, end)?;
            }
            Ok(())
        })?;
        dc.set_fill_color(Color::TRANSPARENT);
        dc.set_stroke_color(Color::RED);
        dc.with_save_restore(|dc| {
            dc.transform(Point::new(0.625, 0.0), AngleDeg::from(0.0), 1.0)?;
            for (center, start, end) in arc_examples {
                dc.arc(center, radius, start, end)?;
            }
            Ok(())
        })?;

        // Draw regular circles
        dc.circle(Point::new(0.0, 0.5), radius)?;
        dc.set_fill_color(Color::TRANSPARENT);
        dc.circle(Point::new(-0.5, 0.5), radius)?;
        dc.set_fill_color(Color::BRIGHT_YELLOW);
        dc.set_stroke_color(Color::TRANSPARENT);
        dc.circle(Point::new(0.5, 0.5), radius)?;

        // Draw elliptical arcs
        let radius_x = 0.075;
        let radius_y = 0.125;
        dc.set_fill_color(Color::BRIGHT_YELLOW);
        dc.set_stroke_color(Color::RED);
        dc.with_save_restore(|dc| {
            dc.transform(Point::new(-0.875, 0.0), AngleDeg::from(0.0), 1.0)?;
            for (center, start, end) in arc_examples {
                dc.ellipse_arc(center, radius_x, radius_y, start, end)?;
            }
            Ok(())
        })?;
        dc.set_fill_color(Color::TRANSPARENT);
        dc.set_stroke_color(Color::RED);
        dc.with_save_restore(|dc| {
            dc.transform(Point::new(-0.375, 0.0), AngleDeg::from(0.0), 1.0)?;
            for (center, start, end) in arc_examples {
                dc.ellipse_arc(center, radius_x, radius_y, start, end)?;
            }
            Ok(())
        })?;

        // Draw regular ellipses
        dc.ellipse(Point::new(0.0, -0.5), radius_x, radius_y)?;
        dc.set_fill_color(Color::TRANSPARENT);
        dc.ellipse(Point::new(-0.5, -0.5), radius_x, radius_y)?;
        dc.set_fill_color(Color::BRIGHT_YELLOW);
        dc.set_stroke_color(Color::TRANSPARENT);
        dc.ellipse(Point::new(0.5, -0.5), radius_x, radius_y)?;

        Ok(())
    }

    #[wasm_bindgen_test]
    fn test_path() -> Result<(), Error> {
        let mut dc =
            test_util::create_dc("test_path", CanvasOptions::default());
        dc.set_math_transform()?;
        dc.set_line_width(0.01);
        use PathCommand::*;
        dc.path(
            Point::new(0.5, 0.5),
            &[
                Line(Point::new(0.5, -0.5)),
                Line(Point::new(-0.5, -0.5)),
                QuadBezier(Point::new(-0.5, 0.5), Point::new(0.0, 0.5)),
            ],
            true,
            Color::TRANSPARENT,
            Color::GREEN,
        )?;
        Ok(())
    }
}
