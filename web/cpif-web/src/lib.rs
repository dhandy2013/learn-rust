//! cpif-wb: Computer Programming Is Fun - simple web app library
pub mod animation;
pub mod application;
pub mod buttons;
pub mod color;
pub mod dynany;
pub mod error;
pub mod events;
pub mod geometry;
pub mod graphics;
pub mod keydata;
pub mod shapes;
pub mod sound;
#[cfg(test)]
mod test_util;
pub mod web_util;

#[macro_use]
extern crate lazy_static;

pub use crate::error::Error;
