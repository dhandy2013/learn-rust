//! Error values returned by this crate.
//! These error values will automatically convert to and from JsValue
//! to return errors to JavaScript and handle errors from JavaScript.
use std::backtrace::Backtrace;
use std::fmt;
use std::sync::{MutexGuard, TryLockError};
use wasm_bindgen::prelude::JsValue;

/// Error returned from this library.
/// Errors constructed from a string message or from another error are
/// assumed to be catastrophic and therefore a backtrace is included.
#[non_exhaustive]
pub enum Error {
    String(String, Backtrace),
    Str(&'static str, Backtrace),
}

impl Error {
    /// Get just the error message, not the backtrace.
    pub fn msg(&self) -> &str {
        use Error::*;
        match self {
            String(msg, _) => &msg,
            Str(msg, _) => msg,
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            String(msg, bt) => write!(f, "{:?}\n{}", msg, bt),
            Str(msg, bt) => write!(f, "{:?}\n{}", msg, bt),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;
        match self {
            String(msg, bt) => write!(f, "{}\n{}", msg, bt),
            Str(msg, bt) => write!(f, "{}\n{}", msg, bt),
        }
    }
}

impl std::error::Error for Error {}

/// Auto-convert Error to JsValue for functions returning
/// Result<_, JsValue>.
impl From<Error> for JsValue {
    fn from(value: Error) -> JsValue {
        let s = format!("{}", value);
        JsValue::from_str(&s)
    }
}

/// Auto-convert JsValue to Error for functions returning
/// Result<_, Error>.
impl From<JsValue> for Error {
    fn from(value: JsValue) -> Error {
        let s = match value.as_string() {
            Some(s) => s,
            None => format!("{:?}", value),
        };
        Error::String(s, Backtrace::force_capture())
    }
}

/// Convert String to Error
impl From<String> for Error {
    fn from(value: String) -> Error {
        Error::String(value, Backtrace::force_capture())
    }
}

/// Convert static string slice to Error
impl From<&'static str> for Error {
    fn from(value: &'static str) -> Error {
        Error::Str(value, Backtrace::force_capture())
    }
}

/// Conversion from error returned by .try_lock()
impl<T: 'static + ?Sized> From<TryLockError<MutexGuard<'_, T>>> for Error {
    fn from(value: TryLockError<MutexGuard<'_, T>>) -> Error {
        Error::String(
            format!(
                "trying to lock smart pointer to {}: {}",
                std::any::type_name::<T>(),
                value
            ),
            Backtrace::force_capture(),
        )
    }
}
