import init from "./pkg/cpif_web_demo.js";

async function run() {
    console.log("index.js:run(): Initializing WASM module");
    await init();
    console.log("index.js:run(): Finished initializing WASM module");
}

run();
