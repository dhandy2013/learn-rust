//! Demo drawing with touch device
use crate::common_controls::APP_ID;
use cpif_web::{
    application::{App, Model, ResourceManager, View},
    color::Color,
    error::Error,
    events::{Event, EventSignal, EventType},
    geometry::{Point, Rect},
    graphics::{CanvasOptions, DrawingCanvas},
};

type TouchdrawApp = App<TouchdrawModel, TouchdrawView>;

pub fn init(options: CanvasOptions) -> Result<(), Error> {
    // Create the Model, View, and Application
    let model = TouchdrawModel { _data: 0 };
    let view = TouchdrawView::new();
    TouchdrawApp::create(APP_ID, model, view, |app, _model, _view| {
        app.create_main_canvas(options)?;

        app.add_main_canvas_listener(EventType::TouchStart, on_pointer_start)?;
        app.add_main_canvas_listener(EventType::MouseDown, on_pointer_start)?;

        app.add_main_canvas_listener(EventType::TouchMove, on_pointer_move)?;
        app.add_main_canvas_listener(EventType::MouseMove, on_pointer_move)?;

        app.add_main_canvas_listener(EventType::TouchEnd, on_pointer_end)?;
        app.add_main_canvas_listener(EventType::TouchCancel, on_pointer_end)?;
        app.add_main_canvas_listener(EventType::MouseUp, on_pointer_end)?;

        Ok(())
    }) // end TouchdrawApp::new
}

fn on_pointer_start(
    app: &mut TouchdrawApp,
    _model: &mut TouchdrawModel,
    view: &mut TouchdrawView,
    event: Event,
) -> Result<EventSignal, Error> {
    let loc = offset_loc_of_event(app, event)?;
    if view.on_pointer_start(loc) {
        app.cause_redraw()?;
    }
    Ok(EventSignal::prevent_default())
}

fn on_pointer_move(
    app: &mut TouchdrawApp,
    _model: &mut TouchdrawModel,
    view: &mut TouchdrawView,
    event: Event,
) -> Result<EventSignal, Error> {
    let loc = offset_loc_of_event(app, event)?;
    if view.on_pointer_move(loc) {
        app.cause_redraw()?;
    }
    Ok(EventSignal::prevent_default())
}

fn on_pointer_end(
    app: &mut TouchdrawApp,
    _model: &mut TouchdrawModel,
    view: &mut TouchdrawView,
    event: Event,
) -> Result<EventSignal, Error> {
    let loc = offset_loc_of_event(app, event)?;
    if view.on_pointer_end(loc) {
        app.cause_redraw()?;
    }
    Ok(EventSignal::prevent_default())
}

/// Extract the pointer event offset coordinates from the event.
/// Offset coordinates are relative to the event target (drawing canvas).
/// Then convert those coordinates from the pixel coordinate system to the
/// main canvas coordinate system and return those transformed coordinates.
fn offset_loc_of_event(
    rm: &dyn ResourceManager,
    event: Event,
) -> Result<Point, Error> {
    let loc_px = event.data().offset_loc().ok_or_else(|| {
        let msg =
            "touchdraw::offset_loc_of_event: expected touch or mouse data";
        log::error!("{}", msg);
        Error::from(msg)
    })?;
    let mut loc = Point::new(0.0, 0.0);
    rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
        loc = dc.transform_px(loc_px)?;
        Ok(())
    })?;
    Ok(loc)
}

struct TouchdrawModel {
    _data: u32,
}

impl Model for TouchdrawModel {
    fn step(&mut self, _dt: f64) -> Result<bool, Error> {
        Ok(true)
    }
}

struct TouchdrawView {
    is_dragging: bool,
    draw_start: Option<Point>,
    draw_end: Option<Point>,
}

impl TouchdrawView {
    fn new() -> Self {
        Self {
            is_dragging: false,
            draw_start: None,
            draw_end: None,
        }
    }
}

impl View<TouchdrawModel> for TouchdrawView {
    fn draw(
        &mut self,
        rm: &dyn ResourceManager,
        _model: &TouchdrawModel,
    ) -> Result<(), Error> {
        log::debug!("TouchdrawView::draw");
        rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
            if log::log_enabled!(log::Level::Debug) {
                let r = dc.get_canvas_rect()?;
                log::debug!("TouchdrawView::draw: canvas rect: {:?}", r);
            }

            dc.fill(Color::BLACK)?;

            if let (Some(start), Some(end)) = (self.draw_start, self.draw_end) {
                let rect = Rect::from_corners(start, end);
                dc.draw_rect_with_color(&rect, Color::BRIGHT_MAGENTA)?;
            }

            Ok(())
        })
    }
}

impl TouchdrawView {
    /// Handle mouse or touch event beginning drag
    /// Return true iff should redraw
    fn on_pointer_start(&mut self, loc: Point) -> bool {
        log::info!("TouchdrawView::on_pointer_start({:?})", loc);
        if self.is_dragging {
            // Mouse pointer went out of target area, then mouse up event
            // occurred but was not noticed because it was not over the target.
            self.is_dragging = false;
            self.draw_end = Some(loc);
        } else {
            self.is_dragging = true;
            self.draw_start = Some(loc);
            self.draw_end = Some(loc);
        }
        true
    }

    /// Handle mouse or touch event continuing drag
    /// Return true iff should redraw
    fn on_pointer_move(&mut self, loc: Point) -> bool {
        if !self.is_dragging {
            return false;
        }
        log::debug!("TouchdrawView::on_pointer_move({:?})", loc);
        self.draw_end = Some(loc);
        true
    }

    /// Handle mouse or touch event ending drag
    /// Return true iff should redraw
    fn on_pointer_end(&mut self, loc: Point) -> bool {
        if !self.is_dragging {
            return false;
        }
        self.is_dragging = false;
        log::info!("TouchdrawView::on_pointer_end({:?})", loc);
        self.draw_end = Some(loc);
        true
    }
}
