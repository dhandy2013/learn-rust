//! Standard controls (buttons, etc) usable in multiple apps
use cpif_web::{
    application::{App, Model, ResourceManager, View},
    error::Error,
    events::{Event, EventSignal},
    web_util,
};
pub const APP_ID: &'static str = "cpif-web-demo";
pub const PLAY_PAUSE_BUTTON_ID: &'static str = "id-button-play-pause";
pub const RESTART_BUTTON_ID: &'static str = "id-button-restart";
pub const FRAMES_PER_SEC: u8 = 10;

pub fn on_button_play_pause<M, V>(
    app: &mut App<M, V>,
    event: Event,
) -> Result<EventSignal, Error>
where
    M: Model,
    V: View<M>,
{
    if app.is_running() {
        log::info!(
            "play-pause button: stopping app at timestamp {:.1}",
            event.time_stamp()
        );
        // Stop animation
        app.stop();
        // Change button title and image
        set_button_title_play()?;
    } else {
        log::info!(
            "play-pause button: starting app at timestamp {:.1}",
            event.time_stamp()
        );
        // Start animation running
        app.start(1.0 / FRAMES_PER_SEC as f64)?;
        // Change button title and image
        set_button_title_pause()?;
    }
    app.cause_redraw()?;
    Ok(EventSignal::prevent_default())
}

pub fn on_button_restart<M, V>(
    app: &mut App<M, V>,
    event: Event,
) -> Result<EventSignal, Error>
where
    M: Model,
    V: View<M>,
{
    log::info!(
        "restart button: restarting app at timestamp {:.1}",
        event.time_stamp()
    );
    // Stop animation
    app.stop();
    // Reset play-pause button title and image
    set_button_title_play()?;
    app.cause_redraw()?;
    Ok(EventSignal::prevent_default())
}

pub fn set_button_title_play() -> Result<(), Error> {
    web_util::set_element_title(PLAY_PAUSE_BUTTON_ID, "play")?;
    web_util::set_svg_button_image(
        PLAY_PAUSE_BUTTON_ID,
        "images/play-svgrepo-com.svg",
    )?;
    Ok(())
}

pub fn set_button_title_pause() -> Result<(), Error> {
    web_util::set_element_title(PLAY_PAUSE_BUTTON_ID, "pause")?;
    web_util::set_svg_button_image(
        PLAY_PAUSE_BUTTON_ID,
        "images/pause-svgrepo-com.svg",
    )?;
    Ok(())
}
