use crate::{
    common_controls::{self, APP_ID, PLAY_PAUSE_BUTTON_ID, RESTART_BUTTON_ID},
    spaceship,
};
use cpif_web::{
    application::{App, Model, ResourceManager, View},
    color::Color,
    error::Error,
    events::{Event, EventData, EventSignal, EventTarget, EventType},
    geometry::{AngleDeg, PixelPoint, Point, Rect, RectSize},
    graphics::{CanvasOptions, DrawingCanvas, TransformStyle},
    keydata::{KeyData, KeyId},
};

type DemoApp = App<DemoModel, DemoView>;

pub fn init() -> Result<(), Error> {
    // Create the Model, View, and Application
    let model = DemoModel { ticks: 0 };
    let view = DemoView { _data: 0 };
    DemoApp::create(APP_ID, model, view, |app, _model, _view| {
        app.create_main_canvas(
            CanvasOptions::builder()
                .transform_style(TransformStyle::Math)
                .build(),
        )?;

        // Handle play/pause button
        app.add_event_listener(
            EventTarget::from_element_id(PLAY_PAUSE_BUTTON_ID),
            EventType::Click,
            on_button_play_pause,
        )?;

        // Handle restart button
        app.add_event_listener(
            EventTarget::from_element_id(RESTART_BUTTON_ID),
            EventType::Click,
            on_button_restart,
        )?;

        // Log key down events
        app.add_event_listener(
            EventTarget::Window,
            EventType::KeyDown,
            on_key_down,
        )?;

        // Log key up events
        app.add_event_listener(
            EventTarget::Window,
            EventType::KeyUp,
            on_key_up,
        )?;

        // Log mouse click on main drawing canvas
        app.add_main_canvas_listener(EventType::MouseDown, on_mouse_down)?;

        Ok(())
    }) // end DemoApp::new
}

fn on_key_down(
    _app: &mut DemoApp,
    _model: &mut DemoModel,
    view: &mut DemoView,
    event: Event,
) -> Result<EventSignal, Error> {
    let result = match event.data() {
        EventData::Key(key_data) => view.on_key_down(&key_data),
        _ => {
            log::info!("demo app on key down: unexpected: {:?}", event);
            EventSignal::default()
        }
    };
    Ok(result)
}

fn on_key_up(
    _app: &mut DemoApp,
    _model: &mut DemoModel,
    view: &mut DemoView,
    event: Event,
) -> Result<EventSignal, Error> {
    let result = match event.data() {
        EventData::Key(key_data) => view.on_key_up(&key_data),
        _ => {
            log::info!("demo app on key up: unexpected: {:?}", event);
            EventSignal::default()
        }
    };
    Ok(result)
}

fn on_mouse_down(
    app: &mut DemoApp,
    _model: &mut DemoModel,
    _view: &mut DemoView,
    event: Event,
) -> Result<EventSignal, Error> {
    let px_pt = event.data().offset_loc().ok_or_else(|| {
        Error::from("demo::on_mouse_down: expected offset_loc")
    })?;
    app.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
        let pt = dc.transform_px(px_pt)?;
        log::info!("demo::on_mouse_down: {:?} -> {:?}", px_pt, pt);
        Ok(())
    })?;
    Ok(EventSignal::prevent_default())
}

fn on_button_play_pause(
    app: &mut DemoApp,
    _model: &mut DemoModel,
    _view: &mut DemoView,
    event: Event,
) -> Result<EventSignal, Error> {
    common_controls::on_button_play_pause(app, event)
}

fn on_button_restart(
    app: &mut DemoApp,
    model: &mut DemoModel,
    _view: &mut DemoView,
    event: Event,
) -> Result<EventSignal, Error> {
    // Reset animation to the beginning
    model.ticks = 0;
    common_controls::on_button_restart(app, event)
}

struct DemoView {
    _data: u32,
}

impl View<DemoModel> for DemoView {
    fn draw(
        &mut self,
        rm: &dyn ResourceManager,
        model: &DemoModel,
    ) -> Result<(), Error> {
        rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
            initial_draw(dc)?;
            draw(dc, model.ticks)?;
            Ok(())
        })
    }
}

impl DemoView {
    /// Handle key down event. Return signal whether browser
    /// should do default action or not. If it is a key we handle,
    /// do *not* do the default action.
    fn on_key_down(&mut self, key_data: &KeyData) -> EventSignal {
        if key_data.caps_lock() {
            log::warn!("CAPS LOCK KEY IS PRESSED");
        }
        let is_handled_key = Self::decide_whether_to_handle(key_data);
        log::info!(
            "DemoView::on_key_down({:?}) handled: {}",
            key_data,
            is_handled_key
        );
        if is_handled_key {
            EventSignal::prevent_default()
        } else {
            EventSignal::default()
        }
    }

    /// Handle key up event. Return signal whether browser
    /// should do default action or not. If it is a key we handle,
    /// do *not* do the default action.
    fn on_key_up(&mut self, key_data: &KeyData) -> EventSignal {
        let is_handled_key = Self::decide_whether_to_handle(key_data);
        log::info!(
            "DemoView::on_key_up({:?}) handled: {}",
            key_data,
            is_handled_key
        );
        if is_handled_key {
            EventSignal::prevent_default()
        } else {
            EventSignal::default()
        }
    }

    fn decide_whether_to_handle(key_data: &KeyData) -> bool {
        let key_id = key_data.key_id();
        (match key_id {
            KeyId::ArrowUp => true,
            KeyId::ArrowDown => true,
            KeyId::ArrowLeft => true,
            KeyId::ArrowRight => true,
            KeyId::Enter => true,
            _ => false,
        }) || (match key_id.function_key_num() {
            // Allow F11 hotkey to work, trap all other non-modified function
            // keys
            Some(n) => n.get() != 11 && key_data.modifiers().is_empty(),
            None => false,
        })
    }
}

struct DemoModel {
    ticks: u32,
}

impl Model for DemoModel {
    fn step(&mut self, _dt: f64) -> Result<bool, Error> {
        self.ticks += 1;
        Ok(true)
    }
}

pub fn initial_draw(dc: &DrawingCanvas) -> Result<(), Error> {
    dc.fill(Color::BLACK)?;
    dc.draw_rect_dashed_with_color(
        &Rect::new(Point::new(-1.0, -1.0), RectSize::new(2.0, 2.0)),
        Color::WHITE,
    )?;
    Ok(())
}

pub fn draw(dc: &mut DrawingCanvas, ticks: u32) -> Result<(), Error> {
    // Draw status text
    let status_text = format!("{:6}", ticks);
    dc.set_fill_color(Color::WHITE);
    dc.draw_text_px(&status_text, PixelPoint::new(50, 30))?;

    if ticks == 0 {
        dc.draw_text_centered_multiline(
            "Click\n\
            the Start button\n\
            to\n\
            begin animation",
            Point::new(0.0, 0.0),
            0.2,
        )?;
        return Ok(());
    }

    // Phase is a number between 1 and 10 inclusive.
    let phase = if ticks > 10 { 10 } else { ticks };

    // Draw a growing circle in the upper right quadrant
    let radius = phase as f64 * 0.02;
    dc.set_stroke_color(Color::BROWN);
    dc.set_line_width(0.01);
    dc.circle(Point::new(0.25, 0.25), radius)?;

    // Draw series of green squares with apparent perspective
    dc.set_stroke_color(Color::GREEN);

    // Projection scaling parameters for perspective effect
    let d: f64 = 2.0; // distance from "eye" to "screen"
    let x: f64 = 1.0; // controls size of square
    for i in 1..=10 {
        if i > phase {
            break;
        }
        let z: f64 = i as f64;
        let x_scaled = (x * d) / (z + d);
        dc.draw_rect(&Rect::new(
            Point::new(-x_scaled, -x_scaled),
            RectSize::new(2.0 * x_scaled, 2.0 * x_scaled),
        ))?;
    }

    // Draw some starships
    dc.set_fill_color(Color::BLUE);
    dc.set_stroke_color(Color::WHITE);
    let mut x = 0.40;
    let mut y = 0.16;
    let mut angle = AngleDeg::from(90.0);
    let mut scale = 0.033554;
    for i in 1..=10 {
        if i > phase {
            break;
        }
        spaceship::draw_ship(dc, Point::new(x, y), angle, scale)?;
        x -= 0.1;
        y -= 0.04;
        angle -= AngleDeg::from(10.0);
        scale *= 1.25;
    }

    Ok(())
}
