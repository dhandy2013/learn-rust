mod common_controls;
mod demo;
mod lander;
mod playsound;
mod spaceship;
mod textdraw;
mod touchdraw;

use cpif_web::{
    geometry::Pixels,
    graphics::{CanvasOptions, TransformStyle},
    web_util,
};
use log;
use std::str::FromStr;
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    // Initialize panic handling
    console_error_panic_hook::set_once();

    // Initialize logging
    let loglevel_param = match web_util::get_page_param("loglevel") {
        Some(s) => s,
        None => String::new(),
    };
    let loglevel = match log::Level::from_str(loglevel_param.as_str()) {
        Ok(level) => level,
        Err(_) => log::Level::Info,
    };
    wasm_logger::init(wasm_logger::Config::new(loglevel));

    // Pick which app to run
    let app_name = match web_util::get_page_param("app") {
        Some(s) => s.to_ascii_lowercase(),
        None => String::from("demo"),
    };

    // For some apps, pick drawing canvas options
    let mut options = CanvasOptions::builder();
    if let Some(s) = web_util::get_page_param("transform-style") {
        options =
            options.transform_style(match s.to_ascii_lowercase().as_str() {
                "pixel" => TransformStyle::Pixel,
                "math" => TransformStyle::Math,
                _ => {
                    log::warn!(
                        "Unknown transform-style {:?}, defaulting to pixel",
                        s
                    );
                    TransformStyle::Pixel
                }
            });
    }
    if let Some(s) = web_util::get_page_param("text-height-px") {
        if let Ok(text_height_px) = s.parse::<Pixels>() {
            options = options.text_height_px(text_height_px);
        } else {
            log::warn!("Ignoring text-height-px value {:?}, not an integer", s);
        }
    }

    log::info!("start(): initializing app: {:?}", app_name);
    match app_name.as_ref() {
        "demo" => demo::init(),
        "lander" => lander::init(),
        "playsound" => playsound::init(),
        "textdraw" => {
            web_util::add_css_class("control-panel", "hidden")?;
            textdraw::init(options.build())
        }
        "touchdraw" => {
            web_util::add_css_class("control-panel", "hidden")?;
            touchdraw::init(options.build())
        }
        _ => {
            return Err(JsValue::from_str(&format!(
                "Unknown app name: {:?}",
                app_name
            )))
        }
    }?;
    log::info!("start(): app initialized");
    Ok(())
}
