//! Sound demo
use crate::common_controls::{
    self, APP_ID, PLAY_PAUSE_BUTTON_ID, RESTART_BUTTON_ID,
};
use cpif_web::{
    application::{App, Model, ResourceManager, View},
    color::Color,
    error::Error,
    events::{Event, EventData, EventSignal, EventTarget, EventType},
    geometry::{Contains, Point, Rect, RectSize},
    graphics::{CanvasOptions, DrawingCanvas, TransformStyle},
    sound::{self, Frequency, SoundController, SoundID, Tone, Waveform},
};

type PlaysoundApp = App<PlaysoundModel, PlaysoundView>;

pub fn init() -> Result<(), Error> {
    // Create the Model, View, and Application
    let model = PlaysoundModel;
    let view = PlaysoundView::new();
    PlaysoundApp::create(APP_ID, model, view, |app, _model, _view| {
        app.create_main_canvas(
            CanvasOptions::builder()
                .transform_style(TransformStyle::Math)
                .build(),
        )?;

        // Handle play/pause button
        app.add_event_listener(
            EventTarget::from_element_id(PLAY_PAUSE_BUTTON_ID),
            EventType::Click,
            PlaysoundView::on_button_play_pause,
        )?;

        // Handle restart button
        app.add_event_listener(
            EventTarget::from_element_id(RESTART_BUTTON_ID),
            EventType::Click,
            PlaysoundView::on_button_restart,
        )?;

        // Handle function keys
        app.add_event_listener(
            EventTarget::Window,
            EventType::KeyDown,
            PlaysoundView::on_key_down,
        )?;

        // Handle clicking on the canvas to play sounds
        log::debug!("Registering on_pointer_start");
        app.on_main_canvas_mouse_start_call(PlaysoundView::on_pointer_start)?;

        // Handle sound end events
        app.set_sound_end_callback(Some(PlaysoundView::on_sound_end));

        Ok(())
    })
}

struct PlaysoundModel;

impl Model for PlaysoundModel {
    fn step(&mut self, _dt: f64) -> Result<bool, Error> {
        // Model never changes, so always return false to
        // signal no redraw needed.
        Ok(false)
    }
}

struct PlaysoundView {
    /// Index of currently highlighted song
    effect_index: usize,

    /// ID of currently playing sound when going through
    /// all sounds in sequence.
    sequential_sound_id: Option<SoundID>,

    /// ID of sounds played by randomly clicking effect titles
    ad_hoc_sound_ids: [Option<SoundID>; ALL_SOUND_EFFECTS.len()],
}

impl PlaysoundView {
    fn new() -> Self {
        Self {
            effect_index: 0,
            sequential_sound_id: None,
            ad_hoc_sound_ids: [None; ALL_SOUND_EFFECTS.len()],
        }
    }

    fn effect(&self) -> SoundEffect {
        ALL_SOUND_EFFECTS[self.effect_index]
    }

    fn go_to_next_effect(&mut self) {
        self.effect_index += 1;
        if self.effect_index >= ALL_SOUND_EFFECTS.len() {
            self.go_to_first_effect();
        }
    }

    fn go_to_first_effect(&mut self) {
        self.effect_index = 0;
    }

    fn on_button_play_pause(
        app: &mut PlaysoundApp,
        _model: &mut PlaysoundModel,
        view: &mut PlaysoundView,
        event: Event,
    ) -> Result<EventSignal, Error> {
        if let Some(sound_id) = view.sequential_sound_id.take() {
            log::info!("playsound: stopping sound: {}", view.effect().as_str());
            app.sound_controller()?.stop_sound(sound_id)?;
            view.go_to_next_effect();
        } else {
            log::info!("playsound: starting sound: {}", view.effect().as_str());
            let sc = app.sound_controller()?;
            view.sequential_sound_id = Some(view.effect().play(sc)?);
        }
        common_controls::on_button_play_pause(app, event)
    }

    fn on_button_restart(
        app: &mut PlaysoundApp,
        _model: &mut PlaysoundModel,
        view: &mut PlaysoundView,
        event: Event,
    ) -> Result<EventSignal, Error> {
        app.sound_controller()?.stop()?;
        view.go_to_first_effect();
        view.sequential_sound_id = None;
        common_controls::on_button_restart(app, event)
    }

    /// Respond to function keys
    fn on_key_down(
        app: &mut PlaysoundApp,
        _model: &mut PlaysoundModel,
        view: &mut Self,
        event: Event,
    ) -> Result<EventSignal, Error> {
        let key_data = match event.data() {
            EventData::Key(key_data) => key_data,
            _ => {
                log::warn!("playsound: on_key_down: unexpected: {:?}", event);
                return Ok(EventSignal::default());
            }
        };
        let func_key_num = match key_data.key_id().function_key_num() {
            Some(n) => n,
            None => return Ok(EventSignal::default()), // not a function key
        };
        if !key_data.modifiers().is_empty() {
            // Not handling Ctrl+function key
            return Ok(EventSignal::default());
        }
        // Convert function key number to effect index
        let effect_index = func_key_num.get() as usize - 1;
        if !view.select_sound_effect_by_index(app, effect_index)? {
            return Ok(EventSignal::default());
        }
        Ok(EventSignal::prevent_default())
    }

    /// Play or stop playing a sound effect by index.
    /// Return false if the index was out-of-bounds.
    /// If the sound effect index is valid:
    /// - Stop that sound effect if it is playing
    /// - Start that sound effect and set it as the current one if it is not
    ///   playing
    /// - Cause a redraw
    /// - Return true
    /// Return an error if there was any problem playing or stopping the sound.
    fn select_sound_effect_by_index(
        &mut self,
        app: &mut PlaysoundApp,
        effect_index: usize,
    ) -> Result<bool, Error> {
        if effect_index >= ALL_SOUND_EFFECTS.len() {
            return Ok(false);
        }
        // The function key matches a sound effect. Is it playing already?
        let sc = app.sound_controller()?;
        if let Some(sound_id) = self.ad_hoc_sound_ids[effect_index].clone() {
            // It is currently playing. Stop it.
            sc.stop_sound(sound_id)?;
        } else {
            // It is not yet playing. Play it.
            self.ad_hoc_sound_ids[effect_index] =
                Some(ALL_SOUND_EFFECTS[effect_index].play(sc)?);
        }
        app.cause_redraw()?;
        Ok(true)
    }

    fn on_sound_end(
        &mut self,
        _model: &mut PlaysoundModel,
        app: &mut PlaysoundApp,
        sound_id: SoundID,
    ) -> Result<(), Error> {
        log::debug!("playsound: sound ended: {:?}", sound_id);
        let sound_identified = self
            .handle_sequencial_sound_end(app, sound_id)?
            || self.handle_ad_hoc_sound_end(sound_id)?;
        if sound_identified {
            app.cause_redraw()?;
        } else {
            log::warn!("Unexpected sound ID: {:?}", sound_id);
        }
        Ok(())
    }

    /// Return true iff the ID was a sequential sound
    fn handle_sequencial_sound_end(
        &mut self,
        app: &mut PlaysoundApp,
        sound_id: SoundID,
    ) -> Result<bool, Error> {
        match self.sequential_sound_id {
            Some(cur_sound_id) if cur_sound_id == sound_id => {
                self.go_to_next_effect();
                if self.effect_index == 0 {
                    // Back at the beginning, stop
                    app.stop();
                    self.sequential_sound_id = None;
                    common_controls::set_button_title_play()?;
                } else {
                    // play next sound in order
                    log::info!(
                        "playsound: starting next sound: {}",
                        self.effect().as_str()
                    );
                    let sc = app.sound_controller()?;
                    self.sequential_sound_id = Some(self.effect().play(sc)?);
                }
                Ok(true)
            }
            _ => Ok(false),
        }
    }

    /// Return true iff this sound ID matched one of the ad hoc sounds
    fn handle_ad_hoc_sound_end(
        &mut self,
        sound_id: SoundID,
    ) -> Result<bool, Error> {
        let sound_index = match self.find_ad_hoc_sound(sound_id) {
            Some(i) => i,
            None => return Ok(false),
        };
        self.ad_hoc_sound_ids[sound_index] = None;
        Ok(true)
    }

    /// Find the index in add_hoc_sound_ids of this sound_id
    fn find_ad_hoc_sound(&self, sound_id: SoundID) -> Option<usize> {
        if let Some((i, _entry)) = self
            .ad_hoc_sound_ids
            .iter()
            .enumerate()
            .find(|(_i, entry)| match entry {
                Some(cur_sound_id) => *cur_sound_id == sound_id,
                None => false,
            })
        {
            Some(i)
        } else {
            None
        }
    }

    /// Draw all sound effect titles centered in the canvas.
    fn draw_menu(&self, dc: &mut DrawingCanvas) -> Result<(), Error> {
        for (effect_index, effect, rect) in EffectIterator::new(dc) {
            // position of current menu item
            let pos = rect.bottom_left(dc.transform_style());

            // Draw the title of the effect
            if self.effect_index == effect_index {
                // Draw title in bold because it is the current one in the
                // sequence
                dc.set_fill_color(Color::BRIGHT_WHITE);
            } else {
                dc.set_fill_color(Color::WHITE);
            }
            dc.draw_text(effect.as_str(), pos, rect.h)?;

            // Draw rectangle around effect title if it is one
            // of the ad-hoc sounds currently playing.
            if self.ad_hoc_sound_ids[effect_index].is_some() {
                dc.set_stroke_color(Color::GREEN);
                dc.draw_rect(&rect)?;
            }
        }
        Ok(())
    }

    fn on_pointer_start(
        &mut self,
        _model: &mut PlaysoundModel,
        app: &mut PlaysoundApp,
        _event: Event,
        dc: &mut DrawingCanvas,
        loc: Point,
    ) -> Result<(), Error> {
        log::debug!("PlaysoundView::on_pointer_start: loc: {loc:?}");
        for (effect_index, _effect, rect) in EffectIterator::new(dc) {
            if rect.contains(loc) {
                if self.select_sound_effect_by_index(app, effect_index)? {
                    break;
                }
            }
        }
        Ok(())
    }
}

impl View<PlaysoundModel> for PlaysoundView {
    fn draw(
        &mut self,
        rm: &dyn ResourceManager,
        _model: &PlaysoundModel,
    ) -> Result<(), Error> {
        rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
            dc.fill(Color::BLACK)?;
            self.draw_menu(dc)?;
            Ok(())
        })
    }
}

const ALL_SOUND_EFFECTS: [SoundEffect; 8] = [
    SoundEffect::Beep,
    SoundEffect::BeepTones,
    SoundEffect::TouchTones,
    SoundEffect::BellTones,
    SoundEffect::Noise100,
    SoundEffect::Noise1000,
    SoundEffect::WhooshUp,
    SoundEffect::WhooshDown,
];

#[derive(Clone, Copy, Debug)]
enum SoundEffect {
    Beep,
    BeepTones,
    TouchTones,
    BellTones,
    Noise100,
    Noise1000,
    WhooshUp,
    WhooshDown,
}

impl SoundEffect {
    /// Return description of the sound effect
    fn as_str(&self) -> &'static str {
        use SoundEffect::*;
        match self {
            Beep => "beep",
            BeepTones => "multiple tones",
            TouchTones => "touch tones",
            BellTones => "bell tones",
            Noise100 => "noise - 100Hz center",
            Noise1000 => "noise - 1000Hz center",
            WhooshUp => "whoosh up",
            WhooshDown => "whoosh down",
        }
    }

    /// Play the sound effect
    fn play(&self, sc: &mut SoundController) -> Result<SoundID, Error> {
        let volume: f32 = 0.25;
        use SoundEffect::*;
        let sound_id = match self {
            Beep => sc.beep(
                Waveform::Square,
                Tone::new(Frequency::from(440.0), 0.250, volume),
            )?,
            BeepTones => sc.beep_tones(
                Waveform::Square,
                &[
                    Tone::new(Frequency::from(440.0), 0.125, volume),
                    Tone::new(Frequency::from(880.0), 0.250, volume),
                    Tone::new(Frequency::from(220.0), 0.500, volume),
                ],
            )?,
            TouchTones => {
                let (lo_tones, hi_tones) =
                    sound::dtmf(0.100, 0.025, volume, "648-0458");
                sc.beep_duet(Waveform::Sine, &lo_tones, &hi_tones)?
            }
            BellTones => {
                let time_constant = 0.5;
                let time_spacing = time_constant * 2.0;
                let volume = 1.0;
                let tones = &[
                    Tone::new(Frequency::from(440.0), time_spacing, volume),
                    Tone::new(Frequency::from(880.0), time_spacing, volume),
                    Tone::new(Frequency::from(220.0), time_spacing, volume),
                ];
                sc.bell_tones(time_constant, tones)?
            }
            Noise100 => {
                let duration_sec = 0.25;
                // low frequencies need to be louder
                let volume = f32::max(volume * 1.5, 1.0);
                sc.noise(Tone::new(
                    Frequency::from(100.0),
                    duration_sec,
                    volume,
                ))?
            }
            Noise1000 => {
                let duration_sec = 0.25;
                sc.noise(Tone::new(
                    Frequency::from(1000.0),
                    duration_sec,
                    volume,
                ))?
            }
            WhooshUp => {
                let duration_sec = 1.0;
                if false {
                    // whoosh up the hard way
                    let num_steps: usize = 10;
                    let delta_t = duration_sec / num_steps as f64;
                    let mut f = 100.0_f32;
                    let f_end = 5000.0_f32;
                    let f_a = (f_end / f).powf(1.0 / num_steps as f32);
                    let mut tones: Vec<Tone> =
                        Vec::<_>::with_capacity(num_steps);
                    for _ in 0..num_steps {
                        tones.push(Tone::new(
                            Frequency::from(f),
                            delta_t,
                            volume,
                        ));
                        f *= f_a;
                    }
                    sc.noise_tones(&tones)?
                } else {
                    sc.whoosh(&[
                        Tone::new(Frequency::from(100.0), duration_sec, volume),
                        Tone::new(Frequency::from(5000.0), 0.0, volume),
                    ])?
                }
            }
            WhooshDown => {
                let duration_sec = 1.0;
                if false {
                    // whoosh down the hard way
                    let num_steps: usize = 10;
                    let delta_t = duration_sec / num_steps as f64;
                    let mut f = 5000.0_f32;
                    let f_end = 100.0_f32;
                    let f_a = (f_end / f).powf(1.0 / num_steps as f32);
                    let mut tones: Vec<Tone> =
                        Vec::<_>::with_capacity(num_steps);
                    for _ in 0..num_steps {
                        tones.push(Tone::new(
                            Frequency::from(f),
                            delta_t,
                            volume,
                        ));
                        f *= f_a;
                    }
                    sc.noise_tones(&tones)?
                } else {
                    sc.whoosh(&[
                        Tone::new(
                            Frequency::from(5000.0),
                            duration_sec,
                            volume,
                        ),
                        Tone::new(Frequency::from(100.0), 0.0, volume),
                    ])?
                }
            }
        };
        Ok(sound_id)
    }
}

struct EffectIterator {
    index: Option<usize>,
    menu_width: f64,
    line_height: f64,
    space_between_lines: f64,
    item_x: f64,
    item_y: f64,
}

impl EffectIterator {
    /// Return an iterator that iterates over all of the sound effects and
    /// yields (effect_index, effect, rect) where:
    /// - effect_index is the index of the sound effect in ALL_SOUND_EFFECTS
    /// - effect is the current SoundEffect
    /// - rect is the Rect indicating where the menu item should be drawn
    ///
    /// This is useful both for drawing the sound effect menu and for detecting
    /// whether a mouse click / screen touch is on a menu item.
    fn new(dc: &DrawingCanvas) -> Self {
        let line_height = 0.1;
        let longest_title_len: usize = ALL_SOUND_EFFECTS
            .iter()
            .map(|effect| effect.as_str().len())
            .fold(0, |len1, len2| usize::max(len1, len2));
        let menu_width = longest_title_len as f64
            * line_height
            * dc.char_width_height_ratio();

        let space_between_lines = 0.05;
        let n = ALL_SOUND_EFFECTS.len() as f64;
        let menu_height = (n * line_height) + (n - 1.0) * space_between_lines;

        // Draw menu items from the bottom towards the top
        let item_x = -menu_width * 0.5;
        let item_y = -menu_height * 0.5;

        Self {
            index: None,
            menu_width,
            line_height,
            space_between_lines,
            item_x,
            item_y,
        }
    }
}

impl Iterator for EffectIterator {
    type Item = (usize, SoundEffect, Rect);

    fn next(&mut self) -> Option<Self::Item> {
        let effect_index = match self.index {
            None => {
                // start iterator
                ALL_SOUND_EFFECTS.len() - 1
            }
            Some(effect_index) => {
                if effect_index == 0 {
                    // iterator has been exhausted
                    return None;
                }
                effect_index - 1
            }
        };
        self.index = Some(effect_index);

        // position of current menu item
        let pos = Point::new(self.item_x, self.item_y);
        let rect =
            Rect::new(pos, RectSize::new(self.menu_width, self.line_height));
        self.item_y += self.line_height + self.space_between_lines;

        Some((effect_index, ALL_SOUND_EFFECTS[effect_index], rect))
    }
}
