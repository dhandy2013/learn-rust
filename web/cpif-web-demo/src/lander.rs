//! Demo: Classic lander game
use crate::{
    common_controls::{
        self, APP_ID, FRAMES_PER_SEC, PLAY_PAUSE_BUTTON_ID, RESTART_BUTTON_ID,
    },
    spaceship,
};
use cpif_web::{
    application::{App, Model, ResourceManager, View},
    buttons::{Button, ButtonBehavior, ButtonState},
    color::Color,
    error::Error,
    events::{Event, EventData, EventSignal, EventTarget, EventType},
    geometry::{AngleDeg, Length, PixelPoint, Pixels, Point, Rect, RectSize},
    graphics::{CanvasOptions, DrawingCanvas, TransformStyle},
    keydata::KeyId,
};
use rand::Rng;
use std::fmt::Write;

type LanderApp = App<LanderModel, LanderView>;

pub fn init() -> Result<(), Error> {
    // Create the Model, View, and Application
    let model = LanderModel::new();
    let view = LanderView::new();
    LanderApp::create(APP_ID, model, view, |app, model, view| {
        app.create_main_canvas(
            CanvasOptions::builder()
                .transform_style(TransformStyle::Math)
                .build(),
        )?;

        // Register event handlers

        app.add_main_canvas_listener(EventType::TouchStart, on_pointer_start)?;
        app.add_main_canvas_listener(EventType::MouseDown, on_pointer_start)?;

        app.add_main_canvas_listener(EventType::TouchMove, on_pointer_move)?;
        app.add_main_canvas_listener(EventType::MouseMove, on_pointer_move)?;

        app.add_main_canvas_listener(EventType::TouchEnd, on_pointer_end)?;
        app.add_main_canvas_listener(EventType::TouchCancel, on_pointer_end)?;
        app.add_main_canvas_listener(EventType::MouseUp, on_pointer_end)?;

        app.add_event_listener(
            EventTarget::Window,
            EventType::KeyDown,
            on_key_down,
        )?;
        app.add_event_listener(
            EventTarget::Window,
            EventType::KeyUp,
            on_key_up,
        )?;

        // Handle play/pause button
        app.add_event_listener(
            EventTarget::from_element_id(PLAY_PAUSE_BUTTON_ID),
            EventType::Click,
            on_button_play_pause,
        )?;

        // Handle restart button
        app.add_event_listener(
            EventTarget::from_element_id(RESTART_BUTTON_ID),
            EventType::Click,
            on_button_restart,
        )?;

        // Handle timer ticks
        app.set_interval_callback(LanderView::on_interval);

        app.set_resize_callback(Some(on_resize));
        on_resize(app, model, view)?; // do first layout

        Ok(())
    })?; // end LanderApp::new

    Ok(())
}

fn on_pointer_start(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    event: Event,
) -> Result<EventSignal, Error> {
    let mut something_changed = false;
    if view.pointer_active {
        // Another button is still pressed. Un-press it.
        view.pointer_active = false;
        view.deactivate_all_buttons(model)?;
        something_changed = true;
    }

    // Find out if we have clicked one of our buttons.
    let pt = get_event_pt(app, &event)?;
    for button in view.buttons.iter_mut() {
        if button.on_pointer_start(pt, model)? {
            view.pointer_active = true;
            something_changed = true;
            break;
        }
    }
    if something_changed {
        app.cause_redraw()?;
        Ok(EventSignal::prevent_default())
    } else {
        Ok(EventSignal::default())
    }
}

fn on_pointer_move(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    event: Event,
) -> Result<EventSignal, Error> {
    match event.data() {
        EventData::Mouse(mouse_data) => {
            if mouse_data.buttons() == 0 {
                // This is a mouse move event but the mouse button is no longer
                // pressed.  This means the mouse up event got missed, probably
                // because the mouse cursor was dragged outside of the window
                // before the button was released.
                // Treat this as a pointer end event.
                return on_pointer_end(app, model, view, event);
            }
        }
        _ => {}
    }
    // The mouse was held down while moving it, or this is a drag on a touch
    // screen. Treat this as a pointer start event.
    on_pointer_start(app, model, view, event)
}

fn on_pointer_end(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    _event: Event,
) -> Result<EventSignal, Error> {
    if view.pointer_active {
        view.pointer_active = false;
        view.deactivate_all_buttons(model)?;
        app.cause_redraw()?;
        Ok(EventSignal::prevent_default())
    } else {
        Ok(EventSignal::default())
    }
}

fn get_event_pt(
    rm: &mut dyn ResourceManager,
    event: &Event,
) -> Result<Point, Error> {
    let px = event.data().offset_loc().ok_or_else(|| {
        Error::from("lander::get_event_pt: expected event offset_loc")
    })?;
    let mut pt = Point::new(0.0, 0.0);
    rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
        pt = dc.transform_px(px)?;
        Ok(())
    })?;
    Ok(pt)
}

fn on_key_down(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    event: Event,
) -> Result<EventSignal, Error> {
    let key_id = get_event_key(&event)?;
    for button in view.buttons.iter_mut() {
        if button.on_key_down(&key_id, model)? {
            app.cause_redraw()?;
            return Ok(EventSignal::prevent_default());
        }
    }
    Ok(EventSignal::default())
}

fn on_key_up(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    event: Event,
) -> Result<EventSignal, Error> {
    let key_id = get_event_key(&event)?;
    for button in view.buttons.iter_mut() {
        if button.on_key_up(&key_id, model)? {
            app.cause_redraw()?;
            return Ok(EventSignal::prevent_default());
        }
    }
    Ok(EventSignal::default())
}

fn get_event_key(event: &Event) -> Result<KeyId, Error> {
    match event.data() {
        EventData::Key(key_data) => Ok(key_data.key_id().clone()),
        _ => {
            let msg = "lander::get_event_key: expected key data";
            log::error!("{}", msg);
            Err(Error::from(msg))
        }
    }
}

fn on_button_play_pause(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    event: Event,
) -> Result<EventSignal, Error> {
    if let LanderMode::Start = model.mode {
        model.mode = LanderMode::InFlight;
    } else if model.mode.is_after_landing_pause_over(model) {
        // If spaceship is sitting on the ground, play/pause button
        // should have the same effect as the restart button.
        return on_button_restart(app, model, view, event);
    }
    common_controls::on_button_play_pause(app, event)
}

fn on_button_restart(
    app: &mut LanderApp,
    model: &mut LanderModel,
    view: &mut LanderView,
    event: Event,
) -> Result<EventSignal, Error> {
    view.deactivate_all_buttons(model)?;
    model.reset();
    common_controls::on_button_restart(app, event)
}

fn on_resize(
    app: &mut LanderApp,
    _model: &mut LanderModel,
    view: &mut LanderView,
) -> Result<(), Error> {
    app.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
        log::trace!(
            "lander::on_resize: new canvas size: {}x{}",
            dc.width(),
            dc.height()
        );
        // Place the "left" button in relation to the bottom right of the
        // canvas. Place all other buttons in relation to that one.
        let corner = dc.get_canvas_rect()?.bottom_right(dc.transform_style());
        let pad: Length = 0.010;
        let p0 = Point::new(
            corner.x() - pad - 0.200 - pad - 0.300 - pad - 0.200,
            corner.y() + pad,
        );
        for button in view.buttons.iter_mut() {
            match button.id() {
                "left" => {
                    button.move_to(p0);
                }
                "upleft" => {
                    button.move_to(Point::new(p0.x(), p0.y() + 0.300 + pad));
                }
                "up" => {
                    button.move_to(Point::new(
                        p0.x() + 0.200 + pad,
                        p0.y() + 0.300 + pad,
                    ));
                }
                "upright" => {
                    button.move_to(Point::new(
                        p0.x() + 0.200 + pad + 0.300 + pad,
                        p0.y() + 0.300 + pad,
                    ));
                }
                "right" => {
                    button.move_to(Point::new(
                        p0.x() + 0.200 + pad + 0.300 + pad,
                        p0.y(),
                    ));
                }
                _ => {
                    log::error!("Unexpected button ID {:?}", button.id());
                }
            }
        }
        Ok(())
    })
}

struct LanderModel {
    /// Number of animation cycles
    ticks: u32,

    /// Current simulation time
    t: f64,

    /// Middle bottom of lander, in universe coordinate system
    pos: Point,

    /// Velocity in horizontal dimension, meters/sec
    vx: f64,

    /// Velocity in veritical dimension, meters/sec
    vy: f64,

    /// Control state
    mode: LanderMode,
    thruster_main_on: bool,
    thruster_left_on: bool,
    thruster_right_on: bool,

    /// Background stars
    stars: Vec<Star>,
}

struct Star {
    pos: Point,
    size: Pixels,
}

impl LanderModel {
    const NUM_STARS: usize = 25;
    const G: f64 = 1.625; // acceleration of gravity, meters/sec^2
    const SAFE_LANDING_SPEED_MAX: f64 = 3.12; // meters/sec
    const MAIN_ROCKET_ACCEL: f64 = Self::G * 2.0;
    const SIDE_ROCKET_ACCEL: f64 = Self::G * 0.4;
    const TOUCHDOWN_PAUSE_TICKS: u32 = FRAMES_PER_SEC as u32;

    fn new() -> Self {
        Self {
            ticks: 0,
            t: 0.0,
            pos: Self::pick_lander_pos(),
            vx: 0.0,
            vy: 0.0,
            mode: LanderMode::Start,
            thruster_main_on: false,
            thruster_left_on: false,
            thruster_right_on: false,
            stars: Self::gen_stars(),
        }
    }

    fn pick_lander_pos() -> Point {
        let mut rng = rand::thread_rng();
        Point::new(rng.gen_range(-8.0..8.0), rng.gen_range(10.0..16.0))
    }

    fn gen_stars() -> Vec<Star> {
        let mut rng = rand::thread_rng();
        let mut stars = Vec::<Star>::new();
        for _ in 0..Self::NUM_STARS {
            let x = rng.gen_range(-20.0..20.0);
            let y = rng.gen_range(0.0..18.0);
            let size: Pixels = rng.gen_range(1..=3);
            stars.push(Star {
                pos: Point::new(x, y),
                size,
            });
        }
        stars
    }

    fn reset(&mut self) {
        self.ticks = 0;
        self.t = 0.0;
        self.pos = Self::pick_lander_pos();
        self.vx = 0.0;
        self.vy = 0.0;
        self.mode = LanderMode::Start;
        self.thruster_main_on = false;
        self.thruster_left_on = false;
        self.thruster_right_on = false;
    }

    fn set_thruster_main(&mut self, on: bool) {
        self.thruster_main_on = on;
    }

    fn set_thruster_left(&mut self, on: bool) {
        self.thruster_left_on = on;
    }

    fn set_thruster_right(&mut self, on: bool) {
        self.thruster_right_on = on;
    }
}

impl Model for LanderModel {
    fn step(&mut self, dt: f64) -> Result<bool, Error> {
        self.ticks += 1;
        match self.mode {
            LanderMode::InFlight => (),
            LanderMode::AfterTouchdown {
                touchdown_ticks, ..
            } => {
                return if self.ticks
                    <= touchdown_ticks + Self::TOUCHDOWN_PAUSE_TICKS
                {
                    // Keep animation going for TOUCHDOWN_PAUSE_TICKS
                    Ok(true)
                } else {
                    Ok(false)
                };
            }
            _ => return Ok(false),
        }

        // Implement Newton's laws of motion
        //
        // Calculate current acceleration due to rocket thrust
        let main_rocket_accel = if self.thruster_main_on {
            Self::MAIN_ROCKET_ACCEL
        } else {
            0.0
        };
        let mut side_rocket_accel: f64 = 0.0;
        if self.thruster_left_on {
            side_rocket_accel += Self::SIDE_ROCKET_ACCEL;
        }
        if self.thruster_right_on {
            side_rocket_accel -= Self::SIDE_ROCKET_ACCEL;
        }
        let dt2 = dt * dt; // delta time squared
        let ax = side_rocket_accel; // accel in x dim
        let ay = main_rocket_accel - Self::G; // accel in y dim

        // Calculate next position of spaceship assuming no landing nor crash
        let mut x = self.pos.x() + (self.vx * dt) + (0.5 * ax * dt2);
        let mut y = self.pos.y() + (self.vy * dt) + (0.5 * ay * dt2);

        if y <= 0.0 {
            // Spaceship made contact with ground
            // Calculate point of contact
            y = 0.0;
            let r = self.vy * self.vy - (4.0 * 0.5 * ay * self.pos.y());
            // Make sure not to take square root of negative number
            if r >= 0.0 {
                // Calculate time of contact
                let r = r.sqrt();
                let t1 = (-self.vy + r) / (2.0 * 0.5 * ay);
                let t2 = (-self.vy - r) / (2.0 * 0.5 * ay);
                // Use lowest time greater than or equal to zero, defaulting
                // to dt if something weird happened (eg starting below ground)
                let mut t = dt;
                if t1 >= 0.0 && t1 < t {
                    t = t1
                };
                if t2 >= 0.0 && t2 < t {
                    t = t2
                };
                // Given time of contact, calculate x dim point of contact
                x = self.pos.x() + (self.vx * t) + (0.5 * ax * t * t);
            }
            // Record touchdown event, change mode
            let landing_speed = self.vx.hypot(self.vy);
            let safe_landing = if landing_speed <= Self::SAFE_LANDING_SPEED_MAX
            {
                log::info!("lander: safe");
                true
            } else {
                log::info!("lander: crash");
                false
            };
            self.mode = LanderMode::AfterTouchdown {
                touchdown_ticks: self.ticks,
                landing_speed,
                safe_landing,
            };
            // Stop lander motion and turn off thrusters
            self.vx = 0.0;
            self.vy = 0.0;
            self.thruster_left_on = false;
            self.thruster_right_on = false;
            self.thruster_main_on = false;
        } else {
            // Update velocity
            self.vx += ax * dt;
            self.vy += ay * dt;
        }

        self.pos = Point::new(x, y);

        // Time passes
        self.t += dt;
        Ok(true)
    }
}

#[derive(Debug)]
enum LanderMode {
    Start,
    InFlight,
    AfterTouchdown {
        touchdown_ticks: u32,
        landing_speed: f64,
        safe_landing: bool,
    },
}

impl LanderMode {
    fn is_after_landing_pause_over(&self, model: &LanderModel) -> bool {
        match self {
            LanderMode::AfterTouchdown {
                touchdown_ticks, ..
            } => {
                model.ticks
                    >= touchdown_ticks + LanderModel::TOUCHDOWN_PAUSE_TICKS
            }
            _ => false,
        }
    }
}

struct LanderView {
    buttons: Vec<Button<LanderModel>>,

    /// True iff moving mouse with button down, or dragging finger on touch
    /// screen.
    pointer_active: bool,
}

impl LanderView {
    fn new() -> Self {
        let buttons: Vec<Button<LanderModel>> = vec![
            Button::new(
                Rect::new(Point::new(0.30, -1.00), RectSize::new(0.20, 0.30)),
                Some(KeyId::ArrowLeft),
                ButtonLeft,
            ),
            Button::new(
                Rect::new(Point::new(0.30, -0.70), RectSize::new(0.20, 0.20)),
                None,
                ButtonUpLeft,
            ),
            Button::new(
                Rect::new(Point::new(0.50, -0.70), RectSize::new(0.30, 0.20)),
                Some(KeyId::ArrowUp),
                ButtonUp,
            ),
            Button::new(
                Rect::new(Point::new(0.80, -0.70), RectSize::new(0.20, 0.20)),
                None,
                ButtonUpRight,
            ),
            Button::new(
                Rect::new(Point::new(0.80, -1.00), RectSize::new(0.20, 0.30)),
                Some(KeyId::ArrowRight),
                ButtonRight,
            ),
        ];
        Self {
            buttons,
            pointer_active: false,
        }
    }

    /// Draw large, scaled buttons for user controls
    fn draw_buttons(
        &self,
        dc: &mut DrawingCanvas,
        model: &LanderModel,
    ) -> Result<(), Error> {
        dc.set_stroke_color(Color::WHITE);
        dc.set_fill_color(Color::GRAY);

        for button in self.buttons.iter() {
            button.draw(dc, model)?;
        }

        Ok(())
    }

    fn deactivate_all_buttons(
        &mut self,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        for button in self.buttons.iter_mut() {
            button.deactivate(model)?;
        }
        Ok(())
    }

    fn draw_safe_landing_msg(dc: &mut DrawingCanvas) -> Result<(), Error> {
        dc.with_save_restore(|dc| {
            dc.set_math_transform()?;
            dc.draw_text_centered_multiline(
                "SAFE\nLANDING",
                Point::new(0.0, 0.0),
                0.5,
            )?;
            Ok(())
        })
    }

    fn draw_crash_landing_msg(dc: &mut DrawingCanvas) -> Result<(), Error> {
        dc.with_save_restore(|dc| {
            dc.set_math_transform()?;
            dc.draw_text_centered_multiline(
                "CRASH\nLANDING",
                Point::new(0.0, 0.0),
                0.5,
            )?;
            Ok(())
        })
    }

    fn draw_restart_msg(dc: &mut DrawingCanvas) -> Result<(), Error> {
        dc.with_save_restore(|dc| {
            dc.set_math_transform()?;
            dc.draw_text_centered_multiline(
                "Click\
                \nthe restart button\
                \nto play again",
                Point::new(0.0, 0.0),
                0.2,
            )?;
            Ok(())
        })
    }

    fn on_interval(
        app: &mut LanderApp,
        model: &mut LanderModel,
        view: &mut Self,
        dt: f64,
    ) -> Result<bool, Error> {
        let mut should_redraw = app.on_interval_default(model, view, dt)?;

        // Detect if we landed
        if model.mode.is_after_landing_pause_over(model) {
            // Stop interval timer
            app.stop();
            // Reset play/pause button state
            common_controls::set_button_title_play()?;
            should_redraw = true;
        }

        Ok(should_redraw)
    }
}

impl View<LanderModel> for LanderView {
    fn draw(
        &mut self,
        rm: &dyn ResourceManager,
        model: &LanderModel,
    ) -> Result<(), Error> {
        rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
            dc.fill(Color::BLACK)?;

            dc.with_save_restore(|dc| {
                dc.transform(
                    Point::new(0.0, -0.8), // move orgin down
                    AngleDeg::from(0.0),   // no rotation
                    0.1,                   // 1 unit = 10 meters
                )?;
                draw_stars(dc, model)?;
                draw_ground(dc)?;
                draw_lander(dc, model)?;
                Ok(())
            })?;

            draw_info(dc, model)?;
            self.draw_buttons(dc, model)?;

            if let LanderMode::AfterTouchdown {
                touchdown_ticks,
                safe_landing,
                ..
            } = model.mode
            {
                if model.ticks
                    < touchdown_ticks + LanderModel::TOUCHDOWN_PAUSE_TICKS
                {
                    if safe_landing {
                        Self::draw_safe_landing_msg(dc)?;
                    } else {
                        Self::draw_crash_landing_msg(dc)?;
                    }
                } else {
                    Self::draw_restart_msg(dc)?;
                }
            }
            Ok(())
        })
    }
}

fn draw_stars(
    dc: &mut DrawingCanvas,
    model: &LanderModel,
) -> Result<(), Error> {
    dc.with_save_restore(|dc| {
        dc.set_fill_color(Color::BRIGHT_WHITE);
        dc.set_stroke_color(Color::TRANSPARENT);
        dc.set_line_width(0.0);
        let pxsize = dc.pxsize();
        for star in model.stars.iter() {
            dc.circle(star.pos, star.size as f64 * pxsize)?;
        }
        Ok(())
    })
}

fn draw_ground(dc: &mut DrawingCanvas) -> Result<(), Error> {
    let r = dc.get_canvas_rect()?;
    let p1 = r.bottom_left(dc.transform_style());
    let h = -p1.y(); // distance from bottom of screen to origin
    let ground = Rect::new(p1, RectSize::new(r.w, h));
    dc.set_fill_color(Color::BROWN);
    dc.fill_rect(&ground)
}

fn draw_lander(
    dc: &mut DrawingCanvas,
    model: &LanderModel,
) -> Result<(), Error> {
    // Draw lander using standard spaceship drawing function

    // Lander is 2 meters high so keep standard spaceship bounding box.
    let scale = 1.0;

    // model.pos is bottom of lander, so adjust drawing position accordingly.
    let pos = Point::new(model.pos.x(), model.pos.y() + 1.0);

    dc.set_fill_color(Color::GREEN);
    spaceship::draw_ship(dc, pos, AngleDeg::from(0.0), scale)?;

    if let LanderMode::InFlight = model.mode {
        if model.thruster_main_on {
            let p = Point::new(pos.x(), pos.y() - (1.0 * scale));
            draw_flame(dc, p, AngleDeg::from(0.0), scale)?;
        }
        if model.thruster_left_on {
            let p =
                Point::new(pos.x() - (0.5 * scale), pos.y() - (0.5 * scale));
            draw_flame(dc, p, AngleDeg::from(90.0), scale * 0.5)?;
        }
        if model.thruster_right_on {
            let p =
                Point::new(pos.x() + (0.5 * scale), pos.y() - (0.5 * scale));
            draw_flame(dc, p, AngleDeg::from(-90.0), scale * 0.5)?;
        }
    }
    Ok(())
}

fn draw_info(dc: &mut DrawingCanvas, model: &LanderModel) -> Result<(), Error> {
    let mut info = String::with_capacity(256);
    let _ = write!(&mut info, "{:16} {:7.1}", "time", model.t);
    use LanderMode::*;
    match model.mode {
        Start => {
            let _ = write!(
                &mut info,
                "\nLander Game\
                \n\
                \nUse arrow keys or click arrow buttons to fire rockets.\
                \nClick the start button to begin\
                \nTry to land safely!"
            );
        }
        InFlight => {
            let _ =
                write!(&mut info, "\n{:16} {:7.1}m", "height", model.pos.y());
        }
        AfterTouchdown {
            landing_speed,
            touchdown_ticks,
            safe_landing,
        } => {
            let _ = write!(
                &mut info,
                "\n{:16} {:7.1}m/s ({})",
                "landing speed",
                landing_speed,
                if safe_landing { "safe" } else { "crash" }
            );
            if log::log_enabled!(log::Level::Debug) {
                let _ = write!(
                    &mut info,
                    "\n{:16} {:7}",
                    "touchdown ticks", touchdown_ticks
                );
            }
        }
    }
    dc.set_fill_color(Color::WHITE);
    dc.draw_text_px(&info, PixelPoint::new(50, 30))?;
    Ok(())
}

fn draw_flame(
    dc: &mut DrawingCanvas,
    pos: Point,
    angle: AngleDeg,
    scale: f64,
) -> Result<(), Error> {
    dc.set_fill_color(Color::BRIGHT_YELLOW);
    dc.draw_polygon_transformed(&FLAME_TAIL_PTS[..], pos, angle, scale, 0)?;
    dc.set_fill_color(Color::BRIGHT_RED);
    dc.draw_polygon_transformed(&FLAME_CORE_PTS[..], pos, angle, scale, 0)?;
    Ok(())
}

/// Outline of arrow facing up
const ARROW_PTS: [Point; 7] = [
    Point::new(0.125, 0.025),
    Point::new(0.175, 0.025),
    Point::new(0.175, 0.100),
    Point::new(0.225, 0.100),
    Point::new(0.150, 0.175),
    Point::new(0.075, 0.100),
    Point::new(0.125, 0.100),
];

/// Outline of arrow facing up and right
const DIAGONAL_ARROW_PTS: [Point; 7] = [
    Point::new(0.075, 0.025),
    Point::new(0.150, 0.100),
    Point::new(0.175, 0.075),
    Point::new(0.175, 0.175),
    Point::new(0.075, 0.175),
    Point::new(0.100, 0.150),
    Point::new(0.025, 0.075),
];

const ARROW_SCALE: f64 = 1.0;

/// Outline of red flame diamond
const FLAME_CORE_PTS: [Point; 4] = [
    Point::new(0.00, 0.00),
    Point::new(0.50, -0.50),
    Point::new(0.00, -1.00),
    Point::new(-0.50, -0.50),
];

/// Outline of yellow flame tail
const FLAME_TAIL_PTS: [Point; 3] = [
    Point::new(0.50, -0.50),
    Point::new(0.00, -1.50),
    Point::new(-0.50, -0.50),
];

#[derive(Debug)]
struct ButtonUp;

impl ButtonBehavior<LanderModel> for ButtonUp {
    fn id(&self) -> &'static str {
        "up"
    }

    fn draw(
        &self,
        dc: &mut DrawingCanvas,
        state: &ButtonState,
        _model: &LanderModel,
    ) -> Result<(), Error> {
        let r = state.bounding_box();
        dc.draw_polygon_transformed(
            &ARROW_PTS[..],
            Point::new(r.x, r.y),
            AngleDeg::from(0.0),
            ARROW_SCALE,
            1,
        )
    }

    fn on_activate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_main(true);
        Ok(())
    }

    fn on_deactivate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_main(false);
        Ok(())
    }
}

#[derive(Debug)]
struct ButtonLeft;

impl ButtonBehavior<LanderModel> for ButtonLeft {
    fn id(&self) -> &'static str {
        "left"
    }

    fn draw(
        &self,
        dc: &mut DrawingCanvas,
        state: &ButtonState,
        _model: &LanderModel,
    ) -> Result<(), Error> {
        let r = state.bounding_box();
        dc.draw_polygon_transformed(
            &ARROW_PTS[..],
            Point::new(r.x + 0.2, r.y),
            AngleDeg::from(-90.0),
            ARROW_SCALE,
            1,
        )
    }

    fn on_activate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_right(true);
        Ok(())
    }

    fn on_deactivate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_right(false);
        Ok(())
    }
}

#[derive(Debug)]
struct ButtonRight;

impl ButtonBehavior<LanderModel> for ButtonRight {
    fn id(&self) -> &'static str {
        "right"
    }

    fn draw(
        &self,
        dc: &mut DrawingCanvas,
        state: &ButtonState,
        _model: &LanderModel,
    ) -> Result<(), Error> {
        let r = state.bounding_box();
        dc.draw_polygon_transformed(
            &ARROW_PTS[..],
            Point::new(r.x, r.y + 0.3),
            AngleDeg::from(90.0),
            ARROW_SCALE,
            1,
        )
    }

    fn on_activate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_left(true);
        Ok(())
    }

    fn on_deactivate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_left(false);
        Ok(())
    }
}

#[derive(Debug)]
struct ButtonUpRight;

impl ButtonBehavior<LanderModel> for ButtonUpRight {
    fn id(&self) -> &'static str {
        "upright"
    }

    fn draw(
        &self,
        dc: &mut DrawingCanvas,
        state: &ButtonState,
        _model: &LanderModel,
    ) -> Result<(), Error> {
        let r = state.bounding_box();
        dc.draw_polygon_transformed(
            &DIAGONAL_ARROW_PTS[..],
            Point::new(r.x, r.y),
            AngleDeg::from(0.0),
            ARROW_SCALE,
            1,
        )
    }

    fn on_activate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_main(true);
        model.set_thruster_left(true);
        Ok(())
    }

    fn on_deactivate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_main(false);
        model.set_thruster_left(false);
        Ok(())
    }
}

#[derive(Debug)]
struct ButtonUpLeft;

impl ButtonBehavior<LanderModel> for ButtonUpLeft {
    fn id(&self) -> &'static str {
        "upleft"
    }

    fn draw(
        &self,
        dc: &mut DrawingCanvas,
        state: &ButtonState,
        _model: &LanderModel,
    ) -> Result<(), Error> {
        let r = state.bounding_box();
        dc.draw_polygon_transformed(
            &DIAGONAL_ARROW_PTS[..],
            Point::new(r.x + 0.20, r.y),
            AngleDeg::from(-90.0),
            ARROW_SCALE,
            1,
        )
    }

    fn on_activate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_main(true);
        model.set_thruster_right(true);
        Ok(())
    }

    fn on_deactivate(
        &mut self,
        _state: &mut ButtonState,
        model: &mut LanderModel,
    ) -> Result<(), Error> {
        model.set_thruster_main(false);
        model.set_thruster_right(false);
        Ok(())
    }
}
