//! Text Drawing Demo for experimentation
use crate::common_controls::APP_ID;
use cpif_web::{
    application::{App, Model, ResourceManager, View},
    color::Color,
    error::Error,
    geometry::{AngleDeg, PixelPoint, Point, Rect, RectSize},
    graphics::{CanvasOptions, DrawingCanvas},
};

type TextdrawApp = App<TextdrawModel, TextdrawView>;

pub fn init(options: CanvasOptions) -> Result<(), Error> {
    // Create the Model, View, and Application
    let model = TextdrawModel;
    let view = TextdrawView;
    TextdrawApp::create(APP_ID, model, view, |app, _model, _view| {
        app.create_main_canvas(options)?;
        Ok(())
    })
}

struct TextdrawModel;

impl Model for TextdrawModel {
    fn step(&mut self, _dt: f64) -> Result<bool, Error> {
        Ok(true)
    }
}

struct TextdrawView;

impl TextdrawView {
    const TEXT1: &'static str = "The rain in spain\nfalls mainly in the plain.";
    const TEXT2: &'static str = "She sells sea shells\nby the sea shore.";
}

impl View<TextdrawModel> for TextdrawView {
    fn draw(
        &mut self,
        rm: &dyn ResourceManager,
        _model: &TextdrawModel,
    ) -> Result<(), Error> {
        rm.with_main_canvas(&mut |dc: &mut DrawingCanvas| {
            dc.fill(Color::BLACK)?;

            // Draw text with the default "pixel" transformation style.
            let loc = Point::new(50.0, 70.0);
            let height = 20.0;

            // Draw rectangle from current origin to anchor of text
            dc.set_stroke_color(Color::GREEN);
            dc.draw_rect(&Rect::new(
                Point::new(0.0, 0.0),
                RectSize::new(loc.x(), loc.y()),
            ))?;

            // Draw text
            dc.set_fill_color(Color::WHITE);
            dc.draw_text(Self::TEXT1, loc, height)?;

            // Translate, rotate, and scale
            dc.transform(Point::new(50.0, 100.0), AngleDeg::from(5.0), 1.5)?;

            // Draw rectangle from current origin to anchor of text
            dc.set_stroke_color(Color::GREEN);
            dc.draw_rect(&Rect::new(
                Point::new(0.0, 0.0),
                RectSize::new(loc.x(), loc.y()),
            ))?;

            // Draw text
            dc.set_fill_color(Color::BRIGHT_RED);
            dc.draw_text(Self::TEXT1, loc, height)?;

            // Draw text with the alternate "math" transformation style.
            dc.set_math_transform()?;
            draw_grid(dc)?;
            let loc = Point::new(-0.2, 0.1);
            let height = 0.075;

            // Draw rectangle from current origin to anchor of text
            dc.set_stroke_color(Color::BRIGHT_BLUE);
            let r = Rect::from_corners(Point::new(0.0, 0.0), loc);
            dc.draw_rect(&r)?;

            // Draw text
            dc.set_fill_color(Color::WHITE);
            dc.draw_text(Self::TEXT2, loc, height)?;

            // Translate, rotate and scale
            dc.transform(Point::new(0.1, -0.2), AngleDeg::from(-45.0), 0.75)?;

            // Draw rectangle from current origin to anchor of text
            dc.set_stroke_color(Color::BRIGHT_BLUE);
            let r = Rect::from_corners(Point::new(0.0, 0.0), loc);
            dc.draw_rect(&r)?;

            // Draw text
            dc.set_fill_color(Color::BRIGHT_RED);
            dc.draw_text(Self::TEXT2, loc, height)?;

            // Draw centered multi-line text in lower-left quadrant
            dc.set_math_transform()?;
            dc.set_fill_color(Color::BRIGHT_YELLOW);
            dc.draw_text_centered_multiline(
                "Now is the time\
                \nfor all good men\
                \nto come to the aid\
                \nof their country",
                Point::new(-0.5, -0.5),
                0.05,
            )?;

            // Draw text at pixel location ignoring any scaling or
            // transformation.
            dc.set_fill_color(Color::BRIGHT_WHITE);
            dc.draw_text_px(
                "Text at pixel location (50, 30)",
                PixelPoint::new(50, 30),
            )?;

            // Text measurements, drawing rectangles around text
            let x_pos = 0.1;
            let height = 0.1;
            for (text, pos, height) in [
                ("M", Point::new(x_pos, 0.6), height),
                ("m", Point::new(x_pos + height, 0.6), height),
                ("Mm", Point::new(x_pos, 0.5), height),
                ("Milk", Point::new(x_pos, 0.4), height),
                ("Sandstorm", Point::new(x_pos, 0.3), height),
                ("Twelve knights", Point::new(x_pos, 0.2), height),
            ] {
                let tm = dc.measure_text(text, height)?;
                log::info!("Metrics for text {:?}: {:?}", text, tm);
                dc.set_fill_color(Color::BRIGHT_WHITE);
                dc.draw_text(text, pos, height)?;
                dc.set_line_width_px(2);
                dc.set_stroke_color(Color::RED);
                dc.draw_rect(&Rect::new(pos, tm.bounding_box()))?;
                dc.set_stroke_color(Color::BRIGHT_GREEN);
                let width_height_ratio = 0.600;
                let width = text.len() as f64 * height * width_height_ratio;
                dc.draw_rect(&Rect::new(pos, RectSize::new(width, height)))?;
            }

            Ok(())
        })
    }
}

/// Draw a square grid with lines spaced 0.1 units apart.
fn draw_grid(dc: &mut DrawingCanvas) -> Result<(), Error> {
    // Draw grid squares
    dc.set_stroke_color(Color::GREEN);
    dc.set_line_width_px(1);
    for i in -10..=10 {
        let y = (i as f64) / 10.0;
        dc.line(Point::new(-1.0, y), Point::new(1.0, y))?;
    }
    for i in -10..=10 {
        let x = (i as f64) / 10.0;
        dc.line(Point::new(x, 1.0), Point::new(x, -1.0))?;
    }

    // Draw axes
    dc.set_stroke_color(Color::BRIGHT_GREEN);
    dc.set_line_width_px(2);
    dc.line(Point::new(0.0, 1.0), Point::new(0.0, -1.0))?;
    dc.line(Point::new(-1.0, 0.0), Point::new(1.0, 0.0))?;

    Ok(())
}
