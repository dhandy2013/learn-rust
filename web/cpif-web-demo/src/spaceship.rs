/// Common spaceship-related code
use cpif_web::{
    color::Color,
    error::Error,
    geometry::{AngleDeg, Point},
    graphics::DrawingCanvas,
};

/// Draw a triangular ship icon with the current fill color,
/// with a 1-pixel white border. Modifies the default line width
/// and stroke color.
///
/// `dc`: canvas on which to draw the spaceship.
/// `center`: Location at which to draw the icon.
/// `angle`: Angle of rotation, in degrees. Positive is counter-clockwise.
/// `scale`: Scale at which to draw the ship. scale=1.0 means bounding box is:
///    (-1.0, 1.0, 1.0, -1.0)
pub fn draw_ship(
    dc: &mut DrawingCanvas,
    center: Point,
    angle: AngleDeg,
    scale: f64,
) -> Result<(), Error> {
    const VERTICES: [Point; 3] = [
        Point::new(0.0, 1.0),
        Point::new(0.5, -1.0),
        Point::new(-0.5, -1.0),
    ];
    dc.set_stroke_color(Color::WHITE);
    dc.draw_polygon_transformed(&VERTICES[..], center, angle, scale, 1)
}
