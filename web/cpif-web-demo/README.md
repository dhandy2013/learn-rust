# cpif-web-demo - Demonstrate use of cpif-web crate

The ``cpif-web`` crate is a library that supports graphics drawing to a web
canvas using (mostly) pure Rust code. This ``cpif-web-demo`` crate is an example
WASM application that uses ``cpif-web`` to draw things and play sound effects.

## First time setup to develop Rust code for the web

- First install the standard Rust toolchain (rustup, rustc, cargo) by following
  the steps here: https://www.rust-lang.org/tools/install
- Then install ``wasm-pack`` using this installer:
  https://rustwasm.github.io/wasm-pack/installer/

## Compiling and Running the Code

To compile:
```
wasm-pack build --target=web
```

To serve the app (any web server that serves files out of this directory will do):
```
python3 -m http.server
```

Then point your web browser to: http://127.0.0.1:8000/

## Firefox Browser Configuration

By default Firefox browser cripples the high-performance timer, so that you
get neither precise nor accurate timing info. To override that, navigate to
about:config and set these property values:
```
privacy.reduceTimerPrecision.unconditional: false
privacy.resistFingerprinting.reduceTimerPrecision.jitter: false
privacy.resistFingerprinting.reduceTimerPrecision.microseconds: 1
```
