import init, { PlotterController } from "./pkg/starcluster.js";

let wasm = null;
let controller = null;

async function run() {
    console.log("index.js:run(): Initializing the WASM module");
    wasm = await init();
    controller = PlotterController.new(PlotterController.get_max_num_stars());
}

const getXCoords = () => {
    return new Float64Array(
        wasm.memory.buffer,
        controller.x_coords_ptr(),
        controller.coords_len());
};

const getYCoords = () => {
    return new Float64Array(
        wasm.memory.buffer,
        controller.y_coords_ptr(),
        controller.coords_len());
};

const onLoad = () => {
    console.log("onLoad");
    finishSetup();
};

// Arrange code to be called when page finishes loading
window.onload = onLoad;

const controlPanel = document.getElementById("control-panel");
const homeButton = document.getElementById("id-button-home");
const showHideButton = document.getElementById("id-button-show-hide");
const previousButton = document.getElementById("id-button-previous");
const restartButton = document.getElementById("id-button-restart");
const playPauseButton = document.getElementById("id-button-play-pause");
const nextButton = document.getElementById("id-button-next");
const infoArea = document.getElementById("info-area");
const infoStats = document.getElementById("info-stats");

let canvas = null;          // the drawing canvas
let initPromise = run();    // load WASM module asynchronously
let lastTimestamp = null;   // To avoid excessive redraws
let redrawType = "animate"; // animate, draw, or clear
let setupPhase = 0;         // to control initialization

// This controls the speed of animation
// bigger is slower
const animationPeriodMS = 4000.0; // milliseconds per animation time period

const onRedraw = (timestamp) => {
    if (lastTimestamp === timestamp) {
        // Skip duplicate redraw
        return;
    }
    let dt = 0.0;   // delta time
    if (redrawType === "animate" && lastTimestamp !== null) {
        dt = (timestamp - lastTimestamp) / animationPeriodMS;
    } else if (redrawType === "draw" && controller.t() === 0.0) {
        dt = 1.0;
    }
    lastTimestamp = timestamp;

    setupCanvas();

    const t0 = performance.now();
    // Note: Clearing the canvas in Javascript is about 2x faster than in Rust
    // PlotterController.clear(canvas); // clear canvas from Rust code
    clearCanvas();  // clear canvas in Javascript code
    const t1 = performance.now();
    let t2 = t1;
    let t3 = t1;
    if (redrawType !== "clear") {
        controller.update(dt);
        t2 = performance.now();
        // Note: Drawing dots in Javascript is about 20% faster than Rust
        drawDots();  // draw in Javascript
        // controller.draw_dots(canvas); // draw in Rust
        t3 = performance.now();
    }

    if (!infoArea.classList.contains("hidden")) {
        updateDisplayInfo({
            clearTime: t1 - t0,
            calcTime: t2 - t1,
            drawTime: t3 - t1,
        });
    }

    if (redrawType === "animate") {
        window.requestAnimationFrame(onRedraw);
    }
};

let initContext = () => {
    const half_width = canvas.width / 2.0;
    const half_height = canvas.height / 2.0;
    const s = (half_width < half_height) ? half_width : half_height;
    const scale = (s <= 0.0) ? 1.0 : s;
    const pxsize = 1.0 / scale;
    const ctx = canvas.getContext('2d', { "alpha": false });
    ctx.setTransform(scale, 0.0, 0.0, -scale, half_width, half_height);
    return { "ctx": ctx, "pxsize": pxsize };
};

let clearCanvas = () => {
    const ctx = canvas.getContext("2d", { "alpha": false });
    ctx.fillStyle = "black";
    const x = 0.0;
    const y = 0.0;
    const w = canvas.width;
    const h = canvas.height;
    // Get original 2D transformation matrix
    const t = ctx.getTransform();
    // Set to straight pixel transformation
    ctx.setTransform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0);
    // Clear the canvas
    ctx.fillRect(x, y, w, h);
    // Restore the original transformation
    ctx.setTransform(t.a, t.b, t.c, t.d, t.e, t.f);
};

let drawDots = () => {
    const x_coords = getXCoords();
    const y_coords = getYCoords();
    const context = initContext();
    const ctx = context.ctx;
    const pxsize = context.pxsize;
    ctx.fillStyle = "white";
    x_coords.forEach((x, i, _array) => {
        const y = y_coords[i];
        ctx.fillRect(x, y, pxsize, pxsize);
    });
};

const setupCanvas = () => {
    // Calculate desired size of canvas
    // Unfortunately if I don't apply this padding I get a vertical scrollbar
    // on desktop browsers (but not on mobile browsers.)
    const pad = 4;
    const de = document.documentElement;
    const canvasWidth = Math.max(de.clientWidth - pad, pad);
    const canvasHeight = Math.max(de.clientHeight - pad, pad);

    if (canvas === null) {
        // Create the drawing canvas and set the `canvas` global variable
        canvas = document.createElement("canvas");
        canvas.id = "starcluster-viewer-canvas";
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        // Put the canvas inside its container
        const container = document.getElementById("canvas-container");
        container.appendChild(canvas);
    } else if (canvas.width !== canvasWidth || canvas.height !== canvasHeight) {
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
    }
};

const updateDisplayInfo = (timingInfo) => {
    document.getElementById("id-clear-time").innerText =
        (timingInfo.clearTime).toFixed(3) + "ms";
    document.getElementById("id-calc-time").innerText =
        (timingInfo.calcTime).toFixed(3) + "ms";
    document.getElementById("id-draw-time").innerText =
        (timingInfo.drawTime).toFixed(3) + "ms";
    document.getElementById("id-canvas-width").innerText = canvas.width;
    document.getElementById("id-canvas-height").innerText = canvas.height;
    const de = document.documentElement;
    document.getElementById("id-client-width").innerText = de.clientWidth;
    document.getElementById("id-client-height").innerText = de.clientHeight;
};

const onButtonHome = (event) => {
    // Reset control panel and info area to their initial state
    {
        // Reset show/hide button
        const button = document.getElementById("id-button-show-hide");
        const newHref = "images/plus-sign-svgrepo-com.svg";
        setButtonImage(button, newHref);
    }
    {
        // Reset play/pause button
        const button = document.getElementById("id-button-play-pause");
        const newHref = "images/pause-svgrepo-com.svg";
        setButtonImage(button, newHref);
        button.title = "pause";
    }
    infoArea.classList.add("hidden");
    infoStats.classList.add("hidden");

    // Go back to main menu
    document.location.hash = "";
};

const setButtonImage = (button, newHref) => {
    const svg = button.firstElementChild;
    const image = svg.firstElementChild;
    svg.removeChild(image);
    const newImage = document.createElementNS("http://www.w3.org/2000/svg",
        "image");
    newImage.setAttribute("href", newHref);
    svg.appendChild(newImage);
};

const onButtonShowHide = (event) => {
    const button = event.currentTarget;
    const svg = button.firstElementChild;
    const image = svg.firstElementChild;
    let newHref = null;
    if (infoArea.classList.contains("hidden")) {
        infoArea.classList.remove("hidden");
        if (redrawType === "animate") {
            infoStats.classList.add("hidden");
        } else {
            infoStats.classList.remove("hidden");
        }
        // Set button image to minus sign SVG file
        newHref = "images/minus-sign-of-a-line-in-horizontal-position-svgrepo-com.svg";
    } else {
        infoArea.classList.add("hidden");
        infoStats.classList.add("hidden");
        // Set button image to plus sign SVG file
        newHref = "images/plus-sign-svgrepo-com.svg";
    }
    svg.removeChild(image);
    const newImage = document.createElementNS("http://www.w3.org/2000/svg",
        "image");
    newImage.setAttribute("href", newHref);
    svg.appendChild(newImage);
};

const onButtonRestart = (event) => {
    // Restart animation from the beginning
    controller.set_plot_mode(controller.plot_mode_id());
    lastTimestamp = null;
    window.requestAnimationFrame(onRedraw);
};

const onButtonPlayPause = (event) => {
    const button = event.currentTarget;
    const svg = button.firstElementChild;
    const image = svg.firstElementChild;
    let newHref = null;
    if (button.title === "pause") {
        redrawType = "draw";    // stop animation
        button.title = "play";
        infoStats.classList.remove("hidden");
        newHref = "images/play-svgrepo-com.svg";
    } else {
        redrawType = "animate";    // restart animation
        lastTimestamp = null;
        button.title = "pause";
        infoStats.classList.add("hidden");
        newHref = "images/pause-svgrepo-com.svg";
    }
    svg.removeChild(image);
    const newImage = document.createElementNS("http://www.w3.org/2000/svg",
        "image");
    newImage.setAttribute("href", newHref);
    svg.appendChild(newImage);
    window.requestAnimationFrame(onRedraw);
};

const onResize = () => {
    // const de = document.documentElement;
    // console.log("onResize: " + de.clientWidth + "X" + de.clientHeight);
    window.requestAnimationFrame(onRedraw);
}

const modeList = [
    "main-menu",
    "plot-uniform",
    "plot-triangular",
    "plot-gauss",
];

const doRouting = (event) => {
    const prevModeId = controller.plot_mode_id();
    let prevElement = null;
    if (prevModeId) {
        prevElement = document.getElementById(prevModeId);
    }
    if (prevElement) {
        prevElement.classList.add("hidden");
    }
    const prevElementTitle = document.getElementById(prevModeId + "-title");
    if (prevElementTitle) {
        prevElementTitle.classList.add("hidden");
    }
    let newModeHash = window.location.hash;
    let newModeId = "main-menu"; // default in case hash not recognized
    if (newModeHash) {
        newModeId = newModeHash.substring(1); // remove leading "#"
    }
    modeList.forEach((value, _index, _array) => {
        let elementId = value;
        let element = document.getElementById(elementId);
        if (element) {
            if (elementId === newModeId) {
                element.classList.remove("hidden");
                const elementTitle = document.getElementById(newModeId + "-title");
                if (elementTitle) {
                    elementTitle.classList.remove("hidden");
                }
                if (prevModeId !== newModeId) {
                    // Mode change
                    controller.set_plot_mode(newModeId);
                    lastTimestamp = null;
                    // Schedule redraw
                    window.requestAnimationFrame(onRedraw);
                }
            } else {
                element.classList.add("hidden");
            }
        }
    });
    if (newModeId === "main-menu") {
        controlPanel.classList.add("hidden");
        infoArea.classList.remove("hidden");
        infoStats.classList.add("hidden");
    } else {
        controlPanel.classList.remove("hidden");
        infoArea.classList.add("hidden");
        infoStats.classList.add("hidden");
    }
    redrawType = "animate"; // draw whenever we have a routing event
}

const finishSetup = () => {
    // Only execute if this is exactly the second time it is called
    setupPhase += 1;
    if (setupPhase !== 2) {
        return;
    }

    // Handle home button
    homeButton.addEventListener("click", onButtonHome);

    // Handle show/hide button
    showHideButton.addEventListener("click", onButtonShowHide);

    // Handle restart button
    restartButton.addEventListener("click", onButtonRestart);

    // Handle play/pause button
    playPauseButton.addEventListener("click", onButtonPlayPause);

    // Handle window resize events
    window.onresize = onResize;

    // Handle hash routing
    window.onhashchange = doRouting;

    // Schedule the first redraw
    window.requestAnimationFrame(onRedraw);

    // Just in case URL has "#" in it, call hash routing function
    doRouting({});
}

initPromise.then(() => {
    // When this code runs it means that the WASM module has finished loading,
    // therefore we can call functions defined in WASM now.
    console.log("initPromise.then");
    finishSetup();
});
