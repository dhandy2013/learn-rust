# StarCluster Demo

Playing around drawing stuff with the HTML5 Canvas API from Rust!

## Compiling and Running the Code

To compile:
```
wasm-pack build --target=web
```

To serve the app:
```
# Any web server that serves files out of this directory will do
python3 -m http.server
```

Then point your web browser to: http://127.0.0.1:8000/

## Firefox Browser Configuration

By default Firefox browser cripples the high-performance timer, so that you
get neither precise nor accurate timing info. To override that, navigate to
about:config and set these property values:
```
privacy.reduceTimerPrecision.unconditional: false
privacy.resistFingerprinting.reduceTimerPrecision.jitter: false
privacy.resistFingerprinting.reduceTimerPrecision.microseconds: 1
```

## Related Links

[web_sys::CanvasRenderingContext2d](https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html) - Rust wrapper around canvas 2D rendering context

[CanvasRenderingContext2D](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D) - Mozilla Developer Network docs on the canvas 2D drawing API

[rand crate v0.8 docs](https://docs.rs/rand/0.8.3/rand/)