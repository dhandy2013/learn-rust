#!/bin/bash -e
if [ "$1" = "" -o "$1" = "--help" ] ; then
    echo "Usage: ./deploy.sh <remote-host>"
    exit 1
fi
remote_host="$1"
ssh $remote_host mkdir -p /var/www/html/public/starcluster
rsync -av ./*.html ./*.css ./*.js ./images ./pkg ${remote_host}:/var/www/html/public/starcluster/
