use crate::web_util::get_render_context_2d;
use log::info;
use rand::{Rng, SeedableRng};
use rand_xoshiro::SplitMix64;
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};

mod web_util;

pub const MAX_NUM_STARS: u32 = 10000;

#[wasm_bindgen]
pub struct PlotterController {
    num_stars: u32,
    plot_mode: Option<Box<dyn PlotMode>>,
    plot_mode_id: String,
    rng: SplitMix64,
    t: f64,
    x_coords: Vec<f64>,
    y_coords: Vec<f64>,
}

#[wasm_bindgen]
impl PlotterController {
    pub fn new(num_stars: u32) -> Self {
        let num_stars = if num_stars > MAX_NUM_STARS {
            MAX_NUM_STARS
        } else {
            num_stars
        };
        Self {
            num_stars,
            plot_mode: None,
            plot_mode_id: String::default(),
            rng: Self::create_rng(),
            t: 0.0,
            x_coords: vec![0.0; num_stars as usize],
            y_coords: vec![0.0; num_stars as usize],
        }
    }

    pub fn get_max_num_stars() -> u32 {
        MAX_NUM_STARS
    }

    pub fn set_plot_mode(&mut self, plot_mode_id: &str) -> Result<(), JsValue> {
        self.plot_mode = match plot_mode_id {
            "main-menu" => None,
            "plot-uniform" => Some(Box::new(UniformPlot)),
            "plot-triangular" => Some(Box::new(TriangularPlot)),
            "plot-gauss" => Some(Box::new(GaussianPlot)),
            _ => {
                return Err(JsValue::from_str(
                    format!("Invalid plot mode: {}", plot_mode_id).as_str(),
                ));
            }
        };
        self.plot_mode_id = plot_mode_id.to_owned();
        self.rng = Self::create_rng();
        self.t = 0.0;
        self.x_coords.fill(0.0);
        self.y_coords.fill(0.0);
        Ok(())
    }

    pub fn plot_mode_id(&self) -> String {
        self.plot_mode_id.clone()
    }

    pub fn t(&self) -> f64 {
        self.t
    }

    pub fn x_coords_ptr(&self) -> *const f64 {
        self.x_coords.as_ptr()
    }

    pub fn y_coords_ptr(&self) -> *const f64 {
        self.y_coords.as_ptr()
    }

    /// Return the number of valid coordinates
    pub fn coords_len(&self) -> u32 {
        if self.t < 1.0 {
            (self.t * self.num_stars as f64).round() as u32
        } else {
            self.num_stars
        }
    }

    /// Update the state of the plot from the previous time ``t`` state to the
    /// state for time ``t + dt``. ``dt`` is in units of "time periods".
    /// If ``dt <= 0.0``: do nothing
    /// If ``0.0 < dt <= 1.0``: Generate new coordinates, possibly overwriting some old.
    /// If ``dt > 1.0``: Behave as though ``dt == 1.0``
    pub fn update(&mut self, dt: f64) {
        if let Some(ref plot_mode) = self.plot_mode {
            // Assert that parallel arrays are consistent
            assert_eq!(self.x_coords.len(), self.y_coords.len());
            assert_eq!(self.x_coords.len(), self.num_stars as usize);
            if dt <= 0.0 {
                return;
            }
            let dt = if dt > 1.0 { 1.0 } else { dt };
            let i = (self.t.fract() * self.num_stars as f64).round() as usize;
            let n = (self.num_stars as f64 * dt).round() as usize;
            if i + n > self.num_stars as usize {
                let n = i + n - self.num_stars as usize;
                plot_mode.calc_points(
                    &mut self.rng,
                    &mut self.x_coords[i..self.num_stars as usize],
                    &mut self.y_coords[i..self.num_stars as usize],
                );
                plot_mode.calc_points(
                    &mut self.rng,
                    &mut self.x_coords[0..n],
                    &mut self.y_coords[0..n],
                );
            } else {
                plot_mode.calc_points(
                    &mut self.rng,
                    &mut self.x_coords[i..i + n],
                    &mut self.y_coords[i..i + n],
                );
            }
            self.t += dt;
        }
    }

    /// Clear the HTML canvas to black.
    /// Note: Doing this from Rust is about 2x slower than from Javascript.
    pub fn clear(canvas: &web_sys::HtmlCanvasElement) -> Result<(), JsValue> {
        let ctx = get_render_context_2d(&canvas);
        ctx.set_fill_style(&JsValue::from_str("black"));
        let (x, y) = (0.0, 0.0);
        let (w, h) = (canvas.width() as f64, canvas.height() as f64);
        // Get original 2D transformation matrix
        let t = ctx.get_transform()?;
        // Set to straight pixel transformation
        ctx.set_transform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0)?;
        // Clear the canvas
        ctx.fill_rect(x, y, w, h);
        // Restore the original transformation
        ctx.set_transform(t.a(), t.b(), t.c(), t.d(), t.e(), t.f())?;
        Ok(())
    }

    /// Draw the current set of dots on the HTML canvas.
    /// Note: Doing this from Rust is about 20% slower than from Javascript.
    pub fn draw_dots(
        &self,
        canvas: &web_sys::HtmlCanvasElement,
    ) -> Result<(), JsValue> {
        let (ctx, pxsize) = init_context(&canvas)?;
        ctx.set_fill_style(&JsValue::from_str("white"));
        assert_eq!(self.x_coords.len(), self.y_coords.len());
        let n = self.x_coords.len();
        for i in 0..n {
            let x = self.x_coords[i];
            let y = self.y_coords[i];
            ctx.fill_rect(x, y, pxsize, pxsize);
        }
        Ok(())
    }
}

impl PlotterController {
    fn create_rng() -> SplitMix64 {
        SeedableRng::seed_from_u64(1776)
    }
}

trait PlotMode {
    fn calc_points(
        &self,
        rng: &mut SplitMix64,
        x_coords: &mut [f64],
        y_coords: &mut [f64],
    );
}

struct UniformPlot;

impl PlotMode for UniformPlot {
    /// Calculate points for a uniform distribution
    fn calc_points(
        &self,
        rng: &mut SplitMix64,
        x_coords: &mut [f64],
        y_coords: &mut [f64],
    ) {
        assert_eq!(x_coords.len(), y_coords.len());
        for i in 0..x_coords.len() {
            let x = (rng.gen::<f64>() * 2.0) - 1.0;
            let y = (rng.gen::<f64>() * 2.0) - 1.0;
            x_coords[i] = x;
            y_coords[i] = y;
        }
    }
}

struct TriangularPlot;

impl PlotMode for TriangularPlot {
    /// Calculate points for a triangular distribution
    fn calc_points(
        &self,
        rng: &mut SplitMix64,
        x_coords: &mut [f64],
        y_coords: &mut [f64],
    ) {
        assert_eq!(x_coords.len(), y_coords.len());
        for i in 0..x_coords.len() {
            let x = rng.gen::<f64>() - rng.gen::<f64>();
            let y = rng.gen::<f64>() - rng.gen::<f64>();
            x_coords[i] = x;
            y_coords[i] = y;
        }
    }
}

struct GaussianPlot;

impl PlotMode for GaussianPlot {
    /// Calculate points for a Gaussian distribution
    fn calc_points(
        &self,
        rng: &mut SplitMix64,
        x_coords: &mut [f64],
        y_coords: &mut [f64],
    ) {
        assert_eq!(x_coords.len(), y_coords.len());
        /*
        # From random.py in the Python standard library:
        # When x and y are two variables from [0, 1), uniformly
        # distributed, then
        #
        #    cos(2*pi*x)*sqrt(-2*log(1-y))
        #    sin(2*pi*x)*sqrt(-2*log(1-y))
        #
        # are two *independent* variables with normal distribution
        # (mu = 0, sigma = 1).
        */
        const PI: f64 = std::f64::consts::PI;
        let std_dev = 0.25;
        for i in 0..x_coords.len() {
            let x = rng.gen::<f64>();
            let y = rng.gen::<f64>();
            let x2pi = 2.0 * PI * x;
            let scale = (-2.0 * (1.0 - y).ln()).sqrt();
            x_coords[i] = x2pi.sin() * scale * std_dev;
            y_coords[i] = x2pi.cos() * scale * std_dev;
        }
    }
}

/// Given an HTML canvas, create a drawing context set up for the kind of drawing
/// we want to do.
///
/// Set the current transformation matrix for "mathematical coordinates" where
/// (0.0, 0.0) is the center of the canvas and (-1.0, 1.0) is the range of
/// coordinates for each axis on a square canvas.  On a non-square canvas, the
/// smaller dimension has the range (-1.0, 1.0) and the range of the larger one
/// is proportionately greater.
///
/// Return (ctx, pxsize) where:
///     ctx is the rendering context,
///     pxsize is the size in "mathematical coordinates" of one pixel,
///         for purpose of determining dot size.
fn init_context(
    canvas: &web_sys::HtmlCanvasElement,
) -> Result<(web_sys::CanvasRenderingContext2d, f64), JsValue> {
    let half_width = canvas.width() as f64 / 2.0;
    let half_height = canvas.height() as f64 / 2.0;
    let scale = if half_width < half_height {
        half_width
    } else {
        half_height
    };
    let scale = if scale <= 0.0 { 1.0 } else { scale };
    let pxsize = 1.0 / scale;
    let ctx = get_render_context_2d(&canvas);
    ctx.set_transform(scale, 0.0, 0.0, -scale, half_width, half_height)?;
    Ok((ctx, pxsize))
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    // Initialize panic logging
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    // Initialize regular logging
    wasm_logger::init(wasm_logger::Config::new(log::Level::Info));
    info!("starcluster::start() - initialized logging");
    Ok(())
}
