#![allow(dead_code)]
use std::collections::HashMap;
use wasm_bindgen::prelude::{Closure, JsValue};
use wasm_bindgen::{Clamped, JsCast};

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct ColorTuple(pub u8, pub u8, pub u8, pub u8); // RGBA

impl ColorTuple {
    pub fn to_array3(&self) -> [u8; 3] {
        [self.0, self.1, self.2]
    }
}

pub type PixelLength = i32;

#[derive(Clone, Copy, Debug)]
pub struct Coords(pub PixelLength, pub PixelLength); // (x, y)

impl Coords {
    /// Return value as (x, y) cast to f64
    /// usable e.g. by web_sys::CanvasRenderingContext2d.move_to()
    pub fn to_xy_f64(&self) -> (f64, f64) {
        (self.0 as f64, self.1 as f64)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Rect(pub Coords, pub Coords); // ((x, y), (width, height))

impl Rect {
    /// Return value as (x, y, width, height) cast to f64
    /// usable e.g. by web_sys::CanvasRenderingContext2d.rect()
    pub fn to_xywh_f64(&self) -> (f64, f64, f64, f64) {
        let (x, y, w, h) = (self.0 .0, self.0 .1, self.1 .0, self.1 .1);
        (x as f64, y as f64, w as f64, h as f64)
    }
}

/// Return the document object for the current web page.
pub fn get_document() -> web_sys::Document {
    let window = web_sys::window().expect("global window should exist");
    let document = window.document().expect("document on window should exist");
    document
}

/// Return the performance object for the current web page.
/// It has a .now() method for performance timing.
pub fn get_performance() -> web_sys::Performance {
    let window = web_sys::window().expect("global window should exist");
    let performance = window
        .performance()
        .expect("window.performance should exist");
    performance
}

/// Return the HtmlCanvasElement with the given ID.
/// Panic if not found or not a canvas element.
pub fn get_canvas_by_id(
    document: &web_sys::Document,
    canvas_id: &str,
) -> web_sys::HtmlCanvasElement {
    let element = document
        .get_element_by_id(canvas_id)
        .unwrap_or_else(|| panic!("canvas #{} should exist", canvas_id));
    match element.dyn_into::<web_sys::HtmlCanvasElement>() {
        Ok(canvas) => canvas,
        Err(element) => panic!(
            "element #{} was {}, expected canvas",
            canvas_id,
            element.tag_name()
        ),
    }
}

/// Return the HtmlFormElement with the given ID.
/// Panic if not found or not a form element.
pub fn get_form_by_id(
    document: &web_sys::Document,
    form_id: &str,
) -> web_sys::HtmlFormElement {
    let element = document
        .get_element_by_id(form_id)
        .unwrap_or_else(|| panic!("form #{} should exist", form_id));
    match element.dyn_into::<web_sys::HtmlFormElement>() {
        Ok(form) => form,
        Err(element) => panic!(
            "element #{} was {}, expected form",
            form_id,
            element.tag_name()
        ),
    }
}

/// Get the string value of the input element with the given ID.
/// Panic if the element does not exist or is not an input.
pub fn get_input_string(
    document: &web_sys::Document,
    input_id: &str,
) -> String {
    match document
        .get_element_by_id(input_id)
        .unwrap_or_else(|| panic!("input #{} should exist", input_id))
        .dyn_into::<web_sys::HtmlInputElement>()
    {
        Ok(input) => input.value(),
        Err(element) => panic!(
            "element #{} was {}, expected input",
            input_id,
            element.tag_name()
        ),
    }
}

/// Get the boolean input value of a checkbox or radio button.
/// Return true of the input is checked or selected, false otherwise.
/// Panic if the element does not exist or is not an input.
pub fn get_input_checked(document: &web_sys::Document, input_id: &str) -> bool {
    match document
        .get_element_by_id(input_id)
        .unwrap_or_else(|| {
            panic!("checkbox or radio input #{} should exist", input_id)
        })
        .dyn_into::<web_sys::HtmlInputElement>()
    {
        Ok(input) => input.checked(),
        Err(element) => panic!(
            "element #{} was {}, expected input",
            input_id,
            element.tag_name()
        ),
    }
}

pub fn set_input_str(
    document: &web_sys::Document,
    input_id: &str,
    value: &str,
) {
    match document
        .get_element_by_id(input_id)
        .unwrap_or_else(|| panic!("input #{} should exist", input_id))
        .dyn_into::<web_sys::HtmlInputElement>()
    {
        Ok(input) => input.set_value(value),
        Err(element) => panic!(
            "element #{} was {}, expected input",
            input_id,
            element.tag_name()
        ),
    }
}

/// Set the HTML contents of the element with the given ID.
/// Panic if that element does not exist.
pub fn set_inner_html(
    document: &web_sys::Document,
    element_id: &str,
    value: &str,
) {
    let element = document
        .get_element_by_id(element_id)
        .unwrap_or_else(|| panic!("Element #{} should exist", element_id))
        .dyn_into::<web_sys::Element>()
        .unwrap();
    element.set_inner_html(value);
}

/// Return a 2D rendering context for the canvas.
/// It is created with option alpha=false to speed up bitmap rendering.
///
/// Note: From the MDN Web Docs for HTMLCanvasElement.getcontext:
/// Later calls to this method on the same canvas element, with the same
/// contextType argument, will always return the same drawing context instance
/// as was returned the first time the method was invoked. It is not possible to
/// get a different drawing context object on a given canvas element.
pub fn get_render_context_2d(
    canvas: &web_sys::HtmlCanvasElement,
) -> web_sys::CanvasRenderingContext2d {
    let mut context_options = HashMap::new();
    context_options.insert("alpha", false);
    let context_options = JsValue::from_serde(&context_options).unwrap();
    let ctx = canvas
        .get_context_with_context_options("2d", &context_options)
        .unwrap() // unwrap Result into Option
        .unwrap() // unwrap Option into Object
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap(); // unwrap Result
    ctx
}

/// Register a callback to be invoked when a mouse event happens on a canvas.
/// callback requirements: See the docs for wasm_bindgen Closure::wrap()
/// Note: This leaks memory! Only call this a few times during app lifecycle.
pub fn add_canvas_mouse_event_listener<T>(
    canvas: &web_sys::HtmlCanvasElement,
    event_type: &str,
    callback: T,
) where
    T: FnMut(web_sys::MouseEvent) + 'static,
{
    let closure = Closure::wrap(Box::new(callback) as Box<dyn FnMut(_)>);
    canvas
        .add_event_listener_with_callback(
            event_type,
            closure.as_ref().unchecked_ref(),
        )
        .unwrap_or_else(|err| {
            panic!("Error registering '{}' callback: {:?}", event_type, err)
        });
    // Yes, this leaks memory, but it is required to avoid dropping
    // the closure and invalidating the javascript callback.
    closure.forget();
}

/// Register a callback to be invoked when an event happens on a canvas.
/// callback requirements: See the docs for wasm_bindgen Closure::wrap()
/// Note: This leaks memory! Only call this a few times during app lifecycle.
pub fn add_canvas_event_listener<T>(
    canvas: &web_sys::HtmlCanvasElement,
    event_type: &str,
    callback: T,
) where
    T: FnMut(web_sys::Event) + 'static,
{
    let closure = Closure::wrap(Box::new(callback) as Box<dyn FnMut(_)>);
    canvas
        .add_event_listener_with_callback(
            event_type,
            closure.as_ref().unchecked_ref(),
        )
        .unwrap_or_else(|err| {
            panic!("Error registering '{}' callback: {:?}", event_type, err)
        });
    // Yes, this leaks memory, but it is required to avoid dropping
    // the closure and invalidating the javascript callback.
    closure.forget();
}

/// Register a callback to be invoked when an event happens to an element.
/// callback requirements: See the docs for wasm_bindgen Closure::wrap()
/// Note: This leaks memory! Only call this a few times during app lifecycle.
pub fn add_element_event_listener<T>(
    document: &web_sys::Document,
    element_id: &str,
    event_type: &str,
    callback: T,
) where
    T: FnMut(web_sys::Event) + 'static,
{
    let closure = Closure::wrap(Box::new(callback) as Box<dyn FnMut(_)>);
    let element = document
        .get_element_by_id(element_id)
        .unwrap_or_else(|| panic!("Element #{} should exist", element_id));
    element
        .add_event_listener_with_callback(
            event_type,
            closure.as_ref().unchecked_ref(),
        )
        .unwrap_or_else(|err| {
            panic!("Error registering '{}' callback: {:?}", event_type, err)
        });
    // Yes, this leaks memory, but it is required to avoid dropping
    // the closure and invalidating the javascript callback.
    closure.forget();
}

/// Return a JsValue containing an RGB hex-encoded color string
pub fn make_js_color(color: &ColorTuple) -> JsValue {
    let color_string =
        format!("#{:02x}{:02x}{:02x}", color.0, color.1, color.2);
    JsValue::from_str(color_string.as_str())
}

/// Copy a pixel buffer to an external canvas.
/// Return Err if creating the ImageData or copying it to the canvas fails.
pub fn try_blit(
    pixel_bytes: &mut [u8],
    width: u32,
    height: u32,
    ctx: &web_sys::CanvasRenderingContext2d,
    x: f64,
    y: f64,
) -> Result<(), JsValue> {
    const BYTES_PER_PIXEL: usize = 4;
    let expected_bytes_len = width as usize * height as usize * BYTES_PER_PIXEL;
    if pixel_bytes.len() != expected_bytes_len {
        return Err(JsValue::from_str(
            format!(
                "pixel_bytes len was {} but should be {}",
                pixel_bytes.len(),
                expected_bytes_len
            )
            .as_str(),
        ));
    }
    let data = web_sys::ImageData::new_with_u8_clamped_array_and_sh(
        Clamped(pixel_bytes),
        width,
        height,
    )?;
    ctx.put_image_data(&data, x, y)
}

/// Copy a pixel buffer to an external canvas.
/// Panic if creating the ImageData or copying it to the canvas fails.
pub fn blit(
    pixel_bytes: &mut [u8],
    width: u32,
    height: u32,
    ctx: &web_sys::CanvasRenderingContext2d,
    x: f64,
    y: f64,
) {
    if let Err(err) = try_blit(pixel_bytes, width, height, ctx, x, y) {
        panic!("Error copying pixel bytes to canvas: {:?}", err);
    }
}
