import init from "./pkg/wasm_mandelbrot_viewer.js";

async function run() {
    console.log("index.js:run(): Initializing the WASM module");
    await init();
    console.log("index.js:run(): done");
}

run();
