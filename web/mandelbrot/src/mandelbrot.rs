const BYTES_PER_PIXEL: usize = 4;

/// Create a buffer for the draw_ functions in this module
pub fn create_pixel_bytes(width: u32, height: u32) -> Vec<u8> {
    let len_bytes = (width as usize) * (height as usize) * BYTES_PER_PIXEL;
    vec![0u8; len_bytes]
}

/// Draw a test pattern into the pixel buffer
pub fn draw_test_pattern(pixel_bytes: &mut [u8], width: u32, height: u32) {
    for x in 0..width {
        for y in 0..height {
            let r = (x & 0xff) as u8;
            let g = (y & 0xff) as u8;
            let b = (((x as f64).hypot(y as f64) as u32) & 0xff) as u8;
            let i = ((y * width) + x) as usize * BYTES_PER_PIXEL;
            pixel_bytes[i] = r;
            pixel_bytes[i + 1] = g;
            pixel_bytes[i + 2] = b;
            pixel_bytes[i + 3] = 255;
        }
    }
}

/// Draw the Mandelbrot set into the given pixel buffer
pub fn draw_mandelbrot(
    pixel_bytes: &mut [u8],
    width: u32,
    height: u32,
    center_x: f64,
    center_y: f64,
    scale: f64,
    max_count: u32,
    set_color: [u8; 3], // RGB
) {
    let (sizex, sizey) = if height < width {
        (scale * (width as f64 / height as f64), scale)
    } else {
        (scale, scale * (height as f64 / width as f64))
    };
    let dx = sizex / width as f64;
    let dy = sizey / height as f64;

    let mut y = center_y + (sizey / 2.0);
    for pixel_col in
        pixel_bytes.chunks_exact_mut((width as usize) * BYTES_PER_PIXEL)
    {
        let mut x = center_x - (sizex / 2.0);
        for pixel in pixel_col.chunks_exact_mut(BYTES_PER_PIXEL) {
            let count = _calc_mandelbrot_count(x, y, max_count);
            if count == u32::max_value() {
                pixel[0] = set_color[0];
                pixel[1] = set_color[1];
                pixel[2] = set_color[2];
                pixel[3] = 255;
            } else {
                let color = ((count as f64 / max_count as f64) * 255.0) as u8;
                pixel[0] = color;
                pixel[1] = color;
                pixel[2] = color;
                pixel[3] = 255;
            }
            x += dx;
        }
        y -= dy;
    }
}

fn _calc_mandelbrot_count(x: f64, y: f64, max_count: u32) -> u32 {
    let (c_r, c_i) = (x, y);
    let (mut z_r, mut z_i) = (c_r, c_i);
    for count in 0..(max_count + 1) {
        if ((z_r * z_r) + (z_i * z_i)).sqrt() >= 2.0 {
            return count;
        }
        let z_r_next = (z_r * z_r) - (z_i * z_i) + c_r;
        z_i = (2.0 * z_r * z_i) + c_i;
        z_r = z_r_next;
    }
    u32::max_value()
}
