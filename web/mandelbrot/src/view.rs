use crate::mandelbrot::{
    create_pixel_bytes, draw_mandelbrot, draw_test_pattern,
};
use crate::view_state::{Event, ViewState, ViewStateStart};
use crate::web_util::{blit, get_performance, ColorTuple, Coords, PixelLength};
use log::info;

const DEFAULT_MAX_COUNT: u32 = 100;
const DEFAULT_SET_COLOR: ColorTuple = ColorTuple(16, 48, 32, 255);

pub struct MandelbrotViewer {
    width: u32,
    height: u32,

    /// X center of image in math coordinate space
    center_x: f64,

    /// Y center of image in math coordinate space
    center_y: f64,

    /// Vertical size of image in math coordinate units
    scale: f64,

    pixel_bytes: Vec<u8>,

    need_draw: bool,
    need_refresh: bool,
    state: Option<Box<dyn ViewState>>,
}

impl MandelbrotViewer {
    pub fn new(width: u32, height: u32) -> MandelbrotViewer {
        info!("Creating MandelbrotViewer");
        let mut viewer = Self {
            width: 0,
            height: 0,
            center_x: 0.0,
            center_y: 0.0,
            scale: 2.0,
            pixel_bytes: vec![0u8; 0],
            need_draw: false,
            need_refresh: false,
            state: Some(Box::new(ViewStateStart::new())),
        };
        viewer.set_size(width, height);
        viewer
    }

    /// Signal that when the time comes we need to re-draw the Mandelbrot set
    /// and re-blit the image to the screen.
    pub fn set_need_draw(&mut self) {
        self.need_draw = true;
        self.need_refresh = true;
    }

    /// Signal that when the time comes we need to re-blit the Mandelbrot set
    /// image to the screen.
    pub fn set_need_refresh(&mut self) {
        self.need_refresh = true;
    }

    pub fn pan_and_zoom(&mut self, vx: f64, vy: f64, scalefactor: f64) {
        self.center_x += vx * self.scale;
        self.center_y += vy * self.scale;
        self.scale *= scalefactor;
    }

    pub fn get_width(&self) -> u32 {
        self.width
    }

    pub fn get_height(&self) -> u32 {
        self.height
    }

    pub fn get_center_x(&self) -> f64 {
        self.center_x
    }

    pub fn get_center_y(&self) -> f64 {
        self.center_y
    }

    pub fn get_scale(&self) -> f64 {
        self.scale
    }

    pub fn set_size(&mut self, width: u32, height: u32) {
        if width == self.width && height == self.height {
            return;
        }
        self.width = width;
        self.height = height;
        self.pixel_bytes = create_pixel_bytes(width, height);
    }

    pub fn canvas_size(&self) -> Coords {
        Coords(self.width as PixelLength, self.height as PixelLength)
    }

    pub fn set_pos(&mut self, center_x: f64, center_y: f64) {
        self.center_x = center_x;
        self.center_y = center_y;
    }

    pub fn set_scale(&mut self, scale: f64) {
        self.scale = scale;
    }

    /// Draw the mandelbrot set on the internal pixel buffer
    pub fn draw(&mut self) -> Result<(), String> {
        if self.width < 1 || self.height < 1 {
            return Err(format!(
                "Invalid pixels width x height: {width} x {height}",
                width = self.width,
                height = self.height
            ));
        }

        if true {
            draw_mandelbrot(
                &mut self.pixel_bytes,
                self.width,
                self.height,
                self.center_x,
                self.center_y,
                self.scale,
                DEFAULT_MAX_COUNT,
                DEFAULT_SET_COLOR.to_array3(),
            );
        } else {
            draw_test_pattern(&mut self.pixel_bytes, self.width, self.height);
        }

        Ok(())
    }

    /// Handle an incoming mouse event.
    /// Return (calc_time, blit_time)
    /// calc_time: Time in ms to recalculate the Mandelbrot set pixel buffer
    /// blit_time: Time in ms to copy the pixel buffer to the canvas
    /// Each time is -1 if the action is not done.
    pub fn process_event(
        &mut self,
        ctx: &web_sys::CanvasRenderingContext2d,
        event: &Event,
    ) -> (f64, f64) {
        if let Some(state) = self.state.take() {
            self.state = Some(state.update(self, event));
        }
        self.run(ctx)
    }

    /// "Do the needful" - redraw and/or refresh.
    /// Return (calc_time, blit_time)
    /// calc_time: Time in ms to recalculate the Mandelbrot set pixel buffer
    /// blit_time: Time in ms to copy the pixel buffer to the canvas
    /// Each time is -1 if the action is not done.
    pub fn run(
        &mut self,
        ctx: &web_sys::CanvasRenderingContext2d,
    ) -> (f64, f64) {
        let performance = get_performance();
        let mut calc_time: f64 = -1.0;
        let mut blit_time: f64 = -1.0;
        if self.need_draw {
            // Draw re-writes the internal pixel buffer
            let t0 = performance.now();
            if let Err(err) = self.draw() {
                panic!("Error while drawing: {:?}", err);
            }
            self.need_draw = false;
            calc_time = performance.now() - t0;
        }
        if self.need_refresh {
            // Refresh blits the current pixel buffer
            // then draws view state over the top of it
            let t0 = performance.now();
            self.refresh(&ctx);
            self.need_refresh = false;
            blit_time = performance.now() - t0;
        }
        (calc_time, blit_time)
    }

    pub fn refresh(&mut self, ctx: &web_sys::CanvasRenderingContext2d) {
        blit(
            &mut self.pixel_bytes,
            self.width,
            self.height,
            ctx,
            0.0,
            0.0,
        );
        if let Some(state) = self.state.as_ref() {
            state.draw(&ctx);
        }
    }
}
