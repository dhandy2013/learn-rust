use crate::view::MandelbrotViewer;
use crate::web_util::{make_js_color, ColorTuple, Coords, PixelLength, Rect};

/// Hold event data for mouse or touch events on a canvas
#[derive(Debug)]
pub struct PointerEventData {
    pos: Coords,
}

impl PointerEventData {
    pub fn new(pos: Coords) -> Self {
        Self { pos }
    }

    pub fn pos(&self) -> Coords {
        self.pos
    }
}

#[derive(Debug)]
pub enum Event {
    StartZoomIn(PointerEventData),
    StartZoomOut(PointerEventData),
    StartPan(PointerEventData),
    MovePointer(PointerEventData),
    EndMovePointer(PointerEventData),
}

pub trait ViewState: Send {
    fn update(
        self: Box<Self>,
        view: &mut MandelbrotViewer,
        event: &Event,
    ) -> Box<dyn ViewState>;
    fn draw(&self, ctx: &web_sys::CanvasRenderingContext2d);
}

pub struct ViewStateStart {}

impl ViewStateStart {
    pub fn new() -> Self {
        Self {}
    }
}

impl ViewState for ViewStateStart {
    fn update(
        self: Box<Self>,
        view: &mut MandelbrotViewer,
        event: &Event,
    ) -> Box<dyn ViewState> {
        match event {
            Event::StartZoomOut(data) => Box::new(ViewStateZoom::new(
                view,
                zoom_out_calc_scale_factor,
                ColorTuple(32, 32, 128, 255),
                data.pos,
            )),
            Event::StartZoomIn(data) => Box::new(ViewStateZoom::new(
                view,
                zoom_in_calc_scale_factor,
                ColorTuple(128, 32, 32, 255),
                data.pos,
            )),
            Event::StartPan(data) => Box::new(ViewStatePan::new(
                view,
                ColorTuple(32, 128, 32, 255),
                data.pos,
            )),
            _ => self,
        }
    }

    fn draw(&self, _ctx: &web_sys::CanvasRenderingContext2d) {
        log::debug!("ViewStateStart: drawing nothing",);
    }
}

struct ViewStateZoom {
    calc_scale_factor: fn(i32, i32) -> f64,
    color: ColorTuple,
    start_pos: Coords,
    rect: Option<Rect>,
}

impl ViewStateZoom {
    fn new(
        view: &mut MandelbrotViewer,
        calc_scale_factor: fn(PixelLength, PixelLength) -> f64,
        color: ColorTuple,
        start_pos: Coords,
    ) -> Self {
        let mut obj = Self {
            calc_scale_factor,
            color,
            start_pos,
            rect: None,
        };
        obj.rect = Some(obj.calc_rect(&start_pos));
        view.set_need_refresh();
        obj
    }

    fn calc_rect(&self, pos: &Coords) -> Rect {
        let (x1, y1) = (self.start_pos.0, self.start_pos.1);
        let (x2, y2) = (pos.0, pos.1);
        let w = x2 - x1;
        let h = y2 - y1;
        Rect(self.start_pos, Coords(w, h))
    }

    fn pan_and_zoom(&self, view: &mut MandelbrotViewer, pos: Coords) {
        let (vx, vy, rect_length, win_length) =
            calc_pan_zoom_stuff(self.start_pos, pos, view.canvas_size());
        if rect_length < 2 {
            // Too short of a distance for reasonable pan/zoom
            return;
        }
        // Calculate zoom scale factor
        let scalefactor = (self.calc_scale_factor)(rect_length, win_length);
        // Pan and zoom
        view.pan_and_zoom(vx, vy, scalefactor);
        view.set_need_draw();
    }
}

/// Return (vx, vy, rect_length, win_length)
/// Where (vx, vy) is the pan vector
/// And (rect_length, win_length) are useful for calculating zoom factor
fn calc_pan_zoom_stuff(
    start_pos: Coords,
    end_pos: Coords,
    canvas_size: Coords,
) -> (f64, f64, PixelLength, PixelLength) {
    let (x1, y1) = (start_pos.0, start_pos.1);
    let (x2, y2) = (end_pos.0, end_pos.1);
    let rect_w = (x2 - x1).abs();
    let rect_h = (y2 - y1).abs();
    let (width, height) = (canvas_size.0, canvas_size.1);
    let (mut win_length, rect_length) = if height < width {
        (height, rect_h)
    } else {
        (width, rect_w)
    };
    if win_length < 1 {
        win_length = 1;
    }
    // Current window center
    let cx1 = width / 2;
    let cy1 = height / 2;
    // Center of drawn rectangle
    let cx2 = (x1 + x2) / 2;
    let cy2 = (y1 + y2) / 2;
    // Calculate pan vector
    let vx = (cx2 - cx1) as f64 / win_length as f64;
    let vy = (cy1 - cy2) as f64 / win_length as f64; // Y axis flipped in math
    (vx, vy, rect_length, win_length)
}

impl ViewState for ViewStateZoom {
    fn update(
        mut self: Box<Self>,
        view: &mut MandelbrotViewer,
        event: &Event,
    ) -> Box<dyn ViewState> {
        match event {
            Event::EndMovePointer(data) => {
                self.pan_and_zoom(view, data.pos);
                view.set_need_refresh();
                Box::new(ViewStateStart::new())
            }
            Event::MovePointer(data) => {
                self.rect = Some(self.calc_rect(&data.pos));
                view.set_need_refresh();
                self
            }
            _ => self,
        }
    }

    fn draw(&self, ctx: &web_sys::CanvasRenderingContext2d) {
        let line_width: f64 = 2.0;
        log::debug!(
            "ViewStateZoom: drawing \
            rectangle {:?} with color {:?} and line width {}",
            self.rect,
            self.color,
            line_width
        );
        if let Some(rect) = self.rect {
            ctx.begin_path();
            ctx.set_stroke_style(&make_js_color(&self.color));
            ctx.set_line_width(line_width);
            let (x, y, w, h) = rect.to_xywh_f64();
            ctx.rect(x, y, w, h);
            ctx.stroke();
        }
    }
}

fn zoom_in_calc_scale_factor(
    rect_length: PixelLength,
    win_length: PixelLength,
) -> f64 {
    rect_length as f64 / win_length as f64
}

fn zoom_out_calc_scale_factor(
    rect_length: PixelLength,
    win_length: PixelLength,
) -> f64 {
    win_length as f64 / rect_length as f64
}

struct ViewStatePan {
    color: ColorTuple,
    start_pos: Coords,
    end_pos: Coords,
}

impl ViewStatePan {
    fn new(
        view: &mut MandelbrotViewer,
        color: ColorTuple,
        start_pos: Coords,
    ) -> Self {
        view.set_need_refresh();
        Self {
            color,
            start_pos,
            end_pos: start_pos,
        }
    }

    fn pan(&self, view: &mut MandelbrotViewer, pos: Coords) {
        let (x1, y1) = (self.start_pos.0, self.start_pos.1);
        let (x2, y2) = (pos.0, pos.1);
        let canvas_size = view.canvas_size();
        let (width, height) = (canvas_size.0, canvas_size.1);
        let mut win_length = if height < width { height } else { width };
        if win_length < 1 {
            win_length = 1;
        }
        let vx = (x1 - x2) as f64 / win_length as f64;
        let vy = (y2 - y1) as f64 / win_length as f64; // flipped Y axis
        view.pan_and_zoom(vx, vy, 1.0);
        view.set_need_draw();
    }
}

impl ViewState for ViewStatePan {
    fn update(
        mut self: Box<Self>,
        view: &mut MandelbrotViewer,
        event: &Event,
    ) -> Box<dyn ViewState> {
        match event {
            Event::EndMovePointer(data) => {
                self.pan(view, data.pos);
                view.set_need_refresh();
                Box::new(ViewStateStart::new())
            }
            Event::MovePointer(data) => {
                self.end_pos = data.pos;
                view.set_need_refresh();
                self
            }
            _ => self,
        }
    }

    fn draw(&self, ctx: &web_sys::CanvasRenderingContext2d) {
        let line_width: f64 = 2.0;
        log::debug!(
            "ViewStatePan: drawing line \
                 from {:?} to {:?} \
                 with color {:?} line width {}",
            self.start_pos,
            self.end_pos,
            self.color,
            line_width
        );
        ctx.begin_path();
        ctx.set_stroke_style(&make_js_color(&self.color));
        ctx.set_line_width(line_width);
        let (x1, y1) = self.start_pos.to_xy_f64();
        let (x2, y2) = self.end_pos.to_xy_f64();
        ctx.move_to(x1, y1);
        ctx.line_to(x2, y2);
        ctx.stroke();
    }
}
