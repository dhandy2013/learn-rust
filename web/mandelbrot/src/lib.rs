use crate::view::MandelbrotViewer;
use crate::view_state::{Event, PointerEventData};
use crate::web_util::{
    add_canvas_event_listener, add_element_event_listener, get_canvas_by_id,
    get_document, get_form_by_id, get_input_checked, get_input_string,
    get_render_context_2d, set_inner_html, set_input_str, Coords,
};
use log::info;
use std::sync::{Mutex, MutexGuard};
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};
use wasm_bindgen::JsCast;

#[macro_use]
extern crate lazy_static;

mod console_timer;
mod mandelbrot;
pub mod view;
pub mod view_state;
pub mod web_util;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

lazy_static! {
    static ref VIEWER: Mutex<MandelbrotViewer> =
        Mutex::new(MandelbrotViewer::new(0, 0));
}

fn try_get_viewer_handle<'a>(
) -> Result<MutexGuard<'a, MandelbrotViewer>, JsValue> {
    VIEWER.try_lock().map_err(|err| {
        JsValue::from_str(format!("Lock failed: {:?}", err).as_str())
    })
}

fn get_viewer_handle<'a>() -> MutexGuard<'a, MandelbrotViewer> {
    try_get_viewer_handle()
        .unwrap_or_else(|err| panic!("Getting viewer failed: {:?}", err))
}

/// Convert a web_sys::Event to a view_state::Event
/// Return None if the web event was not applicable.
fn web_event_to_app_event(
    canvas: &web_sys::HtmlCanvasElement,
    web_event: web_sys::Event,
) -> Option<Event> {
    let web_event = match web_event.dyn_into::<web_sys::MouseEvent>() {
        Ok(mouse_event) => {
            let data = PointerEventData::new(Coords(
                mouse_event.offset_x(),
                mouse_event.offset_y(),
            ));
            return match mouse_event.type_().as_str() {
                "mousedown" => match mouse_event.button() {
                    0 => {
                        if mouse_event.ctrl_key() {
                            Some(Event::StartZoomOut(data))
                        } else {
                            Some(app_start_event_based_on_radio_buttons(data))
                        }
                    }
                    1 => Some(Event::StartPan(data)),
                    _ => None,
                },
                "mousemove" => Some(Event::MovePointer(data)),
                "mouseup" => Some(Event::EndMovePointer(data)),
                _ => None,
            };
        }
        Err(web_event) => web_event,
    };
    match web_event.dyn_into::<web_sys::TouchEvent>() {
        Ok(touch_event) => {
            let touches = touch_event.changed_touches();
            if let Some(touch) = touches.item(0) {
                let data = PointerEventData::new(Coords(
                    touch.page_x() - canvas.offset_left(),
                    touch.page_y() - canvas.offset_top(),
                ));
                touch_event.prevent_default();
                let app_event = match touch_event.type_().as_str() {
                    "touchstart" => {
                        Some(app_start_event_based_on_radio_buttons(data))
                    }
                    "touchmove" => Some(Event::MovePointer(data)),
                    "touchend" => Some(Event::EndMovePointer(data)),
                    "touchcancel" => Some(Event::EndMovePointer(data)),
                    _ => None,
                };
                info!(
                    "touch_event #{} (of {}) -> app_event {:?}",
                    touch.identifier(),
                    touches.length(),
                    app_event
                );
                return app_event;
            }
            return None;
        }
        Err(web_event) => web_event,
    };
    None
}

fn app_start_event_based_on_radio_buttons(data: PointerEventData) -> Event {
    let document = get_document();
    if get_input_checked(&document, "id-pan") {
        Event::StartPan(data)
    } else if get_input_checked(&document, "id-zoom-out") {
        Event::StartZoomOut(data)
    } else {
        Event::StartZoomIn(data)
    }
}

fn on_canvas_event(canvas_event: web_sys::Event) {
    log::debug!("on_canvas_event: {:?}", canvas_event);
    let document = get_document();
    let canvas = get_canvas_by_id(&document, "mandelbrot-viewer-canvas");
    if let Some(app_event) = web_event_to_app_event(&canvas, canvas_event) {
        let ctx = get_render_context_2d(&canvas);
        let mut viewer = get_viewer_handle();
        let (calc_time, blit_time) = viewer.process_event(&ctx, &app_event);
        // Update the control form
        update_form(&document, &viewer, calc_time, blit_time);
    }
}

fn on_form_submit(event: web_sys::Event) {
    info!("on_form_submit: event = {:?}", event);
    event.prevent_default();
    recalculate();
}

fn on_button_reset(event: web_sys::Event) {
    info!("on_button_reset: event = {:?}", event);
    let document = get_document();
    let form = get_form_by_id(&document, "control-form");
    // Note: "If a form control (such as a reset button) has a name or id of
    // reset it will mask the form's reset method."
    // https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/reset
    form.reset();
    recalculate();
}

/// Get the Mandelbrot set drawing parameters from the form on the page
/// and re-draw the Mandelbrot set.
fn recalculate() {
    let document = get_document();
    let mut viewer = get_viewer_handle();

    // Grab the Mandelbrot parameters from the control form
    let x_str = get_input_string(&document, "id-x");
    let y_str = get_input_string(&document, "id-y");
    let x_pos_or_err = x_str.parse::<f64>();
    let y_pos_or_err = y_str.parse::<f64>();
    if let (Ok(x_pos), Ok(y_pos)) = (x_pos_or_err, y_pos_or_err) {
        viewer.set_pos(x_pos, y_pos);
    }
    let scale_str = get_input_string(&document, "id-scale");
    if let Ok(scale) = scale_str.parse::<f64>() {
        viewer.set_scale(scale);
    }

    // Grab the canvas upon which we will draw, resize the viewer if necessary
    let canvas = get_canvas_by_id(&document, "mandelbrot-viewer-canvas");
    viewer.set_size(canvas.width(), canvas.height());

    // Draw the mandelbrot set, measuring the time duration
    viewer.set_need_draw();
    let ctx = get_render_context_2d(&canvas);
    let (calc_time, blit_time) = viewer.run(&ctx);

    // Update the control form
    update_form(&document, &viewer, calc_time, blit_time);
}

/// Update the form input values based on changes possibly made by events
/// If calc_time >= 0.0 then the Mandelbrot parameters probably changed.
/// If blit_time >= 0.0 then at least the blit time changed.
fn update_form(
    document: &web_sys::Document,
    viewer: &MandelbrotViewer,
    calc_time: f64,
    blit_time: f64,
) {
    if calc_time >= 0.0 {
        set_inner_html(
            &document,
            "id-calc-time",
            &format!("{:.3}ms", calc_time),
        );
        set_input_str(
            document,
            "id-x",
            &format!("{:.9}", viewer.get_center_x()),
        );
        set_input_str(
            document,
            "id-y",
            &format!("{:.9}", viewer.get_center_y()),
        );
        set_input_str(
            document,
            "id-scale",
            &format!("{:.9}", viewer.get_scale()),
        );
    }
    if blit_time >= 0.0 {
        set_inner_html(
            &document,
            "id-blit-time",
            &format!("{:.3}ms", blit_time),
        );
    }
    if calc_time >= 0.0 || blit_time >= 0.0 {
        set_inner_html(
            &document,
            "id-width",
            &format!("{}px", viewer.get_width()),
        );
        set_inner_html(
            &document,
            "id-height",
            &format!("{}px", viewer.get_height()),
        );
    }
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    // Initialize panic logging
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    // Initialize regular logging
    wasm_logger::init(wasm_logger::Config::new(log::Level::Info));
    info!("wasm-mandelbrot-viewer:start() - entering");

    let document = get_document();
    let canvas = get_canvas_by_id(&document, "mandelbrot-viewer-canvas");

    // Handle events on drawing canvas
    add_canvas_event_listener(&canvas, "mousedown", on_canvas_event);
    add_canvas_event_listener(&canvas, "mousemove", on_canvas_event);
    add_canvas_event_listener(&canvas, "mouseup", on_canvas_event);
    add_canvas_event_listener(&canvas, "touchstart", on_canvas_event);
    add_canvas_event_listener(&canvas, "touchmove", on_canvas_event);
    add_canvas_event_listener(&canvas, "touchend", on_canvas_event);
    add_canvas_event_listener(&canvas, "touchcancel", on_canvas_event);

    // Handle recalculate button or Enter in text field
    add_element_event_listener(
        &document,
        "control-form",
        "submit",
        on_form_submit,
    );

    // Handle reset button
    add_element_event_listener(
        &document,
        "id-button-reset",
        "click",
        on_button_reset,
    );

    // Do the initial redraw
    recalculate();

    info!("wasm-mandelbrot-viewer:start() - leaving");
    Ok(())
}
