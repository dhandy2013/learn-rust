//! Test suite for the Web and headless browsers.
#![cfg(target_arch = "wasm32")]

use wasm_bindgen::JsCast;
use wasm_bindgen_test::{
    console_log, wasm_bindgen_test, wasm_bindgen_test_configure,
};
use wasm_mandelbrot_viewer::{
    view::MandelbrotViewer,
    view_state::{Event, PointerEventData},
    web_util::{get_document, get_render_context_2d, Coords},
};

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn test_pass() {
    assert_eq!(1, 1);
}

#[wasm_bindgen_test]
fn test_get_document() {
    let _document = get_document();
}

#[wasm_bindgen_test]
fn test_mandelbrot_viewer() {
    let document = get_document();
    let canvas = document
        .create_element("canvas")
        .expect("Error creating canvas")
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .unwrap();
    canvas.set_width(640);
    canvas.set_height(480);

    let mut viewer = MandelbrotViewer::new(0, 0);
    viewer.set_size(canvas.width(), canvas.height());

    if let Err(err) = viewer.draw() {
        panic!("Error while drawing: {:?}", err);
    }

    let ctx = get_render_context_2d(&canvas);
    viewer.refresh(&ctx);
}

#[wasm_bindgen_test]
fn test_process_event() {
    console_log!("Testing event processing");
    let document = get_document();
    let canvas = document
        .create_element("canvas")
        .expect("Error creating canvas")
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .unwrap();
    canvas.set_width(640);
    canvas.set_height(480);

    let mut viewer = MandelbrotViewer::new(0, 0);
    viewer.set_size(canvas.width(), canvas.height());

    let left_mouse_down =
        Event::StartZoomIn(PointerEventData::new(Coords(0, 0)));
    let ctrl_left_mouse_down =
        Event::StartZoomOut(PointerEventData::new(Coords(0, 0)));
    let middle_mouse_down =
        Event::StartPan(PointerEventData::new(Coords(0, 0)));
    let mouse_move_1 =
        Event::MovePointer(PointerEventData::new(Coords(75, 50)));
    let mouse_move_2 =
        Event::MovePointer(PointerEventData::new(Coords(150, 100)));
    let mouse_up =
        Event::EndMovePointer(PointerEventData::new(Coords(150, 100)));

    let drag_left_mouse =
        vec![&left_mouse_down, &mouse_move_1, &mouse_move_2, &mouse_up];
    let drag_left_mouse_with_noise = vec![
        &mouse_move_2, // noise
        &left_mouse_down,
        &mouse_move_1,
        &middle_mouse_down, // noise
        &mouse_move_2,
        &ctrl_left_mouse_down, // noise
        &mouse_up,
        &mouse_move_1, // noise
    ];
    let ctrl_drag_left_mouse = vec![
        &ctrl_left_mouse_down,
        &mouse_move_1,
        &mouse_move_2,
        &mouse_up,
    ];
    let drag_right_mouse =
        vec![&middle_mouse_down, &mouse_move_1, &mouse_move_2, &mouse_up];

    let test_vectors = vec![
        &drag_left_mouse,
        &drag_left_mouse_with_noise,
        &ctrl_drag_left_mouse,
        &drag_right_mouse,
    ];

    let ctx = get_render_context_2d(&canvas);
    for test_vector in test_vectors {
        for &event in test_vector.iter() {
            viewer.process_event(&ctx, event);
        }
    }
}
