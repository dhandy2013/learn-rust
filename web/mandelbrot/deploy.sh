#!/bin/bash -e
if [ "$1" = "" -o "$1" = "--help" ] ; then
    echo "Usage: ./deploy.sh <remote-host>"
    exit 1
fi
remote_host="$1"
ssh $remote_host mkdir -p /var/www/html/public/mandelbrot
rsync -av ./*.html ./*.js ./pkg ${remote_host}:/var/www/html/public/mandelbrot/
