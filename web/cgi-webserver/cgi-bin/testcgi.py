#!/usr/bin/python3
from __future__ import print_function

import cgi
import getpass
import pprint
import sys
import time

# canned test
cgi.test()

# additional useful info
print("<pre>")
print("Current time:", time.asctime())
print("Current user:", getpass.getuser())
print("Python version:", sys.version)
print("sys.executable:", sys.executable)
print("sys.prefix:", sys.prefix)
if sys.prefix != sys.exec_prefix:
    print("sys.exec_prefix:", sys.exec_prefix)
print("sys.path:")
pprint.pprint(sys.path)
print("</pre>")

# end-of-file
