//! CGI webserver - serves static files and runs CGI scripts.
//! * Static files are served out of the "public" subdirectory of the current
//!   directory.
//! * CGI scripts are run from the "cgi-bin" subdirectory of the current
//!   directory.
//! * The server listens on port 8080 of localhost.
//! * The / URL is redirected to /index.html
use std::process::Command;
use std::path::Path;

use rouille::{self, Request, Response, cgi::CgiRun};

const LISTEN_HOST: &'static str = "localhost";
const LISTEN_PORT: u16 = 8080;

fn main() {
    println!("Listening on {LISTEN_HOST}:{LISTEN_PORT}");
    rouille::start_server(
        format!("{LISTEN_HOST}:{LISTEN_PORT}"), 
        move |request| {
        rouille::router!(request,
            (GET) (/) => {
                Response::redirect_302("/index.html")
            },

            (GET) (/cgi-bin/{script_name: String}) => {
                handle_cgi_script(request, &script_name)
            },

            _ => handle_file_request(request)
        )
    })
}

fn handle_cgi_script(request: &Request, script_name: &str) -> Response {
    let msg = format!("Running CGI script: {script_name}");
    println!("{msg}");

    let cgi_bin_dir = match Path::new("cgi-bin").canonicalize() {
        Ok(p) => p,
        Err(_) => return Response::empty_404(),
    };

    let mut script_path = cgi_bin_dir.to_path_buf();
    script_path.push(script_name);

    // We try to canonicalize the file. If this fails, then the file doesn't exist.
    let script_file = match script_path.canonicalize() {
        Ok(f) => f,
        Err(_) => return Response::empty_404(),
    };

    // Check that we're still within `cgi_bin_dir`. This should eliminate
    // security issues with requests like `GET /../private_file`.
    if !script_file.starts_with(cgi_bin_dir) {
        return Response::empty_404();
    }

    // Check that it's a file and not a directory.
    match std::fs::metadata(&script_file) {
        Ok(ref m) if m.is_file() => (),
        _ => return Response::empty_404(),
    };

    // We will get a thread panic and a 500 error if that doesn't work.
    let cmd = create_script_command(&script_file);
    println!("About to execute: {cmd:?}");
    cmd.start_cgi(request).unwrap()
}

fn create_script_command(script_file: &Path) -> Command {
    if cfg!(target_os = "windows") {
        let extension = script_file.extension().and_then(|s| s.to_str());
        if extension == Some("py") {
            let mut cmd = Command::new("py");
            cmd.arg(&script_file);
            return cmd;
        }
    }
    Command::new(script_file)
}

fn handle_file_request(request: &Request) -> Response {
    let url = request.url();
    let response = rouille::match_assets(&request, "public");
    if response.is_success() {
        println!("Serving file  : {url}");
    } else {
        println!("File not found: {url}")
    }
    response
}
