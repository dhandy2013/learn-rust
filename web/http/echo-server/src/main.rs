use futures_util::TryStreamExt;
use std::net::SocketAddr;
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use hyper::service::{make_service_fn, service_fn};

static MAIN_MESSAGE: &str =
r#"Welcome to the Echo Server!

To receive back the exact contents of your POST:

    curl localhost:3000/echo -XPOST -d 'hello world'

Other endpoints:

    /echo/uppercase
        Convert POST body contents to ASCII upppercase

    /echo/reversed
        Return the POST body content bytes in reverse order
"#;

async fn echo(req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/") => {
            Ok(Response::new(Body::from(MAIN_MESSAGE)))
        },
        (&Method::POST, "/echo") => {
            // Echo request body directly as response body
            Ok(Response::new(req.into_body()))
        },
        (&Method::POST, "/echo/uppercase") => {
            // This is actually a new futures::Stream
            let mapping = req
                .into_body()
                .map_ok(|chunk| {
                    chunk.iter()
                        .map(|byte| byte.to_ascii_uppercase())
                        .collect::<Vec<u8>>()
                });

            // Use Body::wrap_stream() to convert stream to Body
            Ok(Response::new(Body::wrap_stream(mapping)))
        },
        (&Method::POST, "/echo/reversed") => {
            let whole_body = hyper::body::to_bytes(req.into_body()).await?;
            let reversed_body = whole_body.iter().rev().cloned()
                .collect::<Vec<u8>>();
            Ok(Response::new(Body::from(reversed_body)))
        },
        _ => {
            // Return 404 for any route not configured
            let mut response = Response::default();
            *response.status_mut() = StatusCode::NOT_FOUND;
            Ok(response)
        },
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    // Bind to 127.0.0.1:3000
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    // A Service is needed for every connection
    let service = make_service_fn(|_conn| async {
        Ok::<_, hyper::Error>(service_fn(echo))
    });

    let server = Server::bind(&addr).serve(service);
    println!("Listing on http://{}", addr);

    server.await?;
    Ok(())
}
