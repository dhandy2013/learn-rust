//! CGI example program in Rust
use dumb_cgi::{Body, EmptyResponse, Query, Request};
use std::io::{self, Write};
#[cfg(not(target_arch = "wasm32"))]
use std::{env, process};
use time::{
    format_description::{well_known::Iso8601, FormatItem},
    OffsetDateTime, UtcOffset,
};

const CGI_VAR_NAMES: [&'static str; 17] = [
    "AUTH_TYPE",
    "CONTENT_LENGTH",
    "CONTENT_TYPE",
    "GATEWAY_INTERFACE",
    "PATH_INFO",
    "PATH_TRANSLATED",
    "QUERY_STRING",
    "REMOTE_ADDR",
    "REMOTE_HOST",
    "REMOTE_IDENT",
    "REMOTE_USER",
    "REQUEST_METHOD",
    "SCRIPT_NAME",
    "SERVER_NAME",
    "SERVER_PORT",
    "SERVER_PROTOCOL",
    "SERVER_SOFTWARE",
];

const OTHER_VAR_NAMES: [&'static str; 1] = ["PATH"];

/// Return (now_utc, Some(now_local)) if local time is available,
/// otherwise return (now_utc, None).
fn get_utc_and_local_time() -> (OffsetDateTime, Option<OffsetDateTime>) {
    if let Ok(now_local) = OffsetDateTime::now_local() {
        // Yay, now_local() succeeded, so we know the local offset from UTC.
        // Now convert this time to UTC.
        let now_utc = now_local.to_offset(UtcOffset::UTC);
        (now_utc, Some(now_local))
    } else {
        let now_utc = OffsetDateTime::now_utc();
        (now_utc, None)
    }
}

/// Format the given time object according to the `time` crate's idea of
/// ISO-8601 formatting, which includes putting "T" between the date and time
/// portions and printing 9 digits of fractional seconds. Examples:
/// ```text
/// 2023-04-08T18:17:26.435573843Z
/// 2023-04-08T14:17:26.435623570-04:00
/// ```
#[allow(unused)]
fn format_time_iso8601(t: OffsetDateTime) -> String {
    t.format(&Iso8601::DEFAULT)
        .unwrap_or_else(|err| format!("{err:?}"))
}

const NICER_TIME_FORMAT: &'static [FormatItem<'static>] = time::macros::format_description!(
    "[year]-[month]-[day] \
        [hour repr:24]:[minute]:[second].[subsecond digits:3] \
        [offset_hour sign:mandatory]:[offset_minute]"
);

/// Format the given time in ISO-8601 style but with some changes to make it
/// more human-readable: put a space instead of "T" between the date and the
/// time, only show the fractional time to the nearest millisecond, put a space
/// between the time and the timezone offset, and don't use "Z" for UTC.
/// Examples:
/// ```text
/// 2023-04-08 18:17:26.435 +00:00
/// 2023-04-08 14:17:26.435 -04:00
/// ```
fn format_time_nicely(t: OffsetDateTime) -> String {
    t.format(NICER_TIME_FORMAT)
        .unwrap_or_else(|err| format!("{err:?}"))
}

#[cfg(not(target_arch = "wasm32"))]
fn show_user_group_info(out: &mut dyn Write) -> io::Result<()> {
    let uid = users::get_current_uid();
    let username = match users::get_user_by_uid(uid) {
        Some(user) => user.name().to_string_lossy().to_string(),
        None => "MISSING".to_string(),
    };
    writeln!(out, "User ID: {uid} ({username})")?;
    let euid = users::get_effective_uid();
    if euid != uid {
        let eusername = match users::get_user_by_uid(euid) {
            Some(user) => user.name().to_string_lossy().to_string(),
            None => "MISSING".to_string(),
        };
        writeln!(out, "Effective user ID: {euid} ({eusername})")?;
    }
    let gid = users::get_current_gid();
    let groupname = match users::get_group_by_gid(gid) {
        Some(group) => group.name().to_string_lossy().to_string(),
        None => "MISSING".to_string(),
    };
    writeln!(out, "Group ID: {gid} ({groupname})")?;
    let egid = users::get_effective_gid();
    if egid != gid {
        let egroupname = match users::get_group_by_gid(egid) {
            Some(group) => group.name().to_string_lossy().to_string(),
            None => "MISSING".to_string(),
        };
        writeln!(out, "Effective group ID: {egid} ({egroupname})")?;
    }
    writeln!(out, "")?;
    Ok(())
}

fn main() -> io::Result<()> {
    let req = Request::new().map_err(|err| {
        io::Error::new(io::ErrorKind::Other, format!("{err:?}"))
    })?;

    // Python test CGI server ignores this status code, always returns 200
    let mut response = EmptyResponse::new(200).with_content_type("text/plain");

    // Show UTC time, and local time if possible.
    let (now_utc, now_local_maybe) = get_utc_and_local_time();
    writeln!(
        &mut response,
        "Current UTC time  : {}",
        format_time_nicely(now_utc)
    )?;
    if let Some(local_now) = now_local_maybe {
        writeln!(
            &mut response,
            "Current local time: {}",
            format_time_nicely(local_now)
        )?;
    }
    writeln!(&mut response, "")?;

    // Show current process information
    #[cfg(not(target_arch = "wasm32"))]
    {
        let exe = env::current_exe()?;
        write!(
            &mut response,
            "Current executable file: {}\n",
            exe.to_string_lossy()
        )?;
        let cwd = env::current_dir()?;
        write!(
            &mut response,
            "Current working directory: {}\n",
            cwd.to_string_lossy()
        )?;
        write!(&mut response, "Current process ID: {}\n", process::id())?;
    }

    #[cfg(not(target_arch = "wasm32"))]
    show_user_group_info(&mut response)?;

    // Show expected environment variables
    write!(
        &mut response,
        "Standard CGI Environment Variables (RFC 3875):\n"
    )?;
    for &var_name in CGI_VAR_NAMES.iter() {
        write!(
            &mut response,
            "{var_name}={}\n",
            req.var(var_name).unwrap_or("(not defined)"),
        )?;
    }
    for (var_name, value) in req.vars() {
        if !var_name.starts_with("HTTP_") {
            continue;
        }
        write!(&mut response, "{}={}\n", var_name, value)?;
    }
    write!(&mut response, "\n")?;
    write!(&mut response, "Standard system environment variables\n")?;
    for &var_name in OTHER_VAR_NAMES.iter() {
        write!(
            &mut response,
            "{var_name}={}\n",
            req.var(var_name).unwrap_or("(not defined)"),
        )?;
    }
    write!(&mut response, "\n")?;

    // Show HTTP headers
    write!(&mut response, "Request headers:\n")?;
    for (header_name, value) in req.headers() {
        write!(&mut response, "{}={}\n", header_name, value)?;
    }
    write!(&mut response, "\n")?;

    // Show parsed query string info
    match req.query() {
        Query::None => {
            // This only happens when the QUERY_STRING environment variable is
            // completely missing from the environment.
            write!(&mut response, "No query string.\n")?;
        }
        Query::Some(map) => {
            write!(&mut response, "Query String Form Data:\n")?;
            for (name, value) in map.iter() {
                write!(&mut response, "{}={}\n", name, value)?;
            }
        }
        Query::Err(e) => {
            // Unfortunately, empty QUERY_STRING results in a bogus error.
            match req.var("QUERY_STRING") {
                Some(query_string) => {
                    if query_string.len() == 0 {
                        write!(&mut response, "Empty QUERY_STRING\n")?;
                    } else {
                        write!(
                            &mut response,
                            "Error reading QUERY_STRING: {:?}\n",
                            &e.details
                        )?;
                    }
                }
                None => {
                    write!(
                        &mut response,
                        "BUG: tried to process non-existent QUERY_STRING\n"
                    )?;
                }
            }
        }
    }
    write!(&mut response, "\n")?;

    // Generate and display the Script-URI
    // See https://www.rfc-editor.org/rfc/rfc3875 section 3.3
    const SCHEME: &str = "https";
    // TODO: try harder to figure out the real scheme
    // TODO: URL-encode SCRIPT_NAME and PATH_INFO as required by the RFC
    let script_uri = format!(
        "{SCHEME}://{}:{}{}{}?{}",
        req.var("SERVER_NAME").unwrap_or("missing-server-name"),
        req.var("SERVER_PORT").unwrap_or("missing-server-port"),
        req.var("SCRIPT_NAME").unwrap_or("missing-script-name"),
        req.var("PATH_INFO").unwrap_or("missing-path-info"),
        req.var("QUERY_STRING").unwrap_or("missing-query-string"),
    );
    write!(&mut response, "Script-URI:\n{script_uri}\n\n")?;

    // Show the request content type
    match req.header("content-type") {
        Some(content_type) => {
            write!(&mut response, "Request content-type: {content_type}\n")?;
        }
        None => {
            write!(
                &mut response,
                "No content-type header present in request\n"
            )?;
        }
    }
    write!(&mut response, "\n")?;

    // Show request body info
    match req.body() {
        Body::None => {
            write!(&mut response, "No body.\n")?;
        }
        Body::Some(bytes) => {
            write!(&mut response, "{} bytes of body.\n", bytes.len())?;
        }
        Body::Multipart(parts) => {
            write!(
                &mut response,
                "Multipart body with {} parts:\n",
                parts.len()
            )?;
            for (n, part) in parts.iter().enumerate() {
                write!(&mut response, "    Part {}:\n", &n)?;
                for (header, value) in part.headers.iter() {
                    write!(&mut response, "        {}: {}\n", header, value)?;
                }
                write!(
                    &mut response,
                    "        {} bytes of body.\n",
                    part.body.len()
                )?;
            }
        }
        Body::Err(e) => {
            write!(&mut response, "Error reading body: {:?}\n", &e.details)?;
        }
    }
    write!(&mut response, "\n")?;

    // Finally, send the response.
    write!(&mut response, "Done.\n")?;
    response.respond()
}
