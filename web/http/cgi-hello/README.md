cgi-hello
=========

Use Rust to create a very simple webapp using one of the oldest techniques,
Common Gateway Interface (CGI).

See:
* [The Common Gateway Interface (CGI) Version 1.1](https://www.rfc-editor.org/rfc/rfc3875)
* [Apache Tutorial: Dynamic Content with CGI](https://httpd.apache.org/docs/2.4/howto/cgi.html)

To run using the Python built-in simple HTTP server:
```
python -m http.server --cgi
```
Then navigate to: http://localhost:8000/cgi-bin/cgi-hello

To run using [WAGI](https://github.com/deislabs/wagi):
```
cargo build --target=wasm32-wasi --release
wagi -c modules.toml
```
Then navigate to: http://127.0.0.1:3000/
