Trying out Rust HTTP clients and servers
========================================

Rust Packages in this Directory
-------------------------------

This directory is organized as a [Cargo
Workspace](https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html).

Packages:

- ``hello-server``: [basic HTTP server](https://hyper.rs/guides/server/hello-world/)
- ``echo-server``: [Echo http server](https://hyper.rs/guides/server/echo/)

Rust HTTP and Web Framework Libraries
-------------------------------------

[hyper](https://hyper.rs/) is "a fast HTTP implementation written in and for
Rust".  Hyper is low level, meant as a building block for other services.

[warp](https://crates.io/crates/warp) is a convienient HTTP server built on top
of hyper.  "A super-easy, composable, web server framework for warp speeds."

[reqwest](https://github.com/seanmonstar/reqwest) is a convenient HTTP client
build on top of hyper.

Asynchronous Rust Resources
----------------------------

[async-book](https://rust-lang.github.io/async-book/)
