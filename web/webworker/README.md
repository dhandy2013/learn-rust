# Web Workers WASM Example

Example webapp using [Web Workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)  and [Rust with Web Assembly](https://www.rust-lang.org/what/wasm) (a.k.a. "WASM")

This is a port of the [Basic dedicated worker example](https://github.com/mdn/simple-web-worker) from Javascript to Rust.

To build this demo:
```
wasm-pack build --target=no-modules
```

To run this demo:
```
python3 -m http.server
```
and then navigate your web browser to the URL that it prints.
