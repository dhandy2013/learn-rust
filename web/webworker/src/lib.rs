use log::info;
use wasm_bindgen::prelude::{wasm_bindgen, Closure, JsValue};
use wasm_bindgen::JsCast;

#[wasm_bindgen]
pub fn start_main() -> Result<(), JsValue> {
    info!("start_main() - entering");
    let window = web_sys::window().expect("Global window should exist");
    let document = window.document().expect("document on window should exist");
    let input1 = document
        .get_element_by_id("number1")
        .unwrap_or_else(|| panic!("#number1 should exist"))
        .dyn_into::<web_sys::HtmlInputElement>()
        .expect("#number1 should have been an input element");
    let input2 = document
        .get_element_by_id("number2")
        .unwrap_or_else(|| panic!("#number2 should exist"))
        .dyn_into::<web_sys::HtmlInputElement>()
        .expect("#number2 should have been an input element");
    let result_p = document
        .get_element_by_id("result")
        .expect("#result should exist");

    let worker = web_sys::Worker::new("worker.js")?;

    let input_callback = {
        let worker = worker.clone();
        let input1 = input1.clone();
        let input2 = input2.clone();
        move |_event: web_sys::Event| {
            info!("input_callback - entering");
            let val1: f64 = input1.value().parse().unwrap_or(0.0);
            let val2: f64 = input2.value().parse().unwrap_or(0.0);
            let message = js_sys::Array::new();
            message.set(0, JsValue::from_f64(val1));
            message.set(1, JsValue::from_f64(val2));
            worker
                .post_message(&message)
                .expect("Expected worker.post_message() to succeed");
            info!("input_callback - leaving");
        }
    };

    let closure1 =
        Closure::wrap(Box::new(input_callback.clone()) as Box<dyn FnMut(_)>);
    input1.set_onchange(Some(closure1.as_ref().unchecked_ref()));
    closure1.forget(); // leaks memory, can't be helped

    let closure2 =
        Closure::wrap(Box::new(input_callback.clone()) as Box<dyn FnMut(_)>);
    input2.set_onchange(Some(closure2.as_ref().unchecked_ref()));
    closure2.forget(); // leaks memory, can't be helped

    let message_callback = {
        let result_p = result_p.clone();
        move |event: web_sys::MessageEvent| {
            info!("main message_callback - entering");
            let result = match event.data().as_f64() {
                Some(r) => r,
                None => {
                    log::error!("main message_callback: data not an f64");
                    return;
                }
            };
            result_p
                .set_text_content(Some(format!("Result: {}", result).as_str()));
            info!("main message_callback - leaving");
        }
    };
    let closure3 =
        Closure::wrap(Box::new(message_callback) as Box<dyn FnMut(_)>);
    worker.set_onmessage(Some(closure3.as_ref().unchecked_ref()));
    closure3.forget(); // leaks memory, can't be helped

    info!("start_main() - leaving");
    Ok(())
}

fn get_this() -> web_sys::DedicatedWorkerGlobalScope {
    js_sys::global()
        .dyn_into::<web_sys::DedicatedWorkerGlobalScope>()
        .expect("this should be a DedicatedWorkerGlobalScope")
}

fn spin_cpu(delay_ms: f64) {
    // TODO: Figure out how to get the Performance object from a web worker. The
    // WorkerGlobalScope.performance property doesn't seem to be supported yet.
    let t0 = js_sys::Date::now();
    while js_sys::Date::now() - t0 < delay_ms {}
}

#[wasm_bindgen]
pub fn start_worker() -> Result<(), JsValue> {
    info!("start_worker() - entering");

    let message_callback = {
        |event: web_sys::MessageEvent| {
            info!("worker message_callback - entering");
            let message = match event.data().dyn_into::<js_sys::Array>() {
                Ok(m) => m,
                Err(err) => {
                    log::error!(
                        "worker message_callback: data not an array: {:?}",
                        err
                    );
                    return;
                }
            };
            let val1: f64 =
                message.get(0).as_f64().expect("val1 should be f64");
            let val2: f64 =
                message.get(1).as_f64().expect("val2 should be f64");
            info!("worker received: val1={} val2={}", val1, val2);

            // Wait a bit before posting result
            spin_cpu(250.0);
            let result = val1 * val2;

            get_this()
                .post_message(&JsValue::from_f64(result))
                .expect("Expected this.post_message() to succeed");
            info!("worker message_callback - leaving");
        }
    };
    let closure =
        Closure::wrap(Box::new(message_callback) as Box<dyn FnMut(_)>);
    get_this().set_onmessage(Some(closure.as_ref().unchecked_ref()));
    closure.forget(); // leaks memory, can't be helped

    info!("start_worker() - leaving");
    Ok(())
}

/// Do the initialization common to both the main thread and worker thread.
/// We can't test for the existence of the main window and use that to
/// determine whether we are running in the main thread vs. a worker thread
/// because of this bug: https://github.com/rustwasm/wasm-bindgen/issues/2272
#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    // Initialize panic logging
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    // Initialize regular logging
    wasm_logger::init(wasm_logger::Config::new(log::Level::Info));
    info!("start(): WASM module initialized");

    Ok(())
}
