self.importScripts("pkg/webworker.js");

async function run() {
    console.log("worker.js:run(): Initializing the WASM module");
    await wasm_bindgen("pkg/webworker_bg.wasm");
    wasm_bindgen.start_worker();
    console.log("worker.js:run(): done");
}

run();
