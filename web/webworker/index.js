// Before running this script, run: "pkg/webworker.js" to define wasm_bindgen

async function run() {
    console.log("index.js:run(): Initializing the WASM module");
    await wasm_bindgen("pkg/webworker_bg.wasm");
    wasm_bindgen.start_main();
    console.log("index.js:run(): done");
}

run();
