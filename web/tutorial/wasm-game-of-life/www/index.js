import { Universe, Cell } from "wasm-game-of-life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg";

const WIDTH = 64;
const HEIGHT = 64;
const CELL_SIZE = 5;  // pixels
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

// Initialization to enable better debugging
Universe.init();
// Construct the universe and get its size
const universe = Universe.new(WIDTH, HEIGHT);
universe.init_random();
// universe.init_2_7();
const width = universe.width();
const height = universe.height();

// Give the canvas room for all of our cells and a 1px border around each.
const canvas = document.getElementById("game-of-life-canvas");
canvas.height = (CELL_SIZE + 1) * height + 1;
canvas.width = (CELL_SIZE + 1) * width + 1;
const ctx = canvas.getContext('2d');

canvas.addEventListener("click", event => {
    const boundingRect = canvas.getBoundingClientRect();

    const scaleX = canvas.width / boundingRect.width;
    const scaleY = canvas.height / boundingRect.height;

    const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
    const canvasTop = (event.clientY - boundingRect.top) * scaleY;

    const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
    const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

    universe.toggle_cell(row, col);

    drawGrid();
    drawCells();
});

let animationId = null;

const renderLoop = () => {
    // debugger;  // Sets a breakpoint allowing us to single-step frames
    universe.tick();
    drawGrid();
    drawCells();
    animationId = requestAnimationFrame(renderLoop);
};

const isPaused = () => {
    return animationId === null;
}

const drawGrid = () => {
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;

    // Vertical lines
    const pixel_height = (CELL_SIZE + 1) * height + 1;
    for (let i=0; i <= width; ++i) {
        const x = i * (CELL_SIZE + 1);
        ctx.moveTo(x, 0);
        ctx.lineTo(x, pixel_height);
    }

    // Horizontal lines
    const pixel_width = (CELL_SIZE + 1) * width + 1;
    for (let j = 0; j <= height; ++j) {
        const y = j * (CELL_SIZE + 1);
        ctx.moveTo(0, y);
        ctx.lineTo(pixel_width, y);
    }

    ctx.stroke();
};

const getIndex = (row, column) => {
    return row * width + column;
};

const drawCells = () => {
    const cellsPtr = universe.cells();
    const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

    ctx.beginPath();

    for (let row=0; row < height; ++row) {
        for (let col = 0; col < width; ++col) {
            const idx = getIndex(row, col);

            ctx.fillStyle = cells[idx] === Cell.Dead ? DEAD_COLOR : ALIVE_COLOR;
            ctx.fillRect(
                col * (CELL_SIZE + 1) + 1,
                row * (CELL_SIZE + 1) + 1,
                CELL_SIZE,
                CELL_SIZE
            );
        }
    }

    ctx.stroke();
};

const playPauseButton = document.getElementById("play-pause");

const play = () => {
    playPauseButton.textContent = "⏸";
    renderLoop();
};

const pause = () => {
    playPauseButton.textContent = "▶";
    cancelAnimationFrame(animationId);
    animationId = null;
};

playPauseButton.addEventListener("click", event => {
    if (isPaused()) {
        play();
    } else {
        pause();
    }
});

const singleStepButton = document.getElementById("single-step");

singleStepButton.addEventListener("click", event => {
    universe.tick();
    drawGrid();
    drawCells();
});

drawGrid();
drawCells();
