Rust and WebAssembly Tutorial
=============================

https://rustwasm.github.io/docs/book/game-of-life/introduction.html

Install prerequisites:

- `Rust <https://www.rust-lang.org/tools/install>`__
- `wasm-pack <https://rustwasm.github.io/wasm-pack/installer/>`__
- ``sudo yum install npm openssl-devel``
- ``cargo install cargo-generate``
- In the wasm-game-of-life/www directory:
  ``npm install webpack-dev-server --save-dev``

WASM Game of Life
-----------------

The ``wasm-game-of-life`` directory contains the code resulting from following
the Rust-WASM tutorial, implementing Conway's Game of Life running in a browser.

To compile the code::

    # In the wasm-game-of-life directory:
    wasm-pack build

To run the game::

    # In the wasm-game-of-life/www directory:
    npm run start
    # Then point the browser to http://localhost:8080/

To run the tests::

    # In the wasm-game-of-life directory:
    wasm-pack test --firefox --headless
