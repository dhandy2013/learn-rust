//! fcgi-util - FastCGI app that traces parameters sent to it.
//! Originally copied and modified from:
//! https://github.com/FlashSystems/tokio-fastcgi/blob/master/examples/apiserver.rs
use std::{
    fmt::{self, Write},
    io::Read,
    sync::Arc,
};

use tokio::{
    net::{tcp::OwnedWriteHalf, TcpListener, TcpStream},
    sync::RwLock,
};
use tokio_fastcgi::{Request, RequestResult, Requests};

// This is a little example of a REST API server implemeted in FastCGI.
//
// To include it into your Apache setup you can use the `proxy_fcgi` module.
// The following configuration will pass all requests to the path `/api` to the
// server application listening on port 8080.
//
// ``` conf
// <Location /api>
//   ProxyPass "fcgi://127.0.0.1:8080/" enablereuse=on
// </Location>
// ```
//
// To try out this FCGI server, configure Apache accordingly and start the
// example by running `cargo run`.
//
// Now you can use curl to interact with it:
//
// `curl http://localhost/api/ping`
// Fetch the string `pong` and the number of times this endpoint has been
// called since the server was started.
//
// `curl http://localhost/api/unknown`
// Return a 404 response, just to show that we can.
//
// `curl http://localhost/api/<any-other-path>`
// Return a page containing the parameters sent by the webserver to the fcgi
// app.

struct HttpResponse {
    status: http::StatusCode,
    response_body: String,
}

impl HttpResponse {
    fn new(status: http::StatusCode, response_body: String) -> Self {
        Self {
            status,
            response_body,
        }
    }
}

fn response_400(reason: &str) -> HttpResponse {
    let status = http::StatusCode::BAD_REQUEST;
    HttpResponse::new(status, String::from(reason))
}

fn response_404(path: &str) -> HttpResponse {
    let status = http::StatusCode::NOT_FOUND;
    HttpResponse::new(status, format!("not found: {path}"))
}

fn response_405(method: &str) -> HttpResponse {
    let status = http::StatusCode::METHOD_NOT_ALLOWED;
    HttpResponse::new(status, format!("method not allowed: {method}"))
}

fn response_500(err: Error) -> HttpResponse {
    let status = http::StatusCode::INTERNAL_SERVER_ERROR;
    HttpResponse::new(status, format!("{err}"))
}

/// A simple in-memory data store implementation
struct Store {
    num_pings: u32,
}

impl Store {
    fn new() -> Self {
        Self { num_pings: 0 }
    }

    fn increment_ping_count(&mut self) -> u32 {
        self.num_pings = self.num_pings.wrapping_add(1);
        self.num_pings
    }
}

#[derive(Debug)]
#[allow(unused)]
enum Error {
    StaticMsgError(&'static str),
    StringMsgError(String),
    FastcgiError(tokio_fastcgi::Error),
    HttpError(http::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "server error: {self:?}")
    }
}

impl std::error::Error for Error {}

impl From<&'static str> for Error {
    fn from(value: &'static str) -> Self {
        Self::StaticMsgError(value)
    }
}

impl From<String> for Error {
    fn from(value: String) -> Self {
        Self::StringMsgError(value)
    }
}

impl From<tokio_fastcgi::Error> for Error {
    fn from(value: tokio_fastcgi::Error) -> Self {
        Self::FastcgiError(value)
    }
}

impl From<http::Error> for Error {
    fn from(value: http::Error) -> Self {
        Self::HttpError(value)
    }
}

impl From<fmt::Error> for Error {
    fn from(_value: fmt::Error) -> Self {
        Self::StaticMsgError("error while formatting message")
    }
}

async fn handle_dump(
    _store: Arc<RwLock<Store>>,
    request: &Request<OwnedWriteHalf>,
    _path_parts: &[String],
) -> Result<HttpResponse, Error> {
    let method = request.get_str_param("request_method").unwrap_or("");
    let mut response = String::new();
    if let Some(params) = request.str_params_iter() {
        let mut param_list: Vec<_> = params.collect();
        param_list.sort();
        writeln!(response, "Request parameter:")?;
        for (param_name, maybe_param_value) in param_list {
            writeln!(
                response,
                "    {}={}",
                param_name,
                maybe_param_value.unwrap_or("(not valid utf-8")
            )?;
        }
    }
    match method {
        "POST" | "PUT" => {
            let mut body = String::default();
            if request.get_stdin().read_to_string(&mut body).is_ok() {
                let body_len = body.len();
                writeln!(response, "Read {body_len} bytes from {method} body")?;
            } else {
                return Err("error reading body")?;
            }
        }
        _ => {}
    }
    Ok(HttpResponse::new(http::StatusCode::OK, response))
}

async fn handle_ping(
    store: Arc<RwLock<Store>>,
    request: &Request<OwnedWriteHalf>,
    path_parts: &[String],
) -> Result<HttpResponse, Error> {
    let method = request.get_str_param("request_method").unwrap_or("");
    if method != "GET" {
        return Ok(response_405(method));
    }
    let last_path_part = path_parts
        .last()
        .map(|part| part.as_str())
        .unwrap_or_default();
    if last_path_part != "ping" {
        return Ok(response_404(last_path_part));
    }
    let count = store.write().await.increment_ping_count();
    Ok(HttpResponse::new(
        http::StatusCode::OK,
        format!("pong {count}"),
    ))
}

/// Encodes the HTTP status code and the response string and sends it back to
/// the webserver.
async fn send_response(
    request: Arc<Request<OwnedWriteHalf>>,
    response: &HttpResponse,
) -> Result<RequestResult, Error> {
    let mut w = request.get_stdout();

    // Write headers
    w.write(
        format!(
            "Status: {} {}\r\n\r\n",
            response.status.as_str(),
            response.status.canonical_reason().unwrap_or_default(),
        )
        .as_bytes(),
    )
    .await?;

    // Write body
    if response.response_body.len() > 0 {
        // Write response body
        w.write(response.response_body.as_bytes()).await?;
    }

    Ok(RequestResult::Complete(0))
}

/// Called when a new request arrives via FastCGI. This function parses the
/// request URI and dispatches to a more specific request handler.
async fn handle_request(
    store: Arc<RwLock<Store>>,
    request: Arc<Request<OwnedWriteHalf>>,
) -> Result<RequestResult, Error> {
    let Some(request_uri) = request.get_str_param("request_uri") else {
        return send_response(request, &response_400("missing request_uri"))
            .await;
    };
    println!("request_uri: {request_uri}");

    let prefix_count = 1; // number of items to drop from start of path
    let path_parts: Vec<_> = request_uri
        .split('/')
        .filter(|item| item.len() > 0)
        .skip(prefix_count)
        .map(String::from)
        .collect();

    let endpoint: &str =
        path_parts.get(0).map(|part| part.as_str()).unwrap_or("");
    println!(
        "Processing endpoint '{}' with path_parts '{:?}'",
        endpoint, path_parts,
    );

    let response_or_err = match endpoint {
        "ping" => handle_ping(store, &request, &path_parts).await,
        "unknown" => Ok(response_404("unknown")),
        _ => handle_dump(store, &request, &path_parts).await,
    };

    match response_or_err {
        Ok(response) => send_response(request, &response).await,
        Err(err) => send_response(request, &response_500(err)).await,
    }
}

#[tokio::main]
async fn main() {
    let addr = "127.0.0.1:8080";
    println!("Listening on address {addr}");

    let listener = TcpListener::bind(addr).await.unwrap();
    let store = Arc::new(RwLock::new(Store::new()));

    loop {
        let connection = listener.accept().await;
        // Accept new connections
        match connection {
            Err(err) => {
                println!("Establishing connection failed: {}", err);
                break;
            }
            Ok((stream, address)) => {
                println!("Connection from {address}");

                let conn_store = store.clone();

                // The socket connection was established successfully.
                // Spawn a new task to handle the requests that the webserver will
                // send us.
                tokio::spawn(handle_connection(stream, conn_store));
            }
        }
    }
}

async fn handle_connection(stream: TcpStream, conn_store: Arc<RwLock<Store>>) {
    // Create a new requests iterator. It will collect the requests from the
    // server and supply a streaming interface.
    let mut requests = Requests::from_split_socket(stream.into_split(), 10, 10);
    loop {
        let request_or_err = requests.next().await;
        let Some(request) = (match request_or_err {
            Ok(maybe_request) => maybe_request,
            Err(err) => {
                println!("Error fetching request: {err}");
                continue;
            }
        }) else {
            println!("Connection closed by server");
            break;
        };
        let req_store = conn_store.clone();
        let process_result = request
            .process(|request| async move {
                match handle_request(req_store, request).await {
                    Ok(request_result) => {
                        print_request_result(request_result);
                        request_result
                    }
                    Err(err) => {
                        println!("Error handling request: {err}");
                        RequestResult::Complete(1)
                    }
                }
            })
            .await;
        if let Err(err) = process_result {
            println!("Processing request failed: {err}");
        }
    }
}

fn print_request_result(result: RequestResult) {
    match result {
        RequestResult::Complete(n) => {
            println!("Request complete with status {n}")
        }
        RequestResult::Overloaded => {
            println!("Request not processed: Overloaded")
        }
        RequestResult::UnknownRole => {
            println!("Request not processed: UnknownRole")
        }
    }
}
