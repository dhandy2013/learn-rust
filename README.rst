Learning the Rust Programming Language
======================================

By typing in, modifying, debugging, and running the examples in `The Book`_.

.. _The Book: https://doc.rust-lang.org/book/

.. contents:: Contents


Setup
-----

To set up Rust on Linux:

- ``curl https://sh.rustup.rs -sSf > rustup.sh``
- ``bash rustup.sh``


Helpful Rust Commands
---------------------

- ``rustup doc`` - serve Rust docs without requiring network connection
- ``rustup update`` - get the latest version of rust
- ``cargo install cargo-expand`` - "expand" command for debugging macros
- ``cargo install cargo-tree`` - "tree" command for visualizing dependencies
- ``rustup toolchain install nightly``
- ``rustup component add rustfmt --toolchain nightly``
- ``rustup target list`` - list all supported platforms
- ``rustup toolchain list`` - list all installed toolchain/target combos


Examples of Compiler Excellence
-------------------------------

If you create a function that is declared to return an i32, but accidentally
don't return any value, and the last thing in the function body is a call to a
function that returns an i32, Rust will suggest that maybe you want to remove
the semicolon from that last function call to use its return value as the return
value from the calling function.

Type inference is amazing. Consider this code for example::

    let (sender, receiver) = mpsc::channel();

In this case the types of ``sender`` and ``receiver`` are determined by what
later code does with them!

Suppose in a method body you refer to a struct field but forget to put ``self.``
in front of the field name. Rust helpfully reminds you::

    (width as usize, height as usize);
     ^^^^^ help: you might have meant to use the available field: `self.width`



Things in Rust that Remind me of Python
---------------------------------------

The "explicit is better than implicit attitude" - yes, the last expression in a
function becomes the return value, BUT you can't accidentally return the value
of the last function call like you can in other languages, because you have to
leave off the semicolon after the function call for that to happen. Otherwise,
it returns () (empty value). So Rust makes practical use of semicolons at the
end of statements.

The explicit "self" parameter to methods - Like Python, Rust takes an explicit
parameter for the receiver object on methods, inside of an invisible "this"
parameter like C++ and Java. Unlike Python, Rust makes you spell it "self"
instead of naming it anything you want, which is a good thing. And, Rust makes
practical use of the explicit receiver object parameter. The way you decorate
it ("&" or "mut &", or with no decoration) determines whether the method gets
an immutable reference, an immutable reference, or consumes it, respectively.

Namespaces - the Rust equivalent of Python's "import" statement is the "use"
statement. Like Python, but unlike, say, Java or C, Rust doesn't dump all of
the imported modules symbols into the current namespace unless you explicitly
tell it to do so with "*".

Rust has tuple destructuring that looks similar to Python's, as in the previous
example::

    let (sender, receiver) = mpsc::channel();


Other Things to Like about Rust
-------------------------------

Rust is "airplane friendly." Run ``rustup doc`` and a complete set of language
documentation pops up in your browser, all served locally without need for
internet access. All docs got installed with the compiler. The standard docs
include not only the standard library API reference but also "The Rust Book"
introduction to the language.

Rust makes all variables immutable by default. That makes you think harder
and produce better, safer code.

Match expressions! Way, way better than C/C++/Java's switch statement.

See wasm-game-of-life/src/lib.rs for a good example of match expressions used to
implement the rules of Conway's Game of Life.

Rust standard style is to use snake_case names, just like Python.


Next Things to Try
------------------

- WASM
- Rust-Python interop
- Embedded Rust
- Rust-SDL2 for graphics/sound/input


Reducing Rust Binary Size
-------------------------

- min-sized-rust: https://github.com/johnthagen/min-sized-rust
- cargo-bloat: https://github.com/RazrFalcon/cargo-bloat
- Run ``cargo tree -d`` to identify duplicate dependencies
- Investigate dependabot: https://app.dependabot.com/


Cross Compilation
-----------------

To build a program for 32-bit Linux on a 64-bit Linux system::

    sudo yum install glibc.i686 glibc-devel.i686
    rustup toolchain install stable-i686-unknown-linux-gnu
    rustup target add i686-unknown-linux-gnu  # fetches std
    cargo build --release --target i686-unknown-linux-gnu

Unfortunately, the steps above only work for modern versions of Linux.

A Rust binary built on Fedora 30 depends on glibc 2.18 but only glibc 2.11 is
available on an older system such as 32-bit x86 running Ubuntu 10.04.

These instructions using Docker to build for older Linux worked as of August
2019::

    docker pull quay.io/pypa/manylinux1_i686

    # cd to your Rust project directory
    docker run -it -v $(pwd):/mnt --user $(id -u):$(id -g) --name oldlinuxbuild quay.io/pypa/manylinux1_i686
    # After exiting the container, restart with this command
    docker start -i oldlinuxbuild
    mkdir -p /mnt/temp
    export HOME=/mnt/temp
    cd $HOME
    curl https://sh.rustup.rs -sSf > rustup.sh
    sh rustup.sh
    source .profile  # to put rustc, etc. in PATH
    cd /mnt  # back to Rust project directory root
    cargo build --release
    # Your binary program will appear under target/release,
    # ready to run on old 32-bit Linux

As of 2023-04-08 the above instructions no longer work. ``rustup.sh`` fails with
messages like ``version `GLIBC_2.12' not found (required by
/tmp/tmp.CzscSknq23/rustup-init)``, probably indicating that the version of
glibc in the docker container is too old.

Now I compile for i686 statically linked against the "musl" library::

    rustup target add i686-unknown-linux-musl
    cargo build --target i686-unknown-linux-musl --release

After building, check to make sure the executable is really statically linked::

    $ ldd target/i686-unknown-linux-musl/release/hexdump
        not a dynamic executable

The "not a dynamic executable" message above indicates static linking.

I was able to successfully run programs compiled this way on Ubuntu 10.04
running on i686 (32-bit Intel processor).

