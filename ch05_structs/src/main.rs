// Learning and practice with structs

// This derive thing is what enables printing the struct
#[derive(Debug)]
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

// Examples of tuple structs
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn build_user(email: String, username: String) -> User {
    User {
        // Using "field init shorthand" syntax
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn f1() {
    let user1 = build_user(
        String::from("joe@example.com"),
        String::from("joe"),
    );
    println!("user1 = {:#?}", user1);

    let _user2 = User {
        email: String::from ("another@example.com"),
        username: String::from("another"),
        // Using struct update syntax
        ..user1
    };

    let _black = Color(0, 0, 0);
    let _origin = Point(0, 0, 0);
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    // "associated function" that returns a new instance of Rectangle
    fn square(size: u32) -> Rectangle {
        Rectangle { width: size, height: size }
    }
}

// You can have multiple impl blocks for the same struct; they are merged.
impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width >= other.width && self.height >= other.height
    }
}

fn f2() {
    let rect1 = Rectangle { width: 50, height: 30 };
    println!("rect1 = {:?}", rect1);
    println!("Area of rectangle: {} square pixels", rect1.area());
}

fn f3() {
    let rect1 = Rectangle { width: 30, height: 50 };
    let rect2 = Rectangle { width: 10, height: 40 };
    let rect3 = Rectangle { width: 60, height: 45 };

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));

    let square = Rectangle::square(10);
    println!("Area of square is: {}", square.area());
}

fn main() {
    f1();
    f2();
    f3();
}
